import React from 'react';
import logo from './Assets/Images/brandLogo.png';
import './App.css';

// import Template from './Template'


import {BrowserRouter,Route,Switch,Link} from 'react-router-dom'
import Main from './main'
import {Container} from 'reactstrap'
import Footer from './Components/Footer'
class App extends React.Component{

  componentDidMount() {
    document.body.classList.add("bg-default");
  }
  componentWillUnmount() {
    document.body.classList.remove("bg-default");
  }
  
  render(){
    return (
      <BrowserRouter>
        <Switch> 
          <Route exact path="/" component={() => <MainScreen />} />
          <Main />
        </Switch>
        <Container fluid>
            <Footer />
        </Container>
      </BrowserRouter>
    );
  }
  
}

function MainScreen() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Click on <code><Link
            className="App-link"
            to="/app"
            rel="noopener noreferrer"
          >
            Application
        </Link></code> to proceed.
        </p>
        
      </header>
    </div>
  );
}

export default App;
