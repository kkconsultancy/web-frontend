import React from 'react'
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import Views from './Views'
import Loading from './Views/loading'
import Middleware from './Middleware'

import Template from './Template'

class Main extends React.Component{
    state = {
        firebaseLoaded : false,
        authenticated : false
    }
    componentDidUpdate(e) {
        document.documentElement.scrollTop = 0
        document.scrollingElement.scrollTop = 0
    }

    componentWillMount = () => {
        this.middleware = new Middleware((state) => {
            // console.log(state)
            this.setState({
                ...this.state,
                ...state,
                "present" : state
            })
            // console.log(this.state)
        })
    }

    born = () => {
        // console.log('born')
        return(
            <Switch>
                <Route path="**" component={() => <Template/>}/>
            </Switch>
        )
    }

    initialized = () => {
        // console.log('initialized')
        return (
            <Switch>
                <Route path="/app" component={() => <Views middleware={this.middleware} authenticated={this.state.authenticated}/>} />
                <Route path="**" component={() => <Template />} />
            </Switch>
        )
    }

    render(){
        // console.log(this.controller)
        if(!this.state.firebaseLoaded || !this.state.authenticated){
            return <Loading />
        }
        else if (this.state.authenticated)
            return this.initialized()
        else if (this.state.firebaseLoaded === "LOADED")
            return this.born()
        else
            return(
                <h1>
                    SOMETHING UNEXPECTED
                    {/* <Footer/> */}
                </h1>
            )
    }
}

export default Main