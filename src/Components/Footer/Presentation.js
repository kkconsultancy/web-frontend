import React from 'react'
import './style.css'
import brandLogo from './../../Assets/Images/brandLogo.png'
import brandingDesign from '../../Assets/Images/brandingDesign.png'

import { NavItem, NavLink, Nav, Container, Row, Col } from "reactstrap";

class Presentation extends React.Component{
  render(){
    return(
      <>
        <footer className="py-5">
          <Container>
            <Row className="align-items-center justify-content-xl-between">
              <Col xl="6">
                <div className="copyright text-center text-xl-left text-muted">
                  © 2020{" "}
                  <a
                    className="font-weight-bold ml-1"
                    href="https://developerswork.online"
                    target="_blank"
                  >
                    Developers@Work
                  </a>
                </div>
              </Col>
              <Col xl="6">
                <Nav className="nav-footer justify-content-center justify-content-xl-end">
                  <NavItem>
                    <NavLink
                      href="https://aaa-automation.firebaseapp.com"
                      target="_blank"
                    >
                      AAA Automation
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://developerswork.online/about"
                      target="_blank"
                    >
                      About Us
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://developerswork.online/blog"
                      target="_blank"
                    >
                      Blog
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      target="_blank"
                    >
                      MIT License
                    </NavLink>
                  </NavItem>
                </Nav>
              </Col>
              <Col>
                <div className="copyright text-center text-xl-left text-muted">
                  <h1 className="footer-copyright container text-center text-white py-3" id="developerswork">
                    <img src={brandLogo} className="brandLogo" alt="Developers@Work" />
                    <a href="https://developerswork.online" target="_blank" rel="noopener noreferrer">
                      <img src={brandingDesign} alt="DevelopersWork" />
                    </a>
                  </h1>
                </div>
              </Col>
            </Row>
          </Container>
        </footer>
      </>
    )
  }
}


export default Presentation
