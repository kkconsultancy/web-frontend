import React from 'react'
import {Redirect} from "react-router-dom"
class Presentation extends React.Component{
    render(){
        return(
            <Redirect from="**" to="/"/>
        )
    }
}

export default Presentation