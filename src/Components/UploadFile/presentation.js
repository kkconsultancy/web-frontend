import React from 'react'
import './style.css'

class Presentation extends React.Component{
    
    render(){
        // console.log(this.props)
        return(
            <div className="input-group mb-2">
                {this.props.state.label}
                <div className="custom-file ">
                    <input 
                        type="file" 
                        className={"custom-file-input "+(this.props.state.className || "")}
                        name={this.props.name}
                        onChange={this.props.state.onChange}
                        onClick={this.props.onClick}
                        onBlur={this.props.onBlur}
                        placeholder={this.props.placeholder}
                        value={this.props.value}
                        id={this.props.name}
                    />
                    <label className="custom-file-label">{this.props.state.filename || "Choose File"}</label>
                </div>
                <div className="invalid-feedback">
                    {this.props.error || ""}
                </div>
                <div className="valid-feedback">
                    {this.props.valid}
                </div>
            </div>
        )
    }
}

export default Presentation