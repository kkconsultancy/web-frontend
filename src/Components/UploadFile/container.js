import React from 'react'
import Presentation from './presentation'


class Container extends React.Component{


    state = {}

    constructor(props){
        super(props)
        let jsx = ""
        if(this.props.inputTagName)
            jsx = (
                <div className="input-group-prepend">
                    <b className="input-group-text">
                        {this.props.inputTagName}
                    </b>
                </div>
            )
        else if(this.props.inputLabel)
            jsx = (
                <label htmlFor={this.props.name}>
                    {this.props.inputLabel}
                </label>
            )
        // console.log(this.props)
        let className = "form-control"
        className += this.props.error ? " is-invalid" : ""
        className += " "+(this.props.className || "")
        this.state = {
            label:jsx,
            className : className,
            onChange : this.onChange,
            filename : "Choose File"
        }
    }

    onChange = (e) => {
        console.log(e.target.files)
        this.setState({
            filename : e.target.files[0].name
        })
        this.props.onChange({
            target:{
                name : this.props.name,
                value : e.target.files[0]
            }
        })
    }

    componentDidUpdate(){
        let className = "form-control"
        // console.log(this.props.error)
        className += this.props.error ? " is-invalid" : ""
        className += this.props.valid ? " is-valid" : ""
        className += " " + (this.props.className || "")
        // console.log(this.state,this.props)
        if(className !== this.state.className)
            this.setState({
                className: className
            })
    }
    
    render() {
        return (
            <Presentation
                {...this.props}
                state={this.state}
            />
        )
    }
}


export default Container