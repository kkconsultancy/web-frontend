import React from 'react'
import Presentation from './presentation'

import Loading from '../Loading'

class Container extends React.Component{
    state = {
        isDisabled : false
    }

    validate = () => {
        let value = this.state.username
        if(!value){
            this.setState({
                username_error : "INVALID USERNAME"
            })
            return false
        }else{
            this.setState({
                username_valid: true
            })
        }

        value = this.state.password
        if (!value) {
            this.setState({
                password_error: "INVALID PASSWORD"
            })
            return false
        }
        else {
            this.setState({
                password_valid: true
            })
        }

        return true
    }

    input = (e) => {
        const name = e.target.name
        const value = e.target.value
        // console.log(e)

        this.setState({
            [name] : value,
            username_error : "",
            password_error : "",
            username_valid: "",
            password_valid : ""
        })
    }
    submit = (e) => {
        // console.log(e)
        e.preventDefault();
        this.setState({
            isDisabled : <Loading/>
        })
        if (!this.validate())
            return this.setState({
                isDisabled : false
            });
        const data = {
            email : this.state.username,
            password : this.state.password
        }
        return this.props.middleware.login(data,(res) => {
            console.log(res)
            if(res.code){
                return this.setState({
                    username_error : "INCORRECT USERNAME OR PASSWORD",
                    password_error : "INCORRECT USERNAME OR PASSWORD",
                    isDisabled: false
                })
            }
        })
    }

    render() {
        // console.log(this.props)
        return (
            <Presentation
                {...this.props}
                {...this.state}
                submit={this.submit}
                input={this.input}
            />
        )
    }
}

export default Container