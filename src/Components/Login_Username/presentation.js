import React from "react";
import {Link} from 'react-router-dom'
// reactstrap components
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Input,
    Row,
    Col
} from "reactstrap";

// import Input from '../Input'
import './style.css'

class Presentation extends React.Component{
    render(){
        return (
            <>
                <Col lg="5" md="7">
                    <Card className="bg-secondary shadow border-0">
                        <CardHeader className="bg-transparent pb-5 d-none">
                            <div className="text-muted text-center mt-2 mb-3">
                                <small>Sign in with</small>
                            </div>
                            <div className="btn-wrapper text-center">
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}
                                >
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../Template/assets/img/icons/common/github.svg")}
                                        />
                                    </span>
                                    <span className="btn-inner--text">Github</span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}
                                >
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../Template/assets/img/icons/common/google.svg")}
                                        />
                                    </span>
                                    <span className="btn-inner--text">Google</span>
                                </Button>
                            </div>
                        </CardHeader>
                        <CardBody className="px-lg-5 py-lg-5">
                            <div className="text-center text-muted mb-4">
                                <small>
                                    Sign in with credentials
                                </small>
                            </div>
                            <Form role="form" onSubmit={this.props.submit}>
                                <FormGroup className={"" + (this.props.username_valid ? "has-success" : (this.props.username_error ? "has-danger" : ""))}>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-email-83" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input 
                                            placeholder="Username / Email" 
                                            disabled={this.props.isDisabled} 
                                            valid={this.props.username_valid && !this.props.username_error}
                                            invalid={this.props.username_error}
                                            className={(this.props.username_valid ? "is-valid" : (this.props.username_error ? "is-invalid" : "")) + " login-input"} 
                                            type="text" 
                                            name="username" 
                                            onChange={this.props.input} 
                                        />
                                        <div className="valid-feedback">
                                            {this.props.username_valid !== true ? this.props.username_valid : "Good to go"}
                                        </div>
                                        <div className="invalid-feedback">
                                            {this.props.username_error !== true ? this.props.username_error : "Nope, this isn't expected here"}
                                        </div>
                                        {/* <Input placeholder="Email" type="email" /> */}
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup className={this.props.password_valid ? "has-success" : this.props.password_error ? "has-danger" : ""}>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input 
                                            placeholder="********"
                                            disabled={this.props.isDisabled}
                                            className={"login-input " + (this.props.password_valid ? "is-valid" : this.props.password_error ? "is-invalid" : "")} 
                                            type="password" 
                                            name="password" 
                                            onChange={this.props.input} 
                                            valid={this.props.password_valid && !this.props.password_error}
                                            invalid={this.props.password_error}
                                            />
                                            <div className="valid-feedback">
                                                {this.props.password_valid !== true ? this.props.password_valid : "Good to go"}
                                            </div>
                                            <div className="invalid-feedback">
                                                {this.props.password_error !== true ? this.props.password_error : "Something is wrong with your password"}
                                            </div>
                                        {/* <Input placeholder="Password" type="password" /> */}
                                    </InputGroup>
                                </FormGroup>
                                <div className="custom-control custom-control-alternative custom-checkbox">
                                    <input
                                        className="custom-control-input"
                                        id=" customCheckLogin"
                                        type="checkbox"
                                        disabled={this.props.isDisabled}
                                    />
                                    <label
                                        className="custom-control-label"
                                        htmlFor=" customCheckLogin"
                                    >
                                        <span className="text-muted">Remember me</span>
                                    </label>
                                </div>
                                <div className="text-center">

                                    {/* <Input type="submit"   className="login-submit" /> */}
                                    <Button 
                                        className="my-4" 
                                        disabled={this.props.username && this.props.password && !this.props.isDisabled ? false : true} 
                                        name="LOGIN" 
                                        color="primary" 
                                        onClick={this.props.submit} 
                                        type="submit">
                                        Sign in
                                        {this.props.isDisabled}
                                    </Button>
                                </div>
                            </Form>
                        </CardBody>
                    </Card>
                    <Row className="mt-3">
                        <Col xs="6">
                            <Link
                                className="text-light"
                                to="/app/login/token"
                            >
                                <small>Login with Token?</small>
                            </Link>
                        </Col>
                        {/* <Col className="text-right" xs="6">
                            <a
                                className="text-light"
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                <small>Create new account</small>
                            </a>
                        </Col> */}
                    </Row>
                </Col>
            </>
        )
    }
}

export default Presentation
