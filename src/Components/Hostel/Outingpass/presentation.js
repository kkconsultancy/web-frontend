import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
   // CardFooter
} from "reactstrap";
import Header from '../../Header'
import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="Hostel Registration" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
                  <Card className="bg-secondary shadow">
                    {this.props.create_hostel_reg_response}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">Outing pass</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                      
                        <h6 className="heading-small text-muted mb-4">
                          Course Information
                    </h6>
                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input 
                                  type="select" 
                                  value={this.props.course} 
                                  valid={this.props.course_valid}
                                  invalid={this.props.course_error}
                                  disabled={this.props.disabled_hostel_reg_create} 
                                  className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} 
                                  name="course" 
                                  onChange={this.props.updateState}
                                >
                                  {this.props.jsx.coursesListJSX}
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.course_valid !== true ? this.props.course_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.course_error !== true ? this.props.course_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>
                             
                          </Row>
                        </div>
                        <div style={{ display: !this.props.course ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Optional Information
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input 
                                    type="select" 
                                    name="branch" 
                                    value={this.props.branch}
                                    valid={this.props.branch_valid}
                                    invalid={this.props.branch_error}
                                    disabled={this.props.course && !this.props.disabled_hostel_reg_create ? false : true } 
                                    className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    {this.props.jsx.branchesListJSX}
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.branch_valid !== true ? this.props.branch_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.branch_error !== true ? this.props.branch_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                              
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select"
                                    name="year" 
                                    value={this.props.year}
                                    valid={this.props.year_valid}
                                    invalid={this.props.year_error}
                                    disabled={this.props.branch && !this.props.disabled_hostel_reg_create ? false : true} 
                                    className={"login-input " + (this.props.year_valid ? "is-valid" : this.props.year_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    {this.props.jsx.yearsListJSX}
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.year_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.year_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                              
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Semester
                              </label>
                                  <Input type="select"
                                    name="semester" 
                                    value={this.props.semester}
                                    valid={this.props.semester_valid}
                                    invalid={this.props.semester_error}
                                    disabled={this.props.year && !this.props.disabled_hostel_reg_create ? false : true} 
                                    className={"login-input " + (this.props.semester_valid ? "is-valid" : this.props.semester_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    <option value="">---</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.semester_valid !== true ? this.props.semester_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.semester_error !== true ? this.props.semester_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Section
                              </label>
                                  <Input type="select"
                                    name="section" 
                                    value={this.props.section}
                                    valid={this.props.section_valid}
                                    invalid={this.props.section_error}
                                    disabled={this.props.semester && !this.props.disabled_hostel_reg_create ? false : true} 
                                    className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.section_valid !== true ? this.props.section_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.section_error !== true ? this.props.section_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                            
                            </Row>
                           
                          </div>
                          
                          </div>
                          
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Hostel Information
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Block name
                              </label>
                                  <Input 
                                    type="input" 
                                    name="blockname" 
                                    value={this.props.blockname}
                                    valid={this.props.blockname_valid}
                                    invalid={this.props.blockname_error}
                                    //disabled={this.props.course && !this.props.disabled_hostel_reg_create ? false : true } 
                                    className={"login-input " + (this.props.blockname_valid ? "is-valid" : this.props.blockname_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                   
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.blockname_valid !== true ? this.props.blockname_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.blockname_error !== true ? this.props.blockname_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                              
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Blocke Code/No
                              </label>
                                  <Input type="input"
                                    name="blockno" 
                                    value={this.props.blockno}
                                    valid={this.props.blockno_valid}
                                    invalid={this.props.blockno_error}
                                    //disabled={this.props.branch && !this.props.disabled_hostel_reg_create ? false : true} 
                                    className={"login-input " + (this.props.blockno_valid ? "is-valid" : this.props.blockno_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    {this.props.jsx.yearsListJSX}
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.blockno_valid !== true ? this.props.blockno_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.blockno_error !== true ? this.props.blockno_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                              
                              
                            
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Permission Given By
                              </label>
                                  <Input type="select"
                                    name="permission" 
                                    value={this.props.permission}
                                    valid={this.props.permission_valid}
                                    invalid={this.props.permission_error}
                                    //disabled={this.props.semester && !this.props.disabled_hostel_reg_create ? false : true} 
                                    className={"login-input " + (this.props.permission_valid ? "is-valid" : this.props.permission_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    <option value="">---</option>
                                    <option value="A">Hostel Incharge</option>
                                    <option value="B">Block incharge</option>
                                    
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.permission_valid !== true ? this.props.permission_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.permission_error !== true ? this.props.permission_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                            
                            </Row>
                            <Row>
                            
                            <Col lg="4">
                              <div className="text-center">

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  //disabled={this.props.course && this.props.branch && this.props.section && this.props.semester && this.props.year && this.props.file_meta.name && !this.props.disabled_hostel_reg_create ? false : true}
                                  onClick = {this.props.submit}
                                >
                                  Click 
                              </Button>
                              </div>
                            </Col>
                          </Row>
                           
                          </div>
                          
                        
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;