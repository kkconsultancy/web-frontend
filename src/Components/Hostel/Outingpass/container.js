import React from 'react'
import Presentation from './presentation'
import './style.css'
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'

class Container extends React.Component{
    
    state={
        course : '',
        course_error: false,
        disabled_hostel_reg_create: false,
        file_meta : {},
        create_hostel_reg_response: <Loading/>,
        jsx : {},
        regno:'',
        regno_error:false,
        
        
        formFields : {"students" : "students","phoneNumber" : "phoneNumber","address" : "address","room_type" : "room_type","room_code":"room_code","category":"category"}
    }

    constructor(props){
        super(props)
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                
                disabled_hostel_reg_create: false,
                create_hostel_reg_response : null,
                jsx : jsx,
                
            }
            console.log(this.state)
        }
    }

    componentWillMount = () => {
        if (this.props.middleware.state.metadata)
            this.props.middleware.getForm({
                category: "HOSTEL",
                type: "ADD"
            }, (data) => {
                const dict1 = []
                const dict2 = []
                console.log(data,"HI")
                for (let i in data){
                    // console.log(data[i].inputName,data[i].name)
                    dict1[data[i].inputName] = data[i].name
                    dict2[data[i].name] = data[i].inputName
                }
                 console.log(dict1)
                 console.log(dict2)
                const header = data.map(field => field.inputName).join(",")
                // const encodedUri = encodeURI(header);
                this.setState({
                    formFieldsDict_inputName: dict1,
                    formFieldsDict_name: dict2,
                    header: header
                    // encodedUri: encodedUri
                })
            })
    }
    
   


    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                blockcode_error : false,
                blockcode_valid : false,
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                csv_file_blob_error : false,
                csv_file_blob_valid : false,
                year_valid : false,
                year_error : false,
                semester_valid : false,
                semester_error : false,
                regno_error : false,
                regno_valid : false,
        })
        this.validate(e)
        // console.log(this.state)
    }
    validate = e => {
        if(e.target.name==="blockcode")
        {
            if(e.target.value){
                return this.setState({
                    blockcode_valid : true,
                    branch : "",
                    year : "",
                    semester : "",
                    section : ""
                })
            }else{
                return this.setState({
                    branch : "",
                    year : "",
                    semester : "",
                    section : ""
                })
            }
        }
        
        //console.log(this.state)
        if(e.target.name === "course"){
            if(e.target.value){
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    // console.log(years)
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : "",
                        year : "",
                        semester : "",
                        section : ""
                    })
                })
                return this.setState({
                    course_valid : true
                })
            }
            this.setState({
                course_error : "Incorrect course selected",
                branch : "",
                year : "",
                semester : "",
                section : ""
            })
        }
        // if (e.target.name === "regno") {
        //     if (e.target.value.match(/^[0-9]{2}[A-Za-z]{1}[0-9]{1}[0-9]{1}[A-Za-z]{1}[0-9]{2}[A-Za-z0-9]{2}$/)) {
        //         return this.setState({
        //             regno_valid: true,
        //             year : "",
        //             semester : "",
        //             section : ""
        //         })
        //     }else{
        //         return this.setState({
        //             year : "",
        //             semester : "",
        //             section : ""
        //         })
        //     }
        // }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true,
                    year : "",
                    semester : "",
                    section : ""
                })
            }else{
                return this.setState({
                    year : "",
                    semester : "",
                    section : ""
                })
            }
        }if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    section : "",
                    semester : ""
                })
            }else{
                return this.setState({
                    semester : "",
                    section : ""
                })
            }
        }
        if (e.target.name === "semester") {
            if (e.target.value) {
                return this.setState({
                    semester_valid: true,
                    section : ""
                })
            }else{
                return this.setState({
                    section : ""
                })
            }
        }
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true
                })
            }
        }
        if (e.target.name === "blockname") {
            if (e.target.value) {
                return this.setState({
                    blockname_valid: true,
                    blockno : "",
                    permission:""
                })
            }else{
                return this.setState({
                    blockno : "",
                    permission:""
                })
            }}
            if (e.target.name === "blockno") {
                if (e.target.value) {
                    return this.setState({
                        blockno_valid: true,
                        
                        permission:""
                    })
                }else{
                    return this.setState({
                       
                        permission:""
                    })
                }
        }
        if (e.target.name === "permission") {
            if (e.target.value) {
                return this.setState({
                    permission_valid: true,})
                    
                   
            }
    } 
       // console.log("course:",this.state.course , "branch:",this.state.branch , "section:",this.state.section , "semester:",this.state.semester , "Year:",this.state.year , "metaname:",this.state.file_meta.name , !this.state.disabled_hostel_reg_create)
        
    }

    hideAlert = () => {
        this.setState({
            create_hostel_reg_response: null,
            disabled_hostel_reg_create: false
        });
    };
    submit = (e) => {
        e.preventDefault();
        
        // this.setState({
        //     disabled_hostel_reg_create : true
        // })

        // if(!this.state.course || !this.state.file_meta)
        //     return this.setState({
        //         disabled_hostel_reg_create: false  
        //     })

        // return this.props.middleware.convertCSVToJSON(this.state.file_meta, [this.state.formFieldsDict_inputName] , json => {
        //     console.log(json)
        //     let stu=[]
        //     for(let i in json){
        //     let hi={
        //         uid:json[i].uid,
        //         phoneNumber:json[i].phoneNumber,
        //         address:json[i].address,
        //         room_type:json[i].room_type,
        //         room_code:json[i].room_code,
        //         category:json[i].category,
        //     }
        //     stu.push(hi)
        //     }
        //     let marksOutput={
        //         academicYear: this.props.middleware.state.metadata.year,
        //         course:this.state.course,
        //         branch:this.state.branch,
        //         year:this.state.year,
        //         studentsList:stu,
        //     }
        //     console.log(marksOutput)
        //     return marksOutput
            
           
        // })
    }
    
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                updateState={this.updateState}
                submit={this.submit}
            />
        )
    }
}

export default Container;
