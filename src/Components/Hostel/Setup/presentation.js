import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// react plugin used to create datetimepicker
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  ListGroupItem,
  ListGroup,
  Table,
  Container,
  Row,
  Col
} from "reactstrap";
 
  import Header from '../../Header'
  // core components
  const Presentation = props =>(
      <>
      <Registration {...props}/>
      </>
  )
  
  
  class Registration extends React.Component {
    state={
      hi:["",""]
    }
   
    render() {
      console.log(this.props.data)
      return (
        <>
          <Header middleware={this.props.middleware} pageTitle="HOSTEL SETUP" /> 
          <Container className="mt--8" fluid>
           <Row className="justify-content-center">
            <Col className="order-xl-1" xl="10">
             <Card className="bg-secondary shadow">
              <CardHeader className="bg-white border-0">
               <Row className="align-items-center">
                <Col xs="8">
                 <h3 className="mb-0">HOSTEL SETUP</h3>
                </Col>
               </Row>
              </CardHeader>
             <CardBody>
             <Form>
              <h6 className="heading-small text-muted mb-4">
                Hostel Details
              </h6>
              <div className="pl-lg-4">
                           <Row>
                           <Col md="4">
                                 <FormGroup>
                                   <label
                                     className="form-control-label"
                                     htmlFor="input-address"
                                   >
                                     Hostel Name
                               </label>
                                   <Input type="text" 
                                   name="hostelname" 
                                   //disabled={this.props.category == "internal" && !this.props.disabled_marks_create ? false : true} 
                                   className={"login-input " + (this.props.hostelname_valid ? "is-valid" : this.props.hostelname_error ? "is-invalid" : "")} 
                                   onChange={this.props.updateState}
                                   
                                   valid={this.props.hostelname_valid}
                                   invalid={this.props.hostelname_error}
                                  >
                                   </Input>
                                   <div className="valid-feedback">
                                     {this.props.hostelname_valid !== true ? this.props.hostelname_valid : "name accepted"}
                                   </div>
                                   <div className="invalid-feedback">
                                     {this.props.hostelname_error !== true ? this.props.phase_error : "Please enter the valid name"}
                                   </div>
                                 </FormGroup>
                               </Col>
                               <Col md="4">
                                 <FormGroup>
                                   <label
                                     className="form-control-label"
                                     htmlFor="input-address"
                                   >
                                     Noof Blocks
                               </label>
                                   <Input type="text" 
                                   name="blocks" 
                                   //disabled={this.props.category == "internal" && !this.props.disabled_marks_create ? false : true} 
                                   className={"login-input " + (this.props.NoofBlocks_valid ? "is-valid" : this.props.hostelname_error ? "is-invalid" : "")} 
                                   onChange={this.props.updateState}
                                   
                                   valid={this.props.NoofBlocks_valid}
                                   invalid={this.props.NoofBlocks_erroe}
                                  >
                                   </Input>
                                   <div className="valid-feedback">
                                     {this.props.NoofBlocks_valid !== true ? this.props.NoofBlocks_valid : "name accepted"}
                                   </div>
                                   <div className="invalid-feedback">
                                     {this.props.NoofBlocks_erroe !== true ? this.props.phase_error : "Please enter the valid name"}
                                   </div>
                                 </FormGroup>
                               </Col>
                               {this.props.jsx}
                               <Row>
                            {console.log(this.props.course , this.props.branch , this.props.section , this.props.semester , this.props.year , this.props.file_meta.name , !this.props.disabled_hostel_reg_create )}
                            <Col lg="4">
                              <div className="text-center">

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  disabled={ this.props.jsx ? false : true}
                                  onClick = {this.props.submit}
                                >
                                  UPLOAD 
                              </Button>
                              </div>
                            </Col>
                          </Row>
                               
                               </Row>
                               
                               </div>
              
             </Form>
             </CardBody>
            </Card>
            </Col>
           </Row>
          </Container>
        </>
      );
    }
  }
  
  export default Presentation
  

