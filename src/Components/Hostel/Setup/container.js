import React,{Component} from 'react'
import Presentation from './presentation';
import {
 
    Media, Alert,
 } from "reactstrap";
 import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    ListGroupItem,
    ListGroup,
    Table,
    
    Row,
    Col
  } from "reactstrap";
  
 class Container extends Component{
    script = null
    constructor(state){
        super(state)
    }
    componentWillMount(){
        this.script = this.state.script
        
    }
    
    state={
      data:[],
        courseS : ["","BTECH","MTECH","MBA","DIPLOMA"],
        branches :["","cse","it"],
        isDisabled : true,
        regnos: ["16K61A05G7","16K61A05E3","16K61A05E0", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        disabledPeriods : '',
        
        
    }
    
   
    updateState = (e) => {
         
        this.setState({
                [e.target.name] : e.target.value,
                
                hostelname_valid:false,
                hostelname_error:false,
                NoofBlocks_valid:false,
                NoofBlocks_erroe:false,
                

        })
        this.validate(e)
        
    }
    validate = e => {
        if(e.target.name === "hostelname"){
            if(e.target.value.match(/^[a-zA-Z]+$/))
                return this.setState({
                    hostelname_valid : true,
                    hostelname_error: false,
                    
                })

            return this.setState({
                
                hostelname_error: true,
               
            })
        }
        if(e.target.name === "blocks"){
           
          var y=[]
          for(let i = 1;i <= e.target.value;i++){
              y.push({
                  id:i,
                  name:"",
                  code:"",
                 hstl:"",
                 type:"",
                 capacity:"",
                 noofrooms:"",
                  incname:"",
                  incnumber:"",
                  contactno:"",
                  error:""
                 
  
  
              })
              
  
          }
          
          
          this.noofBlocks(e.target.value,y)
               
            }
            if(e.target.value.match(/^[0-9]+$/))
                return this.setState({
                    NoofBlocks_valid : true,
                    NoofBlocks_error: false,
                    data:y
                   
                })

            return this.setState({
                
                NoofBlocks_error: true,
                data:y
               
            })
            
            

        }
        
    
    
    
    
    
    //     if (e.target.name === "noofrooms") {
    //         if (e.target.value.match(/^\d+$/)) {
    //             return this.setState({
    //                 noofrooms_valid: true,
    //                 noofrooms_error: false,
    //                 incnumber:'',
    //                 incname:'',
    //                 address:'',
    //                 contactno:'',
    //                 year:'',
    //                 wardenname:''
    //             })
    //         }
    //         else{
    //             return this.setState({
                 
    //                 noofrooms_error: true,
    //                 incnumber:'',
    //                 incname:'',
    //                 address:'',
    //                 contactno:'',
    //                 year:'',
    //                 wardenname:''
    //             })
    //         }
    //     }
    //     if(e.target.name === "incname"){
    //         if(e.target.value.match(/^[a-zA-Z]+$/))
    //             return this.setState({
    //                 incname_valid : true,
    //                 incname_error:false,
    //                 incnumber:'',
    //                 address:'',
    //                 contactno:'',
    //                 year:'',
    //                 wardenname:''
    //             })

    //         return this.setState({
                
    //                 incname_error:true,
    //                 incnumber:'',
    //                 address:'',
    //                 contactno:'',
    //                 year:'',
    //                 wardenname:''
    //         })
    //     }
    //     if (e.target.name === "incnumber") {
    //         if (e.target.value.match(/^[6-9]\d{9}$/))
    //         {
    //             return this.setState({
    //                 incnumber_valid: true,
    //                 incnumber_error:false,
    //                 address:'',
    //                 contactno:'',
    //                 year:'',
    //                 wardenname:''
    //             })
    //         }
    //         else{
    //             return this.setState({
                   
    //                 incnumber_error:true,
    //                 address:'',
    //                 contactno:'',
    //                 year:'',
    //                 wardenname:''
    //             })
    //         }
            
    //     }
    //     if(e.target.name === "address"){
    //         if(e.target.value.match(/^[a-zA-Z]+$/))
    //             return this.setState({
    //                 address_valid : true,
    //                 address_error:false,
    //                 contactno:'',
    //                 year:'',
    //                 wardenname:''
    //             })

    //         return this.setState({
                
    //             address_error: true,
    //             contactno:'',
    //             year:'',
    //             wardenname:''
    //         })
    //     }
    //     if (e.target.name === "contactno") {
    //         if (e.target.value.match(/^[6-9]\d{9}$/))
    //         {
    //             return this.setState({
    //                 contactno_valid: true,
    //                 contactno_error:false,
    //                 year:'',
    //                 wardenname:''
    //             })
    //         }
    //         else{
    //             return this.setState({
                   
    //                 contactno_error: true,
    //                 year:'',
    //                 wardenname:''
    //             })
    //         }
            
    //     }
    //     if (e.target.name === "year") {
    //         if (e.target.value) {
    //             return this.setState({
    //                 year_valid: true,
    //                 year_error:false,
    //                 wardenname:''
    //             })
    //         }
    //         else{
    //             return this.setState({
    //                 year_error:true,
    //                 wardenname:''
    //             })
    //         }
    //     }
    //    if(e.target.name === "wardenname"){
    //         if(e.target.value.match(/^[a-zA-Z]+$/))
    //             return this.setState({
    //                 wardenname_valid : true,
    //                 wardenname_error:false
    //             })

    //         return this.setState({
                
    //             wardenname_error: true
    //         })
    //     }
    validate1=(name,value,n)=>{
      let data=this.state.data.filter(e=>{return(e.id===n)})
      let data1=this.state.data.filter(e=>{return(e.id!==n)})
      data=data[0]
      
      for(let i in data){
        if(i===name){
         data[i]=value
        }
      }
      console.log(data)
      if(name === "name"){
        if(!value.match(/^[a-zA-Z]+$/)){    
               alert("In Hostel Block"+n+"name is not valid")
          }
           data1.push(data)
           
            return this.setState({
            data:data1
          })   
      }
      if(name === "code"){
        if(!value.match(/^\d+$/)){
        alert("In Hostel Block"+n+"code must be  only")           
        }
         data1.push(data)
         return this.setState({
            data:data1
          })
    }
    if (name === "hstl") {
      if (!value){      
              
             
             Alert("Hostel is for block should not be empty")
              
          }
           data1.push(data)
           return this.setState({
            data:data1
          })


      }
      if (name === "type") {
        if (!value) {
         
           Alert("field type shoud not be empty")
         
        }
        data1.push(data)
        return this.setState({
          data:data1
        })
    }
    if (name === "capacity") {
      if (!value.match(/^\d+$/)) {
        
        Alert("hostelcapacity field shoud not be empty")
        
         
      }
      data1.push(data)
        return this.setState({
          data:data1
        })
  }
  if (name === "incname") {
    if (!value) {
      
      Alert("Incharge name should not be empty")
      
       
    }
    data1.push(data)
      return this.setState({
        data:data1
      })
}
if (name === "incnumber") {
  if (!value) {
    
    Alert("Incharge name should not be empty")
    
     
  }
  data1.push(data)
    return this.setState({
      data:data1
    })
}
  
    
    }
    
    noofBlocks=(e,k)=>{
        let y=[]
        for(let i=1;i <=e;i++){
          y.push(i)
      }
        
        let n=k
        const jsx=y.map((n)=>{ 
          let k
          
          k=this.state.data.filter(e=>{return(e.id===n)})
          
          if(k){
            k=n
          }
          let error="hi"+n
          console.log(k.error)
           
            return(
              <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
               <Row className="align-items-center">
                <Col xs="8">
                 <h3 className="mb-0">Hostel Block {n} Details</h3>
                </Col>
               </Row>
              </CardHeader>
              <CardBody>
                <Form >
                
                <div className="pl-lg-4">
                <Row>

                             <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                       Hostel Block Name
                                 </label>
                                     <Input type="text" 
                                     name="name" 
                                     value={k.name}
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}
                                     
                                     >
                                     </Input>
                                     <div className="valid-feedback">
                                       {k.name_valid !== true ? k.name_valid : "name accepted"}
                                     </div>
                                     <div className="invalid-feedback">
                                       {k.name_error !== true ? k.phase_error : "Please enter the valid name"}
                                     </div>
                                   </FormGroup>
                                 </Col>
                                 <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                       Hostel Block Code
                                 </label>
                                     <Input type="text" 
                                     name="code" 
                                     //disabled={this.state.name ? false : true}
                                     
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}
                                     
                                     
                                     
                                     >
                                     </Input>
                                     
                                   </FormGroup>
                                 </Col>
                                 {console.log(k.code ? false : true)}
                             
                             <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                      Hostel Block For
                                 </label>
                                     <Input type="select" 
                                     name="hstl" 
                                    
                                     
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}                                     // value={this.state.hstl}
                                     
                                     >
                                       <option value="">---</option>
                                       <option value="A">girls</option>
                                       <option value="B">boys</option>
                                       
                                     </Input>
                                      
                                   </FormGroup>
                                 </Col>
                                 <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                      Hostel  Type
                                 </label>
                                     <Input type="select" 
                                     name="type" 
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}
                                     >
                                       <option value="">---</option>
                                       <option value="A">A/C</option>
                                       <option value="B">Non A/C</option>
                                      
                                     </Input>
                                     
                                   </FormGroup>
                                 </Col>
                                 <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                       Block Capacity
                                 </label>
                                     <Input type="text" 
                                     name="capacity" 
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}
                                     >
                                     </Input>
                                     
                                   </FormGroup>
                                 </Col>
                                 <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                       No of Rooms in Block
                                 </label>
                                     <Input type="text" 
                                     name="noofrooms" 
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}
                                     >
                                     </Input>
                                     
                                   </FormGroup>
                                 </Col>
                             
                               <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                       Hostel Block Incharge Name
                                 </label>
                                     <Input type="text" 
                                     name="incname" 
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}
                                     >
                                     </Input>
                                     
                                   </FormGroup>
                                 </Col>
                                 <Col md="4">
                                   <FormGroup>
                                     <label
                                       className="form-control-label"
                                       htmlFor="input-address"
                                     >
                                       Hostel Block Phone Number
                                 </label>
                                     <Input type="text" 
                                     name="incnumber" 
                                     onChange={(k)=>{this.validate1(k.target.name,k.target.value,n)}}
                                     >
                                     </Input>
                                    
                                   </FormGroup>
                                 </Col>
                             
                             
                             </Row>
                             </div>
                             </Form></CardBody></Card>
                               
                
 
         )})
         this.setState({
             jsx:jsx
         })
                      
      }
    
    
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                updateState={this.updateState}
                
            />
        )        
    }
}

export default Container








































































