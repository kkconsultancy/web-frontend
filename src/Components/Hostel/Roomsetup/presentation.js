import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// react plugin used to create datetimepicker
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  ListGroupItem,
  ListGroup,
  Table,
  Container,
  Row,
  Col
} from "reactstrap";
 
  import Header from '../../Header'
  // core components
  const Presentation = props =>(
      <>
      <Registration {...props}/>
      </>
  )
  
  
  class Registration extends React.Component {
    render() {
      return (
        <>
         <Header middleware={this.props.middleware} pageTitle="HOSTEL SETUP" />
          <Container className="mt--8" fluid>
           <Row className="justify-content-center">
            <Col className="order-xl-1" xl="10">
             <Card className="bg-secondary shadow">
              <CardHeader className="bg-white border-0">
               <Row className="align-items-center">
                <Col xs="8">
                 <h3 className="mb-0">ROOM SETUP</h3>
                </Col>
               </Row>
              </CardHeader>
             <CardBody>
             <Form>
              <h6 className="heading-small text-muted mb-4">
                Room Details
              </h6>
              <div className="pl-lg-4">
              <Row>
                          <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Hostel Name
                              </label>
                                  <Input type="text" 
                                  name="name" 
                                  //disabled={this.props.category == "internal" && !this.props.disabled_marks_create ? false : true} 
                                  className={"login-input " + (this.props.name_valid ? "is-valid" : this.props.name_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.name}
                                  valid={this.props.name_valid}
                                  invalid={this.props.name_error}
                                  >
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.name_valid !== true ? this.props.name_valid : "name accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.name_error !== true ? this.props.phase_error : "Please enter the valid name"}
                                  </div>
                                </FormGroup>
                              </Col>
                          </Row>
                          <Row>
                          <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                  Hostel bock code
                              </label>
                                  <Input type="text" 
                                  name="hostelcode" 
                                  //disabled={this.props.category == "internal" && !this.props.disabled_marks_create ? false : true} 
                                  
                                  onChange={this.props.updateState}
                                  
                                  >
                                  </Input>
                                  
                                </FormGroup>
                              </Col>
                          </Row>
                          <Row>
                          <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Room No Starts
                              </label>
                                  <Input type="text" 
                                  name="code" 
                                  disabled={this.props.name ? false : true}
                                  className={"login-input " + (this.props.code_valid ? "is-valid" : this.props.code_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.code}
                                  valid={this.props.code_valid}
                                  invalid={this.props.code_error}
                                  >
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.code_valid !== true ? this.props.code_valid : "code accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.code_error !== true ? this.props.code_error : "Please enter the valid code"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Room No ends
                              </label>
                                  <Input type="text" 
                                  name="code2" 
                                  disabled={this.props.name ? false : true}
                                  className={"login-input " + (this.props.code2_valid ? "is-valid" : this.props.code2_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.code2}
                                  valid={this.props.code2_valid}
                                  invalid={this.props.code2_error}
                                  >
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.code2_valid !== true ? this.props.code2_valid : "code accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.code2_error !== true ? this.props.code2_error : "Please enter the valid code"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                   Room  Type
                              </label>
                                  <Input type="select" 
                                  name="type" 
                                  disabled={this.props.code ? false : true}
                                  className={"login-input " + (this.props.type_valid ? "is-valid" : this.props.type_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.type}
                                  valid={this.props.type_valid}
                                  invalid={this.props.type_error}
                                  >
                                    <option value="">---</option>
                                    <option value="A">A/C</option>
                                    <option value="B">Non A/C</option>
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.type_valid !== true ? this.props.type_valid : "Hostel type accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.type_error !== true ? this.props.type_error : "Please select the type of Hostel!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                          
                            <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Room Capacity
                              </label>
                                  <Input type="text" 
                                  name="roomcapacity" 
                                  disabled={this.props.type ? false : true}
                                  className={"login-input " + (this.props.roomcapacity_valid ? "is-valid" : this.props.roomcapacity_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.roomcapacity}
                                  valid={this.props.roomcapacity_valid}
                                  invalid={this.props.roomcapacity_error}
                                  >
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.roomcapacity_valid !== true ? this.props.roomcapacity_valid :" Room Capacity accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.roomcapacity_error !== true ? this.props.roomcapacity_error : "Please enter the valid number"}
                                  </div>
                                </FormGroup>
                              </Col>
                          </Row>
                        </div>
                        <div style={{ display: !this.props.roomcapacity ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            year Information 
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                            <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                   Year
                              </label>
                                  <Input type="select" 
                                  name="year" 
                                  disabled={this.props.roomcapacity ? false : true}
                                  className={"login-input " + (this.props.year_valid ? "is-valid" : this.props.year_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.year}
                                  valid={this.props.year_valid}
                                  invalid={this.props.year_error}
                                  >
                                    <option value="">---</option>
                                    <option value="A">1</option>
                                    <option value="B">2</option>
                                    <option value="C">3</option>
                                    <option value="D">4</option>
                                    
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.year_valid : "Hostel accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.year_error : "Please select the Hostel!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Warden Name
                              </label>
                                  <Input type="text" 
                                  name="wardenname" 
                                  disabled={this.props.year ? false : true}
                                  className={"login-input " + (this.props.wardenname_valid ? "is-valid" : this.props.wardenname_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.wardenname}
                                  valid={this.props.wardenname_valid}
                                  invalid={this.props.wardenname_error}
                                  >
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.wardenname_valid !== true ? this.props.wardenname_valid :" Name  accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.wardenname_error !== true ? this.props.wardenname_error : "Please enter the valid Name"}
                                  </div>
                                </FormGroup>
                              </Col>
                          </Row>
                          <Row>
                            <Col lg="4">
                              <div className="text-center">

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  
                                  onClick = {this.props.submit}
                                >
                                  UPLOAD 
                              </Button>
                              </div>
                            </Col>
                          </Row>
                          </div>
                          
                        
                    </div>
             </Form>
             </CardBody>
            </Card>
            </Col>
           </Row>
          </Container>
        </>
      );
    }
  }
  
  export default Presentation
  

