import React,{Component} from 'react'
import Presentation from './presentation';
import {
 
    Media,
 } from "reactstrap";
  
 class Container extends Component{
    script = null
    constructor(props){
        super(props)
    }
    componentWillMount(){
        this.script = this.props.script
        
    }
    
    state={
        courseS : ["","BTECH","MTECH","MBA","DIPLOMA"],
        branches :["","cse","it"],
        isDisabled : true,
        regnos: ["16K61A05G7","16K61A05E3","16K61A05E0", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        disabledPeriods : '',
        course : '',
        branch:'',
        section:'',
        Date:'',
        attendance:'',
        name:'',
        code:'',
        hstl:'',
        type:'',
        incnumber:'',
        incname:'',
        address:'',
        contactno:'',
        year:'',
        wardenname:'',
        roomcapacity:'',


    }
    
   
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_valid:false,
                year_error:false,
                date_valid: false,
                name_valid:false,
                name_error:false,
                code_valid:false,
                code_error:false,
                code2_valid:false,
                code2_error:false,
                hstl_valid:false,
                hstl_error:false,
                incnumber_valid:false,
                incnumber_error:false,
                incname_valid:false,
                incname_error:false,
                address_error:false,
                address_valid:false,
                contactno_valid:false,
                contactno_error:false,
                type_valid:false,
                type_error:false,
                wardenname_valid:false,
                wardenname_error:false,
                roomcapacity_valid:false,
                roomcapacity_error:false
               

        })
        this.validate(e)
        console.log(this.state)
    }
    validate = e => {
        if(e.target.name === "name"){
            if(e.target.value.match(/^[a-zA-Z]+$/))
                return this.setState({
                    name_valid : true,
                    name_error: false,
                    code:'',
                    type:'',
                    roomcapacity:'',
                    year:'',
                    wardenname:''
                })

            return this.setState({
                
                name_error: true,
                code:'',
                type:'',
                roomcapacity:'',
                year:'',
                wardenname:''
            })
        }
        if(e.target.name === "code"){
            if(e.target.value)
                return this.setState({
                    code_valid : true,
                    code_error : false,
                    type:'',
                    roomcapacity:'',
                    year:'',
                    wardenname:''
                })

            return this.setState({
                
                code_error : true,
                type:'',
                roomcapacity:'',
                year:'',
                wardenname:''
            })
        }
        if(e.target.name === "code2"){
            if(e.target.value)
                return this.setState({
                    code2_valid : true,
                    code2_error : false,
                    type:'',
                    roomcapacity:'',
                    year:'',
                    wardenname:''
                })

            return this.setState({
                
                code2_error : true,
                type:'',
                roomcapacity:'',
                year:'',
                wardenname:''
            })
        }
        if (e.target.name === "type") {
            if (e.target.value) {
                return this.setState({
                    type_valid: true,
                    type_error: false,
                    roomcapacity:'',
                    year:'',
                    wardenname:''
                })
            }
            else{
                return this.setState({
                    type_error: true,
                    roomcapacity:'',
                    year:'',
                    wardenname:''
                })
            }
        }
        if (e.target.name === "roomcapacity") {
            if (e.target.value.match(/^\d$/))
            {
                return this.setState({
                    roomcapacity_valid: true,
                    roomcapacity_error:false,
                    year:'',
                    wardenname:''
                })
            }
            else{
                return this.setState({
                   
                    roomcapacity_error: true,
                    year:'',
                    wardenname:''
                })
            }
            
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error:false,
                    wardenname:''
                })
            }
            else{
                return this.setState({
                    year_error:true,
                    wardenname:''
                })
            }
        }
       if(e.target.name === "wardenname"){
            if(e.target.value.match(/^[a-zA-Z]+$/))
                return this.setState({
                    wardenname_valid : true,
                    wardenname_error:false
                })

            return this.setState({
                
                wardenname_error: true
            })
        }
    }
    
    render(){
        
        return(
            <Presentation
                {...this.props}
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                updateState={this.updateState}
                
            />
        )        
    }
}

export default Container
