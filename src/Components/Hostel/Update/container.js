import React,{Component} from 'react'
import Presentation from './presentation';
import {
 
    Media,
 } from "reactstrap";
  
 class Container extends Component{
    script = null
    constructor(props){
        super(props)
    }
    componentWillMount(){
        this.script = this.props.script
        
    }
    
    state={
        courseS : ["","BTECH","MTECH","MBA","DIPLOMA"],
        branches :["","cse","it"],
        isDisabled : true,
        regnos: ["16K61A05G7","16K61A05E3","16K61A05E0", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        disabledPeriods : '',
        course : '',
        branch:'',
        section:'',
        Date:'',
        attendance:'',
        name:'',
        phonenumber:'',
        address:'',
        type:'',
        room:'',
        passId:'',
        reason:'',

    }
    
   
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_valid:false,
                year_error:false,
                date_valid: false,
                name_valid:false,
                name_error:false,
                phonenumber_valid:false,
                phonenumber_error:false,
                address_error:false,
                address_valid:false,
                hstlname_valid:false,
                hstlname_error:false,
                room_valid:false,
                room_error:false,
                type_valid:false,
                type_error:false,
                passId_valid: false,
                passId_error: false,
                reason_valid: false,
                reason_error: false,

        })
        this.validate(e)
        console.log(this.state)
    }
    validate = e => {
        if(e.target.name === "name"){
            if(e.target.value.match(/^[a-zA-Z'-]+$/))
                return this.setState({
                    name_valid : true
                })

            return this.setState({
                
                name_error: ""
            })
        }
        if (e.target.name === "phonenumber") {
            if (e.target.value.match(/^\+[0-9]{1,}[0-9]{10}$/))
            {
                return this.setState({
                    phonenumber_valid: true
                })
            }
            else{
                return this.setState({
                   
                    phonenumber_error: ""
                })
            }
            
        }
        if(e.target.name === "course"){
            if(e.target.value){
                
                return this.setState({
                    course_valid : true
                })

            }
            else{
                this.setState({
                    course_error : "Incorrect course selected"
                })
            }
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true
                })
            }else{
                return this.setState({
                    year : ""
                })
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true
                })
            }
            else{
                return this.setState({
                    section : ""
                })
            }
        }
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true
                })
            }
        }
        if (e.target.name === "hstlname") {
            if (e.target.value) {
                return this.setState({
                    hstlname_valid: true
                })
            }
            else{
                return this.setState({
                    type : ""
                })
            }
        }
        if (e.target.name === "type") {
            if (e.target.value) {
                return this.setState({
                    type_valid: true
                })
            }
            else{
                return this.setState({
                    room : ""
                })
            }
        }
        if (e.target.name === "room") {
            if (e.target.value.match(/^\+[0-9]{3}$/)) {
                return this.setState({
                    room_valid: true,
                    
                })
                
            }
           
        }
        if (e.target.name === "passId") {
            if (e.target.value.match(/^\+[0-9]{6}$/)) {
                return this.setState({
                    passId_valid: true
                })
            }
            else{
                return this.setState({
                    reason : ""
                })
            }
        }
        if (e.target.name === "reason") {
            if (e.target.value.match(/^\+[A-Z][a-z]+$/)) {
                return this.setState({
                    reason_valid: true
                })
            }
        }
        if(e.target.name==="date"){
            
            console.log("heyy");
             return this.setState({
                 date_valid: true
             })
            
         }
    }
    
    render(){
        
        return(
            <Presentation
                {...this.props}
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                updateState={this.updateState}
                
            />
        )        
    }
}

export default Container
