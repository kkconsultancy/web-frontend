import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// react plugin used to create datetimepicker
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  ListGroupItem,
  ListGroup,
  Table,
  Container,
  Row,
  Col
} from "reactstrap";
import Header from '../../Header'
  // core components
  const Presentation = props =>(
      <>
      <Registration {...props}/>
      </>
  )
  class Registration extends React.Component {
    render() {
      return (
        <>
         <Header middleware={this.props.middleware} pageTitle="UPDATE TRANSPORT DETAILS" />
          <Container className="mt--8" fluid>
           <Row className="justify-content-center">
            <Col className="order-xl-1" xl="10">
             <Card className="bg-secondary shadow">
              <CardHeader className="bg-white border-0">
               <Row className="align-items-center">
                <Col xs="8">
                 <h3 className="mb-0">UPDATE TRANSPORT DETAILS</h3>
                </Col>
               </Row>
              </CardHeader>
             <CardBody>
             <Form>
              <h6 className="heading-small text-muted mb-4">
                Transport Setup
              </h6>
              <div className="pl-lg-4">
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Name
                            </label>
                                <Input type="input" className={"login-input " + (this.props.name_valid ? "is-valid" : this.props.name_error ? "is-invalid" : "")} name="name" onChange={this.props.updateState}>
                                 {/* {this.props.jsx.coursesListJSX}*/}
                                
                                </Input>
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Regd No
                            </label>
                            <Input type="input"  disabled={this.props.name ? false : true} className={"login-input " + (this.props.regdno_valid ? "is-valid" : this.props.regdno_error ? "is-invalid" : "")} name="regdno" onChange={this.props.updateState}></Input>
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                          <Col lg="12">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Address
                              </label>
                                  <Input type="input" name="address" disabled={this.props.regdno ? false : true} className={"login-input " + (this.props.address_valid ? "is-valid" : this.props.address_error ? "is-invalid" : "")} onChange={this.props.updateState}>                                    <option value="">---</option>
                                  </Input>
                                </FormGroup>
                              </Col>
                          </Row>
                        </div>
                        <div style={{ display: !this.props.address ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Branch,year Information 
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input type="select" className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} name="course" onChange={this.props.updateState}>
                                 {/* {this.props.jsx.coursesListJSX}*/}
                                  <option value="">---</option>
                                    <option value="A">Btech</option>
                                    <option value="B">MBA</option>
                                    <option value="C">Mtech</option>
                                    <option value="D">Polytechnic</option>
                                </Input>
                              </FormGroup>
                            </Col>
                              <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input type="select" name="branch"  disabled={this.props.course ? false : true} className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} onChange={this.props.updateState}>
                                    {/*{this.props.jsx.branchesListJSX}*/}
                                    <option value="">---</option>
                                    <option value="A">cse</option>
                                    <option value="B">ece</option>
                                    <option value="C">it</option>
                                    <option value="D">eee</option>
                                  </Input>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select" name="year" disabled={this.props.branch ? false : true} className={"login-input " + (this.props.year_valid ? "is-valid" : this.props.year_error ? "is-invalid" : "")} onChange={this.props.updateState}>
                                    <option value="">---</option>
                                    <option value="A">1</option>
                                    <option value="B">2</option>
                                    <option value="C">3</option>
                                    <option value="D">4</option>
                                  </Input>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    section
                              </label>
                                  <Input type="select" name="section" disabled={this.props.year ? false : true} className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} onChange={this.props.updateState}>
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          <div style={{ display: !this.props.section ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Route Details
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                            <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Detination
                              </label>
                                  <Input type="select" name="destname" className={"login-input " + (this.props.destname_valid ? "is-valid" : this.props.destname_error ? "is-invalid" : "")} onChange={this.props.updateState}>
                                    {/*{this.props.jsx.branchesListJSX}*/}
                                    <option value="">---</option>
                                    <option value="A">Tanuku</option>
                                    <option value="B">Eluru</option>
                                    <option value="C">velivennu</option>
                                    <option value="D">Gudem</option>
                                  </Input>
                                </FormGroup>
                              </Col>
                            <Col lg="6">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  BusNo
                            </label>
                            <Input type="input" disabled={this.props.destname ? false : true}className={"login-input " + (this.props.busno_valid ? "is-valid" : this.props.busno_error ? "is-invalid" : "")} name="busno" onChange={this.props.updateState}/>
                            </FormGroup>
                            </Col>
                           </Row>
                          </div>
                        </div>
                        <div style={{ display: !this.props.busno ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Bus incharge Details
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                            <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    InchargeName
                              </label>
                              <Input type="input" className={"login-input " + (this.props.incname_valid ? "is-valid" : this.props.incname_error ? "is-invalid" : "")} name="incname" onChange={this.props.updateState}/>
                              </FormGroup>
                              </Col>
                            <Col lg="6">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Designation
                            </label>
                            <Input type="input" name="desig" disabled={this.props.incname ? false : true}className={"login-input " + (this.props.desig_valid ? "is-valid" : this.props.desig_error ? "is-invalid" : "")}  onChange={this.props.updateState}/>
                            </FormGroup>
                           </Col>
                          </Row>
                          </div>
                        </div>
                    </div>
             </Form>
             </CardBody>
            </Card>
            </Col>
           </Row>
          </Container>
        </>
      );
    }
  }
  export default Presentation
  

