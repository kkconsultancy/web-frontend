import React,{Component} from 'react'
import Presentation from './presentation';
import './style.css'
import {
    Media,
 } from "reactstrap";
import SyntaxHighlighter from 'react-syntax-highlighter';
class Container extends Component{
    script = null
    constructor(props){
        super(props)
    }
    componentWillMount(){
        this.script = this.props.script
    }
    state={
        courseS : ["","BTECH","MTECH","MBA","DIPLOMA"],
        branches :["","cse","it"],
        isDisabled : true,
        regnos: ["16K61A05G7","16K61A05E3","16K61A05E0", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        disabledPeriods : '',
        course : '',
        branch:'',
        section:'',
        Date:'',
        attendance:'',
        name:'',
        phonenumber:'',
        address:'',
        type:'',
        room:'',
        passId:'',
        reason:'',
        regdno:''
    }
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_valid:false,
                year_error:false,
                date_valid: false,
                name_valid:false,
                name_error:false,
                regdno_valid:false,
                regdno_error:false,
                address_error:false,
                address_valid:false,
                destname_valid:false,
                destname_error:false,
                busno_valid:false,
                busno_error:false,
                incname_valid: false,
                incname_error: false,
                desig_valid: false,
                desig_error: false,
                })
        this.validate(e)
        console.log(this.state)
    }
    validate = e => {
        if(e.target.name === "name"){
            if(e.target.value.match(/^[a-zA-Z]+$/))
                return this.setState({
                    name_valid : true
                })
            else{
                return this.setState({
                    name_error: ""
                })
            }
        }
        if (e.target.name === "regdno") {
            if (e.target.value.match(/^[0-9]{2}[A-Za-z]{1}[0-9]{1}[0-9]{1}[A-Za-z]{1}[0-9]{2}[A-Za-z0-9]{2}$/))
            {
                return this.setState({
                    regdno_valid: true
                })
            }
            else{
                return this.setState({
                   
                    regdno_error: ""
                })
            }
        }
        if(e.target.name === "address"){
            if(e.target.value){
                return this.setState({
                    address_valid : true
                })
            }
            else{
                this.setState({
                    address_error : ""
                })
            }
        }
        if(e.target.name === "course"){
            if(e.target.value){
                return this.setState({
                    course_valid : true
                })
            }
            else{
                this.setState({
                    year: ""
                })
            }
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true
                })
            }
            else{
                return this.setState({
                    year : ""
                })
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true
                })
            }
            else{
                return this.setState({
                    section : ""
                })
            }
        }
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true
                })
            }
        }
        if (e.target.name === "destname") {
            if (e.target.value) {
                return this.setState({
                    destname_valid: true
                })
            }
            else{
                return this.setState({
                     busno : ""
                })
            }
        }
        if (e.target.name === "busno") {
            if (e.target.value.match(/^[0-9]{2}$/)) {
                return this.setState({
                    busno_valid: true,
                    
                })
            }
        }
        if (e.target.name === "incname") {
            if (e.target.value) {
                return this.setState({
                    incname_valid: true
                })
            }
            else{
                return this.setState({
                    desig : ""
                })
            }
        }
        if (e.target.name === "desig") {
            if (e.target.value) {
                return this.setState({
                    desig_valid: true
                })
            }
        }
    }
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                updateState={this.updateState}
            />
        )        
    }
}

export default Container