import React from "react";
import { NavLink as NavLinkRRD, Link } from "react-router-dom";
// nodejs library to set properties for components
//import { PropTypes } from "prop-types";

// reactstrap components
import {
  BreadcrumbItem,
 // NavItem,
    //NavLink
} from "reactstrap";

import Presentation from  './presentation'

class Container extends React.Component{
    constructor(props) {
        super(props);

        const path = this.props.history.location.pathname.split("/").splice(2)
        console.log(path)

        let url = "/app"
        
        const jsx = path.map(location => {
            url += "/"+location
            return (
                <BreadcrumbItem>
                    <Link to={url}>
                        {location.toUpperCase()}
                    </Link>
                </BreadcrumbItem>
            )
        })
        jsx.pop()
        if(path.length > 0)
            jsx.push((
                <BreadcrumbItem aria-current="page" className="active">
                    <Link to={url}>
                        {url.split("/").splice(-1)[0].toUpperCase()}
                    </Link>
                </BreadcrumbItem>
            ))

        this.state = {
            breadcrumbItems : jsx
        }
    }

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
            />
        )
    }
}

export default Container