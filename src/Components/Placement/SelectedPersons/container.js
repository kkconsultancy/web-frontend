import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'
import {
   
    Row,
    Col,
    Button
   
    
} from "reactstrap";

class Container extends Component{
    

    state={
        course : '',
        course_error: false,
        branch:'',
        loading:true,
        disabled_attendance_create: false,
        file_meta : {},
        date : new Date().toLocaleDateString(),
       
    }
    constructor(props){
        super(props)

        this.state = {
            ...this.state,
            templateFileCSV : "attendance_" + this.props.middleware.state.metadata.year + "_" + (this.state.course || "") + "_" + (this.state.branch || "")+ "_" + (this.state.year || "") + "_" + (this.state.section || "") + "_" + (this.state.date || "") + ".csv",
        }
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }

    loadStudents= (data) => {
        
        const inputData = {
            academicYear : this.props.middleware.state.metadata.year,
            course : data.name === "course" ? data.value : this.state.course,
            branch : data.name === "branch" ? data.value : this.state.branch,
            year : data.name === "year" ? data.value : this.state.year,
            semester : data.name === "semester" ? data.value : this.state.semester,
            section : data.name === "section" ? data.value : this.state.section
        }
        if (this.props.middleware.state.metadata)
            return this.props.middleware.state.middlewareAPI.listStudents(inputData, (data) => {

              console.log(data)  
              this.setState({
                loading:false,
                Add:data,
                Added:[],
              })
              this.Add(this.state.Add)
              this.Added(this.state.Added)
            })
    }
    updateState = (e) => {
        // console.log(this.state)
        this.setState({
                [e.target.name] : e.target.value,
                templateFileCSV : "attendance_" + this.props.middleware.state.metadata.year + "_" + (this.state.course || "") + "_" + (this.state.branch || "")+ "_" + (this.state.year || "") + "_" + (this.state.section || "") + "_" + (this.state.date || "") + ".csv",
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_error: false,
                year_valid:false,
                date_error: false,
                date_valid:false,
                
        })
        this.validate(e)
       // console.log("hello"+this.state)
    }
    validate =(e)=>{
        //console.log("hee "+e)
        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : ""
                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false
                })
            }else{
                this.setState({
                    course_error : true,
                    
                })
            }
            
        }
        if(e.target.name==="date"){
            console.log(e.target.value)
            const date = new Date(e.target.value).toLocaleDateString()
            if(e.target.value){
                return this.setState({
                    date : date,
                    date_valid: true,
                    date_error: false
                })
            }else{
                this.setState({
                    branch:"",
                    year:"",
                    section:"",
                    date_error: true
                })    
            }
        }
        if (e.target.name === "branch") {
            if (e.target.value) {

                return this.setState({
                    branch_valid: true,
                    branch_error: false
                })
            }else{
                this.setState({
                    year:"",
                    section:"",
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false
                })
            }else{
                this.setState({
                    section : "",
                    year_error: true
                })
            }
            
        }
        //console.log("hiiii->"+this.state.year)
        if (e.target.name === "section") {
            
            if (e.target.value) {
                this.loadStudents(e.target)
                return this.setState({
                    section_valid: true,
                    section_error: false
                })
            }else{
                this.setState({
                    section_error: true,
                })
            }
        }
        
      
    
    }

    Add = (e) =>{
       
        const jsx1=e.sort().map(e =>{
            return(
                <Col xs="4">
                <Button
                    className="btn-O btn-icon-clipboard"
                    key={e}
                    onClick={() => {this.Add1(e)
                }}
                outline
                    type="button"
                >
                    {e}
                    <i className="ni ni-bold-right" />
                </Button>
            </Col>
        )})
        this.setState({
            jsx1:jsx1
        })
    }
    Added = (e) =>{
        
        
        const jsx2=e.sort().map(e =>{
            return(
                
                <Col xs="4"
                >      
            <Button
            
                          className="btn-icon-clipboard"
                         key={e}
                          onClick={() => {this.Added1(e)
                        }}
                          type="button"
                          outline
                          onlyUnique
                        >
                          <i className="ni ni-bold-left" />
                         {e}
            </Button></Col>
        )})
        this.setState({
            jsx2:jsx2
        })
    }
    Add1 =(e) =>{
        
        const Add = this.state.Add.filter(p=> p!=e ).sort()
        const Added=this.state.Added.sort()
        Added.push(e)
        console.log(Add)
        this.setState({
            Add:Add,
            Added:Added
            
        })
        this.Add(Add)
        this.Added(Added)
        
        

    }
    Added1 =(e) =>{
        
        const Added = this.state.Added.filter(p=> p!=e ).sort()
        const Add=this.state.Add.sort()
        Add.push(e)
        console.log(Add)
        this.setState({
            Add:Add,
            Added:Added
            
        })
        this.Added(Added)
        this.Add(Add)
        
        

    }
    hideAlert = () => {
        this.setState({
            create_attendance_response : null,
            disabled_users_create: false
        });
    };
    
    submit = (e) => {
        e.preventDefault();
        this.setState({
            Add:'',
            Added:'',
            course:'',
            
        })
        
        
    }
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                handleTagsinput={this.handleTagsinput}
                Add={this.Add}
                updateState={this.updateState}
                submit={this.submit}
            />
        )        
    }
}

export default Container