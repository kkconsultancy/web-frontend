import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardFooter,
    Label
} from "reactstrap";
import Header from '../../Header'
import ReactDatetime from "react-datetime";
import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="ADD attendance" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="11">
                  <Card className="bg-secondary shadow">
                    {this.props.create_attendance_response}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">ADD SELECTED PERSONS IN DRIVE</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        
                          <Row>
                           
                            </Row>
                         
                      </Form>
                      
                    </CardBody>
                  </Card>
                  <div style={{ display: !this.props.section ? "none" : "" }}>
                  
                  <Row>
             <Col lg="6">
               
             <Card className="bg-secondary shadow">
               
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="9">
                             <div>{this.props.loading ? <Loading/> : ''}</div>
                               <h3 className="mb-0">Add </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                    <Row className="align-items-center">
                    {this.props.jsx1}
                        </Row>
                    </CardBody>

                </Card>
                </Col>
                <Col lg="6">
             <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                             <div>{this.props.loading ? <Loading/> : ''}</div>
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                      <Row>
                      {this.props.jsx2}
                       </Row>
                       <Col>
                       <Button
                                  type="submit" color="success" outline 
                                  
                                  onClick = {this.props.submit}
                        >
                                  UPLOAD {this.props.disabled_users_create ? <Loading/> : ""}
                              </Button></Col>
                    </CardBody>

                </Card>
                </Col>
                </Row>
                </div>

               
                </Col>
              </Row>
              
            </Container>
          </>

        )
    }
}
export default Presentation;