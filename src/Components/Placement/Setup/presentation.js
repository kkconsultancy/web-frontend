import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    //CardFooter
} from "reactstrap";
import Header from '../../Header'
import Loading from '../../Loading'
import ReactDatetime from "react-datetime";

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="Placement Setup" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
                  <Card className="bg-secondary shadow">
                    
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">PLACEMENT SETUP</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        <h6 className="heading-small text-muted mb-4">
                         Placement Details
                    </h6>
                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                 Placement Type
                            </label>
                                <Input 
                                  type="select" 
                                  value={this.props.pType} 
                                  valid={this.props.ptype_valid}
                                  invalid={this.props.ptype_error}
                                  disabled={this.props.disabled_placement_setup} 
                                  className={"login-input " + (this.props.ptype_valid ? "is-valid" : this.props.ptype_error ? "is-invalid" : "")} 
                                  name="pType" 
                                  onChange={this.props.updateState}
                                >
                                  <option value="">---</option>
                                  <option value="drive">Drive</option>
                                  <option value="training">Training</option>
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.ptype_valid !== true ? this.props.ptype_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.ptype_error !== true ? this.props.ptype_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Start Date
                                  </label>
                                  <ReactDatetime
                                  disabled={this.props.stDate  ? false : true} 
                                    inputProps={{
                                      placeholder: "Select the start date",
                              
                                    }}
                                    
                                    value={this.props.stDate}
                                    defaultValue={new Date()}
                                    timeFormat={false}
                                    onChange={e => this.props.updateState({target : {name : "stDate",value:e._d}})}
                                  />
                                </FormGroup>
                                
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    End Date
                                  </label>
                                  <ReactDatetime
                                    inputProps={{
                                      placeholder: "Select the end date",
                        
                                    }}
                                    disabled={this.props.stDate  ? false : true} 
                                    defaultValue={new Date()}
                                    value={this.props.edDate}
                                    timeFormat={false}
                                    onChange={e => this.props.updateState({target : {name : "edDate",value:e._d}})}
                                  />
                                </FormGroup>
                                
                              </Col>
                              <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Name
                              </label>
                                  <Input type="text"
                                    name="pName" 
                                    value={this.props.pName}
                                    valid={this.props.name_valid}
                                    invalid={this.props.name_error}
                                   
                                    className={"login-input " + (this.props.name_valid ? "is-valid" : this.props.name_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.name_valid !== true ? this.props.name_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.name_error !== true ? this.props.name_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                          </Row>
                          {console.log(this.props.pType,  this.props.stDate , this.props.edDate , this.props.pName , !this.props.disabled_placement_setup)}
                          <Row>
                            <Col lg="4">
                              <div className="text-center">

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  disabled={this.props.pType && this.props.stDate && this.props.edDate && this.props.pName && !this.props.disabled_placement_setup ? false : true}
                                  onClick = {this.props.submit}
                                >
                                  CREATE {this.props.disabled_placement_setup ? <Loading/> : ""}
                              </Button>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;