import React from 'react'
import Presentation from './presentation'
import './style.css'
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'

class Container extends React.Component{
    
    state={
        pType:'',
        course : '',
        course_error: false,
        disabled_placement_setup: false,
        file_meta : {},
        create_users_response: <Loading/>,
        jsx : {},
        regno:'',
        regno_error:false
    }

    constructor(props){
        super(props)
        // console.log(this.props)
        
    }

   
    
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
               ptype_error : false,
               ptype_valid : false,
                stDate_error: false,
                stDate_valid: false,
                edDate_error: false,
                edDate_valid: false,
               
                name_valid : false,
                name_error : false,
                
        })
       
        this.validate(e)
        // console.log(this.state)
    }
    validate = e => {
    
        if (e.target.name === "pType") {
            if (e.target.value) {
                return this.setState({
                    ptype_valid: true,
                    ptype_error: false,
                    stDate:"",
                    edDate:"",
                    name:"",
                })
            }else{
                return this.setState({
                    ptype_error: true,
                    stDate:"",
                    edDate:"",
                    name:"",
                })
            }
        }
        if(e.target.name==="stDate"){
            console.log(e.target.value)
            const stDate = new Date(e.target.value).toLocaleDateString()
            if(e.target.value){
                return this.setState({
                    stDate : stDate,
                    stDate_valid: true,
                    stDate_error: false,
                    edDate:"",
                    name:"",
                })
            }else{
                this.setState({
                    edDate:"",
                    name:"",
                    stDate_error: true
                })    
            }
        }
        if(e.target.name==="edDate"){
            console.log(e.target.value)
            const edDate = new Date(e.target.value).toLocaleDateString()
            if(e.target.value){
                return this.setState({
                    edDate : edDate,
                    edDate_valid: true,
                    edDate_error: false,
                    name:"",
                })
            }else{
                this.setState({
                    edDate:"",
                    name:"",
                    edDate_error: true
                })    
            }
        }
        if (e.target.name === "pName") {
            if (e.target.value) {
                return this.setState({
                    name_valid: true,
                    name_error: false,
                   
                })
            }else{
                return this.setState({
                   name_error: true
                })
            }
        }
    }

    hideAlert = () => {
        this.setState({
            create_users_response: null,
            disabled_placement_setup: false
        });
    };
    submit = (e) => {
        e.preventDefault();
        console.log("hi")
        
        this.setState({
            disabled_placement_setup : true

        })

        const jsx={
            type: this.state.pType,
	        startsAt: this.state.stDate,
	        endsAt : this.state.edDate,
	        name : this.state.pName,
        }
        console.log(jsx)
            return this.props.middleware.state.middlewareAPI.createPlacement(jsx,response => {
                console.log(response)
                this.setState({
                    disabled_placement_setup : false 
                 })
            })

        //         if(!response.code){
        //             this.setState({
        //                 create_users_response: (
        //                     <ReactBSAlert
        //                         success
        //                         style={{ display: "block", marginTop: "100px" }}
        //                         title="Success"
        //                         onConfirm={() => this.hideAlert()}
        //                         onCancel={() => this.hideAlert()}
        //                         confirmBtnBsStyle="success"
        //                         confirmBtnText="Ok"
        //                         btnSize=""
        //                     >
        //                         PLACEMENT ACCOUNTS CREATION HAS BEEN INTIATED
        //                     </ReactBSAlert>
        //                 )
        //             })
        //         }else{
        //             this.setState({
        //                 create_users_response: (
        //                     <ReactBSAlert
        //                         warning
        //                         style={{ display: "block", marginTop: "100px" }}
        //                         title="Warning"
        //                         onConfirm={() => this.hideAlert()}
        //                         onCancel={() => this.hideAlert()}
        //                         confirmBtnBsStyle="warning"
        //                         confirmBtnText="Try Again"
        //                         btnSize=""
        //                     >
        //                         PLACEMENT ACCOUNTS CREATION HAS BEEN FAILED
        //                     </ReactBSAlert>
        //                 )
        //             })
        //         }
        //     })
        // })
    }
    
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                updateState={this.updateState}
                submit={this.submit}
                downloadTemplate={this.downloadTemplate}
            />
        )
    }
}

export default Container;
