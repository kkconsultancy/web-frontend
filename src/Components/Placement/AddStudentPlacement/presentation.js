import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardFooter
} from "reactstrap";
import Header from '../../Header'
import Loading from '../../Loading'
import ReactDatetime from "react-datetime";

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="Add Students to Placement " />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
                  <Card className="bg-secondary shadow">
                    
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">ADD STUDENTS TO PLACEMENT</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        <h6 className="heading-small text-muted mb-4">
                         Placement Student Details
                        </h6>
                        <div className="pl-lg-4">
                          <Row>
                          <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Company ID
                              </label>
                                  <Input type="text"
                                    name="cName" 
                                    value={this.props.cName}
                                    valid={this.props.cname_valid}
                                    invalid={this.props.cname_error}
                                   
                                    className={"login-input " + (this.props.cname_valid ? "is-valid" : this.props.cname_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.cname_valid !== true ? this.props.cname_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.cname_error !== true ? this.props.cname_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            <Col lg="6">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                Eligibility
                            </label>
                                <Input 
                                  type="select" 
                                  value={this.props.pEType} 
                                  valid={this.props.pEtype_valid}
                                  invalid={this.props.pEtype_error}
                                  disabled={this.props.disabled_addstud_placement} 
                                  className={"login-input " + (this.props.pEtype_valid ? "is-valid" : this.props.pEtype_error ? "is-invalid" : "")} 
                                  name="pEType" 
                                  onChange={this.props.updateState}
                                >
                                  <option value="">---</option>
                                  <option value="drive">Eligible</option>
                                  <option value="training">Not Eligible</option>
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.pEtype_valid !== true ? this.props.pEtype_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.pEtype_error !== true ? this.props.pEtype_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>  
                          </Row>
                          {console.log(this.props.pEType, this.props.cName , !this.props.disabled_addstud_placement)}
                          
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
                
                 
                </Row>
                <Row>
                <Col lg="6">
                    <Card className="bg-secondary shadow">
                      <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col lg="6">
                               <h3 className="mb-0">Add </h3>
                             </Col>
                             <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Search
                                    </label>
                                    <Input type="text" name="searchUsers"  className={"login-input "} onChange={this.props.updateState} >
                                    </Input>
                                    </Form>
                              </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                    <Row className="align-items-center">
                        {this.props.jsx1}
                        </Row>
                    </CardBody>

                </Card>
                </Col>
                <Col lg="6">
             <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                           
                    </CardHeader>
                    <CardBody>
                      <Row>
                     
                      {this.props.jsx2}
                      
                       </Row>
                       <Row>
                            <Col lg="4">
                              <div className="text-center">

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  disabled={this.props.pEType && this.props.cName && !this.props.disabled_addstud_placement ? false : true}
                                  onClick = {this.props.submit}
                                >
                                  ADD STUDENTS{this.props.disabled_addstud_placement ? <Loading/> : ""}
                              </Button>
                              </div>
                            </Col>
                          </Row>
                    </CardBody>

                </Card>
                </Col>
                </Row>
               
              
            </Container>
          </>

        )
    }
}
export default Presentation;