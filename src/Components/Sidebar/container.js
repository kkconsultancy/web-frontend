import React from "react";
import { NavLink as NavLinkRRD, Link } from "react-router-dom";
// nodejs library that concatenates classes
import classnames from "classnames";
// nodejs library to set properties for components
import { PropTypes } from "prop-types";
// react library that creates nice scrollbar on windows devices
import PerfectScrollbar from "react-perfect-scrollbar";

import routes from '../../routes'

// reactstrap components
import {
    NavItem,
    Nav,
    NavLink,
    Collapse
} from "reactstrap";

import Presentation from  './presentation'

class Container extends React.Component{
    createLinks = (routes) => {
        return routes.map((prop, key) => {
            if (prop.redirect) {
              return null;
            }
            if (prop.collapse) {
              var st = {};
              st[prop["state"]] = !this.state[prop.state];
              return (
                <NavItem key={key}>
                  <NavLink
                    href="#pablo"
                    data-toggle="collapse"
                    aria-expanded={this.state[prop.state]}
                    className={classnames({
                      active: this.getCollapseInitialState(prop.views)
                    })}
                    onClick={e => {
                      e.preventDefault();
                      this.setState(st);
                    }}
                  >
                    {prop.icon ? <i className={prop.icon} /> : null}
                    <span className="nav-link-text">{prop.name}</span>
                  </NavLink>
                  <Collapse isOpen={this.state[prop.state]}>
                    <Nav className="nav-sm flex-column">
                      {this.createLinks(prop.views)}
                    </Nav>
                  </Collapse>
                </NavItem>
              );
            }
            return (
              <NavItem
                className={this.activeRoute(prop.layout + prop.path)}
                key={key}
              >
                <NavLink
                  to={prop.layout + prop.path}
                  activeClassName=""
                  onClick={this.closeSidenav}
                  tag={NavLinkRRD}
                >
                  {prop.icon !== undefined ? (
                    <>
                      <i className={prop.icon} />
                      <span className="nav-link-text">{prop.name}</span>
                    </>
                  ) : (
                    prop.name
                  )}
                </NavLink>
              </NavItem>
            );
          });
    }
    state = {
        collapseOpen: false
    };

    constructor(props) {
        super(props);
        this.activeRoute.bind(this);
        let services = []
        // const { bgColor, routes, logo } = this.props;
        
        const { bgColor, logo } = this.props;
        let navbarBrandProps;
        if (logo && logo.innerLink) {
            navbarBrandProps = {
                to: logo.innerLink,
                tag: Link
            };
        } else if (logo && logo.outterLink) {
            navbarBrandProps = {
                href: logo.outterLink,
                target: "_blank"
            };
        }

        const rts = this.createLinks(routes)
        console.log(rts)
        // console.log(this.props.middleware.state.authorisation.services)
        if (this.props.middleware.state && this.props.middleware.state.authorisation && Array.isArray(this.props.middleware.state.authorisation.services))
        services = this.props.middleware.state.authorisation.services.map(service => {
            return(
                <NavItem key={service.id}>
                    <NavLink
                        data-toggle="collapse">
                        <NavLinkRRD
                            className="text-dark"
                            to={"/app" + service.route}
                        >
                            {/* {prop.icon ? <i className={prop.icon} /> : null} */}
                            <span className="nav-link-text">{service.name}</span>
                        </NavLinkRRD>
                        </NavLink>
                </NavItem>
            )
        })

        services.push(rts)

        this.state = {
            ...this.state,
            navbarBrandProps: navbarBrandProps,
            logo: logo,
            routes: routes,
            services : services
        }
    }
    // verifies if routeName is the one active (in browser input)
    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }
    // toggles collapse between opened and closed (true/false)
    toggleCollapse = () => {
        this.setState({
            collapseOpen: !this.state.collapseOpen
        });
    };
    // closes the collapse
    closeCollapse = () => {
        this.setState({
            collapseOpen: false
        });
    };
    // creates the links that appear in the left menu / Sidebar

    handling = () => {
        console.log(this.props)
        
    }

    // verifies if routeName is the one active (in browser input)
    activeRoute = routeName => {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    };
    // makes the sidenav normal on hover (actually when mouse enters on it)
    onMouseEnterSidenav = () => {
        if (!document.body.classList.contains("g-sidenav-pinned")) {
            document.body.classList.add("g-sidenav-show");
        }
    };
    // makes the sidenav mini on hover (actually when mouse leaves from it)
    onMouseLeaveSidenav = () => {
        if (!document.body.classList.contains("g-sidenav-pinned")) {
            document.body.classList.remove("g-sidenav-show");
        }
    };
    // toggles collapse between opened and closed (true/false)
    toggleCollapse = () => {
        this.setState({
            collapseOpen: !this.state.collapseOpen
        });
    };
    // closes the collapse
    closeCollapse = () => {
        this.setState({
            collapseOpen: false
        });
    };
    // this creates the intial state of this component based on the collapse routes
    // that it gets through this.props.routes
    getCollapseStates = routes => {
        let initialState = {};
        routes.map((prop, key) => {
            if (prop.collapse) {
                initialState = {
                    [prop.state]: this.getCollapseInitialState(prop.views),
                    ...this.getCollapseStates(prop.views),
                    ...initialState
                };
            }
            return null;
        });
        return initialState;
    };
    // this verifies if any of the collapses should be default opened on a rerender of this component
    // for example, on the refresh of the page,
    // while on the src/views/forms/RegularForms.jsx - route /admin/regular-forms
    getCollapseInitialState(routes) {
        for (let i = 0; i < routes.length; i++) {
            if (routes[i].collapse && this.getCollapseInitialState(routes[i].views)) {
                return true;
            } else if (window.location.href.indexOf(routes[i].path) !== -1) {
                return true;
            }
        }
        return false;
    }
    // this is used on mobile devices, when a user navigates
    // the sidebar will autoclose
    closeSidenav = () => {
        if (window.innerWidth < 1200) {
            this.props.toggleSidenav();
        }
    };
    // this function creates the links and collapses that appear in the sidebar (left menu)

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                getCollapseStates={this.getCollapseStates}
            />
        )
    }
}

export default Container