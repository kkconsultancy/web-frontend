import React from 'react'
import {Link} from 'react-router-dom'
import './style.css'

class Presentation extends React.Component{

    render(){
        return(
           <div className="not-found">
                <div className="circles">
                    <p>404<br/>
                        <small>PAGE NOT FOUND</small>
                    </p>
                    <Link to="/app" className="notfound-link">Go to Homepage</Link>
                    <span className="circle big"></span>
                    <span className="circle med"></span>
                    <span className="circle small"></span>
                    
                </div>
           </div>
        )
    }
}
export default Presentation