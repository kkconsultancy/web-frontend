import React from 'react'
import './style.css'
import {
    Input
} from "reactstrap";

class Presentation extends React.Component{
    
    render(){
        // console.log(this.props)
        return(
            <div className="input-group mb-2">
                {/* {this.props.state.label} */}
                <Input 
                    className={this.props.state.className}
                    name={this.props.name} 
                    onChange={this.props.onChange}
                    onClick={this.props.onClick}
                    onBlur={this.props.onBlur}
                    type={this.props.type}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    id={this.props.name}
                    disabled={this.props.disabled}
                />
                <div className="invalid-feedback">
                    {this.props.error || ""}
                </div>
                <div className="valid-feedback">
                    {this.props.valid}
                </div>
            </div>
        )
    }
}

export default Presentation