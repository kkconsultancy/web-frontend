import React from 'react'
import Presentation from './presentation'

import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Row,
    Col
} from "reactstrap";

class Container extends React.Component{


    state = {}

    constructor(props){
        super(props)
        let jsx = ""
        if(this.props.inputTagName)
            jsx = (
                <div className="input-group-prepend">
                    <b className="input-group-text">
                        {this.props.inputTagName}
                    </b>
                </div>
            )
        else if(this.props.inputLabel)
            jsx = (
                <label htmlFor={this.props.name} className="col">
                    {this.props.inputLabel}
                </label>
            )
        // console.log(this.props)
        let className = "form-control"
        className += this.props.error ? " is-invalid" : ""
        className += " "+(this.props.className || "")
        this.state = {
            label:jsx,
            className : className
        }
    }

    componentDidUpdate(){
        let className = "form-control"
        // console.log(this.props.error)
        className += this.props.error ? " is-invalid" : ""
        className += this.props.valid ? " is-valid" : ""
        className += " " + (this.props.className || "")
        // console.log(this.state,this.props)
        if(className !== this.state.className)
            this.setState({
                className: className
            })
    }
    
    render() {
        return (
            <Presentation
                {...this.props}
                state={this.state}
            />
        )
    }
}


export default Container