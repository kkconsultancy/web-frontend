import React from "react";
import TagsInput from "react-tagsinput";

// reactstrap components
import {
  UncontrolledAlert,
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardImg
} from "reactstrap";

import Header from '../Header'

import Loading from '../../Components/Loading'

class Presentation extends React.Component{
    render(){
        return (
          <>
            <Header middleware={this.props.middleware} pageTitle="ACCOUNT" />
            <Container className="mt--7" fluid>
              <Row className="justify-content-center">
                <Col className="order-md-2 order-sm-1 mb-5 mb-xl-0" xl="4" md="4">
                  <Card className="card-profile shadow">
                    {this.props.reset_password_response}
                    <CardImg
                      onClick={() => document.getElementById("photoURL").click()}
                      alt={this.props.middleware.state.user.uid}
                          className="card-profile-image image-hover-blur"
                      src={this.props.publicURL || this.props.middleware.state.user.photoURL}
                      top
                    />
                    <input 
                      name="photoURL" 
                      id="photoURL" 
                      type="file" 
                      style={{ display:"none"}} 
                      onChange={this.props.updateImage}
                    />
                    {this.props.uploadImageError}
                  
                    <CardHeader className="text-center border-0 pb-0">
                      <div className="d-flex justify-content-between">
                        {this.props.isImageUploading ? <Loading /> : ""}
                        <Button
                          className="float-left"
                          color="default"
                          type="button"
                          onClick={() => document.getElementById("photoURL").click()}
                          size="sm"
                        >
                          Upload
                        </Button>
                        <Button
                          className="float-right"
                          color="danger"
                          type="button"
                          onClick={this.props.requestResetPassword}
                          size="sm"
                        >
                          Reset Password
                        </Button>
                        {this.props.isResetPasswordProcessing ? <Loading /> : ""}
                      </div>
                    </CardHeader>
                    <CardBody className="pt-0">
                      <Row>
                        <div className="col">
                          <div className="card-profile-stats d-flex justify-content-center md-5">
                            <div>
                              <span className="heading">22</span>
                              <span className="description">Friends</span>
                            </div>
                            <div>
                              <span className="heading">10</span>
                              <span className="description">Photos</span>
                            </div>
                            <div>
                              <span className="heading">89</span>
                              <span className="description">Comments</span>
                            </div>
                          </div>
                        </div>
                      </Row>
                      <div className="text-center">
                        <h3>
                          {this.props.middleware.state.user.displayName}
                          <span className="font-weight-light">, 27</span>
                        </h3>
                        <div className="h5 font-weight-300">
                          <i className="ni location_pin mr-2" />
                          {this.props.user.academics.batch || "---"}
                        </div>
                        <div className="h5 mt-4">
                          <i className="ni business_briefcase-24 mr-2" />
                          {this.props.user.academics.course} - {this.props.user.academics.branch} - {this.props.user.academics.year} - {this.props.user.academics.semester}
                        </div>
                        <div>
                          <i className="ni education_hat mr-2" />
                          {"---"}
                        </div>
                        <hr className="my-4" />
                        <p onClick={(e) => {navigator.clipboard.writeText(e.target.value)}}>
                            {this.props.middleware.state.JWT.custom_token}
                        </p>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          Show more
                        </a>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col className="order-md-1 order-sm-2" xl="8" md="8">
              <Card className="bg-secondary shadow">
                {this.props.profile_response}
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                        size="sm"
                      >
                        Settings
                      </Button>
                    </Col>
                  </Row>
                </CardHeader>
                 
                <CardBody>
                  
                  <Form>
                    <h6 className="heading-small text-muted mb-4">
                      User information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-username"
                            >
                              Id
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.uid}
                              id="input-username"
                              placeholder="developerswork"
                              disabled={true}
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                            <FormGroup>
                                <label
                                    className="form-control-label"
                                    htmlFor="input-first-name"
                                >
                                    Name
                                </label>
                                <Input
                                    // className="form-control-alternative"
                                    defaultValue={this.props.user.displayName}
                                    id="input-name"
                                    placeholder="Developers@Work"
                                    disabled={!this.props.disabled_profile ? false : true}
                                    className={"login-input " + (this.props.displayName_valid ? "is-valid" : this.props.displayName_error ? "is-invalid" : "")} 
                                    onChange={this.props.update}
                                    name="displayName"
                                    valid={this.props.displayName_valid}
                                    invalid={this.props.displayName_error}
                                    type="text"
                                />
                                <div className="valid-feedback">
                                  {this.props.displayName_valid !== true ? this.props.displayName_valid : "Good to go"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.displayName_error !== true ? this.props.displayName_error : "Nope, this isn't expected here"}
                                </div>
                            </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-email"
                            >
                              Email address
                            </label>
                            <Input
                              // className="form-control-alternative"
                              disabled={!this.props.disabled_profile ? false : true}
                              className={"login-input " + (this.props.email_valid ? "is-valid" : this.props.email_error ? "is-invalid" : "")} 
                              defaultValue={this.props.user.email}
                              onChange={this.props.update}
                              valid={this.props.email_valid}
                              invalid={this.props.email_error}
                              name="email"
                              placeholder="user@developers.work"
                              type="email"
                            />
                            <div className="valid-feedback">
                              {this.props.email_valid !== true ? this.props.email_valid : "Email is good, isn't it!"}
                            </div>
                            <div className="invalid-feedback">
                                {this.props.email_error !== true ? this.props.email_error : "We don't have time for these!!"}
                            </div>
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                            <FormGroup>
                                <label
                                    className="form-control-label"
                                    htmlFor="input-mobile"
                                >
                                    Mobile
                                </label>
                                <Input
                                    defaultValue={this.props.user.phoneNumber}
                                    name="phoneNumber"
                                    disabled={!this.props.disabled_profile ? false : true}
                                    valid={this.props.phoneNumber_valid}
                                    invalid={this.props.phoneNumber_error}
                                    className={"login-input " + (this.props.phoneNumber_valid ? "is-valid" : this.props.phoneNumber_error ? "is-invalid" : "")}
                                    onChange={this.props.update}
                                    placeholder="+919999999990"
                                    type="text"
                                />
                                <div className="valid-feedback">
                                  {this.props.phoneNumber_valid !== true ? this.props.phoneNumber_valid : "Wow!! Fancy mobile number you got there"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.phoneNumber_error !== true ? this.props.phoneNumber_error : "Make sure you are typing numbers?"}
                                </div>
                            </FormGroup>
                        </Col>
                      </Row>
                      </div>
                      <hr className="my-4" />
                      {/* Description */}
                 <h6 className="heading-small text-muted mb-4">Other Information</h6>
                 
                    <div className="pl-lg-4">
                    <FormGroup>
                    <div className="pl-lg-2">
                    <label
                      className="form-control-label"
                       htmlFor="input-mobile"
                         >
                          Secondary emails
                     </label>
                     <div className="pl-lg-1">
                      <TagsInput
                        onlyUnique
                        className="bootstrap-tagsinput border rounded" sm="12"
                        onChange={this.props.handleTagsinput1}
                        value={this.props.tagsinput1}
                        tagProps={{ className: "tag badge mr-1" }}
                        inputProps={{
                          className: "",
                          placeholder: ""
                        }}
                      />
                      </div>
                      </div>
                    </FormGroup>
                    </div>
                    <div className="pl-lg-4">
                    <Form>
                    <div className="pl-lg-2">
                    <label
                      className="form-control-label"
                       htmlFor="input-mobile"
                         >
                          Secondary Mobile numbers
                     </label>
                     <div className="pl-lg-1">
                       <TagsInput
                        onlyUnique
                        className="bootstrap-tagsinput border rounded"
                        onChange={this.props.handleTagsinput2}
                        value={this.props.tagsinput2}
                        tagProps={{ className: "tag badge mr-1" }}
                        inputProps={{
                          className: "",
                          placeholder: ""
                        }}
                      />
                      </div>
                      </div>
                    </Form>
                    </div>
                   {/* Family Information */}
                   <hr className="my-4" />
                   <h6 className="heading-small text-muted mb-4">
                      Family Information
                    </h6>
                    
                    <div className="pl-lg-4">
                    <h6 className="heading-small text-muted mb-4">
                      Father Information
                    </h6>
                    
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                               Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.personal.father.name}
                              id="input-father-name"
                              placeholder="Father Name"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                               Occupation
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.personal.father.occupation}
                              id="input-Occupation"
                              placeholder="Father Occupation"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Work Address
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue=""
                              id="input-address"
                              placeholder="Home Address"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                             Phone Number
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-phone-number"
                              defaultValue={this.props.user.personal.father.phoneNumber}
                              placeholder="Phone Number"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      
                      <h6 className="heading-small text-muted mb-4">
                      Mother Information
                      </h6>
                      
                      <Row>
                      
                      
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="mother_name"
                            >
                               Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.personal.mother.name}
                              id="mother_name"
                              placeholder="Name"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="mother_occupation"
                            >
                             Occupation
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.personal.mother.occupation}
                              id="mother_occupation"
                              placeholder="House Wife"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Work Address
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09"
                              id="input-address"
                              placeholder="Home Address"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="mother_phone_number"
                            >
                            Phone Number
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultVale={this.props.user.personal.mother.phoneNumber}
                              id="mother_phone_number"
                              placeholder="+919919929999"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      
                      
                    </div>
                    
                    <hr className="my-4" />
                    {/* Address */}
                    <h6 className="heading-small text-muted mb-4">
                      Contact information
                    </h6>
                    <div className="pl-lg-4">
                    <h6 className="heading-small text-muted mb-4">
                      Present Address
                    </h6>
                      <Row>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="present_houseno"
                            >
                              House number
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.address.present.houseNumber}
                              id="present_houseno"
                              placeholder="xx-x-xx/x"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="present_landmark"
                            >
                              Landmark
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.address.present.landmark}
                              id="present_landmark"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              City
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              State
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Country
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="United States"
                              id="input-country"
                              placeholder="Country"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Postal code
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-postal-code"
                              placeholder="Postal code"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                    </div>
                    
                    <div className="pl-lg-4">
                    <h6 className="heading-small text-muted mb-4">
                      Permenent Address
                    </h6>
                      
                    <Row>
                      <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              House number
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              Landmark
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              City
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              State
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Country
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="United States"
                              id="input-country"
                              placeholder="Country"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="3">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Postal code
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-postal-code"
                              placeholder="Postal code"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                        
                    </div>
  
                    <hr className="my-4" />
  {/* SSC Information */}
  <h6 className="heading-small text-muted mb-4">
                     SSC Information
                    </h6>
                    <div className="pl-lg-4">
                    
                      <Row>
                      
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="instution-name"
                            >
                              Instution Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="SASI"
                              id="input-instution-name"
                              placeholder="Instution Name"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="ssc-marks"
                            >
                              SSC Markes
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="ssc-marks"
                              placeholder="SSC Markes"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        <Col md="12">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="ssc-place"
                            >
                              Place
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="TPG"
                              id="ssc-place"
                              placeholder="Place"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      </div>
                      
                    <hr className="my-4" />
 {/* Intermediate Information */}
                    <h6 className="heading-small text-muted mb-4">
                     Intermediate Information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                      
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="institution_name"
                            >
                              Institution Name
                            </label>
                            {console.log(this.props)}
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.education.intermediate.institution.name || ""}
                              id="institution_name"
                              placeholder="Developers@Work"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        
                        
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="intermediate_group"
                            >
                              Group
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="intermediate_group"
                              placeholder="MPC"
                              defaultValue={this.props.user.education.intermediate.group}
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col md="12">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="Intermediate-place"
                            >
                              Institution Address
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.user.education.intermediate.institution.address}
                              id="Intermediate-place"
                              placeholder="Place"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="eamcet-rank"
                            >
                              Score
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="Eamcet Rank"
                              placeholder="eamcet-rank"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="eamcet"
                            >
                              EAMCET
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="eamcet"
                              defaultValue={this.props.user.education.intermediate.eamcet}
                              placeholder="0000000000001"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      </div>
                    <hr className="my-4" />
  {/* Admistration Information */}
                    <h6 className="heading-small text-muted mb-4">
                     Admistration Information
                    </h6>
                    <div className="pl-lg-4">
                    
                      <Row>
                      
                        {/* <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="batch"
                            >
                              Batch
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="2013-2019"
                              id="batch"
                              placeholder="20xx-20xy"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="course"
                            >
                              Course
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Btech"
                              id="course"
                              placeholder="Course"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="branch"
                            >
                              Branch
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="branch"
                              placeholder="cse"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="year"
                            >
                             Year
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="Year"
                              placeholder="year"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="semister"
                            >
                             Semister
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="semister"
                              placeholder="1"
                              type="number"
                            />
                          </FormGroup>
                        </Col> */}
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="date-of-join"
                            >
                              Date of Join
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="date-of-join"
                              defaultValue={this.props.user.admission.doj}
                              placeholder="MM-DD-YYYY"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="admissionNumber"
                            >
                             Admission No
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="admissionNumber"
                              placeholder="20000"
                              defaultValue={this.props.user.admissionNumber}
                              type="numder"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="seat-type"
                            >
                             Seat Type
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="seat-type"
                              defaultValue={this.props.user.admission.seatType}
                              placeholder="Selete"
                              type="Text"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      </div>
                    <hr className="my-4" />
                   
                  </Form>
                </CardBody>
              </Card>
            </Col>
              </Row>
            </Container>
        </>
    
        )
    }
}

export default Presentation


















{/* <div
                      className="dropzone dropzone-single mb-3"
                      id="dropzone-single"
                    >
                      <div className="fallback">
                        <div className="custom-file">
                          <input
                            className="custom-file-input"
                            id="projectCoverUploads"
                            type="file"
                          />
                          <label
                            className="custom-file-label"
                            htmlFor="projectCoverUploads"
                          >
                            Choose file
                          </label>
                        </div>
                      </div>
                      <div className="dz-preview dz-preview-single">
                        <div className="dz-preview-cover">
                          <img
                            alt="..."
                            className="dz-preview-img"
                            data-dz-thumbnail=""
                          />
                        </div>
                      </div>
                    </div> */}
                    