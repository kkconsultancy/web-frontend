import React from 'react';
import { BallBeat } from 'react-pure-loaders';
import './style.css'

class AwesomeComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }
  render() {
    return (
      <div align="center">
        <BallBeat
          color={'#123abc'}
          loading={this.state.loading}
        />
      </div>
    )
  }
}
export default AwesomeComponent