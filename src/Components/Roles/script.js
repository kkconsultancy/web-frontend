import Axios from 'axios';
class Script{
    user = null
    constructor(user,callback){
        // console.log(user)
        this.user = user
        this.firebaseUrl = user.firebaseUrl
        this.callback = callback
        this.metadata = JSON.parse(localStorage.getItem(user.uid))
        this.state.branch = this.metadata.department
        this.state.date = new Date().toISOString().substr(0, 10)
        var date = this.state.date.split('-')
        date = date[2] + "-" + date[1] + "-" + date[0]
        this.state.today = date
    }
    state = {
        academicYear : "2019-2020"
    }
    avail = {
        branchesAvail : {
            BTECH : ["---","CSE","ECE","EEE","ME","CE","PE","IT","ASH"],
            MTECH : ["---","CSE","ECE","EEE","ME"],
            MBA : ["---","MBA"],
            DIPLOMA : ["---","DECE","DME","DEEE"]
        },
        yearsAvail : {
            BTECH: ["---","1", "2", "3", "4"],
            MTECH: ["---","1","2"],
            MBA: ["---","1","2"],
            DIPLOMA: ["---","1","2","3"]
        },
        semistersAvail : ["---","1","2"],
        sectionsAvail : {
            BTECH : {
                CSE : ["---","A","B","C"],
                ECE: ["---", "A", "B", "C"],
                ME: ["---", "A", "B", "C"],
                IT: ["---", "A"],
                EEE: ["---", "A", "B"],
                PE: ["---", "A"],
                CE: ["---", "A"]
            },
            MTECH: {
                CSE: ["---", "A"],
                ECE: ["---", "A"],
                ME: ["---", "A"],
                EEE: ["---", "A"]
            },
            MBA: ["---", "A"],
            DIPLOMA: {
                DECE: ["---", "A"],
                DM: ["---", "A"], 
                DEEE: ["---", "A"]
            }
        }
    }

    newEvent = (id,value) => {
        // console.log(this.state)
        this.state[id] = value
        var data = null
        for(let i=1;i<8;i++)
            if (id === "subjectName"+i)
                return { "nodata": "" }
        if(id === "course"){
            data =  { branch: this.avail.branchesAvail[this.state.course], year: this.avail.yearsAvail[this.state.course]}
        }
        if(id === "year"){
            data =  { semister: this.avail.semistersAvail, section: this.avail.sectionsAvail[this.state.course][this.state.branch]}
        }
        if (this.state.academicYear && this.state.course && this.state.branch && this.state.year && this.state.section)
        if(this.state.date)
            if (id !== "attendance" && id !== "checkedPeriods"){
                // console.log('load')
                this.getRollNo()
            }
        
        if(data)
            return data
        else
            return {"nodata" : ""}
    }

    allSet = () => {
        if (this.state.academicYear && this.state.course && this.state.branch && this.state.year && this.state.section)
            if (this.state.date)
                return true;
        return false
    }

    getRollNo = () => {
        this.state.rollNo = null
        const callback = this.callback
        this.user.getIdToken(true).then(idToken => {
            const url = this.firebaseUrl +"/privilegedAPI/v1/" + this.user.uid + "/" + idToken +"/getRollNo"
            // console.log(url);
            const data = {
                academicYear: this.state.academicYear,
                course: this.state.course,
                branch: this.state.branch,
                year: this.state.year,
                section: this.state.section,
                date : this.state.date
            }
            callback({
                asyncQuery : true
            })
            this.state.metadata = null
            Axios.post(url, data)
                .then(res => {
                    if(res.data)
                        if(res.data.code){
                            callback(res.data)
                            return;
                        }
                    // console.log(res)
                    this.state.rollNo = res.data.regNos
                    this.state.metadata = res.data.metadata
                    // console.log(res.data)
                    if(!this.state.rollNo.length)
                        this.state.rollNo = null
                    callback({
                        target: {
                            id: "regnos",
                            value: this.state.rollNo
                        },
                        asyncQuery: false
                    })
                }).catch(err => {
                    err.asyncQuery = false
                    // console.log(err)
                    this.state.rollNo = null
                    callback(err)
                })
        })
        
    }

    handleSubmit = (e) => {
        // console.log(this.state)
        for (let i in this.state.checkedPeriods){
            if(this.state.checkedPeriods[i]){
                if (!this.state["subjectName"+i]){
                    this.callback({
                        code: "local/invaid-subject-name",
                        message : "given subject is invalid"
                    })
                    return;
                }
            }
        }
        this.postAttendance();
    }

    postAttendance = () => {
        // const callback = this.callback
        this.user.getIdToken(true).then(idToken => {
            const url = this.firebaseUrl+"/privilegedAPI/v1/" + this.user.uid + "/" + idToken + "/postAttendance"
            console.log(url);
            Axios.post(url, this.state)
                .then(res => {
                    // console.log(res)
                    if(!res.data.code)
                        this.callback('POSTED')
                    else
                        this.callback(res.data)
                    // this.state.rollNo = res.data
                }).catch(err => {
                    console.log(err)
                    this.callback(err)
                })
        })
    }
}

export default Script;