import React from 'react'
// import classnames from "classnames";
// import ReactDatetime from "react-datetime";
// // react plugin that creates an input with badges
// import TagsInput from "react-tagsinput";
// // react plugin used to create DropdownMenu for selecting items
// import Select2 from "react-select2-wrapper";
// // plugin that creates slider
// import Slider from "nouislider";
// // react plugin that creates text editor
// import ReactQuill from "react-quill";
// // javascript plugin that creates nice dropzones for files
// import Dropzone from "dropzone";
import {
   // Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    // CardFooter,
    // Label,
    // InputGroup,
    // InputGroupText,
    // InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";
import Header from '../../Header'


class Presentation extends React.Component{

    render(){
        return(
          <>
           <Header middleware={this.props.middleware} pageTitle="MODIFY ATTENDANCE" />
            <Container className="mt--8" fluid>
            <Row className="justify-content-center">
              <Col className="order-xl-1" xl="9">
                  <Card className="bg-secondary shadow">
                    <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">ROLES CREATE</h3>
                             </Col>
                           </Row>
                    </CardHeader>
    {/* name of role        */}
                           <CardBody>
                            <Form>
                              <h6 className="heading-small text-muted mb-4">
                                Roles Information
                              </h6>
                              <div className="pl-lg-4">
                                <Row>
                                <Col lg="6">
                                    <FormGroup className="has-success">
                                      <label
                                        className="form-control-label"
                                        htmlFor="name"
                                      >
                                        Name
                                      </label>
                                      <Input
                                        className={this.props.nameerror2}
                                        name="name"
                                        defaultValue="raj"
                                        id="validationServer01"
                                        placeholder="name"
                                        required
                                        onChange={this.props.updateState}
                                        type="text"
                                      />
                                      <div className="invalid-feedback">{this.props.nameerror1}</div>
                                    </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                    <FormGroup className="has-success">
                                      <label
                                        className="form-control-label"
                                        htmlFor="priority"
                                      >
                                        Priority
                                      </label>
                                      <Input
                                        className={this.props.priorityerror2}
                                        defaultValue="144"
                                        id="validationServer01"
                                        placeholder="priority"
                                        name="priority"
                                        required
                                        onChange={this.props.updateState}
                                        type="text"
                                      />
                                      <div className="invalid-feedback">{this.props.priorityerror1}</div>
                                    </FormGroup>
                                  </Col>
                                </Row>
                            </div>
                            </Form>
                          </CardBody>
                    
                 
                          <hr className="my-4" />

             
     
                    
             </Card>
             

                </Col>
               
               
                
                
              </Row>
            </Container>

          </>

        )
    }
}
export default Presentation;