import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    
    Row,
    Col,
    CardFooter,
    Label,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";

class Container extends Component{
    
    
    state={
        courseS : [],
        branches :[],
        courses:[],
        isDisabled : true,
        name:'',
        nameerror1:"",
        nameerror2:"is-valid",
        code:"",
        codeerror1:"",
        codeerror2:"is-valid",
        priority:"",
        priorityerror1:"",
        priorityerror2:"is-valid",
        

        
       
    }
    updateState = (e) => {
        //  console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                
        })
        this.validate(e)    
    }
   validate = (e) =>{
       let hi=''
       if(e.target.name=="name"){
        if(!e.target.value.match(/[A-Za-z]/)){
            this.setState({
                nameerror1:"please enter a valid name" ,
                nameerror2:"is-invalid"
            })
        }else{
            console.log(hi)
            this.setState({
                nameerror1:hi,
                nameerror2:"is-valid"
            })
        }
       }
       if(e.target.name=="priority"){
        if(!e.target.value.match(/[0-9]{10}$/)){
            this.setState({
                priorityerror1:"please enter a valid Priorty number",
                priorityerror2:"is-invalid" 
            })
        }else{
            this.setState({
                priorityerror1:hi,
                priorityerror2:"is-valid"
            })
        }
       }
   }
    
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
               
            />
        )        
    }
}

export default Container