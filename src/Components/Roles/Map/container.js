import React,{Component} from 'react';
import Presentation from './presentation';
import uniqueTags from 'react-tagsinput';
// import Axios from 'axios';
// import Script from '../script';
import {
   
    Row,
    Col,
    Button
   
    
} from "reactstrap";


class Container extends Component{
    componentDidMount(){
        this.Add(this.state.Add)
        this.Added(this.state.Added)
    }
    
    state = {
        select: null,
       Add : [ "16K61A05G7", "16K61A05D5", "16K61A05E3", "16K61A05E0","16K61A05H0", "16K61A05C1", "16K61A05D7", "16K61A05E1", "16K61A05E9","16K61A05H1"],
       Added : [ ],
       disabled_roles_map: false,
       
      };
      constructor(props){
        super(props)
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }
      
    
    updateState = (e) => {
        //  console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                roleType_error : false,
                roleTpye_valid : false,
                
        })
        this.validate(e)
    }
    validate =(e)=>{
        console.log("state:"+this.state)
        
        if (e.target.name === "roleType") {
            if (e.target.value) {
                return this.setState({
                    roleType_valid: true,
                    roleType_error: false,
                })
            }else{
                this.setState({
                    roleType_error: true,
                })
            }
        }
        if (e.target.name === "searchUsers") {
            if (e.target.value) {
                let val= e.target.value;
            }
        }
    }

    Add = (e) =>{
       
        const jsx1=e.sort().map(e =>{
            return(
                <Col xs="4">
                <Button
                    className="btn-O btn-icon-clipboard"
                    key={e}
                    onClick={() => {this.Add1(e)
                }}
                outline
                    type="button"
                >
                    {e}
                    <i className="ni ni-bold-right" />
                </Button>
            </Col>
        )})
        this.setState({
            jsx1:jsx1
        })
    }
    Added = (e) =>{
        
        
        const jsx2=e.sort().map(e =>{
            return(
                
                <Col xs="4"
                >      
            <Button
            
                          className="btn-icon-clipboard"
                         key={e}
                          onClick={() => {this.Added1(e)
                        }}
                          type="button"
                          outline
                          onlyUnique
                        >
                          <i className="ni ni-bold-left" />
                         {e}
            </Button></Col>
        )})
        this.setState({
            jsx2:jsx2
        })
    }
    Add1 =(e) =>{
        
        const Add = this.state.Add.filter(p=> p!=e ).sort()
        const Added=this.state.Added.sort()
        Added.push(e)
        console.log(Add)
        this.setState({
            Add:Add,
            Added:Added
            
        })
        this.Add(Add)
        this.Added(Added)
        
        

    }
    Added1 =(e) =>{
        
        const Added = this.state.Added.filter(p=> p!=e ).sort()
        const Add=this.state.Add.sort()
        Add.push(e)
        console.log(Add)
        this.setState({
            Add:Add,
            Added:Added
            
        })
        this.Added(Added)
        this.Add(Add)
        
        

    }
    handleTagsinput = tagsinput => {
        this.setState({ tagsinput:tagsinput });
      };
   
    
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                handleTagsinput={this.handleTagsinput}
                Add={this.Add}
                createAcadimicYear={this.createAcadimicYear}
                creatBranch={this.creatBranch}
                creatCourse={this.creatCourse}
                
            />
        )        
    }
}

export default Container