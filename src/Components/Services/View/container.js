import React,{Component} from 'react'
import Presentation from './presentation';
import {
 
    Media,
 } from "reactstrap";
  
 class Container extends Component{
    script = null
    constructor(props){
        super(props)
        
    }
    componentWillMount(){
        this.script = this.props.script
        
    }
    

    state={
        courseS : ["","BTECH","MTECH","MBA","DIPLOMA"],
        branches :["","cse","it"],
        isDisabled : true,
        regnos: ["16K61A05G7","16K61A05E3","16K61A05E0","16K61A05D5"],
        periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        disabledPeriods : '',
        course : '',
        branch:'',
        section:'',
        Date:'',
        attendance:'',
        
    }
   
   
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_valid:false,
                year_error:false,
                date_valid: false
               
        })
        this.validate(e)
        console.log(this.state)
    }
    validate = e => {
        if(e.target.name === "course"){
            if(e.target.value){
                
                return this.setState({
                    course_valid : true
                })

            }
            else{
                this.setState({
                    course_error : "Incorrect course selected"
                })
            }
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true
                })
            }else{
                return this.setState({
                    year : ""
                })
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true
                })
            }
            else{
                return this.setState({
                    section : ""
                })
            }
        }
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true
                })
            }
        }
        if(e.target.name==="date"){
            
            console.log("heyy");
             return this.setState({
                 date_valid: true
             })
            
         }
    }
    periodChecks = () => {
       
        const pc = this.state.periods.map(p => {
            return(
                <th className="sort" scope="col">
                    {p.id}{p.name}
                </th>
            )
        })
        pc.unshift(
            <th className="sort" scope="col">
                REG.NO/PERIODS
            </th>
        )
        return pc
    }
    
    studsdata = () => {
        
        const stu = this.state.regnos.map(c=>{
        console.log(c)
            return(
                <tr>
                      <th scope="row">
                        <Media className="align-items-center">
                            {c}
                        </Media>
                      </th>
                      {this.state.periods.map(p => {
                    return(
                        <td>
                        <label className="custom-toggle custom-toggle-danger mr-1">
                                  <input defaultChecked type="checkbox" />
                                  <span
                                      className="custom-toggle-slider rounded-circle"
                                      data-label-off="No"
                                      data-label-on="Yes"
                                  />
                              </label>
                        </td>
                    )}
                )}
                
                      
                 </tr>
                
            
            )
        })
        return stu;
    }
    
    render(){
        
        return(
            <Presentation
                {...this.props}
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                studsdata={this.studsdata}
                periodChecks={this.periodChecks}
                loadCourses={this.loadCourses}
                loadNewData={this.loadNewData}
                updateState={this.updateState}
                
            />
        )        
    }
}

export default Container
