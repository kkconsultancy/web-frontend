import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// react plugin used to create datetimepicker
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  ListGroupItem,
  ListGroup,
  Table,
  Container,
  Row,
  Col
} from "reactstrap";
 
  import Header from '../../Header'
  // core components
  const Presentation = props =>(
      <>
      <Tables {...props}/>
      </>
  )
  
  
  class Tables extends React.Component {
    render() {
      return (
        <>
         <Header middleware={this.props.middleware} pageTitle="VIEW ATTENDANCE" />
          <Container className="mt--8" fluid>
           <Row className="justify-content-center">
            <Col className="order-xl-1" xl="10">
             <Card className="bg-secondary shadow">
              <CardHeader className="bg-white border-0">
               <Row className="align-items-center">
                <Col xs="8">
                 <h3 className="mb-0">VIEW STUDENTS ATTENDANCE</h3>
                </Col>
               </Row>
              </CardHeader>
             <CardBody>
             <Form>
              <h6 className="heading-small text-muted mb-4">
                Course information
              </h6>
              <div className="pl-lg-4">
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input type="select" className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} name="course" onChange={this.props.updateState}>
                                 {/* {this.props.jsx.coursesListJSX}*/}
                                  <option value="">---</option>
                                    <option value="A">Btech</option>
                                    <option value="B">MBA</option>
                                    <option value="C">Mtech</option>
                                    <option value="D">Polytechnic</option>
                                </Input>
                              </FormGroup>
                            </Col>
                          </Row>
                        </div>
                        <div style={{ display: !this.props.course ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Branch,year & Section Information 
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input type="select" name="branch"   className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} onChange={this.props.updateState}>
                                    {/*{this.props.jsx.branchesListJSX}*/}
                                    <option value="">---</option>
                                    <option value="A">cse</option>
                                    <option value="B">ece</option>
                                    <option value="C">it</option>
                                    <option value="D">eee</option>

                                  </Input>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select" name="year" disabled={this.props.branch ? false : true} className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} onChange={this.props.updateState}>
                                    <option value="">---</option>
                                    <option value="A">1</option>
                                    <option value="B">2</option>
                                    <option value="C">3</option>
                                    <option value="D">4</option>
                                  </Input>

                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Section
                              </label>
                                  <Input type="select" name="section" disabled={this.props.year ? false : true} className={"login-input " + (this.props.year_valid ? "is-valid" : this.props.year_error ? "is-invalid" : "")} onChange={this.props.updateState}>                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>

                                </FormGroup>
                              </Col>
                              <Col md="6">
                                <FormGroup>
                                 <label
                                  className="form-control-label"
                                  htmlFor="exampleDatepicker"
                                 >
                                  Date
                                 </label>
                                 <ReactDatetime
                                   inputProps={{
                                   placeholder: "click here",
                                   name:"date"
                                  }}
                                   timeFormat={false} 
                                   //renderInput={this.renderInput}
                                   onChange={e=>this.props.updateState({
                                      target : {
                                          name :"date",
                                          value :e._d
                                      }
                                    })}
                                     
                                 />
                                </FormGroup>
                               </Col>
                            </Row>
                          </div>
                  <div style={{ display: !this.props.date? "none" : "" }}>   
                <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                 <tr>
                  {this.props.periodChecks()}
                 </tr>
                </thead>
                <tbody className="list">
                 {this.props.studsdata()}
                </tbody>
              </Table>
              </div>  
            </div>
             </Form>
             </CardBody>
            </Card>
            </Col>
           </Row>
          </Container>
        </>
      );
    }
  }
  
  export default Presentation
  

