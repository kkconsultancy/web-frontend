import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    
    Row,
    Col,
    CardFooter,
    Label,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";

class Container extends Component{
    
    
    state={
        courseS : [],
        branches :[],
        courses:[],
        isDisabled : true,
        Year:'',
        courseName:'',
        courseCode:'',
        createBranch:'',
        noofYearsforCourse:'',
        branchCode:'',
        selectCourse:'',
        selectYear:'',
        selectSemister:'',
        specilization:'',
        
       
    }
    updateState = (e) => {
        //  console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                
        })
        this.validate(e)
        console.log(e.target.name,e.target.value)
        
        
    }
    validate =(e) =>{
        if(e.target.value=="Year"){}
    }
    
    createAcadimicYear = (e) =>{
        let course=this.state.courseS
        console.log(this.state.courseS)
        if(this.state.Year!==""){
            course.push(this.state.Year)
         }
        this.setState({

            courseS:course,
            Year:""
        })
        this.view(course,"createAcadimicYear")
        
    }
    creatBranch = (e) =>{
        let y=this.state
        let branch=this.state.branches
         branch.push({
                course:this.state.selectCourse,
                year:this.state.selectYear,
                semister:this.state.selectSemister,
                branche:{
                    branchCode:this.state.branchCode,
                    branchName:this.state.createBranch
                }

            })
        this.setState({
            branches:branch,
            selectCourse:"",
            yeselectYear:"",
            semister:"",
            branchCode:"",
            createBranch:"",
        })
        this.view(branch,"creatBranch")
    }
    creatCourse = (e) =>{
        
        let course=this.state.courses
        course.push({
            courseName:this.state.courseName,
            courseCode:this.state.courseCode,
            noofYearsforCourse:this.state.noofYearsforCourse,
            Specilization:this.state.specilization
                }

            )
        
        console.log(course)
        this.setState({

            courses:course,
            courseName:"",
            courseCode:"",
            noofYearsforCourse:"",
        })
        this.view(course,"creatCourse")
    }
    view = (e,c) =>{
       
        
        if(c=="createAcadimicYear"){
            console.log(e)
            const jsx=e.map(e=>{
                return(
                    <div>
                        <Row>
                        year:{e}
                        </Row>
                    </div>
                )

            })
            this.setState({
                jsx:jsx
            })
        }else if(c=="creatCourse"){
            console.log(e)
           const jsx=e.map(e=>{
                return(
                    <div>
                        <Row>
                        {e.courseName}
                        {e.courseCode}
                        {e.noofYearsforCourse}
                        {e.Specilization}
                        </Row>

                    </div>
                )
            })
            this.setState({
                jsx:jsx
            })
        }else if(c=="creatBranch"){
            console.log(e)
            const jsx=e.map(e=>{
                return(
                    <div>
                        <Row>
                            
                        </Row>

                    </div>
                )
            })
            this.setState({
                jsx:jsx
            })

        }
        
    }
    
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                Add={this.Add}
                createAcadimicYear={this.createAcadimicYear}
                creatBranch={this.creatBranch}
                creatCourse={this.creatCourse}
                
            />
        )        
    }
}

export default Container