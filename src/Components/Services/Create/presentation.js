import React from 'react'
import classnames from "classnames";
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardFooter,
    Label,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";
import Header from '../../Header'


class Presentation extends React.Component{

    render(){
        return(
          <>
           <Header middleware={this.props.middleware} pageTitle="MODIFY ATTENDANCE" />
            <Container className="mt--8" fluid>
            <Row className="justify-content-center">
              <Col className="order-xl-1" xl="9">
                  <Card className="bg-secondary shadow">
                    <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Academics Create</h3>
                             </Col>
                           </Row>
                    </CardHeader>
    {/* Add Academic Year             */}
                           <CardBody>
                            <Form>
                              <h6 className="heading-small text-muted mb-4">
                                Add Academic Year
                              </h6>
                              <div className="pl-lg-4">
                                <Row>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Year(20xx-20xy)
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    
                                    <Input
                                      name="Year"
                                      value={this.props.Year}
                                      placeholder="Year"
                                      type="text"
                                      onChange={this.props.updateState}
                                     />
                                     
                                    </InputGroup>
                                    
                                  </FormGroup>
                                  
                                  <Button  color="primary" type="button"  disabled={this.props.Year ? false : true}  onClick={this.props.createAcadimicYear}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Add</span>
                                    </Button>                           
                                  </Col>
                                </Row>
                            </div>
                            </Form>
                          </CardBody>
                    
                 
                          <hr className="my-4" />
             
      {/* Add course                 */}
                    
                           <CardBody>
                            <Form>
                              <h6 className="heading-small text-muted mb-4">
                                Add course
                              </h6>
                              <div className="pl-lg-4">
                                <Row>

                                <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Corses
                                    </label>
                                    <Input type="select"  value={this.props.Specilization} name="specilization"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                    <option value="others">Others</option>
                                    
                                  </Input>
                                  
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <div style={{ display: (this.props.specilization==="") ? "none" : "" }}>
                                  <div style={{ display: (this.props.specilization==="others") ? "" : "none" }}>
                                  <Col lg="6">
                                  
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Course Name
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                    name="courseName"
                                    disabled={this.props.specilization ? false : true}
                                    value={this.props.courseName}
                                      onChange={this.props.updateState}
                                      placeholder="Enter Course Name"
                                      type="text"
                                     />
                                    </InputGroup>
                                  </FormGroup>
                                  </Col>
                                  </div>
                                  
                                  
                                  <Col lg="6">
                                  
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Course Name
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                    name="courseName"
                                    disabled={this.props.specilization ? false : true}
                                    value={this.props.courseName}
                                      onChange={this.props.updateState}
                                      placeholder="Enter Course Name"
                                      type="text"
                                     />
                                    </InputGroup>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Course code
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                     disabled={this.props.courseName ? false : true}
                                     value={this.props.courseCode}
                                    name="courseCode"
                                      onChange={this.props.updateState}
                                      placeholder="Enter Course code"
                                      type="text"
                                     />
                                    </InputGroup>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      No of Years for Course
                                    </label>
                                    <Input type="select" disabled={this.props.courseCode ? false : true} value={this.props.noofYearsforCourse} name="noofYearsforCourse"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <Button className="btn-icon" color="primary"  disabled={this.props.noofYearsforCourse ? false : true} type="button" onClick={this.props.creatCourse}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Add</span>
                                    </Button> 
                                  </Col>
                                  </div></Col>
                                </Row>
                            </div>
                            </Form>
                          </CardBody>
                    
                  
              
                          <hr className="my-4" />
    {/* Add Branch                 */}
                           <CardBody>
                            <Form>
                              <h6 className="heading-small text-muted mb-4">
                                Add Branch
                              </h6>
                              <div className="pl-lg-4">
                                <Row>
                                <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Select Course
                                    </label>
                                    <Input type="select" value={this.props.selectCourse} value={this.props.selectCourse} name="selectCourse"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    </Form>
                                    </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Create Branch
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                    disabled={this.props.selectCourse ? false : true}
                                    value={this.props.createBranch} 
                                    name="createBranch"
                                      placeholder="Enter Create Branch"
                                      type="text"
                                      onChange={this.props.updateState}
                                     />
                                     
                                    
                            </InputGroup>
                                                                
                              </FormGroup>
                              </Col>
                              
                              <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Branch code
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                    disabled={this.props.createBranch ? false : true}
                                    value={this.props.branchCode} 
                                    name="branchCode"
                                    onChange={this.props.updateState}
                                      placeholder="Enter Branch code"
                                      type="text"
                                     />
                                     
                                    
                            </InputGroup>
                                                                
                              </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Select Year
                                    </label>
                                    <Input type="select" disabled={this.props.branchCode ? false : true} value={this.props.selectYear} name="selectYear"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    </Form>
                                        
                                    </Col>
                                    <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Select Semister
                                    </label>
                                    <Input type="select" disabled={this.props.selectYear ? false : true} value={this.props.selectSemister} name="selectSemister"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    </Form>
                                        
                                    </Col>
                                    <Col lg="6">
                                    <Form>
                                    <Button className="btn-icon" color="primary" disabled={this.props.selectSemister ? false : true} type="button" onClick={this.props.creatBranch}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Add</span>
                                    </Button>
                                    </Form>
                                    </Col>
                                </Row>
                            </div>
                            </Form>
                          </CardBody>
                    
             </Card>
             

                </Col>
               
                <Col className="order-xl-1" xl="3">
                <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                            <Form>
                              {this.props.jsx}
                              
                                
                            </Form>
                          </CardBody>
                </Card>
                </Col>
                
                
              </Row>
            </Container>

          </>

        )
    }
}
export default Presentation;