import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    
    Badge,
       
    CardFooter,
    DropdownMenu,
    DropdownItem,
    DropdownToggle,
    UncontrolledDropdown,
    Media,
    Pagination,
    PaginationItem,
    PaginationLink,
    Progress,
    Table,
    
        
    UncontrolledTooltip,
     
    Row,
    Col,
    
    Label,
} from "reactstrap";
class Container extends Component{
    

    state={
        data:[],
        subjects : [],
        course : '',
        course_error: false,
        branch:'',
        days:["Monday","Thuesday","Wedensday","Thusday","friday","Saterday"],
        create_marks_response : null,
        disabled_marks_create: false,
        file_meta : {},
        
        formFields : {"students" : "students","1" : "1","2" : "2","3" : "3","4":"4","5":"5","6":"6","7":"7"}
        
    }
    constructor(props){
        super(props)
        
        if(this.props.middleware.state.metadata){
          
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,

                branch_error: false,
                branch_valid: false,

                year_error: false,
                year_valid:false,

                semister_valid:false,
                semister_error:false,

                noofSubjects_valid:false,
                noofSubjects_error:false,

                
        })
        this.validate(e)
        // console.log( [e.target.name],e.target.value)
       // console.log("hello"+this.state)
    }
    validate =(e)=>{
        //console.log("hee "+e)
       

        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    // console.log(yearsListJSX)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : "",
                        year:"",
                        section:"",
                        semister:"",
                        category:'',
                        phase:'',
                        type:'',                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false
                })
            }else{
                this.setState({
                    course_error : true,
                    branch: "",
                    year:"",
                    noofSubjects:"",
                    semister:"",
                    

                    
                })
            }
            
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true,
                    branch_error: false,
                    year:"",
                    noofSubjects:"",
                    semister:"",
                    
                })
            }else{
                this.setState({
                    year:"",
                    noofSubjects:"",
                    semister:"",
                   
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false,
                    noofSubjects:"",
                    semister:"",
                    
                })
            }else{
                this.setState({
                    section : "",
                    noofSubjects:"",
                    
                   
                    year_error: true
                })
            }
            
        }
        //console.log("hiiii->"+this.state.year)
        
        if (e.target.name === "semister") {
            if (e.target.value) {
                return this.setState({
                    semister_valid: true,
                    semister_error: false,
                })
            }else{
                this.setState({
                    semister_error: true,
                })
            }
            console.log("corse:",this.state.course , "branch:",this.state.branch , "section:",this.state.section , "semister:",this.state.semester , "Year:",this.state.year , "mataname:",this.state.file_meta.name , !this.state.disabled_users_create)
           
        }

        if (e.target.name === "noofSubjects") {
            if (e.target.value) {
                this.generateSubjectsJSX(parseInt(e.target.value))
                return this.setState({
                    noofSubjects_valid: true,
                    noofSubjects_error: false,
                })
                    
            }else{
                this.setState({
                    noofSubjects_error: true,
                    
                })
            }
        }
        
    }
    hideAlert = () => {
        this.setState({
            create_marks_response: null,
            disabled_marks_create: false
        });
    };
   

    updatestate1 = (day,period,value) =>{
        console.log(day,period.slice(period.length - 1),this.state.data)
        let period1=(period.slice(period.length - 1))-1
        let data1=this.state.data.filter((e)=>{return (e.day===day)}).map((e)=>{
            let data=e
            data.periods[period1]=value
            
            return(data)
        })
        let data2=this.state.data.filter((e)=>{return (e.day!==day)})
        data2.push(data1[0])
        console.log(data2)
        this.setState({
            data:data2
        })
        console.log(this.state.data)
        
        


    } 

generateSubjectsJSX = (noofSubjects) => {
        let Head=[]
        let day=[]
        let data=[]
        this.state.days.map((e)=>{
            let data1=[]
                for(let i=1;i <=noofSubjects;i++){
                    data1.push("")
                }
                console.log(e)
                data.push({
                    day:e,
                    periods:data1
                })
        })
        
        this.setState({
            data:data
        })




        for(let i = 1;i <= noofSubjects;i++){
            let id={
                key:"Period-" +i
            }
            Head.push(id)
        }
        
        
       const Headr=Head.map((e)=>{ 
           
           return(
            
                      <th className="sort" data-sort="name" scope="col">
                          <Col md="12" sm="6">
                    {e.key}
                    </Col>
                      </th>
                              
               

        )

       })
       this.state.days.map((e)=>{
           day.push(e)
       })
       
       let TableBody=day.map((e)=>{
           let i=0
           return(
        <tr>
            <th scope="row" scope="row">
                <Col lg="5">
                <FormGroup>
                <Media className="align-items-center">
                    {e}
                </Media>
                </FormGroup></Col>
            </th>
            {
                Head.map((j)=>{
                    

                    return(
                    <th scope="row"  >
                    <Col md="12" sm="6">
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="example4cols3Input"
                    >
                      
                    </label>
                    <Input
                      id="example4cols3Input"
                      placeholder="subjects"
                      type="text"
                      onChange={(k)=>this.updatestate1(e,j.key,k.target.value)}
                    />
                  </FormGroup>
                </Col>
                </th>
                )})
            }
        </tr>
       )})
       Headr.unshift(
        <th className="sort" scope="col">
            Days
        </th>
    )
       this.setState({
        Headr:Headr,
        TableBody:TableBody,
    })

       













































        
        
        
    }

    submit = (e) => {
        e.preventDefault();
        
        
        
    }
    
        
       
   
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                submit={this.submit}
               
            />
        )        
    }
}

export default Container