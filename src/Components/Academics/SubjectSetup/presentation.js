import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardFooter,
    Label
} from "reactstrap";
import Header from '../../Header'
import ReactDatetime from "react-datetime";
import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="POST marks" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
                  <Card className="bg-secondary shadow">
                    {this.props.create_marks_response}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">ADD SUBJECTS</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form>
                        <h6 className="heading-small text-muted mb-4">
                          Course Information
                        </h6>

                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input type="select" 
                                className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} 
                                name="course" 
                                value={this.props.course}
                                onChange={this.props.updateState}
                                valid={this.props.course_valid}
                                invalid={this.props.course_error}
                                >
                                 {this.props.jsx.coursesListJSX}
                                {/* <option value="">---</option>
                                    <option value="btech">B.Tech</option>
                                    <option value="mtech">M.Tech</option>
                                    <option value="diploma">Diploma</option>
                                    <option value="mba">MBA</option> */}
                                  
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.course_valid !== true ? this.props.course_valid : "Course accepted"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.course_error !== true ? this.props.course_error : "Please select the course!!"}
                                </div>
                              </FormGroup>
                            </Col>
                          </Row>
                        </div>
                        <div style={{ display: !this.props.course? "" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Branch & Section Information
                          </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input type="select" 
                                  name="branch"
                                  disabled={this.props.course && !this.props.disabled_marks_create ? false : true }  
                                  className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.branch}
                                  valid={this.props.branch_valid}
                                  invalid={this.props.branch_error}
                                  >
                                  {this.props.jsx.branchesListJSX}
                              
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.branch_valid !== true ? this.props.branch_valid : "Branch accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.branch_error !== true ? this.props.branch_error : "Please select the branch!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select" 
                                  name="year" 
                                  disabled={this.props.branch && !this.props.disabled_marks_create ? false : true} 
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.year}
                                  valid={this.props.year_valid}
                                  invalid={this.props.year_error}
                                  >
                                    {this.props.jsx.yearsListJSX}
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.year_valid : "Year accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.year_error : "Please select the year!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Semister
                              </label>
                                  <Input type="select" 
                                  name="semister" 
                                  disabled={this.props.year && !this.props.disabled_marks_create ? false : true}
                                  className={"login-input " + (this.props.semister_valid ? "is-valid" : this.props.semister_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.semister}
                                  valid={this.props.semister_valid}
                                  invalid={this.props.semister_error}
                                  >
                                    <option value="">---</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.section_valid !== true ? this.props.section_valid : "Section accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.section_error !== true ? this.props.section_error : "Please select the section!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    No of Subjects
                              </label>
                                  <Input type="text" 
                                  name="noofSubjects" 
                                  disabled={ this.props.semister ? false : true} 
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.noofSubjects}
                                  valid={this.props.noofSubjects_valid}
                                  invalid={this.props.noofSubjects_error}
                                  >
                                    
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.noofSubjects_valid !== true ? this.props.noofSubjects_valid : "Accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.noofSubjects_error !== true ? this.props.noofSubjects_error : "Please enter the No. of Subjects!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          
                          </div> 
                      
                          <div style={{ display: !this.props.noofSubjects ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Subjects Information
                          </h6>
                          <div className="pl-lg-4">
                          <Row>
                            {this.props.jsx2}
                            <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  
                                  onClick = {this.props.submit}
                                >
                                  UPLOAD 
                              </Button>
                          </Row>
                          </div>
                          
                          
                          </div> 
                      
                      </Form>
                      
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;