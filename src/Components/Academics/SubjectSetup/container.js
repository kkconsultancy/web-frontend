import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
   
    Row,
    Col,
    CardFooter,
    Label
} from "reactstrap";
class Container extends Component{
    

    state={
        subjects : [],
        course : '',
        course_error: false,
        branch:'',
        create_marks_response : null,
        disabled_marks_create: false,
        file_meta : {},
        
        formFields : {"students" : "students","1" : "1","2" : "2","3" : "3","4":"4","5":"5","6":"6","7":"7"}
        
    }
    constructor(props){
        super(props)
        //const subjects=["DS","MS","ML","CPP"];
         //console.log("SUB"+this.state.subjects)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,

                branch_error: false,
                branch_valid: false,

                year_error: false,
                year_valid:false,

                semister_valid:false,
                semister_error:false,

                noofSubjects_valid:false,
                noofSubjects_error:false,

                
        })
        this.validate(e)
        // console.log( [e.target.name],e.target.value)
       // console.log("hello"+this.state)
    }
    validate =(e)=>{
        //console.log("hee "+e)
       

        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    // console.log(yearsListJSX)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : "",
                        year:"",
                        section:"",
                        semister:"",
                        category:'',
                        phase:'',
                        type:'',                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false
                })
            }else{
                this.setState({
                    course_error : true,
                    branch: "",
                    year:"",
                    noofSubjects:"",
                    semister:"",
                    

                    
                })
            }
            
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true,
                    branch_error: false,
                    year:"",
                    noofSubjects:"",
                    semister:"",
                    
                })
            }else{
                this.setState({
                    year:"",
                    noofSubjects:"",
                    semister:"",
                   
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false,
                    noofSubjects:"",
                    semister:"",
                    
                })
            }else{
                this.setState({
                    section : "",
                    noofSubjects:"",
                    
                   
                    year_error: true
                })
            }
            
        }
        //console.log("hiiii->"+this.state.year)
        
        if (e.target.name === "semister") {
            if (e.target.value) {
                return this.setState({
                    semister_valid: true,
                    semister_error: false,
                })
            }else{
                this.setState({
                    semister_error: true,
                })
            }
            console.log("corse:",this.state.course , "branch:",this.state.branch , "section:",this.state.section , "semister:",this.state.semester , "Year:",this.state.year , "mataname:",this.state.file_meta.name , !this.state.disabled_users_create)
           
        }

        if (e.target.name === "noofSubjects") {
            if (e.target.value) {
                this.generateSubjectsJSX(parseInt(e.target.value))
                return this.setState({
                    noofSubjects_valid: true,
                    noofSubjects_error: false,
                })
                    
            }else{
                this.setState({
                    noofSubjects_error: true,
                    
                })
            }
        }
        
    }
    hideAlert = () => {
        this.setState({
            create_marks_response: null,
            disabled_marks_create: false
        });
    };
   

    updateState1 = (id,value1,aut) =>{
        console.log(this.state.subjects)
        var id1=id
        let subjescts1=[]
        let subjescts2=[]
        subjescts1=this.state.subjects.filter((e)=>e.id!==id1).map((e)=>{ 
            
            return(e)
        })
        console.log(subjescts1,"out")
        if(aut === "SubjectName"){
                subjescts2=this.state.subjects.filter((e)=>e.id===id1).map((e)=>{
                    let e1={
                        id:e.id,
                        subject:value1,
                        code:e.code,    
                    }
                    return e1
                })

        }else if(aut === "SubjectCode"){
            subjescts2=this.state.subjects.filter((e)=>e.id===id1).map((e)=>{
                let e1={
                    id:e.id,
                    subject:e.subject,
                    code:value1,    
                }
                
                return e1
            })

        }
    subjescts1.push(subjescts2[0])
     this.setState({
        subjects : subjescts1,
            
        })
        console.log(subjescts1)
     
        
    }

    generateSubjectsJSX = (noofSubjects) => {
        let subjects = []
        let jsx1=[]
        for(let i = 1;i <= noofSubjects;i++){
            subjects.push({
                id:i,
                subject:"",
                code:""
            })
            let id={
                key:i
            }
            jsx1.push(id)

        }
        this.setState({
            subjects : subjects,
        })
         
       
        
         let jsx2 =jsx1.map((e)=>{ 
            let hi="Subject"+e.key
            let hi1="SubjectCode"+e.key
            
            return(
                   
                    <>
                        <Col md="6">
                            <FormGroup>
                                <label
                                className="form-control-label"
                                htmlFor="input-address"
                                >
                                Subject {e.key} Name
                                
                            </label>
                                <Input type="text" 
                                name={hi}                                
                                className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                onChange={(k) => this.updateState1(e.key,k.target.value,"SubjectName")}
                                value={this.state.hi}
                                
                                >
                                
                                </Input>
                                <div className="valid-feedback">
                                {this.props.noofSubjects_valid !== true ? this.props.noofSubjects_valid : "number accepted"}
                                </div>
                                <div className="invalid-feedback">
                                {this.props.noofSubjects_error !== true ? this.props.noofSubjects_error : "Please enter the no of Subjects!!"}
                                </div>
                            </FormGroup>
                            </Col>
                        <Col md="6">
                            
                            <FormGroup>
                                <label
                                className="form-control-label"
                                htmlFor="input-address"
                                >
                                Subject {e.key} Code 
                                
                            </label>
                                <Input type="text" 
                                name={hi1}                                  
                                className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                onChange={(k) => this.updateState1(e.key,k.target.value,"SubjectCode")}
                                value={this.state.hi1 }
                                
                                >
                                
                                </Input>
                                <div className="valid-feedback">
                                {this.props.noofSubjects_valid !== true ? this.props.noofSubjects_valid : "number accepted"}
                                </div>
                                <div className="invalid-feedback">
                                {this.props.noofSubjects_error !== true ? this.props.noofSubjects_error : "Please enter the no of Subjects!!"}
                                </div>
                            </FormGroup>
                            </Col>
                    </>
            )
                    

        })
        // console.log(subjects)
        this.setState({
            jsx2:jsx2,
        })
        
        
    }

    submit = (e) => {
        e.preventDefault();
        
        
        
    }
    
        
       
   
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                submit={this.submit}
               
            />
        )        
    }
}

export default Container