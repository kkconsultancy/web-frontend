import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
//import Loading from '../../Loading'
import { Row,Col } from "reactstrap";
class Container extends Component{
    

    state={
        course : '',
        course_error: false,
        //branch:'',
        disabled_academics_create: false,
        file_meta : {},
        year:(new Date().getFullYear()),
        courses:[],
        branches:[],
        yr_sem:[],
        section:[],
        branchCode:'',
        branchName:"",
        branchIntake:"",
        createBranch:"",
        
        
    }
    constructor(props){
        super(props)

        this.state = {
            ...this.state,
            
        }
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }
    updateState = (e) => {
        // console.log(this.state)
        this.setState({
                [e.target.name] : e.target.value,
            
                course_error : false,
                course_valid : false,

                newCourseType_error : false,
                newCourseType_valid : false,

                newCourseName_error : false,
                newCourseName_valid : false,

                newCourseCode_error : false,
                newCourseCode_valid : false,

                newCourseDuration_error : false,
                newCourseDuration_valid : false,

                
               
        })
        this.validate(e)
       //console.log("hello"+this.state.branch)
    }
    validate =(e)=>{
        //console.log("hee "+e)
        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                     branch : ""
                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false,
                   
                  
                })
            }else{
                this.setState({
                    course_error : true,
                   
                 
                })
            }
            
        }
        if(e.target.name==="newCourseType"){
            // console.log(e.target.value)
             if(e.target.value){
                 return this.setState({
                    
                    newCourseType_valid: true,
                    newCourseType_error: false,
                   // newCourseName: e.target.value,
                   newCourseName:"", 
                   newCourseCode: "",
                    newCourseDuration:"",
                 })
             }else{
                 this.setState({
                    newCourseType_error: true,
                    newCourseName:"",
                    newCourseCode: "",
                    newCourseDuration:"",
 
                 })    
             }
         }
        if(e.target.name==="newCourseName"){
           // console.log(e.target.value)
            if(e.target.value){
                return this.setState({
                   
                   newCourseName_valid: true,
                   newCourseName_error: false,
                  // newCourseName: e.target.value,
                   newCourseCode: "",
                   newCourseDuration:"",
                })
            }else{
                this.setState({
                   newCourseName_error: true,
                   newCourseCode: "",
                   newCourseDuration:"",

                })    
            }
        }
        if(e.target.name==="newCourseCode"){
            // console.log(e.target.value)
             if(e.target.value){
                 return this.setState({
                    
                    newCourseCode_valid: true,
                    newCourseCode_error: false,
                    newCourseDuration: "",
                 })
             }else{
                 this.setState({
                    newCourseCode_error: true,
                    newCourseDuration: "",
 
                 })    
             }
         }
         if(e.target.name==="newCourseDuration"){
            // console.log(e.target.value)
             if(e.target.value){
                 return this.setState({
                    
                    newCourseDuration_valid: true,
                    newCourseDuration_error: false,
                 })
             }else{
                 this.setState({
                    newCourseDuration_error: true,
                
                })    
             }
         }
         
        console.log(this.state)
    }
    createAcademicYear = (e) =>{
        console.log("New Academic Year created");
        
        let Year=this.state.courses
        Year.push({
            ac_yr:this.state.year
            
        })
        //console.log("yr::"+Year)
        this.setState({
            courses: Year,
            
        }) 
        this.view(Year,"createAcademicYear")    
    }


    
    createCourse = (e) =>{
        //e.preventDefault();
        let course=this.state.courses
        course.push({
            newCourseType:this.state.newCourseType,
            newCourseName:this.state.newCourseName,
            newCourseCode:this.state.newCourseCode,
            newCourseDuration:this.state.newCourseDuration,
           
                }

            )
        
        console.log("course::"+course)
        this.setState({

            courses:course,
            newCourseType:"",
            newCourseName:"",
            newCourseCode:"",
            newCourseDuration:"",
        })
        this.view(course,"createCourse")
    }
    
    view = (e,c) =>{
       
        //e.preventDefault();
        if(c==="createAcademicYear"){
            console.log("kk-"+e)
            const jsxa=e.map(e=>{
                return(
                    <div>
                        <h3>Academic Year: </h3>{e.ac_yr}
                        <hr/>
                    </div>
                )
            })
            this.setState({
                 jsxa:jsxa
             })
        }else if(c=="createCourse"){
            console.log("view::"+e)
           const jsx1=e.map(e=>{
                return(
                    <div>
                        <h3>Academics Course Details:</h3>
                        <Row>
                        <Col><b>Course Type:</b> {e.newCourseType}</Col>
                        <Col><b>Course:</b>{e.newCourseName}</Col>
                        </Row>
                        <Row>
                        <Col><b>Course Code:</b> {e.newCourseCode}</Col>
                        <Col><b>Course Duration:</b> {e.newCourseDuration}</Col>
                        </Row>
                        <hr/>
                    </div>
                )
            })
            this.setState({
                jsx1:jsx1
            })
        }
    }
    
    // hideAlert = () => {
    //     this.setState({
    //         create_academics_response : null,
    //         disabled_academics_create: false
    //     });
    // };
    // confirm =(e)=>{
    //     e.preventDefault();
    //     const jsxe =e=>{
    //         return(
    //         <div>Successful</div>
    //         )
    //     }
    //     this.setState({
    //         jsxe:jsxe
    //     })
    // }
    submit = (e) => {
        e.preventDefault();
        
        this.setState({
            disabled_academics_create : true
        })
        console.log(this.state)
        if(!this.state.course || !this.state.section || !this.state.year || !this.state.semester || !this.state.branch || !this.state.file_meta)
            return this.setState({
                disabled_acdemics_create: false  
            })

        //return this.props.middleware.convertCSVToJSON(this.state.file_meta, [this.state.formFields, this.state.formFields] , json => {
        //     // console.log(json)
        //     const attendance = {}
        //     json.map(record => {
        //         for(let i in record){
        //             if(i !== "students"){
        //                 if(!Array.isArray(attendance[i]))
        //                     attendance[i] = []
        //                     attendance[i].push(record["students"])
        //             }
        //         }
        //         return attendance
        //     })
        //     console.log(attendance)
        //     const inputData = {
        //         academicYear : this.props.middleware.state.metadata.year,
        //         course : this.state.course,
        //         branch : this.state.branch,
        //         semester : this.state.semester,
        //         date : {
        //             year : this.state.date.split("/")[2],
        //             month : this.state.date.split("/")[0],
        //             day : this.state.date.split("/")[1]
        //         },
        //         year : this.state.year,
        //         section : this.state.section,
        //         periods : attendance
        //     }

        //     return this.props.middleware.state.middlewareAPI.postAttendance(inputData,response => {
        //         console.log(response)
        //         this.setState({
        //             disabled_attendance_create: false 
        //         })
        //         if(!response.code){
        //             this.setState({
        //                 create_attendance_response: (
        //                     <ReactBSAlert
        //                         success
        //                         style={{ display: "block", marginTop: "100px" }}
        //                         title="Success"
        //                         onConfirm={() => this.hideAlert()}
        //                         onCancel={() => this.hideAlert()}
        //                         confirmBtnBsStyle="success"
        //                         confirmBtnText="Ok"
        //                         btnSize=""
        //                     >
        //                         POSTING STUDENTS ATTENDANCE HAS BEEN DONE
        //                     </ReactBSAlert>
        //                 )
        //             })
        //         }else{
        //             this.setState({
        //                 create_attendance_response: (
        //                     <ReactBSAlert
        //                         warning
        //                         style={{ display: "block", marginTop: "100px" }}
        //                         title="Warning"
        //                         onConfirm={() => this.hideAlert()}
        //                         onCancel={() => this.hideAlert()}
        //                         confirmBtnBsStyle="warning"
        //                         confirmBtnText="Try Again"
        //                         btnSize=""
        //                     >
        //                         POSTING STUDENTS ATTENDANCE HAS BEEN FAILED
        //                     </ReactBSAlert>
        //                 )
        //             })
        //         }
        //     })
        // })
    }
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                //submit={this.submit}
                createCourse={this.createCourse}
                createAcademicYear={this.createAcademicYear}
                confirm={this.confirm}
            />
        )        
    }
}

export default Container