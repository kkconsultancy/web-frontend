import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
   // InputGroup,
    Container,
    Row,
    Col,
    //CardFooter,
    //Label
} from "reactstrap";
import Header from '../../Header'
//import ReactDatetime from "react-datetime";
//import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="ACADEMICS COURSE SETUP" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="8">
                  <Card className="bg-secondary shadow">
                    {/* {this.props.create_academics_response} */}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">ACADEMICS COURSE SETUP</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        <h6 className="heading-small text-muted mb-4">
                         Academic Year Details
                        </h6>

                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="6">
                            <Button  color="primary" type="button"  /*disabled={this.props.Year ? false : true}*/  onClick={this.props.createAcademicYear}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Create new Academic Year </span>
                                    </Button> 
                            </Col>
                          </Row>
                        </div>
                        <div /*style={{ display: !this.props.year ? "none" : "" }}*/>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                           Add Academic Course Details 
                          </h6>
                          <div className="pl-lg-4">
                          <Row> 
                            <Col lg="4">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                 Add from Existing Courses
                            </label>
                                <Input type="select" 
                                className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} 
                                name="course" 
                                onChange={this.props.updateState}
                                value={this.props.course}
                                valid={this.props.course_valid}
                                invalid={this.props.course_error}
                                >
                                 {this.props.jsx.coursesListJSX}
                                 <option value="others">Others</option>
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.course_valid !== true ? this.props.course_valid : "Course accepted"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.course_error !== true ? this.props.course_error : "Please select the course!!"}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col lg="3">
                                                <Form>
                                                <label
                                                  className="form-control-label"
                                                  htmlFor="input-username"
                                                >
                                                  Select Semester
                                                </label>
                                                <Input type="select" disabled={this.props.course ? false : true} 
                                                value={this.props.selectSemester}
                                                className={"login-input " + (this.props.selectSemester_valid ? "is-valid" : this.props.selectSemester_error ? "is-invalid" : "")} 
                                                valid={this.props.selectSemester_valid}
                                                invalid={this.props.selectSemester_error}
                                                name="selectSemester"  className={"login-input "} onChange={this.props.updateState} >
                                                <option value="">---</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                </Input>
                                                <div className="valid-feedback">
                                                    {this.props.selectSemester_valid !== true ? this.props.selectSemester_valid : "This is accepted"}
                                                </div>
                                                <div className="invalid-feedback">
                                                  {this.props.selectSemester_error !== true ? this.props.selectSemester_error : "Please select the semester!!"}
                                                </div>
                                                </Form>
                                              </Col>
                                    <Col lg="5">
                          
                                    
                                    <div style={{ display: (this.props.course==="others") ? "" : "none" }}>
                                 
                                    <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Course Type
                                    </label>
                                    <Input
                                    name="newCourseType"
                                    className={"login-input " + (this.props.newCourseType_valid ? "is-valid" : this.props.newCourseType_error ? "is-invalid" : "")}
                                    disabled={this.props.course==="others" ? false : true}
                                    value={this.props.newCourseType}
                                    valid={this.props.newCourseType_valid}
                                    invalid={this.props.newCourseType_error}
                                      onChange={this.props.updateState}
                                      placeholder="Enter new Course Type"
                                      type="text"
                                     />
                                    <div className="valid-feedback">
                                      {this.props.newCourseType_valid !== true ? this.props.newCourseType_valid : "This is accepted"}
                                    </div>
                                    <div className="invalid-feedback">
                                      {this.props.newCourseType_error !== true ? this.props.newCourseType_error : "Please enter the course type!!"}
                                    </div>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Course Name
                                    </label>
                                    <Input
                                    name="newCourseName"
                                    className={"login-input " + (this.props.newCourseName_valid ? "is-valid" : this.props.newCourseName_error ? "is-invalid" : "")}
                                    disabled={this.props.newCourseType ? false : true}
                                    value={this.props.newCourseName}
                                    valid={this.props.newCourseName_valid}
                                    invalid={this.props.newCourseName_error}
                                      onChange={this.props.updateState}
                                      placeholder="Enter new Course Name"
                                      type="text"
                                     />
                                    <div className="valid-feedback">
                                      {this.props.newCourseName_valid !== true ? this.props.newCourseName_valid : "This is accepted"}
                                    </div>
                                    <div className="invalid-feedback">
                                      {this.props.newCourseName_error !== true ? this.props.newCourseName_error : "Please enter the course name!!"}
                                    </div>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Course Code
                                    </label>
                                    
                                    <Input
                                     disabled={this.props.newCourseName ? false : true}
                                     className={"login-input " + (this.props.courseCode_valid ? "is-valid" : this.props.courseCode_error ? "is-invalid" : "")}
                                     value={this.props.newCourseCode}
                                     valid={this.props.newCourseCode_valid}
                                invalid={this.props.newCourseCode_error}
                                    name="newCourseCode"
                                      onChange={this.props.updateState}
                                      placeholder="Enter new Course Code"
                                      type="text"
                                     />
                                    <div className="valid-feedback">
                                      {this.props.newCourseCode_valid !== true ? this.props.newCourseCode_valid : "This is accepted"}
                                    </div>
                                    <div className="invalid-feedback">
                                      {this.props.newCourseCode_error !== true ? this.props.newCourseCode_error : "Please enter the course code!!"}
                                    </div>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Course Duration
                                    </label>
                                    <Input type="text" disabled={this.props.newCourseCode ? false : true} 
                                          value={this.props.newCourseDuration} 
                                          valid={this.props.newCourseDuration_valid}
                                          invalid={this.props.newCourseDuration_error}
                                          name="newCourseDuration"  
                                          className={"login-input " + (this.props.newCourseDuration_valid ? "is-valid" : this.props.newCourseDuration_error ? "is-invalid" : "")}
                                          onChange={this.props.updateState} 
                                    >
                                   
                                  </Input>
                                  <div className="valid-feedback">
                                      {this.props.newCourseDuration_valid !== true ? this.props.newCourseDuration_valid : "This is accepted"}
                                    </div>
                                    <div className="invalid-feedback">
                                      {this.props.newCourseDuration_error !== true ? this.props.newCourseDuration_error : "Please enter the course duration!!"}
                                    </div>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <Button className="btn-icon" color="primary"  
                                          disabled={this.props.newCourseDuration ? false : true} 
                                          type="button" 
                                          onClick={this.props.createCourse}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Create new Course</span>
                                    </Button> 
                                  </Col>
                                  </div>
                                  </Col>
                            </Row>
                          </div>
                          <Button className="btn-icon" color="primary"  
                                          disabled={this.props.course ? false : true} 
                                          type="button" 
                                          onClick={this.props.AddCourse}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Add Course</span>
                                    </Button> 
                        </div>
                        
                      </Form>
                      
                    </CardBody>
                  </Card>
                </Col>

                <Col className="order-xl-1" xl="4">
                <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="6">
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                            <Form>
                              <h4>Hello...</h4>
                              {this.props.jsxa}
                              {this.props.jsx1}
                              
                              <Button className="btn-icon" color="primary"  disabled={this.props.jsxa ? false : true} type="button" 
                                                onClick={this.props.confirm}>
                                                      <span className="btn-inner--icon mr-1">
                                                        <i className="ni ni-bag-17" />
                                                      </span>
                                                      <span className="btn-inner--text">Confirm</span>
                                                  </Button> 

                            </Form>
                          </CardBody>
                </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;