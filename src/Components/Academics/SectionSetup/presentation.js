import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
   // InputGroup,
    Container,
    Row,
    Col,
    //CardFooter,
    //Label
} from "reactstrap";
import Header from '../../Header'
//import ReactDatetime from "react-datetime";
//import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="ACADEMICS SECTION SETUP" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="8">
                  <Card className="bg-secondary shadow">
                    {/* {this.props.create_academics_response} */}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">ACADEMICS SECTION SETUP</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        {/* <h6 className="heading-small text-muted mb-4">
                         Academic Year Details
                        </h6> */}

                        {/* <div className="pl-lg-4">
                          <Row>
                            <Col lg="6">
                            <Button  color="primary" type="button"  disabled={this.props.Year ? false : true}  onClick={this.props.createAcademicYear}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Create new Academic Year </span>
                                    </Button> 
                            </Col>
                          </Row>
                        </div> */}
                        <div /*style={{ display: !this.props.selectSemester? "none" : "" }}*/>
                                    <hr className="my-4" />
                                    <h6 className="heading-small text-muted mb-4">
                                     Add Academic Section Details
                                    </h6>
                                    <div className="pl-lg-4">
                                      <Row>
                                      <Col lg="6">
                                        <Form>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-username"
                                          >
                                            Select Course to add Section
                                          </label>
                                          <Input type="select" value={this.props.selectCourse_section} 
                                                name="selectCourse_section"  
                                                className={"login-input " + (this.props.selectCourse_section_valid ? "is-valid" : this.props.selectCourse_section_error ? "is-invalid" : "")}
                                                 onChange={this.props.updateState}
                                                valid={this.props.selectCourse_section_valid}
                                                invalid={this.props.selectCourse_section_error}
                                                >
                                            {this.props.jsx.coursesListJSX}
                                          </Input>
                                          <div className="valid-feedback">
                                            {this.props.selectCourse_section_valid !== true ? this.props.selectCourse_section_valid : "This is accepted"}
                                          </div>
                                          <div className="invalid-feedback">
                                            {this.props.selectCourse_section_error !== true ? this.props.selectCourse_section_error : "Please select the course!!"}
                                          </div>
                                        </Form>
                                      </Col>
                                      <Col lg="6">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-address"
                                          >
                                            Select Branch to add Section
                                          </label>
                                          <Input type="select" 
                                            name="selectBranch_section"
                                            value={this.props.selectBranch_section}
                                            disabled={this.props.selectCourse_section && !this.props.disabled_academics_create ? false : true }  
                                            className={"login-input " + (this.props.selectBranch_section_valid ? "is-valid" : this.props.selectBranch_section_error ? "is-invalid" : "")} 
                                            onChange={this.props.updateState}
                                            valid={this.props.selectBranch_section_valid}
                                            invalid={this.props.selectBranch_section_error}
                                            >
                                            {/* {this.props.jsx.branchesListJSX} */}
                                            <option value="">---</option>
                                            <option value="cse">CSE</option>
                                            <option value="ece">ECE</option>
                                           
                                          </Input>
                                          <div className="valid-feedback">
                                            {this.props.selectBranch_section_valid !== true ? this.props.selectBranch_section_valid : "This is accepted"}
                                          </div>
                                          <div className="invalid-feedback">
                                            {this.props.selectBranch_section_error !== true ? this.props.selectBranch_section_error : "Please select the branch!!"}
                                          </div>
                                          </FormGroup>
                                          </Col> 
                                          <Col lg="4">
                                                <Form>
                                                <label
                                                  className="form-control-label"
                                                  htmlFor="input-username"
                                                >
                                                  Select Year
                                                </label>
                                                <Input type="select" disabled={this.props.selectBranch_section ? false : true} 
                                                  value={this.props.selectYear_section} name="selectYear_section"  
                                                      className={"login-input " + (this.props.selectYear_section_valid ? "is-valid" : this.props.selectYear_section_error ? "is-invalid" : "")} 
                                                      valid={this.props.selectYear_section_valid}
                                                      invalid={this.props.selectYear_section_error}
                                                      onChange={this.props.updateState} >
                                                {/* {this.props.jsx.yearsListJSX} */}
                                                <option value="">---</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                </Input>
                                                <div className="valid-feedback">
                                                    {this.props.selectYear_section_valid !== true ? this.props.selectYear_section_valid : "This is accepted"}
                                                </div>
                                                <div className="invalid-feedback">
                                                  {this.props.selectYear_section_error !== true ? this.props.selectYear_section_error : "Please select the year!!"}
                                                </div>
                                                </Form>
                                              </Col>
                                              <Col lg="4">
                                                <Form>
                                                <label
                                                  className="form-control-label"
                                                  htmlFor="input-username"
                                                >
                                                  Select Semester
                                                </label>
                                                <Input type="select" disabled={this.props.selectYear_section ? false : true} 
                                                value={this.props.selectSemester_section}
                                                className={"login-input " + (this.props.selectSemester_section_valid ? "is-valid" : this.props.selectSemester_section_error ? "is-invalid" : "")} 
                                                valid={this.props.selectSemester_section_valid}
                                                invalid={this.props.selectSemester_section_error}
                                                name="selectSemester_section"  className={"login-input "} onChange={this.props.updateState} >
                                                <option value="">---</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                </Input>
                                                <div className="valid-feedback">
                                                    {this.props.selectSemester_section_valid !== true ? this.props.selectSemester_section_valid : "This is accepted"}
                                                </div>
                                                <div className="invalid-feedback">
                                                  {this.props.selectSemester_section_error !== true ? this.props.selectSemester_Section_error : "Please select the semester!!"}
                                                </div>
                                                </Form>
                                              </Col> 
                                      
                                              <Col lg="4">
                                                <Form>
                                                <label
                                                  className="form-control-label"
                                                  htmlFor="input-username"
                                                >
                                                 Enter Section
                                                </label>
                                                <Input type="text" disabled={this.props.selectSemester_section ? false : true} 
                                                value={this.props.selectSection} 
                                                valid={this.props.selectSection_valid}
                                                invalid={this.props.selectSection_error}
                                                name="selectSection"  
                                                className={"login-input " + (this.props.selectSection_valid ? "is-valid" : this.props.selectSection_error ? "is-invalid" : "")} 
                                                onChange={this.props.updateState} >
                                                </Input>
                                                <div className="valid-feedback">
                                                    {this.props.selectSection_valid !== true ? this.props.selectSection_valid : "This is accepted"}
                                                </div>
                                                <div className="invalid-feedback">
                                                  {this.props.selectSection_error !== true ? this.props.selectSection_error : "Please select the section!!"}
                                                </div>
                                                </Form>
                                              </Col>
                                              <Col lg="6">
                                                <br/>
                                                <Button className="btn-icon" color="primary"  disabled={this.props.selectSection ? false : true} type="button" 
                                                onClick={this.props.createSection}>
                                                      <span className="btn-inner--icon mr-1">
                                                        <i className="ni ni-bag-17" />
                                                      </span>
                                                      <span className="btn-inner--text">Create new Section</span>
                                                  </Button> 
                                              </Col>
                                            </Row>
                                    </div>
                                    </div> 
                      </Form>
                      
                    </CardBody>
                  </Card>
                </Col>

                <Col className="order-xl-1" xl="4">
                <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="6">
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                            <Form>
                              <h4>Hello..</h4>
                              {this.props.jsx4}
                              
                              <Button className="btn-icon" color="primary"  disabled={this.props.jsx4 ? false : true} type="button" 
                                                onClick={this.props.confirm}>
                                                      <span className="btn-inner--icon mr-1">
                                                        <i className="ni ni-bag-17" />
                                                      </span>
                                                      <span className="btn-inner--text">Confirm</span>
                                                  </Button> 

                            </Form>
                          </CardBody>
                </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;