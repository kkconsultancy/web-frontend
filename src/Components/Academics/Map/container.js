import React,{Component} from 'react';
import Presentation from './presentation';
import uniqueTags from 'react-tagsinput';
// import Axios from 'axios';
// import Script from '../script';
import {
   
    Row,
    Col,
    Button
   
    
} from "reactstrap";


class Container extends Component{
    componentDidMount(){
        this.Add(this.state.Add)
        this.Added(this.state.Added)
    }
    
    state = {
        select: null,
       Add : [ "16K61A05G7", "16K61A05D5", "16K61A05E3", "16K61A05E0","16K61A05H0", "16K61A05G7", "16K61A05D5", "16K61A05E3", "16K61A05E0","16K61A05H0"],
       Added : [ ],
       disabled_academics_map: false,
        
      };
      constructor(props){
        super(props)
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }

    updateState = (e) => {
        //  console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,

                section_error: false,
                section_valid: false,

                branch_error: false,
                branch_valid: false,

                year_error: false,
                year_valid:false,

                semester_error: false,
                semester_valid:false,
                
                academic_yr_valid: false,
                academic_yr_error: false,
        })
        this.validate(e)
    }
    validate =(e)=>{
        console.log("state:"+this.state)
        if(e.target.name=="academic_yr"){
            if(e.target.value){
                return this.setState({
                    academic_yr_valid: true,
                    academic_yr_error: false,
                    course:"",
                    branch: "",
                    year: "",
                    semester: "",
                    section: "",

                })
            }else{
                this.setState({
                   course:"",
                   branch: "",
                    year: "",
                    semester: "",
                    section: "",
                    academic_yr_error: true
                })    
            }
           
        }
        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : ""
                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false,
                    year: "",
                    semester: "",
                    section: "",
                })
            }else{
                this.setState({
                    course_error : true,
                    branch:"",
                    year: "",
                    semester: "",
                    section: "",
                    
                })
            }
            
        }
        if (e.target.name === "branch") {
            if (e.target.value) {

                return this.setState({
                    branch_valid: true,
                    branch_error: false,
                    year: "",
                    semester: "",
                    section: "",
                })
            }else{
                this.setState({
                    year:"",
                    semester: "",
                    section:"",
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false,
                    section:"",
                    semester: "",
                })
            }else{
                this.setState({
                    section:"",
                    semester: "",
                    year_error: true
                })
            }
            
        }
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true,
                    section_error: false,
                })
            }else{
                this.setState({
                    section_error: true,
                })
            }
        }
    }

    Add = (e) =>{
       
        const jsx1=e.sort().map(e =>{
            return(
                <Col xs="4">
                <Button
                    className="btn-O btn-icon-clipboard"
                    key={e}
                    onClick={() => {this.Add1(e)
                }}
                outline
                    type="button"
                >
                    {e}
                    <i className="ni ni-bold-right" />
                </Button>
            </Col>
        )})
        this.setState({
            jsx1:jsx1
        })
    }
    Added = (e) =>{
        
        
        const jsx2=e.sort().map(e =>{
            return(
                
                <Col xs="4"
                >      
            <Button
            
                          className="btn-icon-clipboard"
                         key={e}
                          onClick={() => {this.Added1(e)
                        }}
                          type="button"
                          outline
                          onlyUnique
                        >
                          <i className="ni ni-bold-left" />
                         {e}
            </Button></Col>
        )})
        this.setState({
            jsx2:jsx2
        })
    }
    Add1 =(e) =>{
        
        const Add = this.state.Add.filter(p=> p!=e ).sort()
        const Added=this.state.Added.sort()
        Added.push(e)
        console.log(Add)
        this.setState({
            Add:Add,
            Added:Added
            
        })
        this.Add(Add)
        this.Added(Added)
        
        

    }
    Added1 =(e) =>{
        
        const Added = this.state.Added.filter(p=> p!=e ).sort()
        const Add=this.state.Add.sort()
        Add.push(e)
        console.log(Add)
        this.setState({
            Add:Add,
            Added:Added
            
        })
        this.Added(Added)
        this.Add(Add)
        
        

    }
    handleTagsinput = tagsinput => {
        this.setState({ tagsinput:tagsinput });
      };
   
    
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                handleTagsinput={this.handleTagsinput}
                Add={this.Add}
                createAcadimicYear={this.createAcadimicYear}
                creatBranch={this.creatBranch}
                creatCourse={this.creatCourse}
                
            />
        )        
    }
}

export default Container