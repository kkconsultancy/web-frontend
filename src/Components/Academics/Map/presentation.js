import React from 'react'
// import classnames from "classnames";
// import ReactDatetime from "react-datetime";
// // react plugin that creates an input with badges
// import TagsInput from "react-tagsinput";
// // react plugin used to create DropdownMenu for selecting items
// import Select2 from "react-select2-wrapper";
// // plugin that creates slider
// import Slider from "nouislider";
// // react plugin that creates text editor
// import ReactQuill from "react-quill";
// // javascript plugin that creates nice dropzones for files
// import Dropzone from "dropzone";
// import { Navbar,Nav } from "reactstrap";


import {
    //Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    // CardFooter,
    // Label,
    // InputGroup,
    // InputGroupText,
    // InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";
import Header from '../../Header'


class Presentation extends React.Component{

  state = {
    select: null,
    tagsinput: ["Bucharest", "Cluj", "Iasi", "Timisoara", "Piatra Neamt"],
    
  };
  updateState = (e) => {
    //  console.log(e)
    this.setState({
            [e.target.name] : e.target.value,
            
    })
  }

 

    render(){
        return(
          <>
           <Header middleware={this.props.middleware} pageTitle="MAP ACADEMICS" />
            <Container className="mt--8" fluid>
            
            <Row className="justify-content-center">
              <Col className="order-xl-1" xl="12">
                  <Card className="bg-secondary shadow">
                    <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">ACADEMICS MAPPING</h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                    <Form>
                        <h6 className="heading-small text-muted mb-4">
                          Academic Year & Course Information
                        </h6>
                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="6">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                 Academic Year(20xx-20xx)
                            </label>
                                <Input type="text" 
                                className={"login-input " + (this.props.academic_yr_valid ? "is-valid" : this.props.academic_yr_error ? "is-invalid" : "")} 
                                name="academic_yr" 
                                onChange={this.props.updateState}
                                value={this.props.academic_yr}
                                valid={this.props.academic_yr_valid}
                                invalid={this.props.academic_yr_error}
                                > 
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.academic_yr_valid !== true ? this.props.academic_yr_valid : "Academic Year accepted"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.academic_yr_error !== true ? this.props.academic_yr_error : "Please enter the academic year!!"}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col lg="6">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input type="select" 
                                className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} 
                                name="course" 
                                disabled={this.props.academic_yr && !this.props.disabled_academics_map ? false : true }
                                onChange={this.props.updateState}
                                value={this.props.course}
                                valid={this.props.course_valid}
                                invalid={this.props.course_error}
                                >
                                 {this.props.jsx.coursesListJSX}
          
                                  
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.course_valid !== true ? this.props.course_valid : "Course accepted"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.course_error !== true ? this.props.course_error : "Please select the course!!"}
                                </div>
                              </FormGroup>
                            </Col>
                            </Row>
                        </div>
                        <div style={{ display: !this.props.course? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Branch & Section Information
                          </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input type="select" 
                                  name="branch"
                                  disabled={this.props.course && !this.props.disabled_academics_map ? false : true }  
                                  className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.branch}
                                  valid={this.props.branch_valid}
                                  invalid={this.props.branch_error}
                                  >
                                  {this.props.jsx.branchesListJSX}
                                   
                                
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.branch_valid !== true ? this.props.branch_valid : "Branch accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.branch_error !== true ? this.props.branch_error : "Please select the branch!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select" 
                                  name="year" 
                                  disabled={this.props.branch && !this.props.disabled_academics_map ? false : true} 
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.year}
                                  valid={this.props.year_valid}
                                  invalid={this.props.year_error}
                                  >
                                    {this.props.jsx.yearsListJSX}
                              
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.year_valid : "Year accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.year_error : "Please select the year!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Semester
                              </label>
                                  <Input type="select" 
                                  name="section" 
                                  disabled={this.props.year && !this.props.disabled_academics_map ? false : true}
                                  className={"login-input " + (this.props.semester_valid ? "is-valid" : this.props.semester_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  valid={this.props.semester_valid}
                                  invalid={this.props.semester_error}
                                  >
                                    <option value="">---</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                  
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.semester_valid !== true ? this.props.semester_valid : "Semester accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.semester_error !== true ? this.props.semester_error : "Please select the semester!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Section
                              </label>
                                  <Input type="select" 
                                  name="section" 
                                  disabled={this.props.year && !this.props.disabled_academics_map ? false : true}
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  valid={this.props.section_valid}
                                  invalid={this.props.section_error}
                                  >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.section_valid !== true ? this.props.section_valid : "Section accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.section_error !== true ? this.props.section_error : "Please select the section!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          </div> 
                          
                            
                          
                          
                        </Form>
                      
                    </CardBody>
             </Card>
             
             <Row>
             <Col lg="6">
             <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Add </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                    <Row className="align-items-center">
                        {this.props.jsx1}
                        </Row>
                    </CardBody>

                </Card>
                </Col>
                <Col lg="6">
             <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                      <Row>
                     
                      {this.props.jsx2}
                      
                       </Row>
                    </CardBody>

                </Card>
                </Col>
                </Row>

                </Col>
               
               
                
                
                
                
              </Row>
            </Container>

          </>

























                

        )
    }
}
export default Presentation;