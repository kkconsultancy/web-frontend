import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
//import Loading from '../../Loading'
import { Row,Col } from "reactstrap";
class Container extends Component{
    

    state={
        course : '',
        course_error: false,
        //branch:'',
        disabled_academics_create: false,
        file_meta : {},
        courses:[],
        branches:[],
        yr_sem:[],
        section:[],
        // branchCode:'',
        // branchName:"",
        // branchIntake:"",
        // createBranch:"",
        
        
    }
    constructor(props){
        super(props)

        this.state = {
            ...this.state,
            
        }
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }
    updateState = (e) => {
        // console.log(this.state)
        this.setState({
                [e.target.name] : e.target.value,
            
                course_error : false,
                course_valid : false,

                selectCourse_valid: false,
                selectCourse_error: false,

                selectBranch_error: false,
                selectBranch_valid: false,

                createBranch_error : false,
                createBranch_valid : false,

                branchCode_error : false,
                branchCode_valid : false,

                branchIntake_error : false,
                branchIntake_valid : false,
               
        })
        this.validate(e)
       //console.log("hello"+this.state.branch)
    }
    validate =(e)=>{
        //console.log("hee "+e)
        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                     branch : ""
                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false,
                    selectCourse:"",
                    selectBranch:"",
                    
                })
            }else{
                this.setState({
                    course_error : true,
                   // branch:"",
                //    selectCourse:"",
                //    selectBranch:"",
                //    selectYear:"",
                //    selectSemester:"",
                //    selectSection:"",
                })
            }
            
        }
         if(e.target.name==="selectCourse"){
            // console.log(e.target.value)
             if(e.target.value){
                 return this.setState({
                    
                    selectCourse_valid: true,
                    selectCourse_error: false,
                    
                 })
             }else{
                 this.setState({
                    selectCourse_error: true,
                   
                 })    
             }
         }
        if (e.target.name === "selectBranch") {
            if (e.target.value) {
                console.log("hey--"+e.target.value)
                return this.setState({
                    selectBranch_valid: true,
                    selectBranch_error: false,
                   selectBranch: e.target.value,
                    
                })
            }else{
                this.setState({
                    selectBranch_error: true,
                   
                })    
            }
        }
        if(e.target.name==="createNewBranch"){
            // console.log(e.target.value)
             if(e.target.value){
                 return this.setState({
                    
                    createBranch_valid: true,
                    createBranch_error: false,
                   createBranch: e.target.value,
                    branchCode: "",
                    branchIntake: "",
                 })
             }else{
                 this.setState({
                    branchName_error: true,
                    branchCode: "",
                    branchIntake: "",
 
                 })    
             }
         }
         if(e.target.name==="createNewBranchCode"){
            // console.log(e.target.value)
             if(e.target.value){
                 return this.setState({
                    
                    branchCode_valid: true,
                    branchCode_error: false,
                   branchCode: e.target.value,
                    branchIntake: "",
                 })
             }else{
                 this.setState({
                    branchCode_error: true,
                    branchIntake: "",
 
                 })    
             }
         }
         if(e.target.name==="createNewBranchIntake"){
            // console.log(e.target.value)
             if(e.target.value){
                 return this.setState({
                    
                    branchIntake_valid: true,
                    branchIntake_error: false,
                   branchIntake: e.target.value,
                 })
             }else{
                 this.setState({
                    branchIntake_error: true,
                   
 
                 })    
             }
         }
         console.log(this.state);
        }
    // createAcademicYear = (e) =>{
    //     let course=this.state.courseS
    //     console.log(this.state.courseS)
    //     if(this.state.Year!==""){
    //         course.push(this.state.Year)
    //      }
    //     this.setState({

    //         courseS:course,
    //         Year:""
    //     })
    //     this.view(course,"createAcademicYear")
        
    // }
    
    // createCourse = (e) =>{
    //     //e.preventDefault();
    //     let course=this.state.courses
    //     course.push({
    //         newCourseType:this.state.newCourseType,
    //         newCourseName:this.state.newCourseName,
    //         newCourseCode:this.state.newCourseCode,
    //         newCourseDuration:this.state.newCourseDuration,
    //         //Specilization:this.state.specilization
    //             }

    //         )
        
    //     console.log("course::"+course)
    //     this.setState({

    //         courses:course,
    //         newCourseType:"",
    //         newCourseName:"",
    //         newCourseCode:"",
    //         newCourseDuration:"",
    //     })
    //     this.view(course,"createCourse")
    // }
    createBranch = (e) =>{
        //let y=this.state
       // e.preventDefault();
        let branch=this.state.branches
         branch.push({
                selectCourse:this.state.selectCourse,
               // year:this.state.selectYear,
                //semister:this.state.selectSemister,
                branch:{
                    branchName:this.state.createBranch,
                    branchCode:this.state.branchCode,
                    Intake: this.state.branchIntake,
                  
                }

            })
        this.setState({
            branches:branch,
            selectCourse:"",
            //yeselectYear:"",
            //semister:"",
            branchCode:"",
            createBranch:"",
        })
        this.view(branch,"createBranch")
    }
   
    view = (e,c) =>{
       
        //e.preventDefault();
        if(c==="createAcadimicYear"){
            console.log(e)
            const jsx1=e.map(e=>{
                return(
                    <div>
                        {/* <Row>
                        year:{e}
                        </Row> */}
                    </div>
                )

            })
            this.setState({
                jsx1:jsx1
            })
        }else if(c=="createCourse"){
            console.log("view::"+e)
           const jsx1=e.map(e=>{
                return(
                    <div>
                        <h3>Academics Course Details:</h3>
                        <Row>
                        <Col><b>Course Type:</b> {e.newCourseType}</Col>
                        <Col><b>Course:</b>{e.newCourseName}</Col>
                        </Row>
                        <Row>
                        <Col><b>Code:</b> {e.newCourseCode}</Col>
                        <Col><b>Duration:</b> {e.newCourseDuration}</Col>
                        </Row>
                
                    </div>
                )
            })
            this.setState({
                jsx1:jsx1
            })
        }else if(c==="createBranch"){
            console.log(e)
            const jsx2=e.map(e=>{
                return(
                    <div>
                        <h3>Academics Branch Details:</h3>
                        <Row>
                        <Col><b>Course:</b> {e.selectCourse}</Col>
                        <Col><b>Name:</b> {e.branch.branchName}</Col>
                        </Row>
                        <Row>
                        <Col><b>Code:</b> {e.branch.branchCode}</Col>
                        <Col><b>Intake:</b> {e.branch.Intake}</Col>
                        </Row>

                    </div>
                )
            })
            this.setState({
                jsx2:jsx2
            })

        }else if(c==="createYr_Sem"){
            console.log(e)
            const jsx3=e.map(e=>{
                return(
                    <div>
                        <h3>Academics Year,Semester Details:</h3>
                        <Row>
                        <Col><b>Year: </b> {e.selectYear}</Col>
                        <Col><b>Semester: </b> {e.selectSemester}</Col>
                        </Row>
                        

                    </div>
                )
            })
            this.setState({
                jsx3:jsx3
            })

        }else if(c==="createSection"){
            console.log(e)
            const jsx4=e.map(e=>{
                return(
                    <div>
                        <h3>Academics Section Details:</h3>
                        <Row>
                        <Col><b>Course: </b> {e.selectCourse_section}</Col>
                        <Col><b>Branch: </b> {e.selectBranch_section}</Col>
                        </Row>
                        <Row>
                        <Col><b>Year: </b> {e.selectYear_section}</Col>
                        <Col><b>Semester: </b> {e.selectSemester_section}</Col>
                        <Col><b>Section: </b> {e.selectSection}</Col>
                        </Row>

                    </div>
                )
            })
            this.setState({
                jsx4:jsx4
            })

        }
        
    }
    
    // hideAlert = () => {
    //     this.setState({
    //         create_academics_response : null,
    //         disabled_academics_create: false
    //     });
    // };
    confirm =(e)=>{
        e.preventDefault();
        return(
            <div>Sucessful</div>
        )
    }
    submit = (e) => {
        e.preventDefault();
        
        this.setState({
            disabled_academics_create : true
        })
        console.log(this.state)
        if(!this.state.course || !this.state.section || !this.state.year || !this.state.semester || !this.state.branch || !this.state.file_meta)
            return this.setState({
                disabled_acdemics_create: false  
            })

        //return this.props.middleware.convertCSVToJSON(this.state.file_meta, [this.state.formFields, this.state.formFields] , json => {
        //     // console.log(json)
        //     const attendance = {}
        //     json.map(record => {
        //         for(let i in record){
        //             if(i !== "students"){
        //                 if(!Array.isArray(attendance[i]))
        //                     attendance[i] = []
        //                     attendance[i].push(record["students"])
        //             }
        //         }
        //         return attendance
        //     })
        //     console.log(attendance)
        //     const inputData = {
        //         academicYear : this.props.middleware.state.metadata.year,
        //         course : this.state.course,
        //         branch : this.state.branch,
        //         semester : this.state.semester,
        //         date : {
        //             year : this.state.date.split("/")[2],
        //             month : this.state.date.split("/")[0],
        //             day : this.state.date.split("/")[1]
        //         },
        //         year : this.state.year,
        //         section : this.state.section,
        //         periods : attendance
        //     }

        //     return this.props.middleware.state.middlewareAPI.postAttendance(inputData,response => {
        //         console.log(response)
        //         this.setState({
        //             disabled_attendance_create: false 
        //         })
        //         if(!response.code){
        //             this.setState({
        //                 create_attendance_response: (
        //                     <ReactBSAlert
        //                         success
        //                         style={{ display: "block", marginTop: "100px" }}
        //                         title="Success"
        //                         onConfirm={() => this.hideAlert()}
        //                         onCancel={() => this.hideAlert()}
        //                         confirmBtnBsStyle="success"
        //                         confirmBtnText="Ok"
        //                         btnSize=""
        //                     >
        //                         POSTING STUDENTS ATTENDANCE HAS BEEN DONE
        //                     </ReactBSAlert>
        //                 )
        //             })
        //         }else{
        //             this.setState({
        //                 create_attendance_response: (
        //                     <ReactBSAlert
        //                         warning
        //                         style={{ display: "block", marginTop: "100px" }}
        //                         title="Warning"
        //                         onConfirm={() => this.hideAlert()}
        //                         onCancel={() => this.hideAlert()}
        //                         confirmBtnBsStyle="warning"
        //                         confirmBtnText="Try Again"
        //                         btnSize=""
        //                     >
        //                         POSTING STUDENTS ATTENDANCE HAS BEEN FAILED
        //                     </ReactBSAlert>
        //                 )
        //             })
        //         }
        //     })
        // })
    }
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                //submit={this.submit}
                createCourse={this.createCourse}
                createBranch={this.createBranch}
                createYr_Sem={this.createYr_Sem}
                createSection={this.createSection}
            />
        )        
    }
}

export default Container