import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
   // InputGroup,
    Container,
    Row,
    Col,
    //CardFooter,
    //Label
} from "reactstrap";
import Header from '../../Header'
//import ReactDatetime from "react-datetime";
//import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="ACADEMICS BRANCH SETUP" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="8">
                  <Card className="bg-secondary shadow">
                    {/* {this.props.create_academics_response} */}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">ACADEMICS BRANCH SETUP</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        {/* <h6 className="heading-small text-muted mb-4">
                         Academic Year Details
                        </h6>

                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="6">
                            <Button  color="primary" type="button"  disabled={this.props.Year ? false : true}  onClick={this.props.createAcademicYear}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Create new Academic Year </span>
                                    </Button> 
                            </Col>
                          </Row>
                        </div> */}
                       <div /*style={{ display: !this.props.course ? "none" : "" }}*/>
                                    <h6 className="heading-small text-muted mb-4">
                                     Add Academic Branch Details
                                    </h6>
                                    <div className="pl-lg-4">
                                      <Row>
                                      <Col lg="12">
                                        <Form>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-username"
                                          >
                                            Select Course to add Branch
                                          </label>
                                          <Input type="select" value={this.props.selectCourse} 
                                                name="selectCourse"  
                                                className={"login-input " + (this.props.selectCourse_valid ? "is-valid" : this.props.selectCourse_error ? "is-invalid" : "")}
                                                 onChange={this.props.updateState}
                                                valid={this.props.selectCourse_valid}
                                                invalid={this.props.selectCourse_error}
                                                >
                                            {this.props.jsx.coursesListJSX}
                                          </Input>
                                          <div className="valid-feedback">
                                            {this.props.selectCourse_valid !== true ? this.props.selectCourse_valid : "This is accepted"}
                                          </div>
                                          <div className="invalid-feedback">
                                            {this.props.selectCourse_error !== true ? this.props.selectCourse_error : "Please select the course!!"}
                                          </div>
                                        </Form>
                                      </Col>
                                      <Col lg="6">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-address"
                                          >
                                            Select Branch from Existing
                                          </label>
                                          <Input type="select" 
                                            name="selectBranch"
                                            value={this.props.selectBranch}
                                            disabled={this.props.selectCourse && !this.props.disabled_academics_create ? false : true }  
                                            className={"login-input " + (this.props.selectBranch_valid ? "is-valid" : this.props.selectBranch_error ? "is-invalid" : "")} 
                                            onChange={this.props.updateState}
                                            valid={this.props.selectBranch_valid}
                                            invalid={this.props.selectBranch_error}
                                            >
                                            {/* {this.props.jsx.branchesListJSX} */}
                                            <option value="">---</option>
                                            <option value="cse">CSE</option>
                                            <option value="ece">ECE</option>
                                            <option value="others">Others</option>
                                          </Input>
                                          <div className="valid-feedback">
                                            {this.props.selectBranch_valid !== true ? this.props.selectBranch_valid : "This is accepted"}
                                          </div>
                                          <div className="invalid-feedback">
                                            {this.props.selectBranch_error !== true ? this.props.selectBranch_error : "Please select the branch!!"}
                                          </div>
                                          </FormGroup>
                                          </Col>
                                          <Col lg="6">
                                            {/* <h4>hh-{this.props.selectBranch}</h4> */}
                                            
                                            <div style={{ display: (this.props.selectBranch==="others") ? "" : "none" }}>
                                            <Col lg="6">
                                              <FormGroup>
                                                <label
                                                  className="form-control-label"
                                                  htmlFor="input-username"
                                                >
                                                  Add New Branch
                                                  {this.props.createBranch}
                                                </label>
                                                <Input
                                                  disabled={this.props.selectBranch==="others" ? false : true}
                                                  value={this.props.createBranch} 
                                                  valid={this.props.createBranch_valid}
                                                  invalid={this.props.createBranch_error}
                                                  name="createNewBranch"
                                                  placeholder="Enter new Branch"
                                                  type="text"
                                                  className={"login-input " + (this.props.createBranch_valid ? "is-valid" : this.props.createBranch_error ? "is-invalid" : "")}
                                                  onChange={this.props.updateState}
                                                />
                                                <div className="valid-feedback">
                                                  {this.props.createBranch_valid !== true ? this.props.createBranch_valid : "This is accepted"}
                                                  {this.props.createBranch}
                                                </div>
                                                <div className="invalid-feedback">
                                                  {this.props.createBranch_error !== true ? this.props.createBranch_error : "Please create the branch!!"}
                                                </div>
                                              </FormGroup>
                                            </Col>
                                              <Col lg="6">
                                                <FormGroup>
                                                  <label
                                                    className="form-control-label"
                                                    htmlFor="input-username"
                                                  >
                                                    Add new Branch code
                                                    {this.props.createBranch}
                                                  </label>
                                                  <Input
                                                  disabled={this.props.createBranch ? false : true}
                                                 value={this.props.branchCode}
                                                 valid={this.props.branchCode_valid}
                                                 invalid={this.props.branchCode_error} 
                                                  name="createNewBranchCode"
                                                  onChange={this.props.updateState}
                                                    placeholder="Enter Branch code"
                                                    className={"login-input " + (this.props.branchCode_valid ? "is-valid" : this.props.branchCode_error ? "is-invalid" : "")}
                                                    type="text"
                                                  />
                                                  <div className="valid-feedback">
                                                  {this.props.branchCode_valid !== true ? this.props.branchCode_valid : "This is accepted"}
                                                </div>
                                                <div className="invalid-feedback">
                                                  {this.props.branchCode_error !== true ? this.props.branchCode_error : "Please create the branch code!!"}
                                                </div>
                                                </FormGroup>
                                              </Col>
                                              <Col lg="6">
                                                <FormGroup>
                                                  <label
                                                    className="form-control-label"
                                                    htmlFor="input-username"
                                                  >
                                                    Add new Branch Intake
                                                  </label>
                                                  <Input
                                                  disabled={this.props.branchCode ? false : true}
                                                  value={this.props.branchIntake} 
                                                  valid={this.props.branchIntake_valid}
                                                  invalid={this.props.branchIntake_error}
                                                  name="createNewBranchIntake"
                                                  onChange={this.props.updateState}
                                                    placeholder="Enter Branch Intake"
                                                    className={"login-input " + (this.props.branchIntake_valid ? "is-valid" : this.props.branchIntake_error ? "is-invalid" : "")}
                                                    type="text"
                                                  />
                                                  <div className="valid-feedback">
                                                    {this.props.branchIntake_valid !== true ? this.props.branchIntake_valid : "This is accepted"}
                                                  </div>
                                                  <div className="invalid-feedback">
                                                    {this.props.branchIntake_error !== true ? this.props.branchIntake_error : "Please enter the branch intake!!"}
                                                  </div>
                                                </FormGroup>
                                              </Col>
                                              <Col lg="8">
                                              <br/>
                                              <Button className="btn-icon" color="primary"  
                                              disabled={this.props.branchIntake? false : true} type="button" 
                                              onClick={this.props.createBranch}>
                                                    <span className="btn-inner--icon mr-1">
                                                      <i className="ni ni-bag-17" />
                                                    </span>
                                                    <span className="btn-inner--text">Create new Branch</span>
                                                </Button> 
                                              </Col>
                                              </div>
                                              </Col>
                                              <Button className="btn-icon" color="primary"  
                                              disabled={this.props.selectBranch? false : true} type="button" 
                                              onClick={this.props.createBranch}>
                                                    <span className="btn-inner--icon mr-1">
                                                      <i className="ni ni-bag-17" />
                                                    </span>
                                                    <span className="btn-inner--text">Add Branch</span>
                                                </Button> 
                                      </Row>
                                    </div>
                        </div>
                      </Form>
                      
                    </CardBody>
                  </Card>
                </Col>

                <Col className="order-xl-1" xl="4">
                <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="6">
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                            <Form>
                              <h4>Hello..</h4>
                              {this.props.jsx2}
                              
                              <Button className="btn-icon" color="primary"  disabled={this.props.jsx2 ? false : true} type="button" 
                                onClick={this.props.confirm}>
                                <span className="btn-inner--icon mr-1">
                                  <i className="ni ni-bag-17" />
                                </span>
                                <span className="btn-inner--text">Confirm</span>
                              </Button> 

                            </Form>
                          </CardBody>
                </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;