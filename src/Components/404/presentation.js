import React from 'react'
import './style.css'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Input,
    Row,
    Col
} from "reactstrap";
import {Link} from 'react-router-dom'

class Presentation extends React.Component{

    render(){
        return(
            <Presentation2/>
        )
    }
}
export default Presentation;

class Presentation2 extends React.Component {
    render() {
        return (
            <>
                <Col lg="10" md="12">
                    <Card className="bg-transparent border-0">
                        <CardBody className="px-lg-6 py-lg-6">
                            <div id="notfound">
                                <div class="notfound">
                                    <div class="notfound-404">
                                        <h1>404</h1>
                                        <h2>Page not found</h2>
                                    </div>
                                    <Link href="#">Home</Link>
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            </>
        )
    }
}