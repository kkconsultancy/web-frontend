import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// react plugin used to create datetimepicker
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  ListGroupItem,
  Table,
  ListGroup,
  Container,
  Row,
  Col
} from "reactstrap";

import Header from '../../Header'
// core components
const Presentation = props =>(
  
  
    <>
    
    <Tables {...props} />
    </>
)


class Tables extends React.Component {
  render() {
    return (
      <>
            <Header middleware={this.props.middleware} pageTitle="MODIFY ATTENDANCE" />
            
            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
              <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">UPDATE STUDENTS ATTENDANCE</h3>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Form>
                  <h6 className="heading-small text-muted mb-4">
                          Course information
                    </h6>
                    <div className="pl-lg-4">
                      {console.log(this.props)}
                          <Row>
                            <Col lg="6">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                            
                                <Input type="select" className={"login-input "}  name="course" onChange={this.props.updateState}>
                                  {this.props.loadCourse}
                                </Input>
                                
                              </FormGroup>
                            </Col>
                            <Col md="6">
                            <div style={{ display: !this.props.course  ? "none" : "" }} disabled={this.props.course ? false : true}>
                            
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="exampleDatepicker"
                            >
                              Datepicker
                            </label>
                            <ReactDatetime value={this.props.Date}
                              inputProps={{
                                placeholder: "Date Picker Here"
                              }}
                              onChange={e => this.props.updateState({
                                target : {
                                  name : "Date",
                                  value : e._d
                                }
                              })}
                              
                              timeFormat={false}
                            />
                          </FormGroup>
                        
                        </div>
                        </Col>
                          </Row>
                        </div>

                        <div>{/* <div style={{ display: !(this.props.course ) ? "none" : "" }}> */}
                        
                          <h6 className="heading-small text-muted mb-4">
                            Branch & Section Information (Optional)
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input value={this.props.branch} type="select" name="branch" disabled={this.props.course ? false : true} className={"login-input " } onChange={this.props.updateState} >
                                    {this.props.loadBranch}
                                  </Input>
                                </FormGroup>
                              </Col>
                              <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Section
                              </label>
                                  <Input type="select" value={this.props.section} name="section" disabled={this.props.branch ? false : true} className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>

                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          
                        <div>{/* <div style={{ display: !this.props.section ? "none" : "" }}> */}
                    <h6 className="heading-small text-muted mb-4">
                      Update Table
                    </h6>
                    <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                        {this.props.periodChecks()}
                    </tr>
                  </thead>
                  <tbody className="list">
                    {this.props.stu}
                  </tbody>
                </Table>
                </div>
                </div> 
              </Form>
                </CardBody>
            </Card>
            </Col>
              </Row>
            </Container>
      </>
    );
  }
}

export default Presentation















