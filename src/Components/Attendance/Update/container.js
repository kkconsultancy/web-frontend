import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading';
import {
 
    Media, Table,Col

  } from "reactstrap";

class Container extends Component{
    

    state={
        stu:{},
        atte:{},
        jsx : {},
        course : '',
        disAttence:"true",
        course_error: false,
        loding:"",
        branch:'',
        disabled_attendance_update: false,
        file_meta : {},
        date : new Date().toLocaleDateString(),
        formFields : {"students" : "students","1" : "1","2" : "2","3" : "3","4":"4","5":"5","6":"6","7":"7"},
        regnos: ["16K61A05G7","16K61A05E3","16K61A05E0", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
    }
    constructor(props){
        super(props)
        
        this.state = {
            ...this.state
        }
        //  console.log(this.state.absent,"bye")
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }

    getStudentsList = (data) => {
        this.setState({
            templateFileCSV : <Loading/>,
            disabled_attendance_update : true
        })
        const inputData = {
            academicYear : this.props.middleware.state.metadata.year,
            course : data.name === "course" ? data.value : this.state.course,
            branch : data.name === "branch" ? data.value : this.state.branch,
            year : data.name === "year" ? data.value : this.state.year,
            semester : data.name === "semester" ? data.value : this.state.semester,
            section : data.name === "section" ? data.value : this.state.section
        }
        
        // console.log(inputData)
        if (this.props.middleware.state.metadata){
            this.props.middleware.state.middlewareAPI.listStudents(inputData, (data) => {
                if(!Array.isArray(data))
                    return this.setState({
                        update_attendance_response : (<ReactBSAlert
                        warning
                        style={{ display: "block", marginTop: "100px" }}
                        title="NO DATA FOUND"
                        onConfirm={() => this.hideAlert()}
                        onCancel={() => this.hideAlert()}
                        confirmBtnBsStyle="warning"
                        confirmBtnText="Try Again"
                        btnSize=""
                    >
                        Cannot fetch with the specified values
                    </ReactBSAlert>)
                    })
                const studentsList = data.sort()
                this.setState({
                    studentsList : studentsList
                })
                // console.log(studentsList)
                return this.props.middleware.state.middlewareAPI.viewAttendance({
                    academicYear : this.props.middleware.state.metadata.year,
                    course : this.state.course,
                    branch : this.state.branch,
                    year : this.state.year,
                    semester : this.state.semester,
                    section : this.state.section,
                    month : this.state.date.split("/")[0],
                    day : this.state.date.split("/")[1]
                },response => {
                    this.setState({
                        disabled_attendance_update : false
                    })
                    if(response.code)
                        return this.setState({
                            attendanceList : []
                        })
                    
                    const attendance = response
                    if(attendance.length < 1)
                        this.setState({
                            update_attendance_response : (<ReactBSAlert
                            warning
                            style={{ display: "block", marginTop: "100px" }}
                            title="NO DATA FOUND"
                            onConfirm={() => this.hideAlert()}
                            onCancel={() => this.hideAlert()}
                            confirmBtnBsStyle="warning"
                            confirmBtnText="Try Again"
                            btnSize=""
                        >
                            please make sure attendance has been posted
                        </ReactBSAlert>)
                        })
                    return this.setState({
                        attendanceList : response,
                        jsx : {
                            ...this.state.jsx,
                            attendanceList : attendance.length > 0 ? this.updateStudentsAttendance(studentsList,attendance) : ""
                        }
                    })
                })
            })
        }
    }

    updateStudentsAttendance = (students,attendance) => {
        this.setState({
            loding:"true",
        })
        console.log(attendance,"hi")
       this.setState({
        stu:students,
        atte:attendance

       })
        let dict =[]
        

        attendance.map(record => {
           // let hi=''
            for(let i in record.absent){
                if(!dict[record.absent[i]]){
                    dict[record.absent[i]]=[]
                }
                
                
                dict[record.absent[i]].push(record.metadata.period)
            }
            // console.log(dict)
            return record
            
        })
        
        this.setState({
            ...this.state,
            absent : dict,
           
        })

        const Periods = this.state.periods.map((e) =>{
            return( 
                <th className="sort" scope="col">
                    {e.id}
                </th>
                 
            )
        })
        
        Periods.unshift(
            <th className="sort" scope="col">
                REG.NO/PERIODS
            </th>
        )
        // absent = (period,student) => {
        //     console.log(period,student)
        //     console.log(attendance,this.state.stu)
        //     // console.log(this.state.absent[student])  
        //     // let record = []
        //     // let output = this.state.absent[c]
        //     // if(this.state.absent[c])
        //     //     record = output.filter(period => p.id === period)
        //     // console.log(output,p.id,record)
        //     // if(record.length > 0)
        //     //     output = output.filter(period => p.id !== period)
        //     // else
        //     //     output.push(p.id)
            
        //     // this.setState({
        //     //     absent : {
        //     //         ...this.state.absent,
        //     //         [c] : output
        //     //     }
        //     // })
            
        // }
         const student= students.map(c =>{
             return(
                 <tr>
                       <th scope="row">
                           <Col lg="5">
                         <Media className="align-items-center">
                             {c}
                         </Media></Col>
                       </th>
                       {this.state.periods.map(p => {
                          // let absent = this.state.absent[c] || []
                           
                           let jsx5 = attendance.filter(e=>(e.metadata.period === ""+p.id))
                           if(jsx5.length < 1)
                            jsx5 = [{absent : []}]

                           let jsx2=jsx5[0].absent.filter(e => e === ""+c)
                           //console.log(jsx2) 
                     return(
                         <td>
                             
                         <label className="custom-toggle custom-toggle-default mr-1">
                                
                                   <input key={p.id} defaultChecked={ jsx2.length > 0 ? false : true} type="checkbox"  onChange={
                                       () => {
                                        console.log(attendance,p.id)
                                        let jsx1 = attendance.filter(e => e.metadata.period === ""+p.id)
                                        // console.log(jsx1)
                                        if(!jsx1[0])
                                            jsx1 = [{absent : []}]
                                            
                                        let ab=jsx1[0].absent
                                        let record = []
                                        if(jsx1[0].absent)
                                            record = ab.filter(period => ""+c === period)
                                            
                                        if(record.length > 0){
                                            ab = ab.filter(period => ""+c !== period)
                                            
                                        }
                                        else{
                                           
                                            ab.push(c)
                                        }
                                        // console.log(ab)
                                    jsx1[0].absent = ab
                                    // console.log(jsx1[0].absent)
                                    
                                    let jsx2 = attendance.filter(e => e.metadata.period !== ""+p.id )
                                    
                                    jsx2.push(jsx1[0])
                                    console.log(jsx2)

                                    this.setState({
                                        atte:jsx2
                                    })
                                    // console.log(attendance)
                                    } 
                                        

                                           
                                        }/>
                                   <span
                                       className="custom-toggle-slider rounded-circle"
                                       data-label-on="Yes"
                                       data-label-off="No"
                                   />
                               </label>
                         </td>
                     )}
                )}
                
                      
                  </tr>
                
            
             )
        })
        
        return(
        <Table className="justify-content-center table-flush" responsive>
            <Col md="12">
            <div md="12">
            <thead className="thead-light">
                    <tr>
                        {Periods}
                    </tr>
                  </thead>
                  <tbody className="list">
         
                    {student}
                  </tbody>
                  </div></Col>
        </Table>)
    }


    
    updateState = (e) => {
        //console.log(this.state)
        this.setState({
                [e.target.name] : e.target.value,
                templateFileCSV : "attendance_" + this.props.middleware.state.metadata.year + "_" + (this.state.course || "") + "_" + (this.state.branch || "")+ "_" + (this.state.year || "") + "_" + (this.state.section || "") + "_" + (this.state.date || "") + ".csv",
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_error: false,
                year_valid:false,
                date_error: false,
                date_valid:false,
                csv_file_blob_error : false,
                csv_file_blob_valid : false,
        })
        this.validate(e)
       // console.log("hello"+this.state)
    }
    validate = (e) => {
        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : ""
                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false,
                    branch : "",
                    year : "",
                    semester : "",
                    section : ""
                })
            }else{
                this.setState({
                    course_error : true,
                    branch : "",
                    year : "",
                    semester : "",
                    section : ""
                })
            }
            
        }
        if(e.target.name==="date"){
            // console.log(e.target.value)
            const date = new Date(e.target.value).toLocaleDateString()
            if(e.target.value){
                return this.setState({
                    date : date,
                    branch : "",
                    year : "",
                    semester : "",
                    section : "",
                    date_valid: true,
                    date_error: false
                })
            }else{
                this.setState({
                    branch:"",
                    year:"",
                    section:"",
                    date_error: true
                })    
            }
        }
        if (e.target.name === "branch") {
            if (e.target.value) {

                return this.setState({
                    branch_valid: true,
                    branch_error: false,
                    section :"",
                    year : ""
                })
            }else{
                this.setState({
                    year:"",
                    section:"",
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false,
                    section : ""
                })
            }else{
                this.setState({
                    section : "",
                    year_error: true
                })
            }
            
        }
        
        if (e.target.name === "section") {
            if (e.target.value) {
                this.getStudentsList({name : e.target.name,value:e.target.value})
                return this.setState({
                    section_valid: true,
                    section_error: false,
                    jsx : {
                        ...this.state.jsx,
                        attendanceList : <Loading/>
                    }
                })
                
            }else{
                this.setState({
                    section_error: true,
                })
            }
        }
    }
    hideAlert = () => {
        this.setState({
            create_attendance_response : null,
            update_attendance_response : null,
            disabled_users_create: false,
            disabled_users_update: false,
            disabled_attendance_update : false
        });
    };
    submit = (e) =>{
        e.preventDefault();
        this.setState({
            disabled_attendance_update : true
        })
        let jsx3={}
        console.log(this.state)
        this.state.periods.map((e)=>{
            let jsx2 = this.state.atte.filter(k => k.metadata.period === ""+e.id)
            let jsx1={...jsx3,[e.id] : jsx2[0].absent}
            jsx3 = jsx1
        //    console.log(jsx3,"hi")
            return e
        })
        console.log(jsx3,this.state.atte)
        const inputData = {
                academicYear:this.props.middleware.state.metadata.year,
                course : this.state.atte[0].metadata.course,
                branch : this.state.atte[0].metadata.branch,
                year : this.state.atte[0].metadata.year,
                semester : this.state.atte[0].metadata.semester,
                date:this.state.atte[0].metadata.date,
                section : this.state.atte[0].metadata.section,
                periods:jsx3


            }
            console.log(inputData)
            // this.setState({
            //     outputData:outputData,
            //     disAttence:'',

            // })
            return this.props.middleware.state.middlewareAPI.postAttendance(inputData,response => {
                console.log(response)
                this.setState({
                    disabled_attendance_create: false 
                })
                if(!response.code){
                    this.setState({
                        update_attendance_response: (
                            <ReactBSAlert
                                success
                                style={{ display: "block", marginTop: "100px" }}
                                title="Success"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="success"
                                confirmBtnText="Ok"
                                btnSize=""
                            >
                                POSTING STUDENTS ATTENDANCE HAS BEEN DONE
                            </ReactBSAlert>
                        )
                    })
                }else{
                    this.setState({
                        update_attendance_response: (
                            <ReactBSAlert
                                warning
                                style={{ display: "block", marginTop: "100px" }}
                                title="Warning"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="warning"
                                confirmBtnText="Try Again"
                                btnSize=""
                            >
                                POSTING STUDENTS ATTENDANCE HAS BEEN FAILED
                            </ReactBSAlert>
                        )
                    })
                }
            })
        
        
            
                
            

        
        

    }
    
    
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                submit={this.submit}
                studsdata={this.studsdata}
                periodChecks={this.periodChecks}
            />
        )        
    }
}

export default Container