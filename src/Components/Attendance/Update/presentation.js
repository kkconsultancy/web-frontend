import React from 'react'
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  // InputGroupAddon,
  // InputGroupText,
  // InputGroup,
  // ListGroupItem,
  // Table,
  // ListGroup,
  Container,
  Row,
  Col
} from "reactstrap";
import Header from '../../Header'
import ReactDatetime from "react-datetime";
import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="ADD attendance" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
                  <Card className="bg-secondary shadow">
                    {this.props.create_attendance_response}
                    {this.props.update_attendance_response}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">UPDATE ATTENDANCE</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        <h6 className="heading-small text-muted mb-4">
                          Course Information
                        </h6>

                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="6">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input type="select" 
                                className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} 
                                name="course" 
                                onChange={this.props.updateState}
                                value={this.props.course}
                                disabled={!this.props.disabled_attendance_update ? false : true} 
                                valid={this.props.course_valid}
                                invalid={this.props.course_error}
                                >
                                 {this.props.jsx.coursesListJSX}
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.course_valid !== true ? this.props.course_valid : "Course accepted"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.course_error !== true ? this.props.course_error : "Please select the course!!"}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col md="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Date
                                  </label>
                                  <ReactDatetime
                                    inputProps={{
                                      placeholder: "Select the date",
                                    }}
                                    defaultValue={new Date()}
                                    timeFormat={false}
                                    disabled={!this.props.disabled_attendance_update ? false : true} 
                                    onChange={e => this.props.updateState({target : {name : "date",value:e._d}})}
                                  />
                                </FormGroup>
                                
                              </Col>
                          </Row>
                        </div>
                        <div style={{ display: !this.props.course ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Branch & Section Information
                          </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="12">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input type="select" 
                                  name="branch"
                                  value={this.props.branch}
                                  disabled={this.props.course && !this.props.disabled_attendance_update ? false : true }  
                                  className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  valid={this.props.branch_valid}
                                  invalid={this.props.branch_error}
                                  >
                                    {this.props.jsx.branchesListJSX}
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.branch_valid !== true ? this.props.branch_valid : "Branch accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.branch_error !== true ? this.props.branch_error : "Please select the branch!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="4">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select" 
                                  name="year" 
                                  value={this.props.year}
                                  disabled={this.props.branch && !this.props.disabled_attendance_update ? false : true} 
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  valid={this.props.year_valid}
                                  invalid={this.props.year_error}
                                  >
                                    {this.props.jsx.yearsListJSX}
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.year_valid : "Year accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.year_error : "Please select the year!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="4">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Semester
                              </label>
                                  <Input type="select" 
                                  name="semester" 
                                  value={this.props.semester}
                                  disabled={this.props.year && !this.props.disabled_attendance_update ? false : true} 
                                  className={"login-input " + (this.props.semester_valid ? "is-valid" : this.props.semester_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  valid={this.props.semester_valid}
                                  invalid={this.props.semester_error}
                                  >
                                    <option>---</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.semester_valid : "Semester accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.semester_error : "Please select the semester!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              
                              <Col md="4">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Section
                              </label>
                                  <Input type="select" 
                                  name="section" 
                                  value={this.props.section}
                                  disabled={this.props.semester && !this.props.disabled_attendance_update ? false : true}
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  valid={this.props.section_valid}
                                  invalid={this.props.section_error}
                                  >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.section_valid !== true ? this.props.section_valid : "Section accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.section_error !== true ? this.props.section_error : "Please select the section!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          </div> 
                          <hr className="my-4" />
                          <div md="12" style={{ display: !this.props.section ? "none" : "" }}> 
                          <div style={{ display: !this.props.disAttence ? "none" : "" }}>
                    <h6 className="heading-small text-muted mb-4">
                      ATTENDANCE
                    </h6>
                    
                    {this.props.jsx.attendanceList}
                    </div>
                    <hr className="my-4" />
                          <Row>
                            <Col lg="4">
                              <div className="text-center ">

                                {() => console.log(this.props)}
                                <Button
                                 
                                  type="submit" color="warning" outline 
                                  className="my-4" 
                                  disabled={this.props.course && this.props.branch && this.props.section && this.props.semester && this.props.year && !this.props.disabled_attendance_update ? false : true}
                                  // disabled={this.props.loding ? false :true}
                                  onClick = {this.props.submit}
                                >
                                  UPLOAD
                                  {this.props.disabled_attendance_update ? <Loading/> : ""}
                              </Button>
                              </div>
                            </Col>
                          </Row>
                    
                </div>
                      </Form>
                      
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;