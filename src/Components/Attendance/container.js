import React from 'react'
import Presentation from './presentation'

import {Link} from 'react-router-dom'

import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Card,
    CardBody,
    CardTitle,
    Row, Col
  } from "reactstrap";

class Container extends React.Component{
    state = {}

    constructor(props){
        super(props)

        this.state = {
            ...this.state,
            links : this.props.links || []
        }
    }

    componentWillMount(){
        this.createDashboard()
    }

    createDashboard = () => {
        const cards = this.state.links.map(link => this.renderCard(link))

        this.setState({
            dashboard : cards
        })
    }

    renderCard = (data) => {
        return(
            <Col sm="6" md="4" xl="3">
                <Card className="card-stats">
                    <CardBody>
                    <Link to={data.path} activeClassName="active">
                        <Row>
                            <div className="col">
                                <CardTitle
                                    tag="h3"
                                    className="text-uppercase text-muted mb-0"
                                >
                                    {data.name}
                                </CardTitle>
                                <span className="h2 font-weight-bold mb-0">_____________</span>
                            </div>
                            <Col className="col-auto">
                                <div className="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                    <i className={"ni "+data.icon} />
                                </div>
                            </Col>  
                        </Row>
                        <p className="mt-3 mb-0 text-sm">
                            <span className="text-success mr-2">
                                {"---"} 
                            </span>
                            <span className="text-success mr-1">{"---"}</span>
                        </p>
                    </Link>
                    </CardBody>
                </Card>
            </Col>
        )
    }

    render() {
       console.log(this.props)
        return (
            <Presentation
            {...this.props}
            {...this.state}
            
            />
        )
    }
}


export default Container