import React,{Component} from 'react'
import Presentation from './presentation';
import {
 
    Media,
 } from "reactstrap";
  
 class Container extends Component{
    script = null
    constructor(props){
        super(props)
        const attendance =
        [
            {
                absent : ["16K61A05G7","16K61A05E3","16K61A05D5","16K61A05E0"],
                metadata : {
                    subject : {
                        "name" : "ABCD",
                        code : "abcd"
                    },
                    period : 1,
                    year : 4,
                    semester : 2,
                    course : "btech",
                    branch : "cse",
                    date : {
                        day : "5",
                        month : 5,
                        year : 2020
                    },
                    section : "C",
                    timestamp : "aaa"
                }
            },{
                absent : ["16K61A05G7","16K61A05E3","16K61A05D5","16K61A05E0"],
                metadata : {
                    subject : {
                        "name" : "ABCD",
                        code : "abcd"
                    },
                    period : 2,
                    year : 4,
                    semester : 2,
                    course : "btech",
                    branch : "cse",
                    date : {
                        day : "5",
                        month : 5,
                        year : 2020
                    },
                    section : "C",
                    timestamp : "aaa"
                }
            }
        ]
    const regnos = ["16K61A05G7","16K61A05E3","16K61A05E0", "16K61A0559", "16K61A05A3","16K61A05D5"]
    let dict = []
    const jsx = attendance.map(record => {
        for(let i in record.absent){
            if(!dict[record.absent[i]]){
                dict[record.absent[i]] = []
            }
            dict[record.absent[i]].push(record.metadata.period)
        }
    })
    this.state = {
        ...this.state,
        absent : dict,
        attendance:attendance
    }
        
    }
    componentWillMount(){
        this.script = this.props.script
        this.loadCourses()
        this.studsdata()
    }
    
    state={
        courseS : ["","BTECH","MTECH","MBA","DIPLOMA"],
        branches :["","cse","it"],
        isDisabled : true,
        regnos: ["16K61A05G7","16K61A05E3","16K61A05E0", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        disabledPeriods : '',
        course : '',
        branch:'',
        section:'',
        Date:'',
        attendance:'',
    }
    loadCourses =() =>{ const loadCourses = this.state.courseS.map(c => {
        
        return (
            <option value={c} key={c}>{c}</option>
        )
        
        })
        this.setState({
            loadCourse : loadCourses
        })
    }
    

    load = () => {const loadBranch = this.state.branches.map(c => {
        // this.Validate(e)
        console.log(this.state)
        return (
            <option value={c} key={c}>{c}</option>
        ) 
    })
    this.setState({
        loadBranch : loadBranch
    })}
   
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_valid:false,
                year_error:false,
                date_valid: false
               
        })
        this.validate(e)
        console.log(this.state)
    }
    validate = e => {
        if(e.target.name === "course"){
            if(e.target.value){
                
                return this.setState({
                    course_valid : true
                })

            }
            else{
                this.setState({
                    course_error : "Incorrect course selected"
                })
            }
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true
                })
            }else{
                return this.setState({
                    year : ""
                })
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true
                })
            }
            else{
                return this.setState({
                    section : ""
                })
            }
        }
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true
                })
            }
        }
        if(e.target.name==="date"){
            
            console.log("heyy");
             return this.setState({
                 date_valid: true
             })
            
         }
    }
    periodChecks = () => {
       
        const pc = this.state.periods.map(p => {
            return(
                <th className="sort" scope="col">
                    {p.id}{p.name}
                </th>
            )
        })
        pc.unshift(
            <th className="sort" scope="col">
                REG.NO/PERIODS
            </th>
        )
        return pc
    }
    absent = (p,c) => {
        let record = []
        let output = this.state.absent[c]
        if(this.state.absent[c])
            record = output.filter(period => p.id === period)
        console.log(output,p.id,record)
        if(record.length > 0)
            output = output.filter(period => p.id !== period)
        else
            output.push(p.id)
        
        this.setState({
            absent : {
                ...this.state.absent,
                [c] : output
            }
        })
        
    } 
    
    studsdata = () => {
        
        const stu = this.state.regnos.map(c=>{
        console.log(c)
            return(
                <tr>
                      <th scope="row">
                        <Media className="align-items-center">
                            {c}
                        </Media>
                      </th>
                      {this.state.periods.map(p => {
                           let absent = this.state.absent[c] || []
                           console.log(absent,p,c)
                    return(
                        <td>
                        <label className="custom-toggle custom-toggle-danger mr-1">
                               <input key={p.id+c} defaultChecked={absent[p.id-1] ? true : false} type="checkbox"  onChange={
                                      () => this.absent(p,c) }/>
                                  <span
                                      className="custom-toggle-slider rounded-circle"
                                      data-label-off="No"
                                      data-label-on="Yes"
                                  />
                              </label>
                        </td>
                    )}
                )}
                
                      
                 </tr>
                
            
            )
        })
        return stu;
    }
    
    render(){
        
        return(
            <Presentation
                {...this.props}
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                studsdata={this.studsdata}
                periodChecks={this.periodChecks}
                loadCourses={this.loadCourses}
                loadNewData={this.loadNewData}
                updateState={this.updateState}
                
            />
        )        
    }
}

export default Container
