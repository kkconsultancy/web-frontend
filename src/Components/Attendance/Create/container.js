import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'

class Container extends Component{
    

    state={
        course : '',
        course_error: false,
        branch:'',
        disabled_attendance_create: false,
        file_meta : {},
        date : new Date().toLocaleDateString(),
        formFields : {"students" : "students","1" : "1","2" : "2","3" : "3","4":"4","5":"5","6":"6","7":"7"}
    }
    constructor(props){
        super(props)

        this.state = {
            ...this.state,
            templateFileCSV : "attendance_" + this.props.middleware.state.metadata.year + "_" + (this.state.course || "") + "_" + (this.state.branch || "")+ "_" + (this.state.year || "") + "_" + (this.state.section || "") + "_" + (this.state.date || "") + ".csv",
        }
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }

    createCSV = (data) => {
        this.setState({
            templateFileCSV : <Loading/>
        })
        const inputData = {
            academicYear : this.props.middleware.state.metadata.year,
            course : data.name === "course" ? data.value : this.state.course,
            branch : data.name === "branch" ? data.value : this.state.branch,
            year : data.name === "year" ? data.value : this.state.year,
            semester : data.name === "semester" ? data.value : this.state.semester,
            section : data.name === "section" ? data.value : this.state.section
        }
        if (this.props.middleware.state.metadata)
            return this.props.middleware.state.middlewareAPI.listStudents(inputData, (data) => {
                const csvData = [["students","1","2","3","4","5","6","7"].join(",")]
                if(!data.code){
                    data = data.sort()
                    csvData.push(...data)
                    // console.log(dict1)
                    // console.log(dict2)
                    const header = "data:text/csv;charset=utf-8," + (csvData.join("\n")) + "\n"
                    const encodedUri = encodeURI(header);
                    return this.setState({
                        templateFileCSV : "attendance_" + inputData.academicYear + "_" + (inputData.course || "") + "_" + (inputData.branch || "")+ "_" + (inputData.year || "") + "_" + (inputData.section || "") + "_" + (inputData.date || "") + ".csv",
                        encodedUri: encodedUri
                    })
                }else
                this.setState({
                    create_attendance_response: (
                        <ReactBSAlert
                            warning
                            style={{ display: "block", marginTop: "100px" }}
                            title="Warning"
                            onConfirm={() => this.hideAlert()}
                            onCancel={() => this.hideAlert()}
                            confirmBtnBsStyle="warning"
                            confirmBtnText="Try Again"
                            btnSize=""
                        >
                            NO DATA FOUND
                        </ReactBSAlert>
                    )
                })
            })
    }
    updateState = (e) => {
        // console.log(this.state)
        this.setState({
                [e.target.name] : e.target.value,
                templateFileCSV : "attendance_" + this.props.middleware.state.metadata.year + "_" + (this.state.course || "") + "_" + (this.state.branch || "")+ "_" + (this.state.year || "") + "_" + (this.state.section || "") + "_" + (this.state.date || "") + ".csv",
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                year_error: false,
                year_valid:false,
                date_error: false,
                date_valid:false,
                csv_file_blob_error : false,
                csv_file_blob_valid : false,
        })
        this.validate(e)
       // console.log("hello"+this.state)
    }
    validate =(e)=>{
        //console.log("hee "+e)
        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : ""
                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false
                })
            }else{
                this.setState({
                    course_error : true,
                    
                })
            }
            
        }
        if(e.target.name==="date"){
            console.log(e.target.value)
            const date = new Date(e.target.value).toLocaleDateString()
            if(e.target.value){
                return this.setState({
                    date : date,
                    date_valid: true,
                    date_error: false
                })
            }else{
                this.setState({
                    branch:"",
                    year:"",
                    section:"",
                    date_error: true
                })    
            }
        }
        if (e.target.name === "branch") {
            if (e.target.value) {

                return this.setState({
                    branch_valid: true,
                    branch_error: false
                })
            }else{
                this.setState({
                    year:"",
                    section:"",
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false
                })
            }else{
                this.setState({
                    section : "",
                    year_error: true
                })
            }
            
        }
        //console.log("hiiii->"+this.state.year)
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true,
                    section_error: false
                })
            }else{
                this.setState({
                    section_error: true,
                })
            }
        }
        
        if (e.target.name === "csv_file_blob") {
            if (e.target.value) {
                const file = e.target.files[0]
                // console.log(file)
                if(file.name.split(".").splice(-1)[0] !== "csv"){
                    // console.log(file.name.split(".").splice(-1))
                    return this.setState({
                        csv_file_blob_error: "Incorrect file uploaded",
                        file_meta : "",
                        csv_file_blob : ""
                    })

                }
                return this.setState({
                    csv_file_blob_valid: true,
                    file_meta: file,
                    csv_file_blob : e.target.files[0]
                })
            } 
            return this.setState({
                csv_file_blob_error: true
            })
        }else
            this.createCSV(e.target)
    
    }
    hideAlert = () => {
        this.setState({
            create_attendance_response : null,
            disabled_users_create: false
        });
    };
    
    submit = (e) => {
        e.preventDefault();
        
        this.setState({
            disabled_attendance_create : true
        })
        console.log(this.state)
        if(!this.state.course || !this.state.section || !this.state.year || !this.state.semester || !this.state.branch || !this.state.file_meta)
            return this.setState({
                disabled_attendance_create: false  
            })

        return this.props.middleware.convertCSVToJSON(this.state.file_meta, [this.state.formFields, this.state.formFields] , json => {
            // console.log(json)
            const attendance = {}
            json.map(record => {
                for(let i in record){
                    if(i !== "students"){
                        if(!Array.isArray(attendance[i]))
                            attendance[i] = []
                            attendance[i].push(record["students"])
                    }
                }
                return attendance
            })
            console.log(attendance)
            const inputData = {
                academicYear : this.props.middleware.state.metadata.year,
                course : this.state.course,
                branch : this.state.branch,
                semester : this.state.semester,
                date : {
                    year : this.state.date.split("/")[2],
                    month : this.state.date.split("/")[0],
                    day : this.state.date.split("/")[1]
                },
                year : this.state.year,
                section : this.state.section,
                periods : attendance
            }

            return this.props.middleware.state.middlewareAPI.postAttendance(inputData,response => {
                console.log(response)
                this.setState({
                    disabled_attendance_create: false 
                })
                if(!response.code){
                    this.setState({
                        create_attendance_response: (
                            <ReactBSAlert
                                success
                                style={{ display: "block", marginTop: "100px" }}
                                title="Success"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="success"
                                confirmBtnText="Ok"
                                btnSize=""
                            >
                                POSTING STUDENTS ATTENDANCE HAS BEEN DONE
                            </ReactBSAlert>
                        )
                    })
                }else{
                    this.setState({
                        create_attendance_response: (
                            <ReactBSAlert
                                warning
                                style={{ display: "block", marginTop: "100px" }}
                                title="Warning"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="warning"
                                confirmBtnText="Try Again"
                                btnSize=""
                            >
                                POSTING STUDENTS ATTENDANCE HAS BEEN FAILED
                            </ReactBSAlert>
                        )
                    })
                }
            })
        })
    }
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                submit={this.submit}
            />
        )        
    }
}

export default Container