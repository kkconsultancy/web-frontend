import React,{Component} from 'react'
import Presentation from './presentation'

class Container extends Component {
    constructor(props){
        super(props);
    this.state={
        opwd :"",
        npwd :""
    }
    this.submit=this.submit.bind(this);
}
    handle =(e)=>{
        var name=e.target.name;
        var value=e.target.value;
        console.log(name);
        this.setState({
            opwd: value
        })
        console.log("oldpwd->"+this.state.opwd)
        this.validate();
    }  
    state={}
    handle1 =(e)=>{
        var name=e.target.name;
        var value=e.target.value;
        console.log(name);
        
        this.setState({
            npwd: value,
            opwd: this.state.npwd
        
        })
        console.log("newpwd->"+this.state.npwd)
        this.validatepaswd();
    }
    
    validate =()=>{
        var value = document.f1.oldpassword.value;
        var pattern=/^[A-Z|a-z|0-9].{4,10}$/;
        if(value.match(pattern))
        {
                console.log("valid");
        }
        else{
            console.log("please enter a valid password");
        }
    }
    validatepaswd =()=>{
        var value = document.f1.newpassword.value;
        var pattern=/^[A-Z|a-z|0-9]*$/;
        if(value.match(pattern))
        {
                console.log("valid");
        }
        else{
            console.log("please enter a valid password");
        }
    }
   
    submit=()=>{
        /*console.log("hello"+e.target.value)
        var name=e.target.name;
        var value=e.target.value;
        console.log(value)*/
        let value= document.f1.newpassword.value;
        console.log("npwd->"+value)
        this.setState({
            opwd: this.state.npwd
        })
        console.log("changepwd-->"+this.state.npwd)
    }
    onButtonClickHandler = () => {
        
        window.alert('password has been changed')
      };
     
    render(){
        return(
           
            <Presentation
                {...this.props}
                {...this.state}
                handle={this.handle}
                handle1={this.handle1}
                submit={this.submit}
                onButtonClickHandler={this.onButtonClickHandler}
              

            />
        )
    }
}


export default Container;