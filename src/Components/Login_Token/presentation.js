import React from 'react'

import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Input,
    Row,
    Col
} from "reactstrap";

class Presentation extends React.Component{
    render(){
        return (
            <>
                <Col lg="5" md="7">
                    <Card className="bg-secondary shadow border-0">

                        <CardBody className="px-lg-5 py-lg-5">
                            <div className="text-center text-muted mb-4">
                                <small>Sign in with Token</small>
                            </div>
                            <Form role="form" onSubmit={this.props.submit}>
                                <FormGroup className={this.props.token_valid ? "has-success" : this.props.token_error ? "has-danger" : ""}>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input 
                                            placeholder="********" 
                                            className={"login-input " + (this.props.token_valid ? "is-valid" : this.props.token_error ? "is-invalid" : "")} 
                                            valid={this.props.token_valid}
                                            invalid={this.props.token_error}
                                            type="password" 
                                            name="token" 
                                            disabled={this.props.isDisabled}
                                            onChange={this.props.input} 
                                        />
                                        <div className="valid-feedback">
                                            {this.props.token_valid !== true ? this.props.token_valid : "Good to go"}
                                        </div>
                                        <div className="invalid-feedback">
                                            {this.props.token_error !== true ? this.props.token_error : "Something is wrong with your token"}
                                        </div>
                                    </InputGroup>
                                </FormGroup>
                                <div className="text-center">

                                    {/* <Input type="submit"   className="login-submit" /> */}
                                    <Button 
                                        disabled={this.props.token && !this.props.isDisabled ? false : true} 
                                        className="my-4" 
                                        name="LOGIN" 
                                        color="primary" 
                                        onClick={this.props.submit} 
                                        type="submit">
                                        Sign in
                                        {this.props.isDisabled}
                                    </Button>
                                </div>
                            </Form>
                        </CardBody>
                    </Card>
                    <Row className="mt-3">
                        <Col>
                            <a
                                className="text-light"
                                href="/app/login/"
                            >
                                <small>Login with Username and Password?</small>
                            </a>
                        </Col>
                        {/* <Col className="text-right" xs="6">
                            <a
                                className="text-light"
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                <small>Create new account</small>
                            </a>
                        </Col> */}
                    </Row>
                </Col>
            </>
        )
    }
}

export default Presentation