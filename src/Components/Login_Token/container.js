import React from 'react'
import Presentation from './presentation'
import Loading from '../../Components/Loading'

class Container extends React.Component{
    state = {
        isDisabled: false
    }

    validate = () => {
        let value = this.state.token

        if (!value) {
            this.setState({
                token_error: "INVALID TOKEN"
            })
            return false
        }

        this.setState({
            token_valid : true
        })
    
        return true
    }

    input = (e) => { 
        this.setState({
            [e.target.name] : e.target.value,
            token_error: false,
            token_valid: false
        })
    }
    submit = (e) => {
        e.preventDefault();
        this.setState({
            isDisabled: <Loading />
        })
        if (!this.validate())
            return this.setState({
                isDisabled : false
            })
        const data = {
            token : this.state.token
        }
        this.props.middleware.customTokenLogin(data,(res) => {
            if (res.code) {
                return this.setState({
                    token_error: res.message || "TOKEN VERIFICATION FAILED",
                    isDisabled: false
                })
            }
        })
    }

    render() {
        // console.log(this.props)
        return (
            <Presentation
                {...this.props}
                {...this.state}
                submit={this.submit}
                input={this.input}
            />
        )
    }
}

export default Container