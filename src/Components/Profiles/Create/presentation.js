import React from 'react'
import Input from '../../Input'
import './style.css'

class Presentation extends React.Component{

    render(){
        console.log(this.props)
        return(
            <div className="createusers-main">
                <h2>Create Users Component</h2>
                <label>Course: </label>&nbsp;
                
                <select name="course" value={this.props.course} onChange={this.props.handleEvent}>
                    {this.props.jsx.coursesListJSX}
                    {/* <option value="Default">---Select---</option>
                    <option value="B.Tech">B.Tech</option>
                    <option value="Diploma">Diploma</option>
                    <option value="MBA">MBA</option> */}
                </select>
                <div className="error">{this.props.error1}</div>
                <br/>
                <label>Branch: </label>
                <select name="branch" value={this.props.branch} onChange={this.props.handleEvent}>
                    <option value="Default">---Select---</option>
                    <option value="CSE">CSE</option>
                    <option value="ECE">ECE</option>
                    <option value="EEE">EEE</option>
                    <option value="ME">ME</option>
                    <option value="CE">CE</option>
                </select>
                <div className="error">{this.props.error2}</div>
                <br/>
                <label>Year: </label>
                <select  name="year" value={this.props.year} onChange={this.props.handleEvent}>
                    <option value="Default">---Select---</option>
                    <option value="I">I</option>
                    <option value="II">II</option>
                    <option value="III">III</option>
                    <option value="IV">IV</option>
                </select>
                <div className="error">{this.props.error3}</div>
                <br/>
                <label>Template File: </label>
                <a href={this.props.url} name="sample_file" id="SampleDownload" onClick={this.props.sampleDwnld}>Sample File</a>
                <br/>
                <label>Attach File Link:</label>
                <Input type="text" name="csv_file" value={this.props.link} onChange={this.props.handleEvent} className="createusers-input"/>
                
                <Input type="submit" name="submit" value="SUBMIT" onClick={this.props.submit} className="createusers-submit"/>
            </div>
        )
    }
}
export default Presentation;