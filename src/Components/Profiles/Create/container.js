import React from 'react'
import Presentation from './presentation'
import './style.css'

class Container extends React.Component{
    
    state={
        course:'',
        year:'',
        branch:'',
        Submit:"no",
        link:'',
        url:'https://firebasestorage.googleapis.com/v0/b/site-org-automation.appspot.com/o/test.csv?alt=media&token=4fe45dee-3b91-4b70-bfb6-b6638fb9a3b3',
        error1:'',
        error2:'',
        error3:'',
        responseponse:'',
        jsx : ""
    }

    constructor(props){
        super(props)

        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }
    
    handleEvent = (e)=>{
       // console.log("helloo")
        const name=e.target.name;
        const value=e.target.value;
        this.setState(
            {
                [name]:value
            }
        )
        console.log(this.state)
        this.validate(e)
        
    }
    
    validate=(e)=>{
        console.log("hiii")
        let err= this.state.error1
        //console.log("err:"+error)
        let name=e.target.name;
        //console.log("errn1:"+name)
        let value=e.target.value;
        if(name==='course'){
            console.log("course validation:"+value)
            if(value==="Default"){
                err= "*Please select any course"
                this.setState({
                    error1: err
                })
               // console.log("err:"+err)
            }
            else{
                this.setState({
                    error1:''
                })
               // console.log("valid--"+value);
            }
                
        }
        else if(name==='branch'){
            console.log("branch validation")
            if(value==="Default"){
                err= "*Please select any branch"
                this.setState({
                    error2:err
                })
                //console.log(this.state)
            }
            else{
                this.setState({
                    error2:''
                })
                //console.log("valid--"+value);
            }
        }
        else if(name==="year"){
                console.log("year validation")
                if(value==="Default"){
                    err= "*Please select any year"
                    this.setState({
                        error3:err
                    })
                    console.log(this.state)
                }
                else{
                    this.setState({
                        error3:''
                    })
                   // console.log("valid--"+value);
                }
        }else{
            if(name==="csv_file"){
                const value=e.target.value;
                console.log("CSV--"+value)
               this.convertToJSON(e);

            }
        }
    }
    convertToJSON=(e)=>{
        const csv2json = (require('csvtojson'))()
        //const url='https://firebasestorage.googleapis.com/v0/b/site-org-automation.appspot.com/o/test.csv?alt=media&token=4fe45dee-3b91-4b70-bfb6-b6638fb9a3b3'
        const csvFile = e.target.value;
        fetch(csvFile,{

        })
        .then(response=>response.text())
        .then(response=>{ 
            console.log("CSV FILE:\n"+response)
            // []
            const headers = response.split("\n")[0].split(",")
            console.log(headers)
            
            csv2json.fromString(response,{})
            .then(response=>{
                console.log("JSON:\n"+typeof(response))
                console.log(response)         //JSON
                this.op = response.map(response=>{
                   // response= JSON.parse(response)
                    //console.log(typeof(response))
                   return this.display(response,headers)
                })
            const headJSX = headers.map(value => <th>{value}</th>)
            const data = [(<tr>{headJSX}</tr>),...this.op]
                this.setState({
                    op: data
                })                             
            }).catch(err=>{
                console.log(err)
            })
        }).catch(err=>{
            console.log(err)
        })
    }
    display=(r,headers)=>{
      // console.log("DISPLAY:\n")
        return(
           <tr>
               {headers.map(value => <td>{r[value]}</td>)}
           </tr>
        )
    }
    submit=(e)=>{
        console.log("SUBMIT")
        e.preventDefault();
        this.setState({
            [e.target.name]:e.target.value,
            Submit:"yes"
        })
        //this.convertToJSON(this.state.link)
    }
    render(){
        return(
            <div><Presentation
                {...this.props}
                {...this.state}
                handleEvent={this.handleEvent}
                submit={this.submit}
            /><hr/><br/>
            {this.state.op}
    
            </div>
        )
    }
}

export default Container;
