import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import {
 
    Media,
    Input
 } from "reactstrap";
class Container extends Component{
    

    state={
        course : '',
        course_error: false,
        branch:'',
        disabled_attendance_create: false,
        file_meta : {},
        subjects: ["SUB1","SUB2","SUB3","SUB4","SUB5","SUB6","SUB7","SUB8"],
        reg: ["16K61A0501","16K61A0502","16K61A0503","16K61A0504"],
       // marks: [{id:1,val:"85"},{id:2,val:"80"},{id:3,val:"72"},{id:4,val:"74"}]
       mark:["85","76","74","88"],
       periods : [{id : 1,name:"hi"},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        
    }
    constructor(props){
        super(props)
        //const subjects=["DS","MS","ML","CPP"];
         //console.log("SUB"+this.state.subjects)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }
    componentWillMount(){
        this.script = this.props.script
        //this.loadCourses()
       // this.stuList()
        
    }
    updateMarks = ()=>{
        
        const sub = this.state.subjects.map((s) =>{
           // console.log("gg "+s);
            return( 
                <th className="sort" scope="col">
                    {s}
                </th>
                 
            )
        })
        sub.unshift(
            <th className="sort" scope="col">
                REG.NO/SUBJECTS
            </th>
        )
        return sub
    }
    stuData = () => {
        
        const stu = this.state.reg.map(c=>{
        console.log(c.slice(7,))
        let r_no= c.slice(7,)
            return(
                <tr>
                      <th scope="row">
                        <Media className="align-items-center">
                            {c}
                        </Media>
                      </th>
                      {this.state.subjects.map(s => {
                           //let absent = this.state.absent[c] || []
                           //console.log(absent,p,c)
                        return(
                        <td>
                            <Input type="text" name={r_no+"_"+s}/>

                        </td>
                        )}
                )}
             </tr>
            )
        })
        return stu;
    }
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,

                section_error: false,
                section_valid: false,

                branch_error: false,
                branch_valid: false,

                year_error: false,
                year_valid:false,
                
                category_error: false,
                category_valid:false,

                phase_error: false,
                phase_valid:false,

                type_error: false,
                type_valid:false,

                semester_valid: false,
                semester_error: false,
        })
        this.validate(e)
       // console.log("hello"+this.state)
    }
    validate =(e)=>{
        //console.log("hee "+e)
        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    // console.log(yearsListJSX)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : "",
                        year:"",
                        section:"",
                        semester:"",
                        category:'',
                        phase:'',
                        type:'',
                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false,
                   
                })
            }else{
                this.setState({
                    course_error : true,
                    branch: "",
                    year:"",
                    section:"",
                    semester:"",
                    category:'',
                    phase:'',
                    type:'',
                    
                })
            }
            
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true,
                    branch_error: false,
                    year:"",
                    section:"",
                    semester:"",
                    category:'',
                    phase:'',
                    type:'',
                })
            }else{
                this.setState({
                    year:"",
                    section:"",
                    semester:"",
                    category:'',
                    phase:'',
                    type:'',
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false,
                    section:"",
                    semester:"",
                    category:'',
                    phase:'',
                    type:'',
                })
            }else{
                this.setState({
                    section : "",
                    semester:"",
                    category:'',
                    phase:'',
                    type:'',
                    year_error: true
                })
            }
            
        }
        //console.log("hiiii->"+this.state.year)
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true,
                    section_error: false,
                    category:'',
                    phase:'',
                    type:'',
                    semester:"",
                })
            }else{
                this.setState({
                    section_error: true,
                    category:'',
                    phase:'',
                    type:'',
                    semester:"",
                })
            }
        }
        if (e.target.name === "semester") {
            if (e.target.value) {
                return this.setState({
                    semester_valid: true,
                    semester_error: false,
                    category:'',
                    phase:'',
                    type:'',
                })
            }else{
                this.setState({
                    semester_error: true,
                    category:'',
                    phase:'',
                    type:'',
                })
            }
           // console.log("corse:",this.state.course , "branch:",this.state.branch , "section:",this.state.section , "semester:",this.state.semester , "Year:",this.state.year , "mataname:",this.state.file_meta.name , !this.state.disabled_marks_create)
           
        }
        if (e.target.name === "category") {
           
            if (e.target.value) {
                this.setState({
                    category_valid: true,
                    category_error: false,
                    type:'',
                    phase:'',
                    maxScore:'',
                })
                if(e.target.value==="external"){
                   this.setState({
                        type:'',
                        phase:'',
                        maxScore:'',
                    })
                }
            }else{
                this.setState({
                    category_error: true,
                    phase:'',
                    type:'',
                    maxScore:'',
                })
            }
        }
        if (e.target.name === "phase") {
            if (e.target.value) {
               return this.setState({
                    phase_valid: true,
                    phase_error: false,
                    type:'',
                    maxScore:'',

                })
            }else{
                this.setState({
                    phase_error: true,
                    type:'',
                    maxScore:'',
                })
            }
        }
        if (e.target.name === "type") {
            if (e.target.value) {
                return this.setState({
                    type_valid: true,
                    type_error: false,
                    maxScore:'',
                })
            }else{
                this.setState({
                    type_error: true,
                    maxScore:''
                
                })
            }
        }
    
    }
    /*hideAlert = () => {
        this.setState({
            create_users_response: null,
            disabled_users_create: false
        });
    };*/
    submit = (e) => {
        e.preventDefault();
        
        this.setState({
            disabled_attendance_create : true
        })
        if(!this.state.course || !this.state.file_meta)
            return this.setState({
                disabled_attendance_create: false  
            })

        /*return this.props.middleware.convertCSVToJSON(this.state.file_meta, [this.state.formFieldsDict_inputName, this.state.formFieldsDict_name] , json => {
            // console.log(json)
            return this.props.middleware.state.middlewareAPI.createUsers(json,this.state.file_meta,response => {
                console.log(response)
                this.setState({
                    disabled_attendance_create: false 
                })

                if(!response.code){
                    this.setState({
                        create_attendance_response: (
                            <ReactBSAlert
                                success
                                style={{ display: "block", marginTop: "100px" }}
                                title="Success"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="success"
                                confirmBtnText="Ok"
                                btnSize=""
                            >
                                STUDENTS ATTENDANCE POSTING HAS BEEN INTIATED
                            </ReactBSAlert>
                        )
                    })
                }else{
                    this.setState({
                        create_users_response: (
                            <ReactBSAlert
                                warning
                                style={{ display: "block", marginTop: "100px" }}
                                title="Warning"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="warning"
                                confirmBtnText="Try Again"
                                btnSize=""
                            >
                                STUDENTS ATTENDANCE POSTING HAS BEEN FAILED
                            </ReactBSAlert>
                        )
                    })
                }
            })
        })*/
    }
    render(){       
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                updateMarks={this.updateMarks}
                stuData={this.stuData}
               // stuList={this.stuList}
               // marks={this.marks}
            />
        )        
    }
}

export default Container