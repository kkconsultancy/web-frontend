import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'

class Container extends Component{
    

    state={
        course : '',
        course_error: false,
        branch:'',
        create_marks_response : null,
        disabled_marks_create: false,
        file_meta : {},
        subjects: ["DS","MS","ML","CPP"],
        formFields : {"students" : "students","1" : "1","2" : "2","3" : "3","4":"4","5":"5","6":"6","7":"7"}
        
    }
    constructor(props){
        super(props)
        //const subjects=["DS","MS","ML","CPP"];
         //console.log("SUB"+this.state.subjects)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                jsx : jsx
            }
        }
    }

    createCSV = (data) => {
        this.setState({
            templateFileCSV : <Loading/>
        })
        const inputData = {
            academicYear : this.props.middleware.state.metadata.year,
            course : data.name === "course" ? data.value : this.state.course,
            branch : data.name === "branch" ? data.value : this.state.branch,
            year : data.name === "year" ? data.value : this.state.year,
            semester : data.name === "semester" ? data.value : this.state.semester,
            section : data.name === "section" ? data.value : this.state.section,
        }
        if (this.props.middleware.state.metadata)
            return this.props.middleware.state.middlewareAPI.listStudents(inputData, (data) => {
                const csvData = [["students","1","2","3","4","5","6","7"].join(",")]
                if(!data.code){
                    data = data.sort()
                    csvData.push(...data)
                    // console.log(dict1)
                    // console.log(dict2)
                    const header = "data:text/csv;charset=utf-8," + (csvData.join("\n")) + "\n"
                    const encodedUri = encodeURI(header);
                    return this.setState({
                        templateFileCSV : "marks_" + inputData.academicYear + "_" + (inputData.course || "") + "_" + (inputData.branch || "")+ "_" + (inputData.year || "") + "_" + (inputData.section || "") + ".csv",
                        encodedUri: encodedUri
                    })
                }else
                this.setState({
                    create_marks_response: (
                        <ReactBSAlert
                            warning
                            style={{ display: "block", marginTop: "100px" }}
                            title="Warning"
                            onConfirm={() => this.hideAlert()}
                            onCancel={() => this.hideAlert()}
                            confirmBtnBsStyle="warning"
                            confirmBtnText="Try Again"
                            btnSize=""
                        >
                            NO DATA FOUND
                        </ReactBSAlert>
                    )
                })
            })
    }
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,

                section_error: false,
                section_valid: false,

                branch_error: false,
                branch_valid: false,

                year_error: false,
                year_valid:false,

                semister_valid:false,
                semister_error:false,
                
                category_error: false,
                category_valid:false,

                phase_error: false,
                phase_valid:false,

                type_error: false,
                type_valid:false,

                maxScore_error : false,
                maxScore_valid: false,

                csv_file_blob_error : false,
                csv_file_blob_valid : false,
        })
        this.validate(e)
       // console.log("hello"+this.state)
    }
    validate =(e)=>{
        //console.log("hee "+e)
        this.createCSV({name : e.target.name,value:e.target.value})
        // console.log(this.state)

        if(e.target.name==="course"){
            if(e.target.value){
                //console.log("hii"+e.target.value);
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    // console.log(yearsListJSX)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : "",
                        year:"",
                        section:"",
                        semister:"",
                        category:'',
                        phase:'',
                        type:'',                    })
                })
                return this.setState({
                    course_valid: true,
                    course_error: false
                })
            }else{
                this.setState({
                    course_error : true,
                    branch: "",
                    year:"",
                    section:"",
                    semister:"",
                    category:'',
                    phase:'',
                    type:'',

                    
                })
            }
            
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true,
                    branch_error: false,
                    year:"",
                    section:"",
                    semister:"",
                    category:'',
                    phase:'',
                    type:'',
                })
            }else{
                this.setState({
                    year:"",
                    section:"",
                    semister:"",
                    category:'',
                    phase:'',
                    type:'',
                    branch_error: true
                })    
            }
        }
        if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    year_error: false,
                    section:"",
                    semister:"",
                    category:'',
                    phase:'',
                    type:'',
                })
            }else{
                this.setState({
                    section : "",
                    semister:"",
                    category:'',
                    phase:'',
                    type:'',
                    year_error: true
                })
            }
            
        }
        //console.log("hiiii->"+this.state.year)
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true,
                    section_error: false,
                    category:'',
                    phase:'',
                    type:'',
                    semister:"",
                })
            }else{
                this.setState({
                    section_error: true,
                    category:'',
                    phase:'',
                    type:'',
                    semister:"",
                })
            }
        }
        if (e.target.name === "semister") {
            if (e.target.value) {
                return this.setState({
                    semister_valid: true,
                    semister_error: false,
                    category:'',
                    phase:'',
                    type:'',
                })
            }else{
                this.setState({
                    semister_error: true,
                    category:'',
                    phase:'',
                    type:'',
                })
            }
            console.log("corse:",this.state.course , "branch:",this.state.branch , "section:",this.state.section , "semister:",this.state.semester , "Year:",this.state.year , "mataname:",this.state.file_meta.name , !this.state.disabled_users_create)
           
        }
        if (e.target.name === "category") {
           
            if (e.target.value) {
                this.setState({
                    category_valid: true,
                    category_error: false
                })
               }
            else{
                this.setState({
                    category_error: true,
                    phase:'',
                    type:'',
                    maxScore:'',
                })
                console.log(this.state.displayBut)
            }
        }
        if (e.target.name === "type") {
            if (e.target.value) {
                return this.setState({
                    type_valid: true,
                    type_error: false,
                    phase : "",
                    maxScore:'',
                })
            }else{
                this.setState({
                    type_error: true,
                    maxScore:''
                
                })
            }
        }
        if (e.target.name === "phase") {
            if (e.target.value) {
               return this.setState({
                    phase_valid: true,
                    phase_error: false,
                    maxScore:'',
                })
            }else{
                this.setState({
                    phase_error: true,
                    type:'',
                    maxScore:'',
                })
            }
        }
        if (e.target.name === "maxScore") {
            if (e.target.value) {
                return this.setState({
                    maxScore_valid: true,
                    maxScore_error: false
                })
            }else{
                this.setState({
                    maxScore_error: true
                })
            }
        }
        if (e.target.name === "csv_file_blob") {
            if (e.target.value) {
                
                const file = e.target.files[0]
                // console.log(file)
                if(file.name.split(".").splice(-1)[0] !== "csv"){
                    // console.log(file.name.split(".").splice(-1))
                    return this.setState({
                        csv_file_blob_error: "Incorrect file uploaded",
                        file_meta : "",
                        csv_file_blob : ""
                    })
                    
    

                }
                
                return this.setState({
                    csv_file_blob_valid: true,
                    file_meta: file,
                    csv_file_blob : e.target.files[0]
                })
                
            } 
            return this.setState({
                csv_file_blob_error: true
            })
            
            
        }
    
    }
    hideAlert = () => {
        this.setState({
            create_marks_response: null,
            disabled_marks_create: false
        });
    };
   
    k = ()=>{ let k 
        if(this.props.category=="internal"){ k=(this.props.course && this.props.branch && this.props.section && this.props.semister && this.props.year && this.props.file_meta.name && this.props.category && this.props.phase && this.props.type && !this.props.disabled_users_create)}else if(this.props.category=="external"){k=(this.props.course && this.props.branch && this.props.section && this.props.semister && this.props.year && this.props.file_meta.name && !this.props.disabled_users_create)}
        this.setState({
            k:k
        })  
}
    
    submit = (e) => {
        e.preventDefault();
        
        this.setState({
            disabled_marks_create : true
        })
        if(!this.state.course || !this.state.file_meta)
            return this.setState({
                disabled_marks_create: false  
            })
            return this.props.middleware.convertCSVToJSON(this.state.file_meta, [this.state.formFields, this.state.formFields] , json => {
                console.log(json)
                const marksList = json.map(student => {
                    const data = {
                        "uid" : student.students,
                    }
                    const marks = []
                    for(let i in student){
                        if(i === "students")
                            continue
                        
                        marks.push({
                            subject : i,
                            score : student[i]
                        })
                    }
                    data.scores = marks
                    return data
                })

                console.log(marksList)

                const inputData = {
                    academicYear : this.props.middleware.metadata.year,
                    course : this.state.course,
                    branch : this.state.branch
                }
    
                return;
                this.props.middleware.state.middlewareAPI.postAttendance({},response => {
                    console.log(response)
                    this.setState({
                        disabled_attendance_create: false 
                    })
                    if(!response.code){
                        this.setState({
                            create_attendance_response: (
                                <ReactBSAlert
                                    success
                                    style={{ display: "block", marginTop: "100px" }}
                                    title="Success"
                                    onConfirm={() => this.hideAlert()}
                                    onCancel={() => this.hideAlert()}
                                    confirmBtnBsStyle="success"
                                    confirmBtnText="Ok"
                                    btnSize=""
                                >
                                    POSTING STUDENTS ATTENDANCE HAS BEEN DONE
                                </ReactBSAlert>
                            )
                        })
                    }else{
                        this.setState({
                            create_attendance_response: (
                                <ReactBSAlert
                                    warning
                                    style={{ display: "block", marginTop: "100px" }}
                                    title="Warning"
                                    onConfirm={() => this.hideAlert()}
                                    onCancel={() => this.hideAlert()}
                                    confirmBtnBsStyle="warning"
                                    confirmBtnText="Try Again"
                                    btnSize=""
                                >
                                    POSTING STUDENTS ATTENDANCE HAS BEEN FAILED
                                </ReactBSAlert>
                            )
                        })
                    }
                })
            })
    }
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                submit={this.submit}
                k={this.k}
            />
        )        
    }
}

export default Container