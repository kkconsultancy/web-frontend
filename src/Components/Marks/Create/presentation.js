import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    // CardFooter,
    // Label
} from "reactstrap";
import Header from '../../Header'
//import ReactDatetime from "react-datetime";
import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="POST marks" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
                  <Card className="bg-secondary shadow">
                    {this.props.create_marks_response}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">POST MARKS</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form>
                        <h6 className="heading-small text-muted mb-4">
                          Course Information
                        </h6>

                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input type="select" 
                                className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} 
                                name="course" 
                                value={this.props.course}
                                onChange={this.props.updateState}
                                valid={this.props.course_valid}
                                invalid={this.props.course_error}
                                >
                                 {this.props.jsx.coursesListJSX}
                                {/* <option value="">---</option>
                                    <option value="btech">B.Tech</option>
                                    <option value="mtech">M.Tech</option>
                                    <option value="diploma">Diploma</option>
                                    <option value="mba">MBA</option> */}
                                  
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.course_valid !== true ? this.props.course_valid : "Course accepted"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.course_error !== true ? this.props.course_error : "Please select the course!!"}
                                </div>
                              </FormGroup>
                            </Col>
                          </Row>
                        </div>
                        <div style={{ display: !this.props.course? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Branch & Section Information
                          </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input type="select" 
                                  name="branch"
                                  disabled={this.props.course && !this.props.disabled_marks_create ? false : true }  
                                  className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.branch}
                                  valid={this.props.branch_valid}
                                  invalid={this.props.branch_error}
                                  >
                                  {this.props.jsx.branchesListJSX}
                              
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.branch_valid !== true ? this.props.branch_valid : "Branch accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.branch_error !== true ? this.props.branch_error : "Please select the branch!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select" 
                                  name="year" 
                                  disabled={this.props.branch && !this.props.disabled_marks_create ? false : true} 
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.year}
                                  valid={this.props.year_valid}
                                  invalid={this.props.year_error}
                                  >
                                    {this.props.jsx.yearsListJSX}
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.year_valid : "Year accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.year_error : "Please select the year!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Section
                              </label>
                                  <Input type="select" 
                                  name="section" 
                                  disabled={this.props.year && !this.props.disabled_marks_create ? false : true}
                                  className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.section}
                                  valid={this.props.section_valid}
                                  invalid={this.props.section_error}
                                  >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.section_valid !== true ? this.props.section_valid : "Section accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.section_error !== true ? this.props.section_error : "Please select the section!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Semister
                              </label>
                                  <Input type="select" 
                                  name="semister" 
                                  disabled={this.props.year && !this.props.disabled_marks_create ? false : true}
                                  className={"login-input " + (this.props.semister_valid ? "is-valid" : this.props.semister_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.semister}
                                  valid={this.props.semister_valid}
                                  invalid={this.props.semister_error}
                                  >
                                    <option value="">---</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.section_valid !== true ? this.props.section_valid : "Section accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.section_error !== true ? this.props.section_error : "Please select the section!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          </div> 
                          <div style={{ display: !this.props.semister? "none" : "" }}>
                            <hr className="my-4" />
                            <h6 className="heading-small text-muted mb-4">
                              Marks Cateogory Information
                            </h6>
                            <div className="pl-lg-4">
                            <Row>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Category
                              </label>
                                  <Input type="select" 
                                  name="category"
                                  disabled={this.props.semister && !this.props.disabled_marks_create ? false : true }  
                                  className={"login-input " + (this.props.category_valid ? "is-valid" : this.props.category_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.category}
                                  valid={this.props.category_valid}
                                  invalid={this.props.category_error}
                                  >
                                    <option value="">---</option>
                                    <option value="internal">INTERNAL</option>
                                    <option value="external">EXTERNAL</option>
                                
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.category_valid !== true ? this.props.category_valid : "Category accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.category_error !== true ? this.props.category_error : "Please select the category(internal/external)!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Type
                              </label>
                                  <Input type="select" 
                                  name="type" 
                                  disabled={this.props.category === "internal" && !this.props.disabled_marks_create ? false : true}
                                  className={"login-input " + (this.props.type_valid ? "is-valid" : this.props.type_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.type}
                                  valid={this.props.type_valid}
                                  invalid={this.props.type_error}
                                  >
                                    <option value="">---</option>
                                    <option value="assignment">Assignment Marks</option>
                                    <option value="classTest">Class Test Marks</option>
                                    <option value="quizExam">Quiz Examination Marks</option>
                                    <option value="midExam">Mid Examination Marks</option>
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.type_valid !== true ? this.props.type_valid : "Test type accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.type_error !== true ? this.props.type_error : "Please select the type of test!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Phase
                              </label>
                                  <Input type="text" 
                                  name="phase" 
                                  disabled={this.props.type && !this.props.disabled_marks_create ? false : true} 
                                  className={"login-input " + (this.props.phase_valid ? "is-valid" : this.props.phase_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.phase}
                                  valid={this.props.phase_valid_valid}
                                  invalid={this.props.phase_error}
                                  >
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.phase_valid !== true ? this.props.phase_valid : "Phase accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.phase_error !== true ? this.props.phase_error : "Please enter the valid phase"}
                                  </div>
                                </FormGroup>
                              </Col>
                              
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Max Marks (each subject)
                              </label>
                                  <Input type="text"
                                  name="maxScore" 
                                  disabled={this.props.type && !this.props.disabled_marks_create ? false : true}
                                  className={"login-input " + (this.props.maxScore_valid ? "is-valid" : this.props.maxScore_error ? "is-invalid" : "")} 
                                  onChange={this.props.updateState}
                                  value={this.props.maxScore}
                                  valid={this.props.maxScore_valid}
                                  invalid={this.props.maxScore_error}
                                  >
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.maxScore_valid !== true ? this.props.maxScore_valid : "Test max marks accepted"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.maxScore_error !== true ? this.props.maxScore_error : "Please enter the max of each subject in the test!!"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                            </Row>
                            </div>
                          </div>
                          

                          
                          <div style={{ display: !this.props.exel ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            FILE UPLOAD
                          </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col lg="12">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-city"
                                  >
                                    CSV
                                  </label>
                                  <div class="custom-file">
                                    <Input type="file" 
                                    disabled={this.props.section && !this.props.disabled_marks_create ? false : true} 
                                    className={"custom-file-input " + (this.props.csv_file_blob_valid ? "is-valid" : this.props.csv_file_blob_error ? "is-invalid" : "")} 
                                    id="customFile" 
                                    name="csv_file_blob" 
                                    onChange={this.props.updateState} />
                                    <label class="custom-file-label" for="customFile">
                                      {this.props.file_meta.name || this.props.csv_file_blob_error || "UPLOAD A CSV FILE"}
                                    </label>
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col lg="12">
                                <FormGroup>
                                  <label
                                    className="form-control-label d-flex justify-content-between"
                                    htmlFor="input-city"
                                  >
                                    Download CSV Template
                                    <a
                                      download={this.props.templateFileCSV}
                                      href={this.props.course && !this.props.disabled_marks_create ? this.props.encodedUri : "#disabled"}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                    >
                                      <button
                                        type="button"
                                        className="btn btn-secondary"
                                      >
                                      {this.props.templateFileCSV}
                                      {this.props.k}
                                      </button>
                                    </a>
                                  
                                  </label>
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          <hr className="my-4" />
                          <Row>
                            <Col lg="4">
                              <div className="text-center">
                                    

                                   
                                    

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  disabled={ this.props.k ? false : true}
                                  onClick = {this.props.submit}
                                >

                                  UPLOAD {this.props.disabled_marks_create ? <Loading/> : ""}
                              </Button>
                              </div>
                            </Col>
                          </Row>
                          {/* </div> */}
                          </div>
                      </Form>
                      
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;