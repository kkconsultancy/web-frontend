import React from 'react'
import Presentation from './presentation'
import './style.css'
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
import Loading from '../../Loading'

class Container extends React.Component{
    
    state={
        course : '',
        course_error: false,
        disabled_users_create: true,
        file_meta : {},
        create_users_response: <Loading/>,
        jsx : {}
    }

    constructor(props){
        super(props)
        // console.log(this.props)
        if(this.props.middleware.state.metadata){
            // console.log(this.props.middleware.state.metadata)
            const coursesListJSX = this.props.middleware.state.metadata.courses.filter(course => course.code && course.name).map(course => {
                return <option key={course.code} value={course.code}>{course.name}</option>
            })
            coursesListJSX.unshift(<option key="" value="">---</option>)
            const jsx = {
                coursesListJSX: coursesListJSX
            }
            this.state = {
                ...this.state,
                disabled_users_create: false,
                create_users_response : null,
                jsx : jsx
            }
        }
    }

    componentWillMount = () => {
        if (this.props.middleware.state.metadata)
            this.props.middleware.getForm({
                category: "USERS",
                type: "ADD"
            }, (data) => {
                const dict1 = []
                const dict2 = []
                console.log(data)
                for (let i in data){
                    // console.log(data[i].inputName,data[i].name)
                    dict1[data[i].inputName] = data[i].name
                    dict2[data[i].name] = data[i].inputName
                }
                // console.log(dict1)
                // console.log(dict2)
                const header = "data:text/csv;charset=utf-8," + (data.map(field => field.inputName).join(",")) + "\n"
                const encodedUri = encodeURI(header);
                this.setState({
                    formFieldsDict_inputName: dict1,
                    formFieldsDict_name: dict2,
                    formFields: header,
                    encodedUri: encodedUri
                })
            })
    }
    
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                course_error : false,
                course_valid : false,
                section_error: false,
                section_valid: false,
                branch_error: false,
                branch_valid: false,
                csv_file_blob_error : false,
                csv_file_blob_valid : false,
                year_valid : false,
                year_error : false,
                semester_valid : false,
                semester_error : false
        })
        this.validate(e)
        // console.log(this.state)
    }

    validate = e => {
        if(e.target.name === "course"){
            if(e.target.value){
                this.props.middleware.state.metadata.courses.filter(course => course.code && course.name && course.code === e.target.value)
                .map(course => {
                    const branchesListJSX = course.specializations.map(branch => {
                        return <option key={branch.code} value={branch.code}>{branch.name}</option>
                    })
                    branchesListJSX.unshift(<option key="" value="">---</option>)
                    const years = course.years
                    // console.log(years)
                    const yearsListJSX = []
                    for(let i=1;i<=years;i++){
                        yearsListJSX.push(<option key={i} value={i}>{i}</option>)
                    }
                    yearsListJSX.unshift(<option key="" value="">---</option>)
                    return this.setState({
                        jsx : {
                            ...this.state.jsx,
                            branchesListJSX: branchesListJSX,
                            yearsListJSX : yearsListJSX
                        },
                        branch : "",
                        year : "",
                        semester : "",
                        section : ""
                    })
                })
                return this.setState({
                    course_valid : true
                })
            }
            this.setState({
                course_error : "Incorrect course selected",
                branch : "",
                year : "",
                semester : "",
                section : ""
            })
        }
        if (e.target.name === "branch") {
            if (e.target.value) {
                return this.setState({
                    branch_valid: true,
                    year : "",
                    semester : "",
                    section : ""
                })
            }else{
                return this.setState({
                    year : "",
                    semester : "",
                    section : ""
                })
            }
        }if (e.target.name === "year") {
            if (e.target.value) {
                return this.setState({
                    year_valid: true,
                    section : "",
                    semester : ""
                })
            }else{
                return this.setState({
                    semester : "",
                    section : ""
                })
            }
        }
        if (e.target.name === "semester") {
            if (e.target.value) {
                return this.setState({
                    semester_valid: true,
                    section : ""
                })
            }else{
                return this.setState({
                    section : ""
                })
            }
        }
        if (e.target.name === "section") {
            if (e.target.value) {
                return this.setState({
                    section_valid: true
                })
            }
        }
        if (e.target.name === "csv_file_blob") {
            if (e.target.value) {
                const file = e.target.files[0]
                console.log(file)
                if(file.name.split(".").splice(-1)[0] !== "csv"){
                    // console.log(file.name.split(".").splice(-1))
                    return this.setState({
                        csv_file_blob_error: true,
                        file_meta : "",
                        csv_file_blob : ""
                    })

                }
                return this.setState({
                    csv_file_blob_valid: true,
                    file_meta: file,
                    csv_file_blob : e.target.files[0]
                })
            } 
            return this.setState({
                csv_file_blob_error: true
            })
        }
    }

    hideAlert = () => {
        this.setState({
            create_users_response: null,
            disabled_users_create: false
        });
    };
    submit = (e) => {
        e.preventDefault();
        
        this.setState({
            disabled_users_create : true
        })

        if(!this.state.course || !this.state.file_meta)
            return this.setState({
                disabled_users_create: false  
            })

        return this.props.middleware.convertCSVToJSON(this.state.file_meta, [this.state.formFieldsDict_inputName, this.state.formFieldsDict_name] , json => {
            console.log(json)
            json = json.map(record => {
                record.course = this.state.course
                record.branch = this.state.branch || record.branch
                record.year = this.state.year || record.year
                record.semester = this.state.semester || record.semester
                record.gender = record.gender.toLowerCase().match("f") ? "FEMALE" : "MALE"

                return record
            })
            return this.props.middleware.state.middlewareAPI.createUsers(json,response => {
                console.log(response)
                this.setState({
                    disabled_users_create: false 
                })

                if(!response.code){
                    this.setState({
                        create_users_response: (
                            <ReactBSAlert
                                success
                                style={{ display: "block", marginTop: "100px" }}
                                title="Success"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="success"
                                confirmBtnText="Ok"
                                btnSize=""
                            >
                                USERS ACCOUNTS CREATION HAS BEEN INTIATED
                            </ReactBSAlert>
                        )
                    })
                }else{
                    this.setState({
                        create_users_response: (
                            <ReactBSAlert
                                warning
                                style={{ display: "block", marginTop: "100px" }}
                                title="Warning"
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="warning"
                                confirmBtnText="Try Again"
                                btnSize=""
                            >
                                USERS ACCOUNTS CREATION HAS BEEN FAILED
                            </ReactBSAlert>
                        )
                    })
                }
            })
        })
    }
    
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                updateState={this.updateState}
                submit={this.submit}
                downloadTemplate={this.downloadTemplate}
            />
        )
    }
}

export default Container;
