import React from 'react'
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    //CardFooter
} from "reactstrap";
import Header from '../../Header'
import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="ADD users" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="10">
                  <Card className="bg-secondary shadow">
                    {this.props.create_users_response}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">ADD NEW USER ACCOUNTS</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        <h6 className="heading-small text-muted mb-4">
                          Course information
                    </h6>
                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="12">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Course
                            </label>
                                <Input 
                                  type="select" 
                                  value={this.props.course} 
                                  valid={this.props.course_valid}
                                  invalid={this.props.course_error}
                                  disabled={this.props.disabled_users_create} 
                                  className={"login-input " + (this.props.course_valid ? "is-valid" : this.props.course_error ? "is-invalid" : "")} 
                                  name="course" 
                                  onChange={this.props.updateState}
                                >
                                  {this.props.jsx.coursesListJSX}
                                </Input>
                                <div className="valid-feedback">
                                  {this.props.course_valid !== true ? this.props.course_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.course_error !== true ? this.props.course_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>
                          </Row>
                        </div>
                        <div style={{ display: !this.props.course ? "none" : "" }}>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            Optional Information
                      </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Branch
                              </label>
                                  <Input 
                                    type="select" 
                                    name="branch" 
                                    value={this.props.branch}
                                    valid={this.props.branch_valid}
                                    invalid={this.props.branch_error}
                                    disabled={this.props.course && !this.props.disabled_users_create ? false : true } 
                                    className={"login-input " + (this.props.branch_valid ? "is-valid" : this.props.branch_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    {this.props.jsx.branchesListJSX}
                                  </Input>
                                  <div className="valid-feedback">
                                    {this.props.branch_valid !== true ? this.props.branch_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.branch_error !== true ? this.props.branch_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                              
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Year
                              </label>
                                  <Input type="select"
                                    name="year" 
                                    value={this.props.year}
                                    valid={this.props.year_valid}
                                    invalid={this.props.year_error}
                                    disabled={this.props.branch && !this.props.disabled_users_create ? false : true} 
                                    className={"login-input " + (this.props.year_valid ? "is-valid" : this.props.year_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    {this.props.jsx.yearsListJSX}
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.year_valid !== true ? this.props.year_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.year_error !== true ? this.props.year_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                              
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Semester
                              </label>
                                  <Input type="select"
                                    name="semester" 
                                    value={this.props.semester}
                                    valid={this.props.semester_valid}
                                    invalid={this.props.semester_error}
                                    disabled={this.props.year && !this.props.disabled_users_create ? false : true} 
                                    className={"login-input " + (this.props.semester_valid ? "is-valid" : this.props.semester_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    <option value="">---</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.semester_valid !== true ? this.props.semester_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.semester_error !== true ? this.props.semester_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                              <Col md="3">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Section
                              </label>
                                  <Input type="select"
                                    name="section" 
                                    value={this.props.section}
                                    valid={this.props.section_valid}
                                    invalid={this.props.section_error}
                                    disabled={this.props.semester && !this.props.disabled_users_create ? false : true} 
                                    className={"login-input " + (this.props.section_valid ? "is-valid" : this.props.section_error ? "is-invalid" : "")} 
                                    onChange={this.props.updateState}
                                  >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                  
                                  <div className="valid-feedback">
                                    {this.props.section_valid !== true ? this.props.section_valid : "This is accepted here"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.section_error !== true ? this.props.section_error : "Nope, this isn't expected here"}
                                  </div>
                                </FormGroup>
                              </Col>
                            
                            
                            </Row>
                          </div>
                          <div style={{ display: !this.props.year ? "none" : "" }}></div>
                          <hr className="my-4" />
                          <h6 className="heading-small text-muted mb-4">
                            FILE UPLOAD
                          </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col lg="12">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-city"
                                  >
                                    CSV
                                  </label>
                                  <div class="custom-file">
                                    <Input 
                                      type="file" 
                                      disabled={this.props.course && !this.props.disabled_users_create ? false : true} 
                                      valid={this.props.csv_file_blob_valid}
                                      invalid={this.props.csv_file_blob_error}
                                      className={"custom-file-input " + (this.props.csv_file_blob_valid ? "is-valid" : this.props.csv_file_blob_error ? "is-invalid" : "")} 
                                      id="customFile" 
                                      name="csv_file_blob" 
                                      onChange={this.props.updateState}
                                    />
                                    <div className="valid-feedback">
                                    {this.props.csv_file_blob_valid !== true ? this.props.csv_file_blob_valid : "Time to upload the file"}
                                  </div>
                                  <div className="invalid-feedback">
                                    {this.props.csv_file_blob_error !== true ? this.props.csv_file_blob_error : ".csv is only expected"}
                                  </div>
                                    
                                    <label class="custom-file-label" for="customFile">
                                      {this.props.file_meta.name || this.props.csv_file_blob_error || "UPLOAD A CSV FILE"}
                                    </label>
                                  </div>
                                
                                </FormGroup>
                              </Col>
                              <Col lg="12">
                                <FormGroup>
                                  <label
                                    className="form-control-label d-flex justify-content-between"
                                    htmlFor="input-city"
                                  >
                                    Download CSV Template
                                    <a
                                      download={"users_" + (this.props.course || "") + "_" + (this.props.branch || "") + "_" + (this.props.section || "") + ".csv"}
                                      href={this.props.course && !this.props.disabled_users_create ? this.props.encodedUri : "#disabled"}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                    >
                                      <button
                                        type="button"
                                        className="btn btn-secondary"
                                      >
                                        {"users_" + (this.props.course || "") + "_" + (this.props.branch || "") + "_" +(this.props.year || "")+ "_" +(this.props.semester||"")+ "_" + (this.props.section || "") + ".csv"}
                                      </button>
                                    </a>
                                  
                                  </label>
                                </FormGroup>
                              </Col>

                            </Row>
                          </div>
                          <hr className="my-4" />
                          <Row>
                            <Col lg="4">
                              <div className="text-center">

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="success" outline 
                                  className="my-4" 
                                  disabled={this.props.course && this.props.file_meta.name && !this.props.disabled_users_create ? false : true}
                                  onClick = {this.props.submit}
                                >
                                  UPLOAD {this.props.disabled_users_create ? <Loading/> : ""}
                              </Button>
                              </div>
                            </Col>
                          </Row>
                        </div>
                        
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </>

        )
    }
}
export default Presentation;