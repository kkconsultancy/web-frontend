import React from 'react'
import {
    Button,
    Card,
    //CardImg,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    // CardFooter,
    // UncontrolledAlert
} from "reactstrap";
import Loading from '../../Loading'

import Header from '../../Header'
  
//import Loading from '../../Loading'

class Presentation extends React.Component{

    render(){
        return(
          <>
            <Header middleware={this.props.middleware} pageTitle="ADD users" />

            <Container className="mt--8" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-1" xl="12">
                  <Card className="bg-secondary shadow">
                    {this.props.list_users_response}
                    <CardHeader className="bg-white border-0">
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h3 className="mb-0">LIST USERS</h3>
                        </Col>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form onSubmit={this.props.submit}>
                        <h6 className="heading-small text-muted mb-4">
                          QUery information
                    </h6>
                        <div className="pl-lg-4">
                          <Row>
                            <Col lg="3">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Query (UID)
                            </label>
                                <Input 
                                  type="text"
                                  value={this.props.query_uid} 
                                  valid={this.props.query_uid_valid}
                                  invalid={this.props.query_uid_error}
                                  disabled={this.props.disabled_users_list} 
                                  className={"login-input " + (this.props.query_uid_valid ? "is-valid" : this.props.query_uid_error ? "is-invalid" : "")} 
                                  name="query_uid" 
                                  onChange={this.props.updateState}
                                />
                                <div className="valid-feedback">
                                  {this.props.query_uid_valid !== true ? this.props.query_uid_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.query_uid_error !== true ? this.props.query_uid_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col lg="3">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Query (Name)
                            </label>
                                <Input 
                                  type="text"
                                  value={this.props.query_name} 
                                  valid={this.props.query_name_valid}
                                  invalid={this.props.query_name_error}
                                  disabled={this.props.disabled_users_list} 
                                  className={"login-input " + (this.props.query_name_valid ? "is-valid" : this.props.query_name_error ? "is-invalid" : "")} 
                                  name="query_name" 
                                  onChange={this.props.updateState}
                                />
                                <div className="valid-feedback">
                                  {this.props.query_name_valid !== true ? this.props.query_name_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.query_name_error !== true ? this.props.query_name_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col lg="3">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Query (Email)
                            </label>
                                <Input 
                                  type="text"
                                  value={this.props.query_email} 
                                  valid={this.props.query_email_valid}
                                  invalid={this.props.query_email_error}
                                  disabled={this.props.disabled_users_list} 
                                  className={"login-input " + (this.props.query_email_valid ? "is-valid" : this.props.query_email_error ? "is-invalid" : "")} 
                                  name="query_email" 
                                  onChange={this.props.updateState}
                                />
                                <div className="valid-feedback">
                                  {this.props.query_email_valid !== true ? this.props.query_email_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.query_email_error !== true ? this.props.query_email_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col lg="3">
                              <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-username"
                                >
                                  Query (Mobile)
                            </label>
                                <Input 
                                  type="text"
                                  value={this.props.query_mobile} 
                                  valid={this.props.query_mobile_valid}
                                  invalid={this.props.query_mobile_error}
                                  disabled={this.props.disabled_users_list} 
                                  className={"login-input " + (this.props.query_mobile_valid ? "is-valid" : this.props.query_mobile_error ? "is-invalid" : "")} 
                                  name="query_mobile" 
                                  onChange={this.props.updateState}
                                />
                                <div className="valid-feedback">
                                  {this.props.query_mobile_valid !== true ? this.props.query_mobile_valid : "This is accepted here"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.query_mobile_error !== true ? this.props.query_mobile_error : "Nope, this isn't expected here"}
                                </div>
                              </FormGroup>
                            </Col>
                            
                            <Col lg="4">
                              <div className="text-center">

                                {/* <Input type="submit"   className="login-submit" /> */}
                                <Button
                                  type="submit" color="orange" outline 
                                  className="my-4" 
                                  disabled={ !this.props.disabled_users_list ? false : true}
                                  onClick = {this.props.submit}
                                >
                                  SEARCH {this.props.disabled_users_list ? <Loading/> : ""}
                              </Button>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>

                {this.props.usersCardsList}

              </Row>
            </Container>

          </>

        )
    }
}
export default Presentation;