import React from 'react'
import Presentation from './presentation'

// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";

import {Link} from 'react-router-dom'

import {
    Button,
    Card,
    CardImg,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Row,
    Col,
    CardFooter,
    UncontrolledAlert
} from "reactstrap";
  
import Loading from '../../Loading'

class Container extends React.Component{
    
    state={
        course : '',
        course_error: false,
        disabled_users_list: false,
        file_meta : {},
        list_users_response: null,
        usersCardList : "",
        query_uid : "",
        query_name : "",
        query_email : "",
        query_mobile : ""
    }

    constructor(props){
        super(props)

        this.state = {
            ...this.state,
            usersCardsList : []
        }
    }

    getUsersList = () => {
        this.setState({
            usersCardList : <Loading/>,
            disabled_users_list : true
        })
        this.props.middleware.state.middlewareAPI.listUsers({
                query_uid : this.state.query_uid,
                query_name : this.state.query_name,
                query_email : this.state.query_email,
                query_mobile : this.state.query_mobile
        },(users) => {
            const usersData = {}
            // console.log(users)
            if(!Array.isArray(users))
                return this.setState({
                    usersCardList : "",
                    list_users_response : <ReactBSAlert
                    warning
                    style={{ display: "block", marginTop: "100px" }}
                    title="Warning"
                    onConfirm={() => this.hideAlert()}
                    onCancel={() => this.hideAlert()}
                    confirmBtnBsStyle="warning"
                    confirmBtnText="Try Again"
                    btnSize=""
                >
                    NO USER RECORDS FOUND WITH GIVEN FILTERS
                </ReactBSAlert> 
                })
            const jsx = users.map(user => {
                usersData[user.uid] = user
                return this.userCard(user)
            })
            this.setState({
                usersCardsList : jsx,
                users : usersData,
                disabled_users_list : false
            })
        })
    }

    componentWillMount = () => {
        if (this.props.middleware.state.metadata)
            this.getUsersList()
    }
    
    updateState = (e) => {
        // console.log(e)
        this.setState({
                [e.target.name] : e.target.value,
                query_uid_error : false,
                query_uid_valid : false,
                query_name_error : false,
                query_name_valid : false,
                query_email_error : false,
                query_email_valid : false,
                query_mobile_error : false,
                query_mobile_valid : false
        })
        this.validate(e)
        // this.getUsersList()
        // console.log(this.state)
    }

    validate = e => {
        if (e.target.name === "query_uid")
        if (e.target.value)
            return this.setState({
                query_uid_valid: true
            })
        
        if (e.target.name === "query_name")
        if (e.target.value)
            return this.setState({
                query_name_valid: true
            })
        
        if (e.target.name === "query_email")
        if (e.target.value)
            return this.setState({
                query_email_valid: true
            })

        if (e.target.name === "query_mobile")
        if (e.target.value)
            return this.setState({
                query_mobile_valid: true
            })
    }

    hideAlert = () => {
        this.setState({
            list_users_response: null,
            disabled_users_list: false
        });
    }
    
    submit = (e) => {
        e.preventDefault();
        
        this.setState({
            disabled_users_list : true,
            usersCardList : <Loading/>
        })

        this.getUsersList()
    }

    userCard = (user) => {
        return(
            <Col className="order-xl-2 mb-5 mb-xl-0" md="4" lg="3">
            <Card className="card-profile shadow">
              {this.props.reset_password_response}
              <CardImg
                onClick={e => e.preventDefault()}
                alt={user.displayName}
                className="card-profile-image image-hover-blur"
                src={
                    this.state.users ? this.state.users[user.uid] ? this.state.users[user.uid].publicURL : user.photo ? user.photo.photoURL : user.photoURL : "" }
                top
              />
              <CardHeader className="text-center border-0 pb-0">
                <div className="d-flex justify-content-between">
                </div>
              </CardHeader>
              <CardBody className="pt-0">
                <Row>
                  <div className="col">
                    <div className="card-profile-stats d-flex justify-content-center md-5">
                      <div>
                        <span className="heading">22</span>
                        <span className="description">Friends</span>
                      </div>
                      <div>
                        <span className="heading">10</span>
                        <span className="description">Photos</span>
                      </div>
                      <div>
                        <span className="heading">89</span>
                        <span className="description">Comments</span>
                      </div>
                    </div>
                  </div>
                </Row>
                <div className="text-center">
                  <h3>
                    {user.displayName}
                    <span className="font-weight-light">, {user.uid}</span>
                  </h3>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    Bucharest, Romania
                  </div>
                  <div className="h5 mt-4">
                    <i className="ni business_briefcase-24 mr-2" />
                    Solution Manager - Creative Tim Officer
                  </div>
                  <div>
                    <i className="ni education_hat mr-2" />
                    University of Computer Science
                  </div>
                  <hr className="my-4" />
                  <Link to={"/app/users/update/"+user.uid}>
                    <Button
                        className="float-right"
                        color="warning"
                        type="button"
                        onClick={() => this.updateUser(user.uid)}
                        size="sm"
                    >
                        UPDATE
                    </Button>
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
          
        )
    }

    updateUser = () => {
    }
    
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                updateState={this.updateState}
                submit={this.submit}
            />
        )
    }
}

export default Container;
