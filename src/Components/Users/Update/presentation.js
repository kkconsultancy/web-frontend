import React from "react";
import TagsInput from "react-tagsinput";

// reactstrap components
import {
  UncontrolledAlert,
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardImg
} from "reactstrap";

import Header from '../../Header'

import Loading from '../../Loading'

class Presentation extends React.Component{
    render(){
      console.log(this.props)
        return (
          <>
            <Header middleware={this.props.middleware} pageTitle="ACCOUNT" />
            <Container className="mt--7" fluid>
              <Row className="justify-content-center">
                <Col className="order-xl-2 mb-5 mb-xl-0" xl="4" sm="12">
                  <Card className="card-profile shadow">
                    {this.props.reset_password_response}
                    <CardImg
                      onClick={() => document.getElementById("photoURL").click()}
                      alt={this.props.middleware.state.user.uid}
                          className="card-profile-image image-hover-blur"
                      src={this.props.publicURL || this.props.middleware.state.user.photoURL}
                      top
                    />
                    <input 
                      name="photoURL" 
                      id="photoURL" 
                      type="file" 
                      style={{ display:"none"}} 
                      onChange={this.props.updateImage}
                    />
                    {this.props.uploadImageError}
                  
                    <CardHeader className="text-center border-0 pb-0">
                      <div className="d-flex justify-content-between">
                        {this.props.isImageUploading ? <Loading /> : ""}
                        <Button
                          className="float-left"
                          color="default"
                          type="button"
                          onClick={() => document.getElementById("photoURL").click()}
                          size="sm"
                        >
                          Upload
                        </Button>
                        <Button
                          className="float-right"
                          color="danger"
                          type="button"
                          onClick={this.props.requestResetPassword}
                          size="sm"
                        >
                          Reset Password
                        </Button>
                        {this.props.isResetPasswordProcessing ? <Loading /> : ""}
                      </div>
                    </CardHeader>
                    <CardBody className="pt-0">
                      <Row>
                        <div className="col">
                          <div className="card-profile-stats d-flex justify-content-center md-5">
                            <div>
                              <span className="heading">22</span>
                              <span className="description">Friends</span>
                            </div>
                            <div>
                              <span className="heading">10</span>
                              <span className="description">Photos</span>
                            </div>
                            <div>
                              <span className="heading">89</span>
                              <span className="description">Comments</span>
                            </div>
                          </div>
                        </div>
                      </Row>
                      <div className="text-center">
                        <h3>
                          {this.props.middleware.state.user.displayName}
                          <span className="font-weight-light">, 27</span>
                        </h3>
                        <div className="h5 font-weight-300">
                          <i className="ni location_pin mr-2" />
                          Bucharest, Romania
                        </div>
                        <div className="h5 mt-4">
                          <i className="ni business_briefcase-24 mr-2" />
                          Solution Manager - Creative Tim Officer
                        </div>
                        <div>
                          <i className="ni education_hat mr-2" />
                          University of Computer Science
                        </div>
                        <hr className="my-4" />
                        <p>
                            {this.props.middleware.state.JWT.custom_token}
                        </p>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          Show more
                        </a>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col className="order-xl-1" xl="8" sm="12">
              <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                        size="sm"
                      >
                        Settings
                      </Button>
                    </Col>
                  </Row>
                </CardHeader>
                 
                <CardBody>
                  
                  <Form>
                    <h6 className="heading-small text-muted mb-4">
                      User information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-username"
                            >
                              Id
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={this.props.middleware.state.user.uid}
                              id="input-username"
                              placeholder="developerswork"
                              disabled={true}
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                            <FormGroup>
                                <label
                                    className="form-control-label"
                                    htmlFor="input-first-name"
                                >
                                    Name
                                </label>
                                <Input
                                    // className="form-control-alternative"
                                    defaultValue={this.props.middleware.state.user.displayName}
                                    id="input-name"
                                    placeholder="Developers@Work"
                                    disabled={!this.props.disabled_profile ? false : true}
                                    className={"login-input " + (this.props.displayName_valid ? "is-valid" : this.props.displayName_error ? "is-invalid" : "")} 
                                    onChange={this.props.update}
                                    name="displayName"
                                    valid={this.props.displayName_valid}
                                    invalid={this.props.displayName_error}
                                    type="text"
                                />
                                <div className="valid-feedback">
                                  {this.props.displayName_valid !== true ? this.props.displayName_valid : "Good to go"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.displayName_error !== true ? this.props.displayName_error : "Nope, this isn't expected here"}
                                </div>
                            </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-email"
                            >
                              Email address
                            </label>
                            <Input
                              // className="form-control-alternative"
                              disabled={!this.props.disabled_profile ? false : true}
                              className={"login-input " + (this.props.email_valid ? "is-valid" : this.props.email_error ? "is-invalid" : "")} 
                              defaultValue={this.props.middleware.state.user.email}
                              onChange={this.props.update}
                              valid={this.props.email_valid}
                              invalid={this.props.email_error}
                              name="email"
                              placeholder="user@developers.work"
                              type="email"
                            />
                            <div className="valid-feedback">
                              {this.props.email_valid !== true ? this.props.email_valid : "Email is good, isn't it!"}
                            </div>
                            <div className="invalid-feedback">
                                {this.props.email_error !== true ? this.props.email_error : "We don't have time for these!!"}
                            </div>
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                            <FormGroup>
                                <label
                                    className="form-control-label"
                                    htmlFor="input-mobile"
                                >
                                    Mobile
                                </label>
                                <Input
                                    defaultValue={this.props.middleware.state.user.phoneNumber}
                                    name="phoneNumber"
                                    disabled={!this.props.disabled_profile ? false : true}
                                    valid={this.props.phoneNumber_valid}
                                    invalid={this.props.phoneNumber_error}
                                    className={"login-input " + (this.props.phoneNumber_valid ? "is-valid" : this.props.phoneNumber_error ? "is-invalid" : "")}
                                    onChange={this.props.update}
                                    placeholder="+919999999990"
                                    type="text"
                                />
                                <div className="valid-feedback">
                                  {this.props.phoneNumber_valid !== true ? this.props.phoneNumber_valid : "Wow!! Fancy mobile number you got there"}
                                </div>
                                <div className="invalid-feedback">
                                  {this.props.phoneNumber_error !== true ? this.props.phoneNumber_error : "Make sure you are typing numbers?"}
                                </div>
                            </FormGroup>
                        </Col>
                      </Row>
                      </div>
                      <hr className="my-4" />
                      {/* Description */}
                 <h6 className="heading-small text-muted mb-4">Other Information</h6>
                 
                    <div className="pl-lg-4">
                    <FormGroup>
                    <div className="pl-lg-2">
                    <label
                      className="form-control-label"
                       htmlFor="input-mobile"
                    >
                        Secondary Email
                     </label>
                     <div className="pl-lg-1">
                      <TagsInput
                        onlyUnique
                        className="bootstrap-tagsinput"
                        onChange={this.props.handleTagsinput1}
                        value={this.props.tagsinput1}
                        tagProps={{ className: "tag badge mr-1" }}
                        inputProps={{
                          className: "",
                          placeholder: ""
                        }}
                      />
                      </div>
                      </div>
                    </FormGroup>
                    </div>
                    <div className="pl-lg-4">
                    <Form>
                    <div className="pl-lg-2">
                    <label
                      className="form-control-label"
                       htmlFor="input-mobile"
                         >
                                    Secondary Phone Number
                     </label>
                     <div className="pl-lg-1">
                       <TagsInput
                        onlyUnique
                        className="bootstrap-tagsinput"
                        onChange={this.props.handleTagsinput2}
                        value={this.props.tagsinput2}
                        tagProps={{ className: "tag badge mr-1" }}
                        inputProps={{
                          className: "",
                          placeholder: ""
                        }}
                      />
                      </div>
                      </div>
                    </Form>
                    </div>
                   {/* Family Information */}
                   <hr className="my-4" />
                   <h6 className="heading-small text-muted mb-4">
                      Family Information
                    </h6>
                    
                    <div className="pl-lg-4">
                    <h6 className="heading-small text-muted mb-4">
                      Father Information
                    </h6>
                    
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                               Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Raf"
                              id="input-father-name"
                              placeholder="Father Name"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                               Occupation
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Farmer"
                              id="input-Occupation"
                              placeholder="Father Occupation"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Work Address
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09"
                              id="input-address"
                              placeholder="Home Address"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                             Work Phone Number
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-phone-number"
                              placeholder="Phone Number"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      
                      <h6 className="heading-small text-muted mb-4">
                      Mother Information
                      </h6>
                      
                      <Row>
                      
                      
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                               Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Raf"
                              id="input-father-name"
                              placeholder="Father Name"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                             Occupation
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Farmer"
                              id="input-Occupation"
                              placeholder="Father Occupation"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Work Address
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09"
                              id="input-address"
                              placeholder="Home Address"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                          Work Phone Number
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-phone-number"
                              placeholder="Phone Number"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      
                      
                    </div>
                    
                    <hr className="my-4" />
                    {/* Address */}
                    <h6 className="heading-small text-muted mb-4">
                      Contact information
                    </h6>
                    <div className="pl-lg-4">
                    <h6 className="heading-small text-muted mb-4">
                      Present Address
                    </h6>
                      <Row>
                        <Col md="12">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Address
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09"
                              id="input-address"
                              placeholder="Home Address"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              City
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Country
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="United States"
                              id="input-country"
                              placeholder="Country"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Postal code
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-postal-code"
                              placeholder="Postal code"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                    </div>
                    
                    <div className="pl-lg-4">
                    <h6 className="heading-small text-muted mb-4">
                      Permenent Address
                    </h6>
                      <Row>
                     
                        <Col md="12">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Address
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09"
                              id="input-address"
                              placeholder="Home Address"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              City
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="New York"
                              id="input-city"
                              placeholder="City"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Country
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="United States"
                              id="input-country"
                              placeholder="Country"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Postal code
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-postal-code"
                              placeholder="Postal code"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                    </div>
  
                    <hr className="my-4" />
  {/* SSC Information */}
  <h6 className="heading-small text-muted mb-4">
                     SSC Information
                    </h6>
                    <div className="pl-lg-4">
                    
                      <Row>
                      
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="instution-name"
                            >
                              Instution Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="SASI"
                              id="input-instution-name"
                              placeholder="Instution Name"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="ssc-marks"
                            >
                              SSC Markes
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="ssc-marks"
                              placeholder="SSC Markes"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        <Col md="12">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="ssc-place"
                            >
                              Place
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="TPG"
                              id="ssc-place"
                              placeholder="Place"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      </div>
                      
                    <hr className="my-4" />
 {/* Intermediate Information */}
                    <h6 className="heading-small text-muted mb-4">
                     Intermediate Information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                      
                        <Col lg="8">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="instution-name"
                            >
                              Instution Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="SASI"
                              id="input-instution-name"
                              placeholder="Instution Name"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        
                        
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="Intermediate-marks"
                            >
                              Intermediate Markes
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="ssc-marks"
                              placeholder="Intermediate Markes"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        <Col md="12">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="Intermediate-place"
                            >
                              Intermediate Place
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="TPG"
                              id="Intermediate-place"
                              placeholder="Place"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="eamcet-rank"
                            >
                              Eamcet Rank
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="Eamcet Rank"
                              placeholder="eamcet-rank"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="eamcet-score"
                            >
                              Eamcet Score
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="Eamcet Score"
                              placeholder="eamcet-score"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      </div>
                    <hr className="my-4" />
  {/* Admistration Information */}
                    <h6 className="heading-small text-muted mb-4">
                     Admistration Information
                    </h6>
                    <div className="pl-lg-4">
                    
                      <Row>
                      
                        {/* <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="batch"
                            >
                              Batch
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="2013-2019"
                              id="batch"
                              placeholder="20xx-20xy"
                              type="text"
                              onChange={this.props.update}
                            />
                          </FormGroup>
                        </Col>
                      
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="course"
                            >
                              Course
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue="Btech"
                              id="course"
                              placeholder="Course"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="branch"
                            >
                              Branch
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="branch"
                              placeholder="cse"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="year"
                            >
                             Year
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="Year"
                              placeholder="year"
                              type="number"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="semister"
                            >
                             Semister
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="semister"
                              placeholder="1"
                              type="number"
                            />
                          </FormGroup>
                        </Col> */}
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="date-of-join"
                            >
                              Date Of Join
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="date-of-join"
                              placeholder="12-14-2000"
                              type="date"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor=" admission-no"
                            >
                             Admission No
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="admission"
                              placeholder="20000"
                              type="numder"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="4">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="seat-type"
                            >
                             Seat Type
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="seat-type"
                              placeholder="Selete"
                              type="Text"
                            />
                          </FormGroup>
                        </Col>
                        
                      </Row>
                      </div>
                    <hr className="my-4" />
                   
                  </Form>
                </CardBody>
              </Card>
            </Col>
              </Row>
            </Container>
        </>
    
        )
    }
}

export default Presentation


















