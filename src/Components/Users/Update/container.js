import React from 'react'// reactstrap components
import {
    UncontrolledAlert
} from "reactstrap";
import Presentation from './presentation'

// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";


class Container extends React.Component{
    state = {
        disabled_profile : false,
        uploadImageError : false,
        tagsinput1: ["Neeraj5d5@sasi.ac.in",],
        tagsinput2: ["Neeraj5d5@sasi.ac.in",],
    }
    handleTagsinput1 = tagsinput1 => {
        this.setState({ tagsinput1 });
      };
      handleTagsinput2 = tagsinput2 => {
        this.setState({ tagsinput2 });
      };
    updateImage = (e) => {
        // console.log(e.target.files)
        this.setState({
            [e.target.name] : e.target.files[0],
            publicURL : URL.createObjectURL(e.target.files[0]),
            isImageUploading : true
        })

        this.props.middleware.state.middlewareAPI.uploadPhotoURL(e.target.files[0],(result) => {
            this.setState({
                isImageUploading: false
            })
            console.log(result)
            if(result.code){
                this.setState({
                    uploadImageError : (<UncontrolledAlert className="alert-danger m-1">
                    <span className="alert-icon">
                      <i className="ni ni-world-2" />
                    </span>
                    <span className="alert-text ml-1">
                      <strong>Upload Error</strong> Failed to Upload the image
                      out!
                    </span>
                </UncontrolledAlert>)
                })
            }
        })
    }

    hideAlert = () => {
        this.setState({
            reset_password_response: null,
            isResetPasswordProcessing: false
        });
    };

    requestResetPassword = (e) => {
        this.setState({
            isResetPasswordProcessing : true
        })

        this.props.middleware.state.middlewareAPI.requestResetPassword((response) => {
            if(!response.code){
                this.setState({
                    reset_password_response: (
                        <ReactBSAlert
                            success
                            style={{ display: "block", marginTop: "100px" }}
                            title="Success"
                            onConfirm={() => this.hideAlert()}
                            onCancel={() => this.hideAlert()}
                            confirmBtnBsStyle="success"
                            confirmBtnText="Ok"
                            btnSize=""
                        >
                            A LINK HAS BEEN SENT TO YOUR PRIMARY EMAIL ADDRESS
                        </ReactBSAlert>
                    )
                })
            }else{
                this.setState({
                    reset_password_response: (
                        <ReactBSAlert
                            warning
                            style={{ display: "block", marginTop: "100px" }}
                            title="Warning"
                            onConfirm={() => this.hideAlert()}
                            onCancel={() => this.hideAlert()}
                            confirmBtnBsStyle="warning"
                            confirmBtnText="Try Again"
                            btnSize=""
                        >
                            RESET PASSWORD FAILED, PLEASE CONTACT ADMINISTRATOR
                        </ReactBSAlert>
                    )
                })
            }
        })

    }

    update = (e) => {
        this.setState({
            [e.target.name] : e.target.value,
            displayName_valid : false,
            phoneNumber_valid : false,
            email_valid : false
        })

        this.validate(e)
    }
    validate = e => {
        // console.log(this.state,e.target.name,e.target.value)
        if(e.target.name === "displayName"){
            if(!e.target.value || e.target.value.length < 3)
                return this.setState({
                    displayName_error : true
                })

            return this.setState({
                displayName_valid: true,
                displayName_error: false
            })
        }
        if (e.target.name === "phoneNumber") {
            if (!e.target.value)
                return this.setState({
                    phoneNumber_error: true
                })
            if(!e.target.value.match(/^\+[0-9]{1,}[0-9]{10}$/))
                return this.setState({
                    phoneNumber_error: true
                })
            return this.setState({
                phoneNumber_valid: true,
                phoneNumber_error: false
            })
        }
        if(e.target.name === "email"){
            if (!e.target.value)
                return this.setState({
                    email_error: true
                })
            if (!e.target.value.match(/^.{3,}@.{3,}\..{2,}$/))
                return this.setState({
                    email_error: true
                })
            return this.setState({
                email_valid : true,
                email_error: false
            })
        }
    }

    render() {

        return (
            <Presentation
                {...this.props}
                {...this.state}
                updateImage={this.updateImage}
                update={this.update}
                requestResetPassword={this.requestResetPassword}
                handleTagsinput1={this.handleTagsinput1}
                handleTagsinput2={this.handleTagsinput2}
            />
        )
    }
}


export default Container