import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';
import ReactBSAlert from "react-bootstrap-sweetalert";
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    
    Row,
    Col,
    CardFooter,
    Label,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";

class Container extends Component{
    
    
    state={
        
        isDisabled : true,
        instCode:'',
        instName:'',
        instShortName:'',
        instAddr:'',
        instEmail:'',
        instNo:'',
        
       
    }
    updateState = (e) => {
        // console.log(this.state)
        this.setState({
                [e.target.name] : e.target.value,
        
                instCode_error : false,
                instCode_valid : false,
                instName_error: false,
                instName_valid: false,
                instShortName_error: false,
                instShortName_valid: false,
                instAddr_error: false,
                instAddr_valid:false,
                instEmail_error: false,
                instEmail_valid:false,
                instNo_error: false,
                instNo_valid:false,
                
        })
        this.validate(e)
       // console.log("hello"+this.state)
    }
    validate =(e) =>{
        
        if (e.target.name === "instCode") {
            if (e.target.value) {

                return this.setState({
                    instCode_valid: true,
                    instCode_error: false
                })
            }else{
                this.setState({
                    instName:"",
                    instShortName:"",
                    instAddr:"",
                    instEmail:"",
                    instNo:"",
                    instCode_error: true
                })    
            }
        }
        if (e.target.name === "instName") {
            if (e.target.value) {

                return this.setState({
                    instName_valid: true,
                    instName_error: false
                })
            }else{
                this.setState({
                    
                    instShortName:"",
                    instAddr:"",
                    instEmail:"",
                    instNo:"",
                    instName_error: true
                })    
            }
        }
        if (e.target.name === "instShortName") {
            if (e.target.value) {

                return this.setState({
                    instShortName_valid: true,
                    instShortName_error: false
                })
            }else{
                this.setState({
                    
                    instAddr:"",
                    instEmail:"",
                    instNo:"",
                    instShortName_error: true
                })    
            }
        }
        if (e.target.name === "instAddr") {
            if (e.target.value) {

                return this.setState({
                    instAddr_valid: true,
                    instAddr_error: false
                })
            }else{
                this.setState({
                   
                    instEmail:"",
                    instNo:"",
                    instAddr_error: true
                })    
            }
        }
        if (e.target.name === "instEmail") {
            if (e.target.value.match(/^.+@.+\..+$/)) {
                console.log("valid email::"+e.target.value)
                return this.setState({
                    instEmail_valid: true,
                    instEmail_error: false
                })
            }else{
                this.setState({
                   
                    instNo:"",
                    instEmail_error: true
                })    
            }
        }
        if (e.target.name === "instNo") {
            if (e.target.value.match(/^\+[0-9]{1,}[0-9]{10}$/)) {
                console.log("valid no::"+e.target.value)
                return this.setState({
                    instNo_valid: true,
                    instNo_error: false
                })
            }else{
                this.setState({
                   
                    instNo_error: true
                })    
            }
        }
    }
    
    instSetup =(e)=>{
        e.preventDefault();
        console.log(this.state);
        console.log("Confirmed!!")
    }

    
    render(){
        
        return(
            <Presentation
                {...this.state}
                {...this.props}
                updateState={this.updateState}
                //Add={this.Add}
                instSetup={this.instSetup}
                
            />
        )        
    }
}

export default Container