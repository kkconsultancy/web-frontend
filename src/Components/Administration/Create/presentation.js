import React from 'react'
import classnames from "classnames";
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardFooter,
    Label,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";
import Header from '../../Header'


class Presentation extends React.Component{

    render(){
        return(
          <>
           <Header middleware={this.props.middleware} pageTitle="ADMINISTRATION SETUP" />
            <Container className="mt--8" fluid>
            <Row className="justify-content-center">
              <Col className="order-xl-1" xl="9">
                  <Card className="bg-secondary shadow">
                    <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Administration Setup</h3>
                             </Col>
                           </Row>
                    </CardHeader>
                           <CardBody>
                            <Form>
                              <h6 className="heading-small text-muted mb-4">
                                Instituion Details
                              </h6>
                              <div className="pl-lg-4">
                                <Row>
                                  <Col lg="12">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                     Instituion Code
                                    </label>
                                    <Input type="text"  value={this.props.instCode} name="instCode"  className={"login-input "} onChange={this.props.updateState} >
                                    
                                  </Input>
                                  
                                  </FormGroup>
                                  </Col>
                                 
                                  {/* <div style={{ display: (this.props.instCode==="") ? "none" : "" }}> */}
                                  
                                  <Col lg="6">
                                  
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                     Instituion Name
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                    name="instName"
                                    disabled={this.props.instCode ? false : true}
                                    value={this.props.instName}
                                      onChange={this.props.updateState}
                                      placeholder="Enter Instituion Name"
                                      type="text"
                                     />
                                    </InputGroup>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Instituion ShortName
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                     disabled={this.props.instName ? false : true}
                                     value={this.props.instShortName}
                                      name="instShortName"
                                      onChange={this.props.updateState}
                                      placeholder="Enter Instituion ShortName"
                                      type="text"
                                     />
                                    </InputGroup>
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                     Address
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                     disabled={this.props.instShortName ? false : true}
                                     value={this.props.instAddr}
                                      name="instAddr"
                                      onChange={this.props.updateState}
                                      placeholder="Enter Instituion Address"
                                      type="text"
                                     />
                                    </InputGroup>
                                    
                                  </FormGroup>
                                  </Col>
                                  {/* </div> */}
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                     Email ID
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                     disabled={this.props.instAddr ? false : true}
                                     value={this.props.instEmail}
                                      name="instEmail"
                                      onChange={this.props.updateState}
                                      placeholder="Enter Instituion Email ID"
                                      type="text"
                                     />
                                    </InputGroup>
                                    
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                  <FormGroup>
                                    <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                     Contact No.
                                    </label>
                                    <InputGroup
                                      className={classnames("input-group-merge",)}
                                    >
                                    <Input
                                     disabled={this.props.instEmail ? false : true}
                                     value={this.props.instNo}
                                      name="instNo"
                                      onChange={this.props.updateState}
                                      placeholder="Enter Institution Contact no."
                                      type="text"
                                     />
                                    </InputGroup>
                                    
                                  </FormGroup>
                                  </Col>
                                  <Col lg="6">
                                    <Form>
                                    <Button className="btn-icon" color="primary" 
                                    disabled={this.props.instCode||this.props.instName||this.props.instShortName||this.props.instAddr||this.props.instEmail||this.props.instNo ? false : true} 
                                    type="button" 
                                    onClick={this.props.instSetup}>
                                        <span className="btn-inner--icon mr-1">
                                          <i className="ni ni-bag-17" />
                                        </span>
                                        <span className="btn-inner--text">Confirm </span>
                                    </Button>
                                    </Form>
                                    </Col> 
                                </Row>
                            </div>
                            </Form>
                          </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>

          </>

        )
    }
}
export default Presentation;