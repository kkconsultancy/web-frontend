import React from 'react'
import classnames from "classnames";
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// plugin that creates slider
import Slider from "nouislider";
// react plugin that creates text editor
import ReactQuill from "react-quill";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
import { Navbar,Nav } from "reactstrap";


import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
    CardFooter,
    Label,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
  
   
  
    
    
    
   
    
  
    
} from "reactstrap";
import Header from '../../Header'


class Presentation extends React.Component{

  state = {
    select: null,
    tagsinput: ["Bucharest", "Cluj", "Iasi", "Timisoara", "Piatra Neamt"],
    
  };
  updateState = (e) => {
    //  console.log(e)
    this.setState({
            [e.target.name] : e.target.value,
            
    })
  }

 

    render(){
        return(
          <>
           <Header middleware={this.props.middleware} pageTitle="MODIFY ATTENDANCE" />
            <Container className="mt--8" fluid>
            
            <Row className="justify-content-center">
              <Col className="order-xl-1" xl="12">
                  <Card className="bg-secondary shadow">
                    <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Academics Create</h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <Row>
                           <CardBody>
                           <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Select Semister
                                    </label>
                                    <Input type="select"  value={this.props.selectSemister} name="selectSemister"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    </Form>
                                        
                              </Col>
                              <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Select Semister
                                    </label>
                                    <Input type="select"  value={this.props.selectSemister} name="selectSemister"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    </Form>
                                        
                              </Col>
                              <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Select Semister
                                    </label>
                                    <Input type="select"  name="selectSemister"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    </Form>
                                        
                              </Col>
                              <Col lg="6">
                                     <Form>
                                     <label
                                      className="form-control-label"
                                      htmlFor="input-username"
                                    >
                                      Select Semister
                                    </label>
                                    <Input type="select"  value={this.props.selectSemister} name="selectSemister"  className={"login-input "} onChange={this.props.updateState} >
                                    <option value="">---</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                  </Input>
                                    </Form>
                                        
                              </Col>
                          </CardBody>
                        </Row>  
             </Card>
             <Row>
             <Col lg="6">
             <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Add </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                    <Row className="align-items-center">
                        {this.props.jsx1}
                        </Row>
                    </CardBody>

                </Card>
                </Col>
                <Col lg="6">
             <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                           <Row className="align-items-center">
                             <Col xs="8">
                               <h3 className="mb-0">Added </h3>
                             </Col>
                           </Row>
                    </CardHeader>
                    <CardBody>
                      <Row>
                     
                      {this.props.jsx2}
                      
                       </Row>
                    </CardBody>

                </Card>
                </Col>
                </Row>

                </Col>
               
               
                
                
                
                
              </Row>
            </Container>

          </>

























                

        )
    }
}
export default Presentation;