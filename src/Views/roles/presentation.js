import React from 'react'
import {Route,Switch} from 'react-router-dom'

import Create from '../../Components/Roles/Create'
import Update from '../../Components/Roles/Update'
import View from '../../Components/Roles/View'
import Map from '../../Components/Roles/Map'
import Roles from '../../Components/Roles'


class Presentation extends React.Component {
    render() {

        const { path } = this.props.match;
        return (
           <Switch>
                <Route exact path={path + "/"} component={() => <Roles middleware={this.props.middleware}/>}/>
                <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
                <Route path={path + "/map"} component={() => <Map middleware={this.props.middleware}/>}/>
                <Route path={path + "/update"} component={() => <Update middleware={this.props.middleware}/>}/>
                <Route path={path + "/view"} component={() => <View middleware={this.props.middleware}/>}/>
           </Switch>
        )
    }
}

export default Presentation