import React from 'react'
import Presentation from './presentation'


class Container extends React.Component {
    render() {
        // console.log(this.props)
        return (
            <Presentation 
                {...this.props}
            />
        )
    }
}


export default Container