import React from 'react'
// nodejs library to set properties for components
import { PropTypes } from "prop-types";

import routes from "../../Template/routes"
import Sidebar from "../../Components/Sidebar"

Sidebar.defaultProps = {
    routes: [{}]
};

Sidebar.propTypes = {
    // links that will be displayed inside the component
    routes: PropTypes.arrayOf(PropTypes.object),
    logo: PropTypes.shape({
        // innerLink is for links that will direct the user within the app
        // it will be rendered as <Link to="...">...</Link> tag
        innerLink: PropTypes.string,
        // outterLink is for links that will direct the user outside the app
        // it will be rendered as simple <a href="...">...</a> tag
        outterLink: PropTypes.string,
        // the image src of the logo
        imgSrc: PropTypes.string.isRequired,
        // the alt for the img
        imgAlt: PropTypes.string.isRequired
    })
};

class Presentation extends React.Component {

    render() {
        return (
            <Sidebar
                {...this.props}
                routes={routes}
                logo={{
                    innerLink: "/app",
                    imgSrc: require("../../Template/assets/img/brand/argon-react.png"),
                    imgAlt: "..."
                }}
                middleware = {this.props.middleware}
            />
        )
    }
}
export default Presentation;