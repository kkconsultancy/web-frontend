import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import Presentation from './presentation'
import Dashboard from './dashboard'


class Container extends React.Component {

    render() {
        const { path } = this.props.match;
        
        //console.log(this.props.middleware)
        if (this.props.authenticated === "DONE")
            return (
                <Switch>
                    <Redirect exact from={"/app/login**"} to="/app"/>
                    <Route exact path={path + "**"} component={() => <Dashboard middleware={this.props.middleware} />} />
                </Switch>
            )
        return (
            <Switch>
                <Route path={path + "**"} component={() => <Presentation middleware={this.props.middleware} />} />
            </Switch>
        )
    }
}

export default Container