import React from 'react'
import {Route,Switch} from 'react-router-dom'

import NotFound from '../../Components/NotFound'


class Presentation extends React.Component {
    render() {
        return (
           <Switch>
                {this.props.routes}
                <NotFound/>
           </Switch>
        )
    }
}

export default Presentation