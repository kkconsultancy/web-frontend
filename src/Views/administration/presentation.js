import React from 'react'
import {Route,Switch} from 'react-router-dom'

import Create from '../../Components/Administration/Create'
import Update from '../../Components/Administration/Update'
import View from '../../Components/Administration/View'
import Administration from '../../Components/Administration'


class Presentation extends React.Component {
    render() {

        const { path } = this.props.match;
        return (
           <Switch>
                <Route exact path={path + "/"} component={() => <Administration middleware={this.props.middleware}/>}/>
                <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
                <Route path={path + "/update"} component={() => <Update middleware={this.props.middleware}/>}/>
                {/* <Route path={path + "/update2"} component={() => <Update2 middleware={this.props.middleware}/>}/> */}
                <Route path={path + "/view"} component={() => <View middleware={this.props.middleware}/>}/>
           </Switch>
        )
    }
}

export default Presentation