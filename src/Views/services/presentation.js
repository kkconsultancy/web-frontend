import React from 'react'
import {Route,Switch} from 'react-router-dom'

import Create from '../../Components/Services/Create'
import Update from '../../Components/Services/Update'
import View from '../../Components/Services/View'
import Map from '../../Components/Services/Map'
import Services from '../../Components/Services'


class Presentation extends React.Component {
    render() {

        const { path } = this.props.match;
        return (
           <Switch>
                <Route exact path={path + "/"} component={() => <Services middleware={this.props.middleware}/>}/>
                <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
                <Route path={path + "/map"} component={() => <Map middleware={this.props.middleware}/>}/>
                <Route path={path + "/update"} component={() => <Update middleware={this.props.middleware}/>}/>
                <Route path={path + "/view"} component={() => <View middleware={this.props.middleware}/>}/>
           </Switch>
        )
    }
}

export default Presentation