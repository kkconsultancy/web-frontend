import React from 'react'
import {Route,Switch} from 'react-router-dom'

import Profile from '../../Components/Profile'

class Presentation extends React.Component {
    render() {

        const { path } = this.props.match;
        return (
           <Switch>
                <Route path={path + "/"} component={() => <Profile middleware={this.props.middleware}/>}/>
           </Switch>
        )
    }
}

export default Presentation