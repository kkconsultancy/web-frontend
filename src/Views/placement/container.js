import React from 'react'
import Presentation from './presentation'
import {Route} from 'react-router-dom'

import Create from '../../Components/Placement/Create'
import Update from '../../Components/Placement/Update'
import Setup from '../../Components/Placement/Setup'
import Add from '../../Components/Placement/AddStudentPlacement'
import Placed from '../../Components/Placement/SelectedPersons'
// import View from '../../Components/Marks/View'
import Marks from '../../Components/Placement'

class Container extends React.Component {

    state = {
        links : []
    }

    componentWillMount(){
        let { path } = this.props.match;
        const routes = [
            <Route exact path={path + "/"} component={() => <Marks middleware={this.props.middleware} links={this.state.links}/>}/>
        ]
        const links = [
            {
                name : "CREATE",
                path : "/app/placements/create",
                icon : "",
                component : <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
            },
            // {
            //     name : "UPDATE",
            //     path : "/app/placements/update",
            //     icon : "",
            //     component : <Route path={path + "/update"} component={() => <Update middleware={this.props.middleware}/>}/>
            // },
            {
                name : "SETUP",
                path : "/app/placements/setup",
                icon : "",
                component : <Route path={path + "/setup"} component={() => <Setup middleware={this.props.middleware}/>}/>
            },{
                name : "ADD STUDENTS",
                path : "/app/placements/add",
                icon : "",
                component : <Route path={path + "/add"} component={() => <Add middleware={this.props.middleware}/>}/>
            },{
                name : "PLACED STUDENTS",
                path : "/app/placements/placed",
                icon : "",
                component : <Route path={path + "/placed"} component={() => <Placed middleware={this.props.middleware}/>}/>
            },
            
        ]

        path = path.split("/").splice(-1)[0]
        const authorisation = this.props.middleware.state.authorisation
        
        if(authorisation && authorisation.services){
            // console.log(authorisation["services"])
            const services = authorisation.services[path] || {}
            const linksJSX = links.filter(link => services[link.name.toLowerCase()] || true).map(link => {
                routes.push(link.component)
                return link
            })
            // console.log(links,routes)
            this.setState({
                links : linksJSX,
                routes : routes
            })
        }
    }
    

    render() {
        // console.log(this.props)
        return (
            <Presentation 
                {...this.props}
                {...this.state}
            />
        )
    }
}


export default Container