import React from 'react'
import {Route,Switch,Redirect} from 'react-router-dom'

import Users from '../../Components/Users'
import Create from '../../Components/Users/Create'
import List from '../../Components/Users/List'
import Update from '../../Components/Users/Update'

class Presentation extends React.Component {
    render() {

        const { path } = this.props.match;
        return (
           <Switch>
                <Route exact path={path + "/"} component={() => <Users middleware={this.props.middleware}/>}/>
                <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
                <Route path={path + "/list"} component={() => <List middleware={this.props.middleware} />} />
                <Route path={path + "/update/:uid"} component={() => <Update middleware={this.props.middleware} />} />
                <Redirect from={path + "/update"} to={path + "/list"} />
           </Switch>
        )
    }
}

export default Presentation