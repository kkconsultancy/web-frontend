import React from 'react'
import {Switch} from 'react-router-dom'
import NotFound from '../../Components/NotFound'
// import Dashboard from '../../Components/Dashboard'


class Presentation extends React.Component {
    render() {
        // console.log(this.props)
        return (
            <div className="main-content">      
                <Switch>
                    {this.props.routes}
                    <NotFound/>
                </Switch>
            </div>
        )
    }
}

export default Presentation