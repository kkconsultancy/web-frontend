import React from 'react'
import Presentation from './presentation'
import {Route,Switch} from 'react-router-dom'

import Dashboard from '../../Components/Dashboard'

import Profile from '../profile'
import Users from '../users'
import Attendance from '../attendance'
import Marks from '../marks'
import Academics from '../academics'
import Hostel from '../hostel'
import Placement from '../placement'
import Transport from '../transport'
import Administration from '../administration'
import Services from '../services'
import Roles from '../roles'

{/* <Route
                                    path="/app/hostel"
                                    component={() => <Hostel middleware={this.props.middleware} />}
                                />
                                <Route
                                    path="/app/placement"
                                    component={() => <Placement middleware={this.props.middleware} />}
                                />
                                <Route
                                    path="/app/transport"
                                    component={() => <Transport middleware={this.props.middleware} />}
                                /> */}

class Container extends React.Component {

    state = {
        links : []
    }

    componentWillMount(){
        // console.log(this.props)
        let { path } = this.props.match;
        // console.log(path)
        const routes = [
            <Route
                path="/app/profile"
                component={() => <Profile middleware={this.props.middleware} />}
            />,
            <Route 
              path="/app/logout"
              component={() => {
                  this.props.middleware.logout()
                  return(<></>)
              }}
            />,
            <Route exact path={path + "/"} component={() => <Dashboard middleware={this.props.middleware} links={this.state.links}/>}/>
        ]
        const links = [
          {
            name : "USERS",
            path : "/app/users",
            icon : "",
            component : (<Route
              path="/app/users"
              component={() => <Users middleware={this.props.middleware} />}
            />)
        },{
              name : "SERVICES",
              path : "/app/services",
              icon : "",
              component : (<Route
                path="/app/services"
                component={() => <Services middleware={this.props.middleware} />}
              />)
          },{
              name : "ROLES",
              path : "/app/roles",
              icon : "",
              component : (<Route
                path="/app/roles"
                component={() => <Roles middleware={this.props.middleware} />}
              />)
          },
          {
              name : "ATTENDANCE",
              path : "/app/attendance",
              icon : "",
              component : (<Route
                path="/app/attendance"
                component={() => <Attendance middleware={this.props.middleware} />}
              />)
          },
          {
              name : "MARKS",
              path : "/app/marks",
              icon : "",
              component : (<Route
                path="/app/marks"
                component={() => <Marks middleware={this.props.middleware} />}
              />)
          },{
              name : "ACADEMICS",
              path : "/app/academics",
              icon : "",
              component : (<Route
                path="/app/academics"
                component={() => <Academics middleware={this.props.middleware} />}
              />)
          },
          {
              name : "ADMINISTRATION",
              path : "/app/administration",
              icon : "",
              component : (<Route
                path="/app/administration"
                component={() => <Administration middleware={this.props.middleware} />}
              />)
          },{
            name : "PLACEMENTS",
            path : "/app/placements",
            icon : "",
            component : (<Route
              path="/app/placements"
              component={() => <Placement middleware={this.props.middleware} />}
            />)
        },{
          name : "TRANSPORT",
          path : "/app/transport",
          icon : "",
          component : (<Route
            path="/app/transport"
            component={() => <Transport middleware={this.props.middleware} />}
          />)
      },{
            name : "Hostel",
            path : "/app/hostel",
            icon : "",
            component : (<Route
              path="/app/hostel"
              component={() => <Hostel middleware={this.props.middleware} />}
            />)
        }
          
      ]

        path = path.split("/").splice(-1)[0]
        const authorisation = this.props.middleware.state.authorisation
        
        if(authorisation && authorisation.services){
            // console.log(authorisation["services"])
            const services = authorisation.services || {}
            const linksJSX = links.filter(link => services[link.name.toLowerCase()]).map(link => {
                // console.log(link)
                routes.unshift(link.component)
                return link
            })
            console.log(services,linksJSX,routes)
            this.setState({
                links : linksJSX,
                routes : routes
            })
        }
    }
    

    render() {
        // console.log(this.props)
        return (
            <Presentation 
                {...this.props}
                {...this.state}
            />
        )
    }
}


export default Container