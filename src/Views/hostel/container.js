import React from 'react'
import Presentation from './presentation'
import {Route} from 'react-router-dom'

import Create from '../../Components/Hostel/Create'
import Update from '../../Components/Hostel/Update'
import View from '../../Components/Hostel/View'
import Setup from '../../Components/Hostel/Setup'
import Outing from '../../Components/Hostel/Outingpass'
import Rooms from '../../Components/Hostel/Roomsetup'
import Marks from '../../Components/Hostel'

class Container extends React.Component {

    state = {
        links : []
    }

    componentWillMount(){
        let { path } = this.props.match;
        const routes = [
            <Route exact path={path + "/"} component={() => <Marks middleware={this.props.middleware} links={this.state.links}/>}/>
        ]
        const links = [
            {
                name : "CREATE",
                path : "/app/hostel/create",
                icon : "",
                component : <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
            },
            {
                name : "UPDATE",
                path : "/app/hostel/update",
                icon : "",
                component : <Route path={path + "/update"} component={() => <Update middleware={this.props.middleware}/>}/>
            },{
                name : "VIEW",
                path : "/app/hostel/view",
                icon : "",
                component : <Route path={path + "/view"} component={() => <View middleware={this.props.middleware}/>}/>
            },{
                name : "SETUP",
                path : "/app/hostel/setup",
                icon : "",
                component : <Route path={path + "/setup"} component={() => <Setup middleware={this.props.middleware}/>}/>
            },{
                name : "OUTING PERMISSIONS",
                path : "/app/hostel/outing",
                icon : "",
                component : <Route path={path + "/outing"} component={() => <Outing middleware={this.props.middleware}/>}/>
            },{
                name : "ROOMS ARRANGEMENT",
                path : "/app/hostel/rooms",
                icon : "",
                component : <Route path={path + "/rooms"} component={() => <Rooms middleware={this.props.middleware}/>}/>
            },
            
        ]

        path = path.split("/").splice(-1)[0]
        const authorisation = this.props.middleware.state.authorisation
        
        if(authorisation && authorisation.services){
            // console.log(authorisation["services"])
            const services = authorisation.services[path] || {}
            const linksJSX = links.filter(link => services[link.name.toLowerCase()] || true).map(link => {
                routes.push(link.component)
                return link
            })
            // console.log(links,routes)
            this.setState({
                links : linksJSX,
                routes : routes
            })
        }
    }
    

    render() {
        // console.log(this.props)
        return (
            <Presentation 
                {...this.props}
                {...this.state}
            />
        )
    }
}


export default Container