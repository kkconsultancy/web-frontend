import React from 'react'
import Presentation from './presentation'
import {Route} from 'react-router-dom'

import Create from '../../Components/Marks/Create'
import Update from '../../Components/Marks/Update'
// import View from '../../Components/Marks/View'
import Marks from '../../Components/Marks'

class Container extends React.Component {

    state = {
        links : []
    }

    componentWillMount(){
        let { path } = this.props.match;
        const routes = [
            <Route exact path={path + "/"} component={() => <Marks middleware={this.props.middleware} links={this.state.links}/>}/>
        ]
        const links = [
            {
                name : "CREATE",
                path : "/app/marks/create",
                icon : "",
                component : <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
            },
            {
                name : "UPDATE",
                path : "/app/marks/update",
                icon : "",
                component : <Route path={path + "/update"} component={() => <Update middleware={this.props.middleware}/>}/>
            },
            // {
            //     name : "VIEW",
            //     path : "/app/marks/view",
            //     icon : "",
            //     component : <Route path={path + "/view"} component={() => <View middleware={this.props.middleware}/>}/>
            // }
            
        ]

        path = path.split("/").splice(-1)[0]
        const authorisation = this.props.middleware.state.authorisation
        
        if(authorisation && authorisation.services){
            // console.log(authorisation["services"])
            const services = authorisation.services[path] || {}
            const linksJSX = links.filter(link => services[link.name.toLowerCase()]).map(link => {
                routes.push(link.component)
                return link
            })
            // console.log(links,routes)
            this.setState({
                links : linksJSX,
                routes : routes
            })
        }
    }
    

    render() {
        // console.log(this.props)
        return (
            <Presentation 
                {...this.props}
                {...this.state}
            />
        )
    }
}


export default Container