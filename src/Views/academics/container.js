import React from 'react'
import Presentation from './presentation'
import {Route,Switch} from 'react-router-dom'

import Create from '../../Components/Academics/Create'
import Update from '../../Components/Academics/Update'
import View from '../../Components/Academics/View'
import Map from '../../Components/Academics/Map'

import CourseSetup from '../../Components/Academics/CourseSetup'
import BranchSetup from '../../Components/Academics/BranchSetup'
import SectionSetup from '../../Components/Academics/SectionSetup'
import SubjectSetup from '../../Components/Academics/SubjectSetup'
import Academics from '../../Components/Academics'


{/* <Route path={path + "/create"} component={() => <Create middleware={this.props.middleware}/>}/>
<Route path={path + "/map"} component={() => <Map middleware={this.props.middleware}/>}/>
<Route path={path + "/update"} component={() => <Update middleware={this.props.middleware}/>}/>
<Route path={path + "/view"} component={() => <View middleware={this.props.middleware}/>}/>
<Route path={path + "/coursesetup"} component={() => <CourseSetup middleware={this.props.middleware}/>}/>
<Route path={path + "/branchsetup"} component={() => <BranchSetup middleware={this.props.middleware}/>}/>
<Route path={path + "/sectionsetup"} component={() => <SectionSetup middleware={this.props.middleware}/>}/> */}

class Container extends React.Component {

    state = {
        links : []
    }

    componentWillMount(){
        let { path } = this.props.match;
        const routes = [
            <Route exact path={path + "/"} component={() => <Academics middleware={this.props.middleware} links={this.state.links} />} />
        ]
        const links = [
            // {
            //     name : "YEAR",
            //     path : "/app/academics/year",
            //     icon : "",
            //     component : <Route path={path + "/year"} component={() => <Create middleware={this.props.middleware}/>}/>
            // },
            {
                name : "BRANCH",
                path : "/app/academics/branch",
                icon : "",
                component : <Route path={path + "/branch"} component={() => <BranchSetup middleware={this.props.middleware}/>}/>
            },{
                name : "COURSE",
                path : "/app/academics/course",
                icon : "",
                component : <Route path={path + "/course"} component={() => <CourseSetup middleware={this.props.middleware}/>}/>
            },{
                name : "SECTION",
                path : "/app/academics/section",
                icon : "",
                component : <Route path={path + "/section"} component={() => <SectionSetup middleware={this.props.middleware}/>}/>
            },{
                name : "SUBJECT",
                path : "/app/academics/subject",
                icon : "",
                component : <Route path={path + "/subject"} component={() => <SubjectSetup middleware={this.props.middleware}/>}/>
            },{
                name : "MAP",
                path : "/app/academics/map",
                icon : "",
                component : <Route path={path + "/map"} component={() => <Map middleware={this.props.middleware}/>}/>
            },{
                name : "VIEW",
                path : "/app/academics/view",
                icon : "",
                component : <Route path={path + "/view"} component={() => <View middleware={this.props.middleware}/>}/>
            }
            ,
            
        ]

        path = path.split("/").splice(-1)[0]
        const authorisation = this.props.middleware.state.authorisation
        
        if(authorisation && authorisation.services){
            // console.log(authorisation["services"])
            const services = authorisation.services[path] || {}
            const linksJSX = links.filter(link => services[link.name.toLowerCase()]).map(link => {
                routes.push(link.component)
                return link
            })
            // console.log(links,routes)
            this.setState({
                links : linksJSX,
                routes : routes
            })
        }
    }
    

    render() {
        // console.log(this.props)
        return (
            <Presentation 
                {...this.props}
                {...this.state}
            />
        )
    }
}


export default Container