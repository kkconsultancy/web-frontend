import React from 'react'

import NotFound from '../../Components/NotFound'
import {Route,Switch} from 'react-router-dom'




class Presentation extends React.Component {
    render() {
        const { path } = this.props.match;
        return (
           <Switch>
                {this.props.routes}
                
                <NotFound/>
           </Switch>
        )
    }
}

export default Presentation