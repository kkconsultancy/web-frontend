import React from 'react'

import LoginUsername from '../../Components/Login_Username'

// import LoginToken from '../../Components/Login_Token'

class Presentation extends React.Component {
    render() {
        console.log(this.props)
        if(!this.props.middleware.state.auth.currentUser)
        return (
            <LoginUsername middleware={this.props.middleware}/>
        )
        else
            return(
                <h1>
                    Token:<h4>{this.props.middleware.state.JWT.token}</h4>
                    Custom Token<h4>{this.props.middleware.state.JWT.custom_token}</h4>
                </h1>
            )
    }
}

export default Presentation