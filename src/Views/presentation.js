import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
// reactstrap components
import { Link } from "react-router-dom";
// reactstrap components
import {
    UncontrolledCollapse,
    NavbarBrand,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    Row, 
    Col,
    UncontrolledTooltip,
    Button
} from "reactstrap";

// core components
import LoginUsername from '../Components/Login_Username'
import LoginToken from '../Components/Login_Token'
// import Footer from "../Components/Footer";
import NotFound from "../Components/NotFound"

class Presentation extends React.Component {
    render() {
        return (
            <>
                <div className="main-content">
                    <Navbar
                        className="navbar-horizontal navbar-main navbar-dark navbar-transparent"
                        expand="lg"
                        id="navbar-main"
                    >
                        <Container>
                            <NavbarBrand to="/" tag={Link}>
                                <div className="display-3">
                                    <img
                                        alt="AAA AUTOMATION"
                                        src={""}
                                    />
                                </div>
                            </NavbarBrand>
                            <button
                                aria-controls="navbar-collapse"
                                aria-expanded={false}
                                aria-label="Toggle navigation"
                                className="navbar-toggler"
                                data-target="#navbar-collapse"
                                data-toggle="collapse"
                                id="navbar-collapse"
                                type="button"
                            >
                                <span className="navbar-toggler-icon" />
                            </button>
                            <UncontrolledCollapse
                                className="navbar-custom-collapse"
                                navbar
                                toggler="#navbar-collapse"
                            >
                                <div className="navbar-collapse-header">
                                    <Row>
                                        <Col className="collapse-brand">
                                            <Link to="/admin/dashboard">
                                                <img
                                                    alt="..."
                                                    src={require("../Template/assets/img/brand/blue.png")}
                                                />
                                            </Link>
                                        </Col>
                                        <Col className="collapse-close" xs="6">
                                            <button
                                                aria-controls="navbar-collapse"
                                                aria-expanded={false}
                                                aria-label="Toggle navigation"
                                                className="navbar-toggler"
                                                data-target="#navbar-collapse"
                                                data-toggle="collapse"
                                                id="navbar-collapse"
                                                type="button"
                                            >
                                                <span />
                                                <span />
                                            </button>
                                        </Col>
                                    </Row>
                                </div>
                                <Nav className="align-items-lg-center ml-lg-auto" navbar>
                                    <NavItem>
                                        <NavLink
                                            className="nav-link-icon"
                                            href="https://www.facebook.com/developerswork"
                                            id="tooltip601201423"
                                            target="_blank"
                                        >
                                            <i className="fab fa-facebook-square" />
                                            <span className="nav-link-inner--text d-lg-none">
                                                Facebook
                                            </span>
                                        </NavLink>
                                        <UncontrolledTooltip delay={0} target="tooltip601201423">
                                            Like us on Facebook
                  </UncontrolledTooltip>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className="nav-link-icon"
                                            href="https://www.instagram.com/developerswork"
                                            id="tooltip871243015"
                                            target="_blank"
                                        >
                                            <i className="fab fa-instagram" />
                                            <span className="nav-link-inner--text d-lg-none">
                                                Instagram
                    </span>
                                        </NavLink>
                                        <UncontrolledTooltip delay={0} target="tooltip871243015">
                                            Follow us on Instagram
                  </UncontrolledTooltip>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className="nav-link-icon"
                                            href="https://twitter.com/developersworky"
                                            id="tooltip366258619"
                                            target="_blank"
                                        >
                                            <i className="fab fa-twitter-square" />
                                            <span className="nav-link-inner--text d-lg-none">
                                                Twitter
                    </span>
                                        </NavLink>
                                        <UncontrolledTooltip delay={0} target="tooltip366258619">
                                            Follow us on Twitter
                  </UncontrolledTooltip>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className="nav-link-icon"
                                            href="https://github.com/developerswork"
                                            id="tooltip931502898"
                                            target="_blank"
                                        >
                                            <i className="fab fa-github" />
                                            <span className="nav-link-inner--text d-lg-none">
                                                Github
                                            </span>
                                        </NavLink>
                                        <UncontrolledTooltip delay={0} target="tooltip931502898">
                                            Star us on Github
                                        </UncontrolledTooltip>
                                    </NavItem>
                                    <NavItem className="d-lg-block ml-lg-4">
                                        <Link to="/app/login">
                                            <Button
                                                className="btn-neutral btn-icon"
                                                color="default"
                                                type="button"
                                            >
                                                <i className="ni ni-key-25" />
                                                <span className="nav-link-inner--text">Login</span>
                                            </Button>
                                        </Link>
                                    </NavItem>
                                </Nav>
                            </UncontrolledCollapse>
                        </Container>
                    </Navbar>
                    <div className="header bg-gradient-info py-7 py-lg-8">
                        <Container>
                            
                        </Container>
                        <div className="separator separator-bottom separator-skew zindex-100">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                preserveAspectRatio="none"
                                version="1.1"
                                viewBox="0 0 2560 100"
                                x="0"
                                y="0"
                            >
                                <polygon
                                    className="fill-default"
                                    points="2560 0 2560 100 0 100"
                                />
                            </svg>
                        </div>
                    </div>
                    {/* Page content */}
                    <Container className="mt--8 pb-5">
                        <Row className="justify-content-center">
                            <Switch>
                                <Route
                                    exact path="/app"
                                    component={() => (
                                                <Col lg="5" md="6">
                                                    <h1 className="text-white">Welcome!</h1>
                                                    <p className="text-lead text-light">
                                                        You have to Login First !!
                                                    </p>
                                                </Col>
                                    )}
                                />
                                {/* {this.getRoutes(routes)} */}
                                
                                <Route
                                    exact path="/app/login/"
                                    component={() => <LoginUsername middleware={this.props.middleware}/>}
                                />
                                <Route
                                    path="/app/login/token"
                                    component={() => <LoginToken middleware={this.props.middleware} />}
                                />
                                <Redirect from="/app/logout" to="/app/login/"/>
                                <Route
                                    path="/app/**"
                                    component={()=><NotFound/>}
                                />
                            </Switch>
                        </Row>
                    </Container>
                </div>
                {/* <Footer/> */}
            </>
        )
    }
}

export default Presentation