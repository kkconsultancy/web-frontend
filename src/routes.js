
const routes = [
  {
    path: "/",
    name: "Dashboard",
    icon: "ni ni-shop text-default",
    // component: Widgets,
    layout: "/app"
  },
  {
    collapse: true,
    name: "Users",
    icon: "ni ni-shop text-primary",
    state: "UsersCollapse",
    views: [
      {
        path: "/create",
        name: "Create",
        // component: manage-users,
        layout: "/app/users"
      },
      {
        path: "/activate",
        name: "Activate",
        // component: manage-users,
        layout: "/app/users"
      },
      {
        path: "/deactivate",
        name: "Deactivate",
        // component: manage-users,
        layout: "/app/users"
      },
      {
        path: "/view",
        name: "View",
        // component: manage-users,
        layout: "/app/users"
      },
    ]
  },
  {
    collapse: true,
    name: "Roles",
    icon: "circle-08",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/create",
        name: "Create",
        // component: manage-users,
        layout: "/app/roles"
      },
      {
        path: "/ativate",
        name: "Ativate",
        // component: manage-users,
        layout: "/app/roles"
      },
      {
        path: "/deativate",
        name: "deativate",
        // component: manage-users,
        layout: "/app/roles"
      },
      {
        path: "/modify",
        name: "Modify",
        // component: manage-users,
        layout: "/app/roles"
      },
      {
        path: "/view",
        name: "View",
        // component: manage-users,
        layout: "/app/roles"
      },
      
    ]
  },
  {
    collapse: true,
    name: "Services",
    icon: "circle-08",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/view",
        name: "Read",
        // component: manage-users,
        layout: "/app/services"
      },
      {
        path: "/map",
        name: "Map",
        // component: manage-users,
        layout: "/app/services"
      },
      {
        path: "/deactivate",
        name: "Deactivate",
        // component: manage-users,
        layout: "/app/services"
      },
      {
        path: "/activate",
        name: "Activate",
        // component: manage-users,
        layout: "/app/services"
      },
      
    ]
  },
  {
    collapse: true,
    name: "Academics",
    icon: "circle-08",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/create",
        name: "Create",
        // component: manage-users,
        layout: "/app/academics"
      },
      {
        path: "/update",
        name: "Update",
        // component: manage-users,
        layout: "/app/academics"
      },
      {
        path: "/map",
        name: "Map",
        // component: manage-users,
        layout: "/app/academics"
      },
      {
        path: "/deativate",
        name: "Deativate",
        // component: manage-users,
        layout: "/app/academics"
      },
    ]
  },
  {
    collapse: true,
    name: "Attendances",
    icon: "circle-08",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/view",
        name: "View",
        // component: manage-users,
        layout: "/app/attendances"
      },
      {
        path: "/modify",
        name: "Modify",
        // component: manage-users,
        layout: "/app/attendances"
      },
      {
        path: "/postattendances",
        name: "Post Attendances",
        // component: manage-users,
        layout: "/app/attendances"
      },
    ]
  },
  {
    collapse: true,
    name: "Markes",
    icon: "circle-08",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/view",
        name: "Read",
        // component: manage-users,
        layout: "/app/markes"
      },
      {
        path: "/update",
        name: "Update",
        // component: manage-users,
        layout: "/app/markes"
      },
      {
        path: "/postmarks",
        name: "Post Markes",
        // component: manage-users,
        layout: "/app/markers"
      },
    ]
  },
  {
    collapse: true,
    name: "Transport",
    icon: "circle-08",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/view",
        name: "view",
        // component: manage-users,
        layout: "/app/transport"
      },
      {
        path: "/activate",
        name: "Activate",
        // component: manage-users,
        layout: "/app/transport"
      },
      {
        path: "/deactivate",
        name: "Dectivate",
        // component: manage-users,
        layout: "/app/transport"
      },
      
      {
        path: "/allocate",
        name: "Allocate",
        // component: manage-users,
        layout: "/app/transport"
      },
      {
        path: "/update",
        name: "Update",
        // component: manage-users,
        layout: "/app/transport"
      },
    ]
  },
  {
    collapse: true,
    name: "Hostel",
    icon: "circle-08",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/view",
        name: "view",
        // component: manage-users,
        layout: "/app/hostel"
      },
      {
        path: "/activate",
        name: "Activate",
        // component: manage-users,
        layout: "/app/hostel"
      },
      {
        path: "/deactivate",
        name: "Dectivate",
        // component: manage-users,
        layout: "/app/hostel"
      },
      
      {
        path: "/allocate",
        name: "Allocate",
        // component: manage-users,
        layout: "/app/hostel"
      },
      {
        path: "/update",
        name: "Update",
        // component: manage-users,
        layout: "/app/hostel"
      },
    ]
  }
  
];

export default routes;
