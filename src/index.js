import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// plugins styles from node_modules
import "react-notification-alert/dist/animate.css";
import "react-perfect-scrollbar/dist/css/styles.css";
// plugins styles downloaded
import "./Template/assets/vendor/fullcalendar/dist/fullcalendar.min.css";
import "./Template/assets/vendor/sweetalert2/dist/sweetalert2.min.css";
import "./Template/assets/vendor/select2/dist/css/select2.min.css";
import "./Template/assets/vendor/quill/dist/quill.core.css";
import "./Template/assets/vendor/nucleo/css/nucleo.css";
import "./Template/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css";
// core styles
import "./Template/layouts/Documentation/assets-for-demo/docs.scss";
import "./Template/layouts/Documentation/assets-for-demo/react-docs.scss";
import "./Template/assets/scss/argon-dashboard-pro-react.scss";

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
