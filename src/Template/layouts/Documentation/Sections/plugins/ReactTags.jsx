/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";

const codeExample = `import React from "react";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";

class Tags extends React.Component {
  state = {
    tagsinput: ["Bucharest", "Cluj", "Iasi", "Timisoara", "Piatra Neamt"]
  };
  handleTagsinput = tagsinput => {
    this.setState({ tagsinput });
  };
  render() {
    return (
      <>
        <TagsInput
          className="bootstrap-tagsinput"
          onChange={this.handleTagsinput}
          tagProps={{ className: "tag badge mr-1" }}
          value={this.state.tagsinput}
          inputProps={{
            className: "",
            placeholder: ""
          }}
        />
      </>
    );
  }
}

export default Tags;
`;

class Tags extends React.Component {
  state = {
    tagsinput: ["Bucharest", "Cluj", "Iasi", "Timisoara", "Piatra Neamt"]
  };
  handleTagsinput = tagsinput => {
    this.setState({ tagsinput });
  };
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            React Tags v3.19.0
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          A simple plugin providing a Bootstrap user interface for managing
          tags.
        </p>
        <h2 id="example">Example</h2>
        <div className="ct-example bg-white">
          <TagsInput
            className="bootstrap-tagsinput"
            onChange={this.handleTagsinput}
            tagProps={{ className: "tag badge mr-1" }}
            value={this.state.tagsinput}
            inputProps={{
              className: "",
              placeholder: ""
            }}
          />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://github.com/olahol/react-tagsinput#readme?ref=creativetim"
            target="_blank"
          >
            react-tagsinput documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Tags;
