/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import { Alert, Button, ListGroupItem, ListGroup, Row, Col } from "reactstrap";

const codeSingle = `import React from "react";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import {
  Alert,
  Button,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

// we want to initialize ourselves this plugin
Dropzone.autoDiscover = false;

class Dropzones extends React.Component {
  componentDidMount() {
    // this variable is to delete the previous image from the dropzone state
    // it is just to make the HTML DOM a bit better, and keep it light
    let currentSingleFile = undefined;
    // single dropzone file - accepts only images
    new Dropzone(document.getElementById("dropzone-single"), {
      url: "/",
      thumbnailWidth: null,
      thumbnailHeight: null,
      previewsContainer: document.getElementsByClassName(
        "dz-preview-single"
      )[0],
      previewTemplate: document.getElementsByClassName("dz-preview-single")[0]
        .innerHTML,
      maxFiles: 1,
      acceptedFiles: "image/*",
      init: function() {
        this.on("addedfile", function(file) {
          if (currentSingleFile) {
            this.removeFile(currentSingleFile);
          }
          currentSingleFile = file;
        });
      }
    });
    document.getElementsByClassName("dz-preview-single")[0].innerHTML = "";
  }
  render() {
    return (
      <>
        <div className="dropzone dropzone-single mb-3" id="dropzone-single">
          <div className="fallback">
            <div className="custom-file">
              <input
                className="custom-file-input"
                id="projectCoverUploads"
                type="file"
              />
              <label
                className="custom-file-label"
                htmlFor="projectCoverUploads"
              >
                Choose file
              </label>
            </div>
          </div>
          <div className="dz-preview dz-preview-single">
            <div className="dz-preview-cover">
              <img
                alt="..."
                className="dz-preview-img"
                data-dz-thumbnail=""
              />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Dropzones;
`;
const codeMultiple = `import React from "react";
// javascript plugin that creates nice dropzones for files
import Dropzone from "dropzone";
// reactstrap components
import {
  Alert,
  Button,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

// we want to initialize ourselves this plugin
Dropzone.autoDiscover = false;

class Dropzones extends React.Component {
  componentDidMount() {
    // this variable is to delete the previous image from the dropzone state
    // it is just to make the HTML DOM a bit better, and keep it light
    let currentMultipleFile = undefined;
    // multiple dropzone file - accepts any type of file
    new Dropzone(document.getElementById("dropzone-multiple"), {
      url: "https://",
      thumbnailWidth: null,
      thumbnailHeight: null,
      previewsContainer: document.getElementsByClassName(
        "dz-preview-multiple"
      )[0],
      previewTemplate: document.getElementsByClassName("dz-preview-multiple")[0]
        .innerHTML,
      maxFiles: null,
      acceptedFiles: null,
      init: function() {
        this.on("addedfile", function(file) {
          if (currentMultipleFile) {
          }
          currentMultipleFile = file;
        });
      }
    });
    document.getElementsByClassName("dz-preview-multiple")[0].innerHTML = "";
  }
  render() {
    return (
      <>
        <div className="dropzone dropzone-multiple" id="dropzone-multiple">
          <div className="fallback">
            <div className="custom-file">
              <input
                className="custom-file-input"
                id="customFileUploadMultiple"
                multiple="multiple"
                type="file"
              />
              <label
                className="custom-file-label"
                htmlFor="customFileUploadMultiple"
              >
                Choose file
              </label>
            </div>
          </div>
          <ListGroup
            className=" dz-preview dz-preview-multiple list-group-lg"
            flush
          >
            <ListGroupItem className=" px-0">
              <Row className=" align-items-center">
                <Col className=" col-auto">
                  <div className=" avatar">
                    <img
                      alt="..."
                      className=" avatar-img rounded"
                      data-dz-thumbnail
                      src="..."
                    />
                  </div>
                </Col>
                <div className=" col ml--3">
                  <h4 className=" mb-1" data-dz-name>
                    ...
                  </h4>
                  <p className=" small text-muted mb-0" data-dz-size>
                    ...
                  </p>
                </div>
                <Col className=" col-auto">
                  <Button size="sm" color="danger" data-dz-remove>
                    <i className="fas fa-trash" />
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </div>
      </>
    );
  }
}

export default Dropzones;
`;

// we want to initialize ourselves this plugin
Dropzone.autoDiscover = false;

class Dropzones extends React.Component {
  componentDidMount() {
    // this variable is to delete the previous image from the dropzone state
    // it is just to make the HTML DOM a bit better, and keep it light
    let currentSingleFile = undefined;
    // single dropzone file - accepts only images
    new Dropzone(document.getElementById("dropzone-single"), {
      url: "/",
      thumbnailWidth: null,
      thumbnailHeight: null,
      previewsContainer: document.getElementsByClassName(
        "dz-preview-single"
      )[0],
      previewTemplate: document.getElementsByClassName("dz-preview-single")[0]
        .innerHTML,
      maxFiles: 1,
      acceptedFiles: "image/*",
      init: function() {
        this.on("addedfile", function(file) {
          if (currentSingleFile) {
            this.removeFile(currentSingleFile);
          }
          currentSingleFile = file;
        });
      }
    });
    document.getElementsByClassName("dz-preview-single")[0].innerHTML = "";
    // this variable is to delete the previous image from the dropzone state
    // it is just to make the HTML DOM a bit better, and keep it light
    let currentMultipleFile = undefined;
    // multiple dropzone file - accepts any type of file
    new Dropzone(document.getElementById("dropzone-multiple"), {
      url: "https://",
      thumbnailWidth: null,
      thumbnailHeight: null,
      previewsContainer: document.getElementsByClassName(
        "dz-preview-multiple"
      )[0],
      previewTemplate: document.getElementsByClassName("dz-preview-multiple")[0]
        .innerHTML,
      maxFiles: null,
      acceptedFiles: null,
      init: function() {
        this.on("addedfile", function(file) {
          if (currentMultipleFile) {
          }
          currentMultipleFile = file;
        });
      }
    });
    document.getElementsByClassName("dz-preview-multiple")[0].innerHTML = "";
  }
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Dropzone v5.5.1
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          DropzoneJS is an open source library that provides drag’n’drop file
          uploads with image previews.
        </p>
        <hr />
        <h2 id="examples">Examples</h2>
        <p>
          Copy any of the code examples below and paste it in your project after
          you integrated the needed resources.
        </p>
        <Alert color="danger">
          Dropzones will throw an error if you try to upload any file since it
          is not configured for server-side processing.
        </Alert>
        <h3 id="single-file">Single file</h3>
        <div className="ct-example bg-white">
          <div className="dropzone dropzone-single mb-3" id="dropzone-single">
            <div className="fallback">
              <div className="custom-file">
                <input
                  className="custom-file-input"
                  id="projectCoverUploads"
                  type="file"
                />
                <label
                  className="custom-file-label"
                  htmlFor="projectCoverUploads"
                >
                  Choose file
                </label>
              </div>
            </div>
            <div className="dz-preview dz-preview-single">
              <div className="dz-preview-cover">
                <img
                  alt="..."
                  className="dz-preview-img"
                  data-dz-thumbnail=""
                />
              </div>
            </div>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeSingle}
        </SyntaxHighlighter>
        <h3 id="multiple-files">Multiple files</h3>
        <div className="ct-example bg-white">
          <div className="dropzone dropzone-multiple" id="dropzone-multiple">
            <div className="fallback">
              <div className="custom-file">
                <input
                  className="custom-file-input"
                  id="customFileUploadMultiple"
                  multiple="multiple"
                  type="file"
                />
                <label
                  className="custom-file-label"
                  htmlFor="customFileUploadMultiple"
                >
                  Choose file
                </label>
              </div>
            </div>
            <ListGroup
              className=" dz-preview dz-preview-multiple list-group-lg"
              flush
            >
              <ListGroupItem className=" px-0">
                <Row className=" align-items-center">
                  <Col className=" col-auto">
                    <div className=" avatar">
                      <img
                        alt="..."
                        className=" avatar-img rounded"
                        data-dz-thumbnail
                        src="..."
                      />
                    </div>
                  </Col>
                  <div className=" col ml--3">
                    <h4 className=" mb-1" data-dz-name>
                      ...
                    </h4>
                    <p className=" small text-muted mb-0" data-dz-size>
                      ...
                    </p>
                  </div>
                  <Col className=" col-auto">
                    <Button size="sm" color="danger" data-dz-remove>
                      <i className="fas fa-trash" />
                    </Button>
                  </Col>
                </Row>
              </ListGroupItem>
            </ListGroup>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMultiple}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a href="https://www.dropzonejs.com/?ref=creativetim" target="_blank">
            dropzone documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Dropzones;
