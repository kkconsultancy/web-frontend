/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
// reactstrap components
import { Button } from "reactstrap";

const codeExample = `import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
// reactstrap components
import { Button } from "reactstrap";

class SweetAlerts extends React.Component {
  state = {};
  basicAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          style={{ display: "block", marginTop: "-100px" }}
          title="Here's a message!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          btnSize=""
          text="A few words about this sweet alert ..."
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  infoAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          info
          style={{ display: "block", marginTop: "-100px" }}
          title="Info"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  successAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Success"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="success"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  warningAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Warning"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="warning"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  questionAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          custom
          style={{ display: "block", marginTop: "-100px" }}
          title="Question"
          customIcon={
            <div
              className="swal2-icon swal2-question swal2-animate-question-icon"
              style={{ display: "flex" }}
            >
              <span className="swal2-icon-text">?</span>
            </div>
          }
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="default"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  hideAlert = () => {
    this.setState({
      alert: null
    });
  };
  render() {
    return (
      <>
        {this.state.alert}
        <Button color="primary" onClick={this.basicAlert}>
          Basic alert
        </Button>
        <Button color="info" onClick={this.infoAlert}>
          Info alert
        </Button>
        <Button color="success" onClick={this.successAlert}>
          Success alert
        </Button>
        <Button color="warning" onClick={this.warningAlert}>
          Warning alert
        </Button>
        <Button color="default" onClick={this.questionAlert}>
          Question
        </Button>
      </>
    );
  }
}

export default SweetAlerts;
`;
const codeAdvanced = `import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
// reactstrap components
import { Button } from "reactstrap";

class SweetAlerts extends React.Component {
  state = {};
  hideAlert = () => {
    this.setState({
      alert: null
    });
  };
  confirmAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Are you sure?"
          onConfirm={() => this.hideAlert()}
          onCancel={this.confirmedAlert}
          showCancel
          confirmBtnBsStyle="secondary"
          confirmBtnText="Cancel"
          cancelBtnBsStyle="danger"
          cancelBtnText="Yes, delete it!"
          btnSize=""
        >
          You won't be able to revert this!
        </ReactBSAlert>
      )
    });
  };
  confirmedAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Deleted!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="primary"
          confirmBtnText="Ok"
          btnSize=""
        >
          Your file has been deleted.
        </ReactBSAlert>
      )
    });
  };
  timerAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          style={{ display: "block", marginTop: "-100px" }}
          title="Auto close alert!"
          onConfirm={() => this.hideAlert()}
          showConfirm={false}
        >
          I will close in 2 seconds.
        </ReactBSAlert>
      )
    });
    setTimeout(this.hideAlert, 2000);
  };
  imageAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          custom
          style={{ display: "block", marginTop: "-100px" }}
          title="Sweet"
          customIcon={
            <img
              className="swal2-image"
              src={require("../../../../assets/img/docs/getting-started/overview.svg")}
              alt=""
              style={{ display: "flex" }}
            />
          }
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="primary"
          confirmBtnText="Super"
          btnSize=""
        >
          Modal with a custom image ...
        </ReactBSAlert>
      )
    });
  };
  render() {
    return (
      <>
        {this.state.alert}
        <Button color="primary" onClick={this.confirmAlert}>
          Confim alert
        </Button>
        <Button color="primary" onClick={this.timerAlert}>
          Timer alert
        </Button>
        <Button color="primary" onClick={this.imageAlert}>
          Image alert
        </Button>
      </>
    );
  }
}

export default SweetAlerts;
`;

class SweetAlerts extends React.Component {
  state = {};
  basicAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          style={{ display: "block", marginTop: "-100px" }}
          title="Here's a message!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          btnSize=""
          text="A few words about this sweet alert ..."
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  infoAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          info
          style={{ display: "block", marginTop: "-100px" }}
          title="Info"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  successAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Success"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="success"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  warningAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Warning"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="warning"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  questionAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          custom
          style={{ display: "block", marginTop: "-100px" }}
          title="Question"
          customIcon={
            <div
              className="swal2-icon swal2-question swal2-animate-question-icon"
              style={{ display: "flex" }}
            >
              <span className="swal2-icon-text">?</span>
            </div>
          }
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="default"
          confirmBtnText="Ok"
          btnSize=""
        >
          A few words about this sweet alert ...
        </ReactBSAlert>
      )
    });
  };
  hideAlert = () => {
    this.setState({
      alert: null
    });
  };
  confirmAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Are you sure?"
          onConfirm={() => this.hideAlert()}
          onCancel={this.confirmedAlert}
          showCancel
          confirmBtnBsStyle="secondary"
          confirmBtnText="Cancel"
          cancelBtnBsStyle="danger"
          cancelBtnText="Yes, delete it!"
          btnSize=""
        >
          You won't be able to revert this!
        </ReactBSAlert>
      )
    });
  };
  confirmedAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Deleted!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="primary"
          confirmBtnText="Ok"
          btnSize=""
        >
          Your file has been deleted.
        </ReactBSAlert>
      )
    });
  };
  timerAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          style={{ display: "block", marginTop: "-100px" }}
          title="Auto close alert!"
          onConfirm={() => this.hideAlert()}
          showConfirm={false}
        >
          I will close in 2 seconds.
        </ReactBSAlert>
      )
    });
    setTimeout(this.hideAlert, 2000);
  };
  imageAlert = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          custom
          style={{ display: "block", marginTop: "-100px" }}
          title="Sweet"
          customIcon={
            <img
              className="swal2-image"
              src={require("../../../../assets/img/docs/getting-started/overview.svg")}
              alt=""
              style={{ display: "flex" }}
            />
          }
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="primary"
          confirmBtnText="Super"
          btnSize=""
        >
          Modal with a custom image ...
        </ReactBSAlert>
      )
    });
  };
  render() {
    return (
      <>
        {this.state.alert}
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Reac Sweet Alerts v4.4.1
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          A beautiful, responsive, customizable, accessible replacement for
          Javascript’s popup boxes.
        </p>
        <hr />
        <h2 id="usage">Usage</h2>
        <h3 id="css">CSS</h3>
        <p>
          In order to use this plugin on your page you will need to import the
          following styles inside your index.js:
        </p>
        <h2 id="examples">Examples</h2>
        <div className="ct-example bg-white">
          <Button color="primary" onClick={this.basicAlert}>
            Basic alert
          </Button>
          <Button color="info" onClick={this.infoAlert}>
            Info alert
          </Button>
          <Button color="success" onClick={this.successAlert}>
            Success alert
          </Button>
          <Button color="warning" onClick={this.warningAlert}>
            Warning alert
          </Button>
          <Button color="default" onClick={this.questionAlert}>
            Question
          </Button>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h3 id="advanced">Advanced</h3>
        <div className="ct-example bg-white">
          <Button color="primary" onClick={this.confirmAlert}>
            Confim alert
          </Button>
          <Button color="primary" onClick={this.timerAlert}>
            Timer alert
          </Button>
          <Button color="primary" onClick={this.imageAlert}>
            Image alert
          </Button>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeAdvanced}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="http://djorg83.github.io/react-bootstrap-sweetalert/?ref=creativetim"
            target="_blank"
          >
            react-bootstrap-sweetalert documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default SweetAlerts;
