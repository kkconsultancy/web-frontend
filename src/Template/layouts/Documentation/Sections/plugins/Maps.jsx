/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// react plugin used to create google maps
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

const MapCustom = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={12}
      defaultCenter={{ lat: 40.748817, lng: -73.985428 }}
      defaultOptions={{
        scrollwheel: false,
        styles: [
          {
            featureType: "administrative",
            elementType: "labels.text.fill",
            stylers: [{ color: "#444444" }]
          },
          {
            featureType: "landscape",
            elementType: "all",
            stylers: [{ color: "#f2f2f2" }]
          },
          {
            featureType: "poi",
            elementType: "all",
            stylers: [{ visibility: "off" }]
          },
          {
            featureType: "road",
            elementType: "all",
            stylers: [{ saturation: -100 }, { lightness: 45 }]
          },
          {
            featureType: "road.highway",
            elementType: "all",
            stylers: [{ visibility: "simplified" }]
          },
          {
            featureType: "road.arterial",
            elementType: "labels.icon",
            stylers: [{ visibility: "off" }]
          },
          {
            featureType: "transit",
            elementType: "all",
            stylers: [{ visibility: "off" }]
          },
          {
            featureType: "water",
            elementType: "all",
            stylers: [{ color: "#5e72e4" }, { visibility: "on" }]
          }
        ]
      }}
    >
      <Marker position={{ lat: 40.748817, lng: -73.985428 }} />
    </GoogleMap>
  ))
);

const MapDefault = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={8}
      defaultCenter={{ lat: 40.748817, lng: -73.985428 }}
      defaultOptions={{
        scrollwheel: false
      }}
    >
      <Marker position={{ lat: 40.748817, lng: -73.985428 }} />
    </GoogleMap>
  ))
);

const codeExample = `import React from "react";
// react plugin used to create google maps
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

const MapDefault = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={8}
      defaultCenter={{ lat: 40.748817, lng: -73.985428 }}
      defaultOptions={{
        scrollwheel: false
      }}
    >
      <Marker position={{ lat: 40.748817, lng: -73.985428 }} />
    </GoogleMap>
  ))
);

class Google extends React.Component {
  render() {
    return (
      <>
        <MapDefault
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"
          loadingElement={<div style={{ height: \`100%\` }} />}
          containerElement={
            <div
              style={{ height: \`600px\` }}
              className="map-canvas"
              id="map-default"
            />
          }
          mapElement={
            <div style={{ height: \`100%\`, borderRadius: "inherit" }} />
          }
        />
      </>
    );
  }
}

export default Google;
`;
const codeCustom = `import React from "react";
// react plugin used to create google maps
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

const MapCustom = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={12}
      defaultCenter={{ lat: 40.748817, lng: -73.985428 }}
      defaultOptions={{
        scrollwheel: false,
        styles: [
          {
            featureType: "administrative",
            elementType: "labels.text.fill",
            stylers: [{ color: "#444444" }]
          },
          {
            featureType: "landscape",
            elementType: "all",
            stylers: [{ color: "#f2f2f2" }]
          },
          {
            featureType: "poi",
            elementType: "all",
            stylers: [{ visibility: "off" }]
          },
          {
            featureType: "road",
            elementType: "all",
            stylers: [{ saturation: -100 }, { lightness: 45 }]
          },
          {
            featureType: "road.highway",
            elementType: "all",
            stylers: [{ visibility: "simplified" }]
          },
          {
            featureType: "road.arterial",
            elementType: "labels.icon",
            stylers: [{ visibility: "off" }]
          },
          {
            featureType: "transit",
            elementType: "all",
            stylers: [{ visibility: "off" }]
          },
          {
            featureType: "water",
            elementType: "all",
            stylers: [{ color: "#5e72e4" }, { visibility: "on" }]
          }
        ]
      }}
    >
      <Marker position={{ lat: 40.748817, lng: -73.985428 }} />
    </GoogleMap>
  ))
);

class Google extends React.Component {
  render() {
    return (
      <>
        <MapCustom
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"
          loadingElement={<div style={{ height: \`100%\` }} />}
          containerElement={
            <div
              style={{ height: \`600px\` }}
              className="map-canvas"
              id="map-custom"
            />
          }
          mapElement={
            <div style={{ height: \`100%\`, borderRadius: "inherit" }} />
          }
        />
      </>
    );
  }
}

export default Google;
`;

class Maps extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            React Google Maps v9.4.5
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Our map component uses the Google Maps API and it comes with a custom
          interface that matches the theme’s colors
        </p>
        <hr />
        <h2 id="usage">Usage</h2>
        <h3 id="get-your-api-key">Get your API key</h3>
        <p>
          In order to get your Google Maps API key navigate to the following
          page:{" "}
          <a href="https://developers.google.com/maps/documentation/javascript/get-api-key?ref=creativetim">
            Google Maps
          </a>
        </p>
        <h2 id="example">Example</h2>
        <div className="ct-example bg-white">
          <MapDefault
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTTfWur0PDbZWPr7Pmq8K3jiDp0_xUziI"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={
              <div
                style={{ height: `600px` }}
                className="map-canvas"
                id="map-default"
              />
            }
            mapElement={
              <div style={{ height: `100%`, borderRadius: "inherit" }} />
            }
          />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h2 id="custom-map">Custom map</h2>
        <div className="ct-example bg-white">
          <MapCustom
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTTfWur0PDbZWPr7Pmq8K3jiDp0_xUziI"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={
              <div
                style={{ height: `600px` }}
                className="map-canvas"
                id="map-custom"
              />
            }
            mapElement={
              <div style={{ height: `100%`, borderRadius: "inherit" }} />
            }
          />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCustom}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://tomchentw.github.io/react-google-maps/?ref=creativetim"
            target="_blank"
          >
            react-google-maps documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Maps;
