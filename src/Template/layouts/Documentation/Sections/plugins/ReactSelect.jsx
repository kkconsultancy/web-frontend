/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// react plugin used to create DropdownMenu for selecting items
import Select2 from "react-select2-wrapper";
// reactstrap components
import { Form } from "reactstrap";

const codeExample = `import React from "react";
 // react plugin used to create DropdownMenu for selecting items
 import Select2 from "react-select2-wrapper";
 // reactstrap components
 import {
   Form
 } from "reactstrap";

 class Dropdown extends React.Component {
   render() {
     return (
       <>
         <Form>
           <Select2
             className="form-control"
             defaultValue="1"
             options={{
               placeholder: "Select"
             }}
             data={[
               { id: "1", text: "Alerts" },
               { id: "2", text: "Badges" },
               { id: "3", text: "Buttons" },
               { id: "4", text: "Cards" },
               { id: "5", text: "Forms" },
               { id: "6", text: "Modals" }
             ]}
           />
         </Form>
       </>
     );
   }
 }

 export default Dropdown;
`;
const codePlaceholder = `import React from "react";
 // react plugin used to create DropdownMenu for selecting items
 import Select2 from "react-select2-wrapper";
 // reactstrap components
 import {
   Form
 } from "reactstrap";

 class Dropdown extends React.Component {
   render() {
     return (
       <>
         <Form>
           <Select2
             className="form-control"
             defaultValue="1"
             options={{
               placeholder: "Select"
             }}
             data={[
               { id: "1", text: "Alerts" },
               { id: "2", text: "Badges" },
               { id: "3", text: "Buttons" },
               { id: "4", text: "Cards" },
               { id: "5", text: "Forms" },
               { id: "6", text: "Modals" }
             ]}
           />
         </Form>
       </>
     );
   }
 }

 export default Dropdown;
`;
const codeDisabledSelect = `import React from "react";
 // react plugin used to create DropdownMenu for selecting items
 import Select2 from "react-select2-wrapper";
 // reactstrap components
 import {
   Form
 } from "reactstrap";

 class Dropdown extends React.Component {
   render() {
     return (
       <>
         <Form>
           <Select2
             className="form-control"
             defaultValue="1"
             options={{
               disabled: true
             }}
             data={[
               { id: "1", text: "Alerts" },
               { id: "2", text: "Badges" },
               { id: "3", text: "Buttons" },
               { id: "4", text: "Cards" },
               { id: "5", text: "Forms" },
               { id: "6", text: "Modals" }
             ]}
           />
         </Form>
       </>
     );
   }
 }

 export default Dropdown;
`;
const codeDisabledResults = `import React from "react";
 // react plugin used to create DropdownMenu for selecting items
 import Select2 from "react-select2-wrapper";
 // reactstrap components
 import {
   Form
 } from "reactstrap";

 class Dropdown extends React.Component {
   state = {
     select: null
   };
   render() {
     return (
       <>
         <Form>
           <Select2
             className="form-control"
             defaultValue="1"
             options={{
               placeholder: "Select"
             }}
             data={[
               { id: "1", text: "Alerts" },
               { id: "2", text: "Badges" },
               { id: "3", text: "Buttons", disabled: true },
               { id: "4", text: "Cards" },
               { id: "5", text: "Forms", disabled: true },
               { id: "6", text: "Modals" }
             ]}
           />
         </Form>
       </>
     );
   }
 }

 export default Dropdown;
`;
const codeHideSearchBox = `import React from "react";
 // react plugin used to create DropdownMenu for selecting items
 import Select2 from "react-select2-wrapper";
 // reactstrap components
 import {
   Form
 } from "reactstrap";

 class Dropdown extends React.Component {
   state = {
     select: null
   };
   render() {
     return (
       <>
         <Form>
           <Select2
             className="form-control"
             data-minimum-results-for-search="Infinity"
             defaultValue="1"
             data={[
               { id: "1", text: "Alerts" },
               { id: "2", text: "Badges" },
               { id: "3", text: "Buttons" },
               { id: "4", text: "Cards" },
               { id: "5", text: "Forms" },
               { id: "6", text: "Modals" }
             ]}
           />
         </Form>
       </>
     );
   }
 }

 export default Dropdown;
`;
const codeMultiple = `import React from "react";
 // react plugin used to create DropdownMenu for selecting items
 import Select2 from "react-select2-wrapper";
 // reactstrap components
 import {
   Form
 } from "reactstrap";

 class Dropdown extends React.Component {
   state = {
     select: null
   };
   render() {
     return (
       <>
         <Form>
           <Select2
             className="form-control"
             defaultValue="1"
             multiple
             data={[
               { id: "1", text: "Alerts" },
               { id: "2", text: "Badges" },
               { id: "3", text: "Buttons" },
               { id: "4", text: "Cards" },
               { id: "5", text: "Forms" },
               { id: "6", text: "Modals" }
             ]}
           />
         </Form>
       </>
     );
   }
 }

 export default Dropdown;
`;

class Dropdown extends React.Component {
  state = {
    select: null
  };
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            React Select 2 Wrapper v1.0.4-beta6 for Select2
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Select2 gives you a customizable select box with support for
          searching, tagging, remote data sets, infinite scrolling, and many
          other highly used options.
        </p>
        <hr />
        <h2 id="usage">Usage</h2>
        <h3 id="css">CSS</h3>
        <p>
          In order to use this plugin on your page you will need to import the
          following styles inside your index.js:
        </p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {`import "../../../../assets/vendor/select2/dist/css/select2.min.css";`}
        </SyntaxHighlighter>
        <h2 id="example">Example</h2>
        <div className="ct-example bg-white">
          <Form>
            <Select2
              className="form-control"
              defaultValue="1"
              options={{
                placeholder: "Select"
              }}
              data={[
                { id: "1", text: "Alerts" },
                { id: "2", text: "Badges" },
                { id: "3", text: "Buttons" },
                { id: "4", text: "Cards" },
                { id: "5", text: "Forms" },
                { id: "6", text: "Modals" }
              ]}
            />
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h3 id="placeholder">Placeholder</h3>
        <div className="ct-example bg-white">
          <Form>
            <Select2
              className="form-control"
              defaultValue="1"
              options={{
                placeholder: "Select"
              }}
              data={[
                { id: "1", text: "Alerts" },
                { id: "2", text: "Badges" },
                { id: "3", text: "Buttons" },
                { id: "4", text: "Cards" },
                { id: "5", text: "Forms" },
                { id: "6", text: "Modals" }
              ]}
            />
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codePlaceholder}
        </SyntaxHighlighter>
        <h3 id="disabled-select">Disabled select</h3>
        <div className="ct-example bg-white">
          <Form>
            <Select2
              className="form-control"
              defaultValue="1"
              options={{
                disabled: true
              }}
              data={[
                { id: "1", text: "Alerts" },
                { id: "2", text: "Badges" },
                { id: "3", text: "Buttons" },
                { id: "4", text: "Cards" },
                { id: "5", text: "Forms" },
                { id: "6", text: "Modals" }
              ]}
            />
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeDisabledSelect}
        </SyntaxHighlighter>
        <h3 id="disabled-results">Disabled results</h3>
        <div className="ct-example bg-white">
          <Form>
            <Select2
              className="form-control"
              defaultValue="1"
              options={{
                placeholder: "Select"
              }}
              data={[
                { id: "1", text: "Alerts" },
                { id: "2", text: "Badges" },
                { id: "3", text: "Buttons", disabled: true },
                { id: "4", text: "Cards" },
                { id: "5", text: "Forms", disabled: true },
                { id: "6", text: "Modals" }
              ]}
            />
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeDisabledResults}
        </SyntaxHighlighter>
        <h3 id="hide-search-box">Hide search box</h3>
        <div className="ct-example bg-white">
          <Form>
            <Select2
              className="form-control"
              data-minimum-results-for-search="Infinity"
              defaultValue="1"
              data={[
                { id: "1", text: "Alerts" },
                { id: "2", text: "Badges" },
                { id: "3", text: "Buttons" },
                { id: "4", text: "Cards" },
                { id: "5", text: "Forms" },
                { id: "6", text: "Modals" }
              ]}
            />
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeHideSearchBox}
        </SyntaxHighlighter>
        <h3 id="multiple">Multiple</h3>
        <div className="ct-example bg-white">
          <Form>
            <Select2
              className="form-control"
              defaultValue="1"
              multiple
              data={[
                { id: "1", text: "Alerts" },
                { id: "2", text: "Badges" },
                { id: "3", text: "Buttons" },
                { id: "4", text: "Cards" },
                { id: "5", text: "Forms" },
                { id: "6", text: "Modals" }
              ]}
            />
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMultiple}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://github.com/rkit/react-select2-wrapper#readme?ref=creativetim"
            target="_blank"
          >
            react-select2-wrapper documentation
          </a>{" "}
          and/or{" "}
          <a href="https://select2.org/?ref=creativetim" target="_blank">
            select2 documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Dropdown;
