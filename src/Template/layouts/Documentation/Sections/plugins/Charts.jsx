/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
// reactstrap components
import { Card, CardHeader, CardBody } from "reactstrap";
// core components
import {
  // global options for the charts
  chartOptions,
  // function that adds the global options to our charts
  parseOptions,
  chartExample2,
  chartExample3,
  chartExample4,
  chartExample5,
  chartExample6,
  chartExample7
} from "../../../../variables/charts.jsx";

const codeChartExample3 = `import React from "react";
 // javascipt plugin for creating charts
 import Chart from "chart.js";
 // react plugin used to create charts
 import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
 // reactstrap components
 import { Card, CardHeader, CardBody } from "reactstrap";
 // core components
 import {
   // global options for the charts
   chartOptions,
   // function that adds the global options to our charts
   parseOptions,
   chartExample3
 } from "variables/charts.jsx";

 class Charts extends React.Component {
   componentWillMount() {
     if (window.Chart) {
       parseOptions(Chart, chartOptions());
     }
   }
   render() {
     return (
       <>
         <Card>
           <CardHeader>
             <h5 className="h3 mb-0">Line chart</h5>
           </CardHeader>
           <CardBody>
             <div className="chart">
               <Line
                 data={chartExample3.data}
                 options={chartExample3.options}
                 id="chart-sales"
                 className="chart-canvas"
               />
             </div>
           </CardBody>
         </Card>
       </>
     );
   }
 }

 export default Charts;
`;
const codeChartExample2 = `import React from "react";
 // javascipt plugin for creating charts
 import Chart from "chart.js";
 // react plugin used to create charts
 import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
 // reactstrap components
 import { Card, CardHeader, CardBody } from "reactstrap";
 // core components
 import {
   // global options for the charts
   chartOptions,
   // function that adds the global options to our charts
   parseOptions,
   chartExample2
 } from "variables/charts.jsx";

 class Charts extends React.Component {
   componentWillMount() {
     if (window.Chart) {
       parseOptions(Chart, chartOptions());
     }
   }
   render() {
     return (
       <>
         <Card>
           <CardHeader>
             <h5 className="h3 mb-0">Bars chart</h5>
           </CardHeader>
           <CardBody>
             <div className="chart">
               <Bar
                 data={chartExample2.data}
                 options={chartExample2.options}
                 className="chart-canvas"
                 id="chart-bars"
               />
             </div>
           </CardBody>
         </Card>
       </>
     );
   }
 }

 export default Charts;
`;
const codeChartExample4 = `import React from "react";
 // javascipt plugin for creating charts
 import Chart from "chart.js";
 // react plugin used to create charts
 import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
 // reactstrap components
 import { Card, CardHeader, CardBody } from "reactstrap";
 // core components
 import {
   // global options for the charts
   chartOptions,
   // function that adds the global options to our charts
   parseOptions,
   chartExample4
 } from "variables/charts.jsx";

 class Charts extends React.Component {
   componentWillMount() {
     if (window.Chart) {
       parseOptions(Chart, chartOptions());
     }
   }
   render() {
     return (
       <>
         <Card>
           <CardHeader>
             <h5 className="h3 mb-0">Points chart</h5>
           </CardHeader>
           <CardBody>
             <div className="chart">
               <Line
                 data={chartExample4.data}
                 options={chartExample4.options}
                 id="chart-points"
                 className="chart-canvas"
               />
             </div>
           </CardBody>
         </Card>
       </>
     );
   }
 }

 export default Charts;
`;
const codeChartExample5 = `import React from "react";
 // javascipt plugin for creating charts
 import Chart from "chart.js";
 // react plugin used to create charts
 import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
 // reactstrap components
 import { Card, CardHeader, CardBody } from "reactstrap";
 // core components
 import {
   // global options for the charts
   chartOptions,
   // function that adds the global options to our charts
   parseOptions,
   chartExample5
 } from "variables/charts.jsx";

 class Charts extends React.Component {
   componentWillMount() {
     if (window.Chart) {
       parseOptions(Chart, chartOptions());
     }
   }
   render() {
     return (
       <>
         <Card>
           <CardHeader>
             <h5 className="h3 mb-0">Doughnut chart</h5>
           </CardHeader>
           <CardBody>
             <div className="chart">
               <Doughnut
                 data={chartExample5.data}
                 options={chartExample5.options}
                 className="chart-canvas"
                 id="chart-doughnut"
               />
             </div>
           </CardBody>
         </Card>
       </>
     );
   }
 }

 export default Charts;
`;
const codeChartExample6 = `import React from "react";
 // javascipt plugin for creating charts
 import Chart from "chart.js";
 // react plugin used to create charts
 import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
 // reactstrap components
 import { Card, CardHeader, CardBody } from "reactstrap";
 // core components
 import {
   // global options for the charts
   chartOptions,
   // function that adds the global options to our charts
   parseOptions,
   chartExample6
 } from "variables/charts.jsx";

 class Charts extends React.Component {
   componentWillMount() {
     if (window.Chart) {
       parseOptions(Chart, chartOptions());
     }
   }
   render() {
     return (
       <>
         <Card>
           <CardHeader>
             <h5 className="h3 mb-0">Pie chart</h5>
           </CardHeader>
           <CardBody>
             <div className="chart">
               <Pie
                 data={chartExample6.data}
                 options={chartExample6.options}
                 className="chart-canvas"
                 id="chart-pie"
               />
             </div>
           </CardBody>
         </Card>
       </>
     );
   }
 }

 export default Charts;
`;
const codeChartExample7 = `import React from "react";
 // javascipt plugin for creating charts
 import Chart from "chart.js";
 // react plugin used to create charts
 import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
 // reactstrap components
 import { Card, CardHeader, CardBody } from "reactstrap";
 // core components
 import {
   // global options for the charts
   chartOptions,
   // function that adds the global options to our charts
   parseOptions,
   chartExample7
 } from "variables/charts.jsx";

 class Charts extends React.Component {
   componentWillMount() {
     if (window.Chart) {
       parseOptions(Chart, chartOptions());
     }
   }
   render() {
     return (
       <>
         <Card>
           <CardHeader>
             <h5 className="h3 mb-0">Bar stacked chart</h5>
           </CardHeader>
           <CardBody>
             <div className="chart">
               <Bar
                 data={chartExample7.data}
                 options={chartExample7.options}
                 className="chart-canvas"
                 id="chart-bar-stacked"
               />
             </div>
           </CardBody>
         </Card>
       </>
     );
   }
 }

 export default Charts;
`;
const codeChartExample3Dark = `import React from "react";
 // javascipt plugin for creating charts
 import Chart from "chart.js";
 // react plugin used to create charts
 import { Line, Bar, Doughnut, Pie } from "react-chartjs-2";
 // reactstrap components
 import { Card, CardHeader, CardBody } from "reactstrap";
 // core components
 import {
   // global options for the charts
   chartOptions,
   // function that adds the global options to our charts
   parseOptions,
   chartExample3
 } from "variables/charts.jsx";

 class Charts extends React.Component {
   componentWillMount() {
     if (window.Chart) {
       parseOptions(Chart, chartOptions());
     }
   }
   render() {
     return (
       <>
         <Card className="bg-default">
           <CardBody>
             <div className="chart">
               <Line
                 data={chartExample3.data}
                 options={chartExample3.options}
                 id="chart-sales"
                 className="chart-canvas"
               />
             </div>
           </CardBody>
         </Card>
       </>
     );
   }
 }

 export default Charts;
`;

class Charts extends React.Component {
  componentWillMount() {
    if (window.Chart) {
      parseOptions(Chart, chartOptions());
    }
  }
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            React wrapper (v2.7.4) for Chart.js (v2.7.3)
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Simple yet flexible React charting for designers &amp; developers.
          Made by{" "}
          <a
            href="https://github.com/jerairrest?ref=creativetim"
            target="_blank"
          >
            Jeremy Ayerst
          </a>{" "}
          over the{" "}
          <a href="https://www.chartjs.org/?ref=creativetim" target="_blank">
            Charts.js
          </a>{" "}
          Javascript plugin. Please check the{" "}
          <a
            href="https://www.chartjs.org/docs/latest/?ref=creativetim"
            target="_blank"
          >
            Charts.js full documentation
          </a>{" "}
          and{" "}
          <a
            href="https://github.com/jerairrest/react-chartjs-2?ref=creativetim"
            target="_blank"
          >
            react-chartjs-2 full documentation
          </a>
          .
        </p>
        <hr />
        <h2 id="usage">Usage</h2>
        <p>
          In order to use this charts on your page you will need to include the
          following script in the “Optional JS” area from the page’s footer:
        </p>
        <p>
          After that, simply copy one of the code examples demonstrated below
          and include it in your page.
        </p>
        <h2 id="examples">Examples</h2>
        <div className="ct-example bg-white">
          <Card>
            <CardHeader>
              <h5 className="h3 mb-0">Line chart</h5>
            </CardHeader>
            <CardBody>
              <div className="chart">
                <Line
                  data={chartExample3.data}
                  options={chartExample3.options}
                  id="chart-sales"
                  className="chart-canvas"
                />
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChartExample3}
        </SyntaxHighlighter>
        <h3 id="points">Points</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardHeader>
              <h5 className="h3 mb-0">Bars chart</h5>
            </CardHeader>
            <CardBody>
              <div className="chart">
                <Bar
                  data={chartExample2.data}
                  options={chartExample2.options}
                  className="chart-canvas"
                  id="chart-bars"
                />
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChartExample2}
        </SyntaxHighlighter>
        <h3 id="points-1">Points</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardHeader>
              <h5 className="h3 mb-0">Points chart</h5>
            </CardHeader>
            <CardBody>
              <div className="chart">
                <Line
                  data={chartExample4.data}
                  options={chartExample4.options}
                  id="chart-points"
                  className="chart-canvas"
                />
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChartExample4}
        </SyntaxHighlighter>
        <h3 id="doughnut">Doughnut</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardHeader>
              <h5 className="h3 mb-0">Doughnut chart</h5>
            </CardHeader>
            <CardBody>
              <div className="chart">
                <Doughnut
                  data={chartExample5.data}
                  options={chartExample5.options}
                  className="chart-canvas"
                  id="chart-doughnut"
                />
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChartExample5}
        </SyntaxHighlighter>
        <h3 id="pie">Pie</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardHeader>
              <h5 className="h3 mb-0">Pie chart</h5>
            </CardHeader>
            <CardBody>
              <div className="chart">
                <Pie
                  data={chartExample6.data}
                  options={chartExample6.options}
                  className="chart-canvas"
                  id="chart-pie"
                />
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChartExample6}
        </SyntaxHighlighter>
        <h3 id="bar-stacked">Bar stacked</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardHeader>
              <h5 className="h3 mb-0">Bar stacked chart</h5>
            </CardHeader>
            <CardBody>
              <div className="chart">
                <Bar
                  data={chartExample7.data}
                  options={chartExample7.options}
                  className="chart-canvas"
                  id="chart-bar-stacked"
                />
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChartExample7}
        </SyntaxHighlighter>
        <h2 id="dark-card-with-chart">Dark card with chart</h2>
        <div className="ct-example bg-white">
          <Card className="bg-default">
            <CardBody>
              <div className="chart">
                <Line
                  data={chartExample3.data}
                  options={chartExample3.options}
                  id="chart-sales"
                  className="chart-canvas"
                />
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChartExample3Dark}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a href="https://www.chartjs.org/?ref=creativetim" target="_blank">
            chart.js documentation
          </a>{" "}
          and{" "}
          <a
            href="http://jerairrest.github.io/react-chartjs-2/?ref=creativetim"
            target="_blank"
          >
            react-chartjs-2 documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Charts;
