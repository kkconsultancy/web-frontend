/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// react plugin for creating notifications over the dashboard
import NotificationAlert from "react-notification-alert";
// reactstrap components
import { Button } from "reactstrap";

const codeExample = `import React from "react";
// react plugin for creating notifications over the dashboard
import NotificationAlert from "react-notification-alert";
// reactstrap components
import {
  Button,
} from "reactstrap";

class Notifications extends React.Component {
  notify = place => {
    let options = {
      place: place,
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Bootstrap Notify
          </span>
          <span data-notify="message">
            Turning standard Bootstrap alerts into awesome notifications
          </span>
        </div>
      ),
      type: "default",
      icon: "ni ni-bell-55",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  };
  render() {
    return (
      <>
        <div className="rna-wrapper">
          <NotificationAlert ref="notificationAlert" />
        </div>
        <Button
          color="default"
          onClick={() => this.notify("tl")}
        >
          Top Left
        </Button>
        <Button
          color="default"
          onClick={() => this.notify("tc")}
        >
          Top Center
        </Button>
        <Button
          color="default"
          onClick={() => this.notify("tr")}
        >
          Top Right
        </Button>
        <Button
          color="default"
          onClick={() => this.notify("bl")}
        >
          Bottom Left
        </Button>
        <Button
          color="default"
          onClick={() => this.notify("bc")}
        >
          Bottom Center
        </Button>
        <Button
          color="default"
          onClick={() => this.notify("br")}
        >
          Bottom Right
        </Button>
      </>
    );
  }
}

export default Notifications;
`;

const codeColors = `import React from "react";
// react plugin for creating notifications over the dashboard
import NotificationAlert from "react-notification-alert";
// reactstrap components
import {
  Button,
} from "reactstrap";

class Notifications extends React.Component {
  notify = type => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Bootstrap Notify
          </span>
          <span data-notify="message">
            Turning standard Bootstrap alerts into awesome notifications
          </span>
        </div>
      ),
      type: type,
      icon: "ni ni-bell-55",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  };
  render() {
    return (
      <>
        <div className="rna-wrapper">
          <NotificationAlert ref="notificationAlert" />
        </div>
        <Button
          color="default"
          onClick={() => this.notify("default")}
        >
          Default
        </Button>
        <Button color="info" onClick={() => this.notify("info")}>
          Info
        </Button>
        <Button
          color="success"
          onClick={() => this.notify("success")}
        >
          Success
        </Button>
        <Button
          color="warning"
          onClick={() => this.notify("warning")}
        >
          Warning
        </Button>
        <Button color="danger" onClick={() => this.notify("danger")}>
          Danger
        </Button>
      </>
    );
  }
}

export default Notifications;`;

class Notify extends React.Component {
  notify = type => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Bootstrap Notify
          </span>
          <span data-notify="message">
            Turning standard Bootstrap alerts into awesome notifications
          </span>
        </div>
      ),
      type: type,
      icon: "ni ni-bell-55",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  };
  notify2 = place => {
    let options = {
      place: place,
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Bootstrap Notify
          </span>
          <span data-notify="message">
            Turning standard Bootstrap alerts into awesome notifications
          </span>
        </div>
      ),
      type: "default",
      icon: "ni ni-bell-55",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  };
  render() {
    return (
      <>
        <div className="rna-wrapper">
          <NotificationAlert ref="notificationAlert" />
        </div>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            React Notification Alert v0.0.11
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          A beautiful, responsive, customizable, accessible replacement for
          Javascript’s popup boxes.
        </p>
        <hr />
        <h2 id="usage">Usage</h2>
        <h3 id="css">CSS</h3>
        <p>
          In order to use this plugin on your page you will need to import the
          following styles inside your index.js:
        </p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {`// plugins styles from node_modules\nimport "react-notification-alert/dist/animate.css";`}
        </SyntaxHighlighter>
        <h2 id="examples">Examples</h2>
        <div className="ct-example bg-white">
          <Button color="default" onClick={() => this.notify2("tl")}>
            Top Left
          </Button>
          <Button color="default" onClick={() => this.notify2("tc")}>
            Top Center
          </Button>
          <Button color="default" onClick={() => this.notify2("tr")}>
            Top Right
          </Button>
          <Button color="default" onClick={() => this.notify2("bl")}>
            Bottom Left
          </Button>
          <Button color="default" onClick={() => this.notify2("bc")}>
            Bottom Center
          </Button>
          <Button color="default" onClick={() => this.notify2("br")}>
            Bottom Right
          </Button>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h3 id="colors">Colors</h3>
        <div className="ct-example bg-white">
          <Button color="default" onClick={() => this.notify("default")}>
            Default
          </Button>
          <Button color="info" onClick={() => this.notify("info")}>
            Info
          </Button>
          <Button color="success" onClick={() => this.notify("success")}>
            Success
          </Button>
          <Button color="warning" onClick={() => this.notify("warning")}>
            Warning
          </Button>
          <Button color="danger" onClick={() => this.notify("danger")}>
            Danger
          </Button>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeColors}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://github.com/creativetimofficial/react-notification-alert#readme?ref=creativetim"
            target="_blank"
          >
            react notification alert documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Notify;
