/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// javascript plugin that creates a sortable object from a dom object
import List from "list.js";
// reactstrap components
import {
  Alert,
  Badge,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Media,
  Progress,
  Table,
  UncontrolledTooltip
} from "reactstrap";

const codeExample = `import React from "react";
// javascript plugin that creates a sortable object from a dom object
import List from "list.js";
// reactstrap components
import {
  Alert,
  Badge,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Media,
  Progress,
  Table,
  UncontrolledTooltip
} from "reactstrap";

class ListJs extends React.Component {
  componentDidMount() {
    new List(this.refs["first-list"], {
      valueNames: ["name", "budget", "status", "completion"],
      listClass: "list"
    });
  }
  render() {
    return (
      <>
        <div className="table-responsive" ref="first-list">
          <Table className="align-items-center table-flush">
            <thead className="thead-light">
              <tr>
                <th className="sort" data-sort="name" scope="col">
                  Project
                </th>
                <th className="sort" data-sort="budget" scope="col">
                  Budget
                </th>
                <th className="sort" data-sort="status" scope="col">
                  Status
                </th>
                <th scope="col">Users</th>
                <th className="sort" data-sort="completion" scope="col">
                  Completion
                </th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody className="list">
              <tr>
                <th scope="row">
                  <Media className="align-items-center">
                    <a
                      className="avatar rounded-circle mr-3"
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/bootstrap.jpg")}
                      />
                    </a>
                    <Media>
                      <span className="name mb-0 text-sm">
                        Argon Design System
                      </span>
                    </Media>
                  </Media>
                </th>
                <td className="budget">$2500 USD</td>
                <td>
                  <Badge color="" className="badge-dot mr-4">
                    <i className="bg-warning" />
                    <span className="status">pending</span>
                  </Badge>
                </td>
                <td>
                  <div className="avatar-group">
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip649644480"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-1.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip649644480">
                      Ryan Tompson
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip271118748"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-2.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip271118748">
                      Romina Hadid
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip456883903"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-3.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip456883903">
                      Alexander Smith
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip427983389"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-4.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip427983389">
                      Jessica Doe
                    </UncontrolledTooltip>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <span className="completion mr-2">60%</span>
                    <div>
                      <Progress max="100" value="60" color="warning" />
                    </div>
                  </div>
                </td>
                <td className="text-right">
                  <UncontrolledDropdown>
                    <DropdownToggle
                      className="btn-icon-only text-light"
                      color=""
                      role="button"
                      size="sm"
                    >
                      <i className="fas fa-ellipsis-v" />
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-menu-arrow" right>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Another action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Something else here
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <Media className="align-items-center">
                    <a
                      className="avatar rounded-circle mr-3"
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/angular.jpg")}
                      />
                    </a>
                    <Media>
                      <span className="name mb-0 text-sm">
                        Angular Now UI Kit PRO
                      </span>
                    </Media>
                  </Media>
                </th>
                <td className="budget">$1800 USD</td>
                <td>
                  <Badge color="" className="badge-dot mr-4">
                    <i className="bg-success" />
                    <span className="status">completed</span>
                  </Badge>
                </td>
                <td>
                  <div className="avatar-group">
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip308604570"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-1.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip308604570">
                      Ryan Tompson
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip463448556"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-2.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip463448556">
                      Romina Hadid
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip474990624"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-3.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip474990624">
                      Alexander Smith
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip371113692"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-4.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip371113692">
                      Jessica Doe
                    </UncontrolledTooltip>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <span className="completion mr-2">100%</span>
                    <div>
                      <Progress max="100" value="100" color="success" />
                    </div>
                  </div>
                </td>
                <td className="text-right">
                  <UncontrolledDropdown>
                    <DropdownToggle
                      className="btn-icon-only text-light"
                      color=""
                      role="button"
                      size="sm"
                    >
                      <i className="fas fa-ellipsis-v" />
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-menu-arrow" right>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Another action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Something else here
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <Media className="align-items-center">
                    <a
                      className="avatar rounded-circle mr-3"
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/sketch.jpg")}
                      />
                    </a>
                    <Media>
                      <span className="name mb-0 text-sm">
                        Black Dashboard
                      </span>
                    </Media>
                  </Media>
                </th>
                <td className="budget">$3150 USD</td>
                <td>
                  <Badge color="" className="badge-dot mr-4">
                    <i className="bg-danger" />
                    <span className="status">delayed</span>
                  </Badge>
                </td>
                <td>
                  <div className="avatar-group">
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip178353307"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-1.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip178353307">
                      Ryan Tompson
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip110940759"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-2.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip110940759">
                      Romina Hadid
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip721474482"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-3.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip721474482">
                      Alexander Smith
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip248059973"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-4.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip248059973">
                      Jessica Doe
                    </UncontrolledTooltip>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <span className="completion mr-2">72%</span>
                    <div>
                      <Progress max="100" value="72" color="danger" />
                    </div>
                  </div>
                </td>
                <td className="text-right">
                  <UncontrolledDropdown>
                    <DropdownToggle
                      className="btn-icon-only text-light"
                      color=""
                      role="button"
                      size="sm"
                    >
                      <i className="fas fa-ellipsis-v" />
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-menu-arrow" right>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Another action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Something else here
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <Media className="align-items-center">
                    <a
                      className="avatar rounded-circle mr-3"
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/react.jpg")}
                      />
                    </a>
                    <Media>
                      <span className="name mb-0 text-sm">
                        React Material Dashboard
                      </span>
                    </Media>
                  </Media>
                </th>
                <td className="budget">$4400 USD</td>
                <td>
                  <Badge color="" className="badge-dot mr-4">
                    <i className="bg-info" />
                    <span className="status">on schedule</span>
                  </Badge>
                </td>
                <td>
                  <div className="avatar-group">
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip338716581"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-1.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip338716581">
                      Ryan Tompson
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip384272281"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-2.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip384272281">
                      Romina Hadid
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip714277819"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-3.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip714277819">
                      Alexander Smith
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip352802795"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-4.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip352802795">
                      Jessica Doe
                    </UncontrolledTooltip>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <span className="completion mr-2">90%</span>
                    <div>
                      <Progress max="100" value="90" color="info" />
                    </div>
                  </div>
                </td>
                <td className="text-right">
                  <UncontrolledDropdown>
                    <DropdownToggle
                      className="btn-icon-only text-light"
                      color=""
                      role="button"
                      size="sm"
                    >
                      <i className="fas fa-ellipsis-v" />
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-menu-arrow" right>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Another action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Something else here
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <Media className="align-items-center">
                    <a
                      className="avatar rounded-circle mr-3"
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/vue.jpg")}
                      />
                    </a>
                    <Media>
                      <span className="name mb-0 text-sm">
                        Vue Paper UI Kit PRO
                      </span>
                    </Media>
                  </Media>
                </th>
                <td className="budget">$2200 USD</td>
                <td>
                  <Badge color="" className="badge-dot mr-4">
                    <i className="bg-success" />
                    <span className="status">completed</span>
                  </Badge>
                </td>
                <td>
                  <div className="avatar-group">
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip261050883"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-1.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip261050883">
                      Ryan Tompson
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip704196347"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-2.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip704196347">
                      Romina Hadid
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip455439475"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-3.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip455439475">
                      Alexander Smith
                    </UncontrolledTooltip>
                    <a
                      className="avatar avatar-sm rounded-circle"
                      href="#pablo"
                      id="tooltip948033723"
                      onClick={e => e.preventDefault()}
                    >
                      <img
                        alt="..."
                        src={require("../../../../assets/img/theme/team-4.jpg")}
                      />
                    </a>
                    <UncontrolledTooltip delay={0} target="tooltip948033723">
                      Jessica Doe
                    </UncontrolledTooltip>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <span className="completion mr-2">100%</span>
                    <div>
                      <Progress max="100" value="100" color="success" />
                    </div>
                  </div>
                </td>
                <td className="text-right">
                  <UncontrolledDropdown>
                    <DropdownToggle
                      className="btn-icon-only text-light"
                      color=""
                      role="button"
                      size="sm"
                    >
                      <i className="fas fa-ellipsis-v" />
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-menu-arrow" right>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Another action
                      </DropdownItem>
                      <DropdownItem
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Something else here
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </>
    );
  }
}

export default ListJs;
`;
const codeInitialization = `componentDidMount() {
  new List(selector, {
    valueNames: ["NAME_1", "NAME_2", "NAME_3", "NAME_4", ...],
    listClass: "list"
  });
}`;

class ListJs extends React.Component {
  componentDidMount() {
    new List(this.refs["first-list"], {
      valueNames: ["name", "budget", "status", "completion"],
      listClass: "list"
    });
  }
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            ListJS v1.5.0
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Tiny, invisible and simple, yet powerful and incredibly fast vanilla
          JavaScript that adds search, sort, filters and flexibility to plain
          HTML lists, tables, or anything.
        </p>
        <hr />
        <h2 id="initialization">Initialization</h2>
        <h3 id="table">Table</h3>
        <p>
          In your class / funtion where you want to render this functionality
          add the following code:
        </p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeInitialization}
        </SyntaxHighlighter>
        <p>
          Where <code className="highlighter-rouge">selector</code> is your
          table selector and{" "}
          <code className="highlighter-rouge">valueNames</code> are the values
          that we want to be sorted from our table.
        </p>
        <h3 id="thead">Thead</h3>
        <p>
          Inside the <code className="highlighter-rouge">thead</code> element
          you will need to make sortable the columns you specified in the{" "}
          <code className="highlighter-rouge">valueNames</code> by adding a{" "}
          <code className="highlighter-rouge">data-sort</code> attribute with
          the colums’s name (e.g{" "}
          <code className="highlighter-rouge">data-sort="NAME_1"</code>) next to
          the <code className="highlighter-rouge">.sort</code> class.
        </p>
        <h3 id="tbody">Tbody</h3>
        <p>
          In order to correlate the sortable actions you will need to add the
          class with the same name as{" "}
          <code className="highlighter-rouge">data-sort</code> on the element
          with the value you want to sort by (e.g{" "}
          <code className="highlighter-rouge">className="NAME_1"</code>).
        </p>
        <h2 id="example">Example</h2>
        <p>
          For now we added the sortable functionality only on table. Soon, we
          will add it on other components too.
        </p>
        <h3 id="table-1">Table</h3>
        <Alert color="warning">
          <strong>Note!</strong>
          The table below is an advanced example in order to demonstrate the
          power of this plugin that can sort items even when columns have many
          different components such as images, progress bars etc.
        </Alert>
        <div className="ct-example bg-white">
          <div className="table-responsive" ref="first-list">
            <Table className="align-items-center table-flush">
              <thead className="thead-light">
                <tr>
                  <th className="sort" data-sort="name" scope="col">
                    Project
                  </th>
                  <th className="sort" data-sort="budget" scope="col">
                    Budget
                  </th>
                  <th className="sort" data-sort="status" scope="col">
                    Status
                  </th>
                  <th scope="col">Users</th>
                  <th className="sort" data-sort="completion" scope="col">
                    Completion
                  </th>
                  <th scope="col" />
                </tr>
              </thead>
              <tbody className="list">
                <tr>
                  <th scope="row">
                    <Media className="align-items-center">
                      <a
                        className="avatar rounded-circle mr-3"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/bootstrap.jpg")}
                        />
                      </a>
                      <Media>
                        <span className="name mb-0 text-sm">
                          Argon Design System
                        </span>
                      </Media>
                    </Media>
                  </th>
                  <td className="budget">$2500 USD</td>
                  <td>
                    <Badge color="" className="badge-dot mr-4">
                      <i className="bg-warning" />
                      <span className="status">pending</span>
                    </Badge>
                  </td>
                  <td>
                    <div className="avatar-group">
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip649644480"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-1.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip649644480">
                        Ryan Tompson
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip271118748"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-2.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip271118748">
                        Romina Hadid
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip456883903"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-3.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip456883903">
                        Alexander Smith
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip427983389"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-4.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip427983389">
                        Jessica Doe
                      </UncontrolledTooltip>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="completion mr-2">60%</span>
                      <div>
                        <Progress max="100" value="60" color="warning" />
                      </div>
                    </div>
                  </td>
                  <td className="text-right">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        className="btn-icon-only text-light"
                        color=""
                        role="button"
                        size="sm"
                      >
                        <i className="fas fa-ellipsis-v" />
                      </DropdownToggle>
                      <DropdownMenu className="dropdown-menu-arrow" right>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Another action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Something else here
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <Media className="align-items-center">
                      <a
                        className="avatar rounded-circle mr-3"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/angular.jpg")}
                        />
                      </a>
                      <Media>
                        <span className="name mb-0 text-sm">
                          Angular Now UI Kit PRO
                        </span>
                      </Media>
                    </Media>
                  </th>
                  <td className="budget">$1800 USD</td>
                  <td>
                    <Badge color="" className="badge-dot mr-4">
                      <i className="bg-success" />
                      <span className="status">completed</span>
                    </Badge>
                  </td>
                  <td>
                    <div className="avatar-group">
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip308604570"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-1.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip308604570">
                        Ryan Tompson
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip463448556"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-2.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip463448556">
                        Romina Hadid
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip474990624"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-3.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip474990624">
                        Alexander Smith
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip371113692"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-4.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip371113692">
                        Jessica Doe
                      </UncontrolledTooltip>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="completion mr-2">100%</span>
                      <div>
                        <Progress max="100" value="100" color="success" />
                      </div>
                    </div>
                  </td>
                  <td className="text-right">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        className="btn-icon-only text-light"
                        color=""
                        role="button"
                        size="sm"
                      >
                        <i className="fas fa-ellipsis-v" />
                      </DropdownToggle>
                      <DropdownMenu className="dropdown-menu-arrow" right>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Another action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Something else here
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <Media className="align-items-center">
                      <a
                        className="avatar rounded-circle mr-3"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/sketch.jpg")}
                        />
                      </a>
                      <Media>
                        <span className="name mb-0 text-sm">
                          Black Dashboard
                        </span>
                      </Media>
                    </Media>
                  </th>
                  <td className="budget">$3150 USD</td>
                  <td>
                    <Badge color="" className="badge-dot mr-4">
                      <i className="bg-danger" />
                      <span className="status">delayed</span>
                    </Badge>
                  </td>
                  <td>
                    <div className="avatar-group">
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip178353307"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-1.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip178353307">
                        Ryan Tompson
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip110940759"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-2.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip110940759">
                        Romina Hadid
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip721474482"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-3.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip721474482">
                        Alexander Smith
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip248059973"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-4.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip248059973">
                        Jessica Doe
                      </UncontrolledTooltip>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="completion mr-2">72%</span>
                      <div>
                        <Progress max="100" value="72" color="danger" />
                      </div>
                    </div>
                  </td>
                  <td className="text-right">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        className="btn-icon-only text-light"
                        color=""
                        role="button"
                        size="sm"
                      >
                        <i className="fas fa-ellipsis-v" />
                      </DropdownToggle>
                      <DropdownMenu className="dropdown-menu-arrow" right>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Another action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Something else here
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <Media className="align-items-center">
                      <a
                        className="avatar rounded-circle mr-3"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/react.jpg")}
                        />
                      </a>
                      <Media>
                        <span className="name mb-0 text-sm">
                          React Material Dashboard
                        </span>
                      </Media>
                    </Media>
                  </th>
                  <td className="budget">$4400 USD</td>
                  <td>
                    <Badge color="" className="badge-dot mr-4">
                      <i className="bg-info" />
                      <span className="status">on schedule</span>
                    </Badge>
                  </td>
                  <td>
                    <div className="avatar-group">
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip338716581"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-1.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip338716581">
                        Ryan Tompson
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip384272281"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-2.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip384272281">
                        Romina Hadid
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip714277819"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-3.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip714277819">
                        Alexander Smith
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip352802795"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-4.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip352802795">
                        Jessica Doe
                      </UncontrolledTooltip>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="completion mr-2">90%</span>
                      <div>
                        <Progress max="100" value="90" color="info" />
                      </div>
                    </div>
                  </td>
                  <td className="text-right">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        className="btn-icon-only text-light"
                        color=""
                        role="button"
                        size="sm"
                      >
                        <i className="fas fa-ellipsis-v" />
                      </DropdownToggle>
                      <DropdownMenu className="dropdown-menu-arrow" right>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Another action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Something else here
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <Media className="align-items-center">
                      <a
                        className="avatar rounded-circle mr-3"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/vue.jpg")}
                        />
                      </a>
                      <Media>
                        <span className="name mb-0 text-sm">
                          Vue Paper UI Kit PRO
                        </span>
                      </Media>
                    </Media>
                  </th>
                  <td className="budget">$2200 USD</td>
                  <td>
                    <Badge color="" className="badge-dot mr-4">
                      <i className="bg-success" />
                      <span className="status">completed</span>
                    </Badge>
                  </td>
                  <td>
                    <div className="avatar-group">
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip261050883"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-1.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip261050883">
                        Ryan Tompson
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip704196347"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-2.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip704196347">
                        Romina Hadid
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip455439475"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-3.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip455439475">
                        Alexander Smith
                      </UncontrolledTooltip>
                      <a
                        className="avatar avatar-sm rounded-circle"
                        href="#pablo"
                        id="tooltip948033723"
                        onClick={e => e.preventDefault()}
                      >
                        <img
                          alt="..."
                          src={require("../../../../assets/img/theme/team-4.jpg")}
                        />
                      </a>
                      <UncontrolledTooltip delay={0} target="tooltip948033723">
                        Jessica Doe
                      </UncontrolledTooltip>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="completion mr-2">100%</span>
                      <div>
                        <Progress max="100" value="100" color="success" />
                      </div>
                    </div>
                  </td>
                  <td className="text-right">
                    <UncontrolledDropdown>
                      <DropdownToggle
                        className="btn-icon-only text-light"
                        color=""
                        role="button"
                        size="sm"
                      >
                        <i className="fas fa-ellipsis-v" />
                      </DropdownToggle>
                      <DropdownMenu className="dropdown-menu-arrow" right>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Another action
                        </DropdownItem>
                        <DropdownItem
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          Something else here
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a href="https://listjs.com/?ref=creativetim" target="_blank">
            ListJS documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default ListJs;
