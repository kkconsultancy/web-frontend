/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// react plugin that creates text editor
import ReactQuill from "react-quill";

const codeExample = `import React from "react";
// react plugin that creates text editor
import ReactQuill from "react-quill";

class Quill extends React.Component {
  state = {
    reactQuillText: ""
  };
  handleReactQuillChange = value => {
    this.setState({
      reactQuillText: value
    });
  };
  render() {
    return (
      <>
        <ReactQuill
          value={this.state.reactQuillText}
          onChange={this.handleReactQuillChange}
          theme="snow"
          modules={{
            toolbar: [
              ["bold", "italic"],
              ["link", "blockquote", "code", "image"],
              [
                {
                  list: "ordered"
                },
                {
                  list: "bullet"
                }
              ]
            ]
          }}
        />
      </>
    );
  }
}

export default Quill;
`;

class Quill extends React.Component {
  state = {
    reactQuillText: ""
  };
  handleReactQuillChange = value => {
    this.setState({
      reactQuillText: value
    });
  };
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            React Quill v1.3.3
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">Your powerful rich text editor.</p>
        <hr />
        <h2 id="usage">Usage</h2>
        <h3 id="css">CSS</h3>
        <p>
          In order to use this plugin on your page you will need to import the
          following styles inside your index.js:
        </p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {`import "../../../../assets/vendor/quill/dist/quill.core.css";`}
        </SyntaxHighlighter>
        <h2 id="example">Example</h2>
        <div className="ct-example bg-white">
          <ReactQuill
            value={this.state.reactQuillText}
            onChange={this.handleReactQuillChange}
            theme="snow"
            modules={{
              toolbar: [
                ["bold", "italic"],
                ["link", "blockquote", "code", "image"],
                [
                  {
                    list: "ordered"
                  },
                  {
                    list: "bullet"
                  }
                ]
              ]
            }}
          />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://github.com/zenoamaro/react-quill#readme?ref=creativetim"
            target="_blank"
          >
            react-quill documentation
          </a>{" "}
          and{" "}
          <a href="https://quilljs.com/?ref=creativetim" target="_blank">
            quill.js documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Quill;
