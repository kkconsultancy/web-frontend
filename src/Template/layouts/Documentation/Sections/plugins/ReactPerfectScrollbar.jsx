/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";

class ReactPerfectScrollbar extends React.Component {
  state = {};
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            React Perfect Scrollbar v1.4.4
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          This is a wrapper to allow use perfect-scrollbar in React. It is used inside our project for creating nice scrollbars for the Sidebar component on Windows machines
        </p>
        <hr />
        <h3 id="initialization">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://github.com/goldenyz/react-perfect-scrollbar?ref=creativetim"
            target="_blank"
          >
            this plugins documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default ReactPerfectScrollbar;
