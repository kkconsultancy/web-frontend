/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// react plugin that prints a given react component
import ReactToPrint from "react-to-print";
// react component for creating dynamic tables
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
// reactstrap components
import {
  Button,
  ButtonGroup,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

import { dataTable } from "../../../../variables/general";

const pagination = paginationFactory({
  page: 1,
  alwaysShowAllBtns: true,
  showTotal: true,
  withFirstAndLast: false,
  sizePerPageRenderer: ({ options, currSizePerPage, onSizePerPageChange }) => (
    <div className="dataTables_length" id="datatable-basic_length">
      <label>
        Show{" "}
        {
          <select
            name="datatable-basic_length"
            aria-controls="datatable-basic"
            className="form-control form-control-sm"
            onChange={e => onSizePerPageChange(e.target.value)}
          >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        }{" "}
        entries.
      </label>
    </div>
  )
});

const { SearchBar } = Search;

const codeExample = `import React from "react";
// react component for creating dynamic tables
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";

import { dataTable } from "variables/general";

const pagination = paginationFactory({
  page: 1,
  alwaysShowAllBtns: true,
  showTotal: true,
  withFirstAndLast: false,
  sizePerPageRenderer: ({ options, currSizePerPage, onSizePerPageChange }) => (
    <div className="dataTables_length" id="datatable-basic_length">
      <label>
        Show{" "}
        {
          <select
            name="datatable-basic_length"
            aria-controls="datatable-basic"
            className="form-control form-control-sm"
            onChange={e => onSizePerPageChange(e.target.value)}
          >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        }{" "}
        entries.
      </label>
    </div>
  )
});

const { SearchBar } = Search;

class ReactBSTables extends React.Component {
  render() {
    return (
      <>
        <ToolkitProvider
          data={dataTable}
          keyField="name"
          columns={[
            {
              dataField: "name",
              text: "Name",
              sort: true
            },
            {
              dataField: "position",
              text: "Position",
              sort: true
            },
            {
              dataField: "office",
              text: "Office",
              sort: true
            },
            {
              dataField: "Age",
              text: "age",
              sort: true
            },
            {
              dataField: "start_date",
              text: "Start date",
              sort: true
            },
            {
              dataField: "salary",
              text: "Salary",
              sort: true
            }
          ]}
          search
        >
          {props => (
            <div className="py-4">
              <div
                id="datatable-basic_filter"
                className="dataTables_filter px-4 pb-1"
              >
                <label>
                  Search:
                  <SearchBar
                    className="form-control-sm"
                    placeholder=""
                    {...props.searchProps}
                  />
                </label>
              </div>
              <BootstrapTable
                {...props.baseProps}
                bootstrap4={true}
                pagination={pagination}
                bordered={false}
              />
            </div>
          )}
        </ToolkitProvider>
      </>
    );
  }
}

export default ReactBSTables;
`;
const codeButtons = `import React from "react";
// react plugin that prints a given react component
import ReactToPrint from "react-to-print";
// react component for creating dynamic tables
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
// reactstrap components
import {
  Button,
  ButtonGroup,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

import { dataTable } from "variables/general";

const pagination = paginationFactory({
  page: 1,
  alwaysShowAllBtns: true,
  showTotal: true,
  withFirstAndLast: false,
  sizePerPageRenderer: ({ options, currSizePerPage, onSizePerPageChange }) => (
    <div className="dataTables_length" id="datatable-basic_length">
      <label>
        Show{" "}
        {
          <select
            name="datatable-basic_length"
            aria-controls="datatable-basic"
            className="form-control form-control-sm"
            onChange={e => onSizePerPageChange(e.target.value)}
          >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        }{" "}
        entries.
      </label>
    </div>
  )
});

const { SearchBar } = Search;

class ReactBSTables extends React.Component {
  state = {
    alert: null
  };
  // this function will copy to clipboard an entire table,
  // so you can paste it inside an excel or csv file
  copyToClipboardAsTable = el => {
    var body = document.body,
      range,
      sel;
    if (document.createRange && window.getSelection) {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      try {
        range.selectNodeContents(el);
        sel.addRange(range);
      } catch (e) {
        range.selectNode(el);
        sel.addRange(range);
      }
      document.execCommand("copy");
    } else if (body.createTextRange) {
      range = body.createTextRange();
      range.moveToElementText(el);
      range.select();
      range.execCommand("Copy");
    }
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Good job!"
          onConfirm={() => this.setState({ alert: null })}
          onCancel={() => this.setState({ alert: null })}
          confirmBtnBsStyle="info"
          btnSize=""
        >
          Copied to clipboard!
        </ReactBSAlert>
      )
    });
  };
  render() {
    return (
      <>
        {this.state.alert}
        <ToolkitProvider
          data={dataTable}
          keyField="name"
          columns={[
            {
              dataField: "name",
              text: "Name",
              sort: true
            },
            {
              dataField: "position",
              text: "Position",
              sort: true
            },
            {
              dataField: "office",
              text: "Office",
              sort: true
            },
            {
              dataField: "Age",
              text: "age",
              sort: true
            },
            {
              dataField: "start_date",
              text: "Start date",
              sort: true
            },
            {
              dataField: "salary",
              text: "Salary",
              sort: true
            }
          ]}
          search
        >
          {props => (
            <div className="py-4">
              <Container fluid>
                <Row>
                  <Col xs={12} sm={6}>
                    <ButtonGroup>
                      <Button
                        className="buttons-copy buttons-html5"
                        color="default"
                        size="sm"
                        id="copy-tooltip"
                        onClick={() =>
                          this.copyToClipboardAsTable(
                            document.getElementById("react-bs-table")
                          )
                        }
                      >
                        <span>Copy</span>
                      </Button>
                      <ReactToPrint
                        trigger={() => (
                          <Button
                            href="#"
                            color="default"
                            size="sm"
                            className="buttons-copy buttons-html5"
                            id="print-tooltip"
                          >
                            Print
                          </Button>
                        )}
                        content={() => this.componentRef}
                      />
                    </ButtonGroup>
                    <UncontrolledTooltip placement="top" target="print-tooltip">
                      This will open a print page with the visible rows of the
                      table.
                    </UncontrolledTooltip>
                    <UncontrolledTooltip placement="top" target="copy-tooltip">
                      This will copy to your clipboard the visible rows of the
                      table.
                    </UncontrolledTooltip>
                  </Col>
                  <Col xs={12} sm={6}>
                    <div
                      id="datatable-basic_filter"
                      className="dataTables_filter px-4 pb-1 float-right"
                    >
                      <label>
                        Search:
                        <SearchBar
                          className="form-control-sm"
                          placeholder=""
                          {...props.searchProps}
                        />
                      </label>
                    </div>
                  </Col>
                </Row>
              </Container>
              <BootstrapTable
                ref={el => (this.componentRef = el)}
                {...props.baseProps}
                bootstrap4={true}
                pagination={pagination}
                bordered={false}
                id="react-bs-table"
              />
            </div>
          )}
        </ToolkitProvider>
      </>
    );
  }
}

export default ReactBSTables;
`;

class Datatables extends React.Component {
  state = {
    alert: null
  };
  // this function will copy to clipboard an entire table,
  // so you can paste it inside an excel or csv file
  copyToClipboardAsTable = el => {
    var body = document.body,
      range,
      sel;
    if (document.createRange && window.getSelection) {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      try {
        range.selectNodeContents(el);
        sel.addRange(range);
      } catch (e) {
        range.selectNode(el);
        sel.addRange(range);
      }
      document.execCommand("copy");
    } else if (body.createTextRange) {
      range = body.createTextRange();
      range.moveToElementText(el);
      range.select();
      range.execCommand("Copy");
    }
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Good job!"
          onConfirm={() => this.setState({ alert: null })}
          onCancel={() => this.setState({ alert: null })}
          confirmBtnBsStyle="info"
          btnSize=""
        >
          Copied to clipboard!
        </ReactBSAlert>
      )
    });
  };
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            react-bootstrap-table-next v3.0.3 and
            react-bootstrap-table2-paginator v2.0.5 and
            react-bootstrap-table2-toolkit v1.4.0
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Add advanced interaction controls to your HTML/React tables the free
          &amp; easy way
        </p>
        <hr />
        <h2 id="example">Example</h2>
        <p>
          This is an exmaple of data table using the well known
          react-bootstrap-table2 plugin. This is a minimal setup in order to get
          started fast.
        </p>
        <div className="ct-example bg-white">
          <ToolkitProvider
            data={dataTable}
            keyField="name"
            columns={[
              {
                dataField: "name",
                text: "Name",
                sort: true
              },
              {
                dataField: "position",
                text: "Position",
                sort: true
              },
              {
                dataField: "office",
                text: "Office",
                sort: true
              },
              {
                dataField: "Age",
                text: "age",
                sort: true
              },
              {
                dataField: "start_date",
                text: "Start date",
                sort: true
              },
              {
                dataField: "salary",
                text: "Salary",
                sort: true
              }
            ]}
            search
          >
            {props => (
              <div className="py-4 table-responsive">
                <div
                  id="datatable-basic_filter"
                  className="dataTables_filter px-4 pb-1"
                >
                  <label>
                    Search:
                    <SearchBar
                      className="form-control-sm"
                      placeholder=""
                      {...props.searchProps}
                    />
                  </label>
                </div>
                <BootstrapTable
                  {...props.baseProps}
                  bootstrap4={true}
                  pagination={pagination}
                  bordered={false}
                />
              </div>
            )}
          </ToolkitProvider>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h2 id="buttons">Buttons</h2>
        <p>
          This example has two buttons used to copy (you can paste it inside a
          .csv file) and print the visible part of the table.
        </p>
        <div className="ct-example bg-white">
          <ToolkitProvider
            data={dataTable}
            keyField="name"
            columns={[
              {
                dataField: "name",
                text: "Name",
                sort: true
              },
              {
                dataField: "position",
                text: "Position",
                sort: true
              },
              {
                dataField: "office",
                text: "Office",
                sort: true
              },
              {
                dataField: "Age",
                text: "age",
                sort: true
              },
              {
                dataField: "start_date",
                text: "Start date",
                sort: true
              },
              {
                dataField: "salary",
                text: "Salary",
                sort: true
              }
            ]}
            search
          >
            {props => (
              <div className="py-4 table-responsive">
                <Container fluid>
                  <Row>
                    <Col xs={12} sm={6}>
                      <ButtonGroup>
                        <Button
                          className="buttons-copy buttons-html5"
                          color="default"
                          size="sm"
                          id="copy-tooltip"
                          onClick={() =>
                            this.copyToClipboardAsTable(
                              document.getElementById("react-bs-table")
                            )
                          }
                        >
                          <span>Copy</span>
                        </Button>
                        <ReactToPrint.default
                          trigger={() => (
                            <Button
                              color="default"
                              size="sm"
                              className="buttons-copy buttons-html5"
                              id="print-tooltip"
                            >
                              Print
                            </Button>
                          )}
                          content={() => this.componentRef}
                        />
                      </ButtonGroup>
                      <UncontrolledTooltip
                        placement="top"
                        target="print-tooltip"
                      >
                        This will open a print page with the visible rows
                        of the table.
                      </UncontrolledTooltip>
                      <UncontrolledTooltip
                        placement="top"
                        target="copy-tooltip"
                      >
                        This will copy to your clipboard the visible rows
                        of the table.
                      </UncontrolledTooltip>
                    </Col>
                    <Col xs={12} sm={6}>
                      <div
                        id="datatable-basic_filter"
                        className="dataTables_filter px-4 pb-1 float-right"
                      >
                        <label>
                          Search:
                          <SearchBar
                            className="form-control-sm"
                            placeholder=""
                            {...props.searchProps}
                          />
                        </label>
                      </div>
                    </Col>
                  </Row>
                </Container>
                <BootstrapTable
                  ref={el => (this.componentRef = el)}
                  {...props.baseProps}
                  bootstrap4={true}
                  pagination={pagination}
                  bordered={false}
                  id="react-bs-table"
                />
              </div>
            )}
          </ToolkitProvider>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeButtons}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/getting-started.html?ref=creativetim"
            target="_blank"
          >
            react-bootstrap-table-next documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Datatables;
