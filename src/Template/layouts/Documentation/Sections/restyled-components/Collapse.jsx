/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  UncontrolledCollapse,
  Collapse
} from "reactstrap";

const codeExample = `import React from "react";

import { UncontrolledCollapse, Button, CardBody, Card } from "reactstrap";

class Collapses extends React.Component {
  render() {
    return (
      <>
        <p>
          <Button color="primary" href="#collapseExample" id="linkToggler">
            Link with id
          </Button>
          <Button color="primary" id="buttonToggler">
            Button with id
          </Button>
        </p>
        <UncontrolledCollapse toggler="#linkToggler,#buttonToggler">
          <Card>
            <CardBody>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Nesciunt magni, voluptas debitis similique porro a molestias
              consequuntur earum odio officiis natus, amet hic, iste sed
              dignissimos esse fuga! Minus, alias.
            </CardBody>
          </Card>
        </UncontrolledCollapse>
      </>
    );
  }
}

export default Collapses;`;
const codeAccordion = `import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Collapse,
} from "reactstrap";

class Panels extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedCollapses: ["collapseOne"]
    };
  }
  // with this function we create an array with the opened collapses
  // it is like a toggle function for all collapses from this page
  collapsesToggle = collapse => {
    let openedCollapses = this.state.openedCollapses;
    if (openedCollapses.includes(collapse)) {
      this.setState({
        openedCollapses: []
      });
    } else {
      this.setState({
        openedCollapses: [collapse]
      });
    }
  };
  render() {
    return (
      <>
        <div className="accordion">
          <Card className="card-plain">
            <CardHeader
              role="tab"
              onClick={() => this.collapsesToggle("collapseOne")}
              aria-expanded={this.state.openedCollapses.includes(
                "collapseOne"
              )}
            >
              <h5 className="mb-0">Collapsible Group Item #1</h5>
            </CardHeader>
            <Collapse
              role="tabpanel"
              isOpen={this.state.openedCollapses.includes("collapseOne")}
            >
              <CardBody>
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. 3 wolf moon officia aute,
                non cupidatat skateboard dolor brunch. Food truck quinoa
                nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                aliqua put a bird on it squid single-origin coffee nulla
                assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                beer labore wes anderson cred nesciunt sapiente ea proident.
                Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                beer farm-to-table, raw denim aesthetic synth nesciunt you
                probably haven't heard of them accusamus labore sustainable
                VHS.
              </CardBody>
            </Collapse>
          </Card>
          <Card className="card-plain">
            <CardHeader
              role="tab"
              onClick={() => this.collapsesToggle("collapseTwo")}
              aria-expanded={this.state.openedCollapses.includes(
                "collapseTwo"
              )}
            >
              <h5 className="mb-0">Collapsible Group Item #2</h5>
            </CardHeader>
            <Collapse
              role="tabpanel"
              isOpen={this.state.openedCollapses.includes("collapseTwo")}
            >
              <CardBody>
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. 3 wolf moon officia aute,
                non cupidatat skateboard dolor brunch. Food truck quinoa
                nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                aliqua put a bird on it squid single-origin coffee nulla
                assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                beer labore wes anderson cred nesciunt sapiente ea proident.
                Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                beer farm-to-table, raw denim aesthetic synth nesciunt you
                probably haven't heard of them accusamus labore sustainable
                VHS.
              </CardBody>
            </Collapse>
          </Card>
          <Card className="card-plain">
            <CardHeader
              role="tab"
              onClick={() => this.collapsesToggle("collapseThree")}
              aria-expanded={this.state.openedCollapses.includes(
                "collapseThree"
              )}
            >
              <h5 className="mb-0">Collapsible Group Item #3</h5>
            </CardHeader>
            <Collapse
              role="tabpanel"
              isOpen={this.state.openedCollapses.includes("collapseThree")}
            >
              <CardBody>
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. 3 wolf moon officia aute,
                non cupidatat skateboard dolor brunch. Food truck quinoa
                nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                aliqua put a bird on it squid single-origin coffee nulla
                assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                beer labore wes anderson cred nesciunt sapiente ea proident.
                Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                beer farm-to-table, raw denim aesthetic synth nesciunt you
                probably haven't heard of them accusamus labore sustainable
                VHS.
              </CardBody>
            </Collapse>
          </Card>
        </div>
      </>
    );
  }
}

export default Panels;`;

class Collapses extends React.Component {
  state = {
    openedCollapses: ["collapseOne"]
  };
  // with this function we create an array with the opened collapses
  // it is like a toggle function for all collapses from this page
  collapsesToggle = collapse => {
    let openedCollapses = this.state.openedCollapses;
    if (openedCollapses.includes(collapse)) {
      this.setState({
        openedCollapses: []
      });
    } else {
      this.setState({
        openedCollapses: [collapse]
      });
    }
  };
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Collapse
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Toggle the visibility of content across your project with a few
          classes and our JavaScript plugins.
        </p>
        <hr />
        <h2 id="examples">Examples</h2>
        <div className="ct-example bg-white">
          <p>
            <Button color="primary" href="#collapseExample" id="linkToggler">
              Link with id
            </Button>
            <Button color="primary" id="buttonToggler">
              Button with id
            </Button>
          </p>
          <UncontrolledCollapse toggler="#linkToggler,#buttonToggler">
            <Card>
              <CardBody>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Nesciunt magni, voluptas debitis similique porro a molestias
                consequuntur earum odio officiis natus, amet hic, iste sed
                dignissimos esse fuga! Minus, alias.
              </CardBody>
            </Card>
          </UncontrolledCollapse>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h2 id="accordion">Accordion</h2>
        <div className="ct-example bg-white">
          <div className="accordion">
            <Card className="card-plain">
              <CardHeader
                role="tab"
                onClick={() => this.collapsesToggle("collapseOne")}
                aria-expanded={this.state.openedCollapses.includes(
                  "collapseOne"
                )}
              >
                <h5 className="mb-0">Collapsible Group Item #1</h5>
              </CardHeader>
              <Collapse
                role="tabpanel"
                isOpen={this.state.openedCollapses.includes("collapseOne")}
              >
                <CardBody>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life
                  accusamus terry richardson ad squid. 3 wolf moon officia aute,
                  non cupidatat skateboard dolor brunch. Food truck quinoa
                  nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                  aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                  beer labore wes anderson cred nesciunt sapiente ea proident.
                  Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                  beer farm-to-table, raw denim aesthetic synth nesciunt you
                  probably haven't heard of them accusamus labore sustainable
                  VHS.
                </CardBody>
              </Collapse>
            </Card>
            <Card className="card-plain">
              <CardHeader
                role="tab"
                onClick={() => this.collapsesToggle("collapseTwo")}
                aria-expanded={this.state.openedCollapses.includes(
                  "collapseTwo"
                )}
              >
                <h5 className="mb-0">Collapsible Group Item #2</h5>
              </CardHeader>
              <Collapse
                role="tabpanel"
                isOpen={this.state.openedCollapses.includes("collapseTwo")}
              >
                <CardBody>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life
                  accusamus terry richardson ad squid. 3 wolf moon officia aute,
                  non cupidatat skateboard dolor brunch. Food truck quinoa
                  nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                  aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                  beer labore wes anderson cred nesciunt sapiente ea proident.
                  Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                  beer farm-to-table, raw denim aesthetic synth nesciunt you
                  probably haven't heard of them accusamus labore sustainable
                  VHS.
                </CardBody>
              </Collapse>
            </Card>
            <Card className="card-plain">
              <CardHeader
                role="tab"
                onClick={() => this.collapsesToggle("collapseThree")}
                aria-expanded={this.state.openedCollapses.includes(
                  "collapseThree"
                )}
              >
                <h5 className="mb-0">Collapsible Group Item #3</h5>
              </CardHeader>
              <Collapse
                role="tabpanel"
                isOpen={this.state.openedCollapses.includes("collapseThree")}
              >
                <CardBody>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life
                  accusamus terry richardson ad squid. 3 wolf moon officia aute,
                  non cupidatat skateboard dolor brunch. Food truck quinoa
                  nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                  aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                  beer labore wes anderson cred nesciunt sapiente ea proident.
                  Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                  beer farm-to-table, raw denim aesthetic synth nesciunt you
                  probably haven't heard of them accusamus labore sustainable
                  VHS.
                </CardBody>
              </Collapse>
            </Card>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeAccordion}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://reactstrap.github.io/components/collapse/?ref=creativetim"
            target="_blank"
          >
            reactstrap collapse documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Collapses;
