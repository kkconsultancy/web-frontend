/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

const codeExamples = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup>
          <ListGroupItem>Cras justo odio</ListGroupItem>
          <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
          <ListGroupItem>Morbi leo risus</ListGroupItem>
          <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
          <ListGroupItem>Vestibulum at eros</ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeActiveItems = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup>
          <ListGroupItem className="active">Cras justo odio</ListGroupItem>
          <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
          <ListGroupItem>Morbi leo risus</ListGroupItem>
          <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
          <ListGroupItem>Vestibulum at eros</ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeLinks = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup>
          <ListGroupItem
            className="list-group-item-action active"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            Cras justo odio
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            Dapibus ac facilisis in
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            Morbi leo risus
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            Porta ac consectetur ac
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action disabled"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            Vestibulum at eros
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeWithBadges = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup>
          <ListGroupItem className="d-flex justify-content-between align-items-center">
            Cras justo odio{" "}
            <Badge color="primary" pill>
              14
            </Badge>
          </ListGroupItem>
          <ListGroupItem className="d-flex justify-content-between align-items-center">
            Dapibus ac facilisis in{" "}
            <Badge color="primary" pill>
              2
            </Badge>
          </ListGroupItem>
          <ListGroupItem className="d-flex justify-content-between align-items-center">
            Morbi leo risus{" "}
            <Badge color="primary" pill>
              1
            </Badge>
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeMembers = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup className="list my--3" flush>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/team-1.jpg")}
                  />
                </a>
              </Col>
              <div className="col ml--2">
                <h4 className="mb-0">
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    John Michael
                  </a>
                </h4>
                <span className="text-success">●</span>
                <small>Online</small>
              </div>
              <Col className="col-auto">
                <Button color="primary" size="sm" type="button">
                  Add
                </Button>
              </Col>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/team-2.jpg")}
                  />
                </a>
              </Col>
              <div className="col ml--2">
                <h4 className="mb-0">
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    Alex Smith
                  </a>
                </h4>
                <span className="text-warning">●</span>
                <small>In a meeting</small>
              </div>
              <Col className="col-auto">
                <Button color="primary" size="sm" type="button">
                  Add
                </Button>
              </Col>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/team-3.jpg")}
                  />
                </a>
              </Col>
              <div className="col ml--2">
                <h4 className="mb-0">
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    Samantha Ivy
                  </a>
                </h4>
                <span className="text-danger">●</span>
                <small>Offline</small>
              </div>
              <Col className="col-auto">
                <Button color="primary" size="sm" type="button">
                  Add
                </Button>
              </Col>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/team-4.jpg")}
                  />
                </a>
              </Col>
              <div className="col ml--2">
                <h4 className="mb-0">
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    John Michael
                  </a>
                </h4>
                <span className="text-success">●</span>
                <small>Online</small>
              </div>
              <Col className="col-auto">
                <Button color="primary" size="sm" type="button">
                  Add
                </Button>
              </Col>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/team-5.jpg")}
                  />
                </a>
              </Col>
              <div className="col ml--2">
                <h4 className="mb-0">
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    John Snow
                  </a>
                </h4>
                <span className="text-success">●</span>
                <small>Online</small>
              </div>
              <Col className="col-auto">
                <Button color="primary" size="sm" type="button">
                  Add
                </Button>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeChecklist = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup data-toggle="checklist" flush>
          <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
            <div className="checklist-item checklist-item-success">
              <div className="checklist-info">
                <h5 className="checklist-title mb-0">Call with Dave</h5>
                <small>10:30 AM</small>
              </div>
              <div>
                <div className="custom-control custom-checkbox custom-checkbox-success">
                  <input
                    className="custom-control-input"
                    defaultChecked
                    id="chk-todo-task-1"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="chk-todo-task-1"
                  />
                </div>
              </div>
            </div>
          </ListGroupItem>
          <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
            <div className="checklist-item checklist-item-warning">
              <div className="checklist-info">
                <h5 className="checklist-title mb-0">Lunch meeting</h5>
                <small>10:30 AM</small>
              </div>
              <div>
                <div className="custom-control custom-checkbox custom-checkbox-warning">
                  <input
                    className="custom-control-input"
                    id="chk-todo-task-2"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="chk-todo-task-2"
                  />
                </div>
              </div>
            </div>
          </ListGroupItem>
          <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
            <div className="checklist-item checklist-item-info">
              <div className="checklist-info">
                <h5 className="checklist-title mb-0">
                  Argon Dashboard Launch
                </h5>
                <small>10:30 AM</small>
              </div>
              <div>
                <div className="custom-control custom-checkbox custom-checkbox-info">
                  <input
                    className="custom-control-input"
                    id="chk-todo-task-3"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="chk-todo-task-3"
                  />
                </div>
              </div>
            </div>
          </ListGroupItem>
          <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
            <div className="checklist-item checklist-item-danger">
              <div className="checklist-info">
                <h5 className="checklist-title mb-0">Winter Hackaton</h5>
                <small>10:30 AM</small>
              </div>
              <div>
                <div className="custom-control custom-checkbox custom-checkbox-danger">
                  <input
                    className="custom-control-input"
                    defaultChecked
                    id="chk-todo-task-4"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="chk-todo-task-4"
                  />
                </div>
              </div>
            </div>
          </ListGroupItem>
          <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
            <div className="checklist-item checklist-item-success">
              <div className="checklist-info">
                <h5 className="checklist-title mb-0">Dinner with Family</h5>
                <small>10:30 AM</small>
              </div>
              <div>
                <div className="custom-control custom-checkbox custom-checkbox-success">
                  <input
                    className="custom-control-input"
                    defaultChecked
                    id="chk-todo-task-5"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="chk-todo-task-5"
                  />
                </div>
              </div>
            </div>
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeProgress = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup className="list my--3" flush>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/bootstrap.jpg")}
                  />
                </a>
              </Col>
              <div className="col">
                <h5>Argon Design System</h5>
                <Progress
                  className="progress-xs mb-0"
                  max="100"
                  value="60"
                  color="warning"
                />
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/angular.jpg")}
                  />
                </a>
              </Col>
              <div className="col">
                <h5>Angular Now UI Kit PRO</h5>
                <Progress
                  className="progress-xs mb-0"
                  max="100"
                  value="100"
                  color="success"
                />
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/sketch.jpg")}
                  />
                </a>
              </Col>
              <div className="col">
                <h5>Black Dashboard</h5>
                <Progress
                  className="progress-xs mb-0"
                  max="100"
                  value="72"
                  color="danger"
                />
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/react.jpg")}
                  />
                </a>
              </Col>
              <div className="col">
                <h5>React Material Dashboard</h5>
                <Progress
                  className="progress-xs mb-0"
                  max="100"
                  value="90"
                  color="info"
                />
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img alt="..." src={require("../../../../assets/img/theme/vue.jpg")} />
                </a>
              </Col>
              <div className="col">
                <h5>Vue Paper UI Kit PRO</h5>
                <Progress
                  className="progress-xs mb-0"
                  max="100"
                  value="100"
                  color="success"
                />
              </div>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeMessages = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup flush>
          <ListGroupItem
            className="list-group-item-action flex-column align-items-start py-4 px-4"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            <div className="d-flex w-100 justify-content-between">
              <div>
                <div className="d-flex w-100 align-items-center">
                  <img
                    alt="..."
                    className="avatar avatar-xs mr-2"
                    src={require("../../../../assets/img/theme/team-1.jpg")}
                  />
                  <h5 className="mb-1">Tim</h5>
                </div>
              </div>
              <small>2 hrs ago</small>
            </div>
            <h4 className="mt-3 mb-1">New order for Argon Dashboard</h4>
            <p className="text-sm mb-0">
              Doasdnec id elit non mi porta gravida at eget metus. Maecenas
              sed diam eget risus varius blandit.
            </p>
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action flex-column align-items-start py-4 px-4"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            <div className="d-flex w-100 justify-content-between">
              <div>
                <div className="d-flex w-100 align-items-center">
                  <img
                    alt="..."
                    className="avatar avatar-xs mr-2"
                    src={require("../../../../assets/img/theme/team-2.jpg")}
                  />
                  <h5 className="mb-1">Mike</h5>
                </div>
              </div>
              <small>1 day ago</small>
            </div>
            <h4 className="mt-3 mb-1">
              <span className="text-info">●</span>
              Your theme has been updated
            </h4>
            <p className="text-sm mb-0">
              Doasdnec id elit non mi porta gravida at eget metus. Maecenas
              sed diam eget risus varius blandit.
            </p>
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action flex-column align-items-start py-4 px-4"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            <div className="d-flex w-100 justify-content-between">
              <div>
                <div className="d-flex w-100 align-items-center">
                  <img
                    alt="..."
                    className="avatar avatar-xs mr-2"
                    src={require("../../../../assets/img/theme/team-3.jpg")}
                  />
                  <h5 className="mb-1">Tim</h5>
                </div>
              </div>
              <small>2 hrs ago</small>
            </div>
            <h4 className="mt-3 mb-1">New order for Argon Dashboard</h4>
            <p className="text-sm mb-0">
              Doasdnec id elit non mi porta gravida at eget metus. Maecenas
              sed diam eget risus varius blandit.
            </p>
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeNotifications = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup flush>
          <ListGroupItem
            className="list-group-item-action"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            <Row className="align-items-center">
              <Col className="col-auto">

                <img
                  alt="..."
                  className="avatar rounded-circle"
                  src={require("../../../../assets/img/theme/team-1.jpg")}
                />
              </Col>
              <div className="col ml--2">
                <div className="d-flex justify-content-between align-items-center">
                  <div>
                    <h4 className="mb-0 text-sm">John Snow</h4>
                  </div>
                  <div className="text-right text-muted">
                    <small>2 hrs ago</small>
                  </div>
                </div>
                <p className="text-sm mb-0">
                  Let's meet at Starbucks at 11:30. Wdyt?
                </p>
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            <Row className="align-items-center">
              <Col className="col-auto">

                <img
                  alt="..."
                  className="avatar rounded-circle"
                  src={require("../../../../assets/img/theme/team-2.jpg")}
                />
              </Col>
              <div className="col ml--2">
                <div className="d-flex justify-content-between align-items-center">
                  <div>
                    <h4 className="mb-0 text-sm">John Snow</h4>
                  </div>
                  <div className="text-right text-muted">
                    <small>3 hrs ago</small>
                  </div>
                </div>
                <p className="text-sm mb-0">
                  A new issue has been reported for Argon.
                </p>
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem
            className="list-group-item-action"
            href="#pablo"
            onClick={e => e.preventDefault()}
            tag="a"
          >
            <Row className="align-items-center">
              <Col className="col-auto">

                <img
                  alt="..."
                  className="avatar rounded-circle"
                  src={require("../../../../assets/img/theme/team-3.jpg")}
                />
              </Col>
              <div className="col ml--2">
                <div className="d-flex justify-content-between align-items-center">
                  <div>
                    <h4 className="mb-0 text-sm">John Snow</h4>
                  </div>
                  <div className="text-right text-muted">
                    <small>5 hrs ago</small>
                  </div>
                </div>
                <p className="text-sm mb-0">
                  Your posts have been liked a lot.
                </p>
              </div>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;
const codeCountries = `import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  ListGroupItem,
  ListGroup,
  Progress,
  Row,
  Col
} from "reactstrap";

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <ListGroup className="list my--3" flush>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <img
                  alt="..."
                  src={require("../../../../assets/img/icons/flags/US.png")}
                />
              </Col>
              <div className="col">
                <small>Country:</small>
                <h5 className="mb-0">United States</h5>
              </div>
              <div className="col">
                <small>Visits:</small>
                <h5 className="mb-0">2500</h5>
              </div>
              <div className="col">
                <small>Bounce:</small>
                <h5 className="mb-0">30%</h5>
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <img
                  alt="..."
                  src={require("../../../../assets/img/icons/flags/DE.png")}
                />
              </Col>
              <div className="col">
                <small>Country:</small>
                <h5 className="mb-0">Germany</h5>
              </div>
              <div className="col">
                <small>Visits:</small>
                <h5 className="mb-0">2500</h5>
              </div>
              <div className="col">
                <small>Bounce:</small>
                <h5 className="mb-0">30%</h5>
              </div>
            </Row>
          </ListGroupItem>
          <ListGroupItem className="px-0">
            <Row className="align-items-center">
              <Col className="col-auto">

                <img
                  alt="..."
                  src={require("../../../../assets/img/icons/flags/GB.png")}
                />
              </Col>
              <div className="col">
                <small>Country:</small>
                <h5 className="mb-0">Great Britain</h5>
              </div>
              <div className="col">
                <small>Visits:</small>
                <h5 className="mb-0">2500</h5>
              </div>
              <div className="col">
                <small>Bounce:</small>
                <h5 className="mb-0">30%</h5>
              </div>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </>
    );
  }
}

export default ListGroups;
`;

class ListGroups extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            List group
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          List groups are a flexible and powerful component for displaying a
          series of content. Modify and extend them to support just about any
          content within.
        </p>
        <hr />
        <h2 id="examples">Examples</h2>
        <p>
          The most basic list group is an unordered list with list items and the
          proper classes. Build upon it with the options that follow, or with
          your own CSS as needed.
        </p>
        <div className="ct-example bg-white">
          <ListGroup>
            <ListGroupItem>Cras justo odio</ListGroupItem>
            <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
            <ListGroupItem>Morbi leo risus</ListGroupItem>
            <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
            <ListGroupItem>Vestibulum at eros</ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExamples}
        </SyntaxHighlighter>
        <h2 id="active-items">Active items</h2>
        <p>
          Add <code className="highlighter-rouge">.active</code>
          to a <code className="highlighter-rouge">.list-group-item</code>
          to indicate the current active selection.
        </p>
        <div className="ct-example bg-white">
          <ListGroup>
            <ListGroupItem className="active">Cras justo odio</ListGroupItem>
            <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
            <ListGroupItem>Morbi leo risus</ListGroupItem>
            <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
            <ListGroupItem>Vestibulum at eros</ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeActiveItems}
        </SyntaxHighlighter>
        <h2 id="links">Links</h2>
        <div className="ct-example bg-white">
          <ListGroup>
            <ListGroupItem
              className="list-group-item-action active"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              Cras justo odio
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              Dapibus ac facilisis in
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              Morbi leo risus
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              Porta ac consectetur ac
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action disabled"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              Vestibulum at eros
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeLinks}
        </SyntaxHighlighter>
        <h2 id="with-badges">With badges</h2>
        <div className="ct-example bg-white">
          <ListGroup>
            <ListGroupItem className="d-flex justify-content-between align-items-center">
              Cras justo odio{" "}
              <Badge color="primary" pill>
                14
              </Badge>
            </ListGroupItem>
            <ListGroupItem className="d-flex justify-content-between align-items-center">
              Dapibus ac facilisis in{" "}
              <Badge color="primary" pill>
                2
              </Badge>
            </ListGroupItem>
            <ListGroupItem className="d-flex justify-content-between align-items-center">
              Morbi leo risus{" "}
              <Badge color="primary" pill>
                1
              </Badge>
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeWithBadges}
        </SyntaxHighlighter>
        <h2 id="advanced-examples">Advanced Examples</h2>
        <p>
          Here are some more advanced custom examples we’ve made in order to
          bring more functionality with some really cool list group examples.
        </p>
        <h3 id="members">Members</h3>
        <div className="ct-example bg-white">
          <ListGroup className="list my--3" flush>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/team-1.jpg")}
                    />
                  </a>
                </Col>
                <div className="col ml--2">
                  <h4 className="mb-0">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      John Michael
                    </a>
                  </h4>
                  <span className="text-success">●</span>
                  <small>Online</small>
                </div>
                <Col className="col-auto">
                  <Button color="primary" size="sm" type="button">
                    Add
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/team-2.jpg")}
                    />
                  </a>
                </Col>
                <div className="col ml--2">
                  <h4 className="mb-0">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      Alex Smith
                    </a>
                  </h4>
                  <span className="text-warning">●</span>
                  <small>In a meeting</small>
                </div>
                <Col className="col-auto">
                  <Button color="primary" size="sm" type="button">
                    Add
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/team-3.jpg")}
                    />
                  </a>
                </Col>
                <div className="col ml--2">
                  <h4 className="mb-0">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      Samantha Ivy
                    </a>
                  </h4>
                  <span className="text-danger">●</span>
                  <small>Offline</small>
                </div>
                <Col className="col-auto">
                  <Button color="primary" size="sm" type="button">
                    Add
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/team-4.jpg")}
                    />
                  </a>
                </Col>
                <div className="col ml--2">
                  <h4 className="mb-0">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      John Michael
                    </a>
                  </h4>
                  <span className="text-success">●</span>
                  <small>Online</small>
                </div>
                <Col className="col-auto">
                  <Button color="primary" size="sm" type="button">
                    Add
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/team-5.jpg")}
                    />
                  </a>
                </Col>
                <div className="col ml--2">
                  <h4 className="mb-0">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      John Snow
                    </a>
                  </h4>
                  <span className="text-success">●</span>
                  <small>Online</small>
                </div>
                <Col className="col-auto">
                  <Button color="primary" size="sm" type="button">
                    Add
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMembers}
        </SyntaxHighlighter>
        <h3 id="checklist">Checklist</h3>
        <div className="ct-example bg-white">
          <ListGroup data-toggle="checklist" flush>
            <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
              <div className="checklist-item checklist-item-success">
                <div className="checklist-info">
                  <h5 className="checklist-title mb-0">Call with Dave</h5>
                  <small>10:30 AM</small>
                </div>
                <div>
                  <div className="custom-control custom-checkbox custom-checkbox-success">
                    <input
                      className="custom-control-input"
                      defaultChecked
                      id="chk-todo-task-1"
                      type="checkbox"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="chk-todo-task-1"
                    />
                  </div>
                </div>
              </div>
            </ListGroupItem>
            <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
              <div className="checklist-item checklist-item-warning">
                <div className="checklist-info">
                  <h5 className="checklist-title mb-0">Lunch meeting</h5>
                  <small>10:30 AM</small>
                </div>
                <div>
                  <div className="custom-control custom-checkbox custom-checkbox-warning">
                    <input
                      className="custom-control-input"
                      id="chk-todo-task-2"
                      type="checkbox"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="chk-todo-task-2"
                    />
                  </div>
                </div>
              </div>
            </ListGroupItem>
            <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
              <div className="checklist-item checklist-item-info">
                <div className="checklist-info">
                  <h5 className="checklist-title mb-0">
                    Argon Dashboard Launch
                  </h5>
                  <small>10:30 AM</small>
                </div>
                <div>
                  <div className="custom-control custom-checkbox custom-checkbox-info">
                    <input
                      className="custom-control-input"
                      id="chk-todo-task-3"
                      type="checkbox"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="chk-todo-task-3"
                    />
                  </div>
                </div>
              </div>
            </ListGroupItem>
            <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
              <div className="checklist-item checklist-item-danger">
                <div className="checklist-info">
                  <h5 className="checklist-title mb-0">Winter Hackaton</h5>
                  <small>10:30 AM</small>
                </div>
                <div>
                  <div className="custom-control custom-checkbox custom-checkbox-danger">
                    <input
                      className="custom-control-input"
                      defaultChecked
                      id="chk-todo-task-4"
                      type="checkbox"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="chk-todo-task-4"
                    />
                  </div>
                </div>
              </div>
            </ListGroupItem>
            <ListGroupItem className="checklist-entry flex-column align-items-start py-4 px-4">
              <div className="checklist-item checklist-item-success">
                <div className="checklist-info">
                  <h5 className="checklist-title mb-0">Dinner with Family</h5>
                  <small>10:30 AM</small>
                </div>
                <div>
                  <div className="custom-control custom-checkbox custom-checkbox-success">
                    <input
                      className="custom-control-input"
                      defaultChecked
                      id="chk-todo-task-5"
                      type="checkbox"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="chk-todo-task-5"
                    />
                  </div>
                </div>
              </div>
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeChecklist}
        </SyntaxHighlighter>
        <h3 id="progress">Progress</h3>
        <div className="ct-example bg-white">
          <ListGroup className="list my--3" flush>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/bootstrap.jpg")}
                    />
                  </a>
                </Col>
                <div className="col">
                  <h5>Argon Design System</h5>
                  <Progress
                    className="progress-xs mb-0"
                    max="100"
                    value="60"
                    color="warning"
                  />
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/angular.jpg")}
                    />
                  </a>
                </Col>
                <div className="col">
                  <h5>Angular Now UI Kit PRO</h5>
                  <Progress
                    className="progress-xs mb-0"
                    max="100"
                    value="100"
                    color="success"
                  />
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/sketch.jpg")}
                    />
                  </a>
                </Col>
                <div className="col">
                  <h5>Black Dashboard</h5>
                  <Progress
                    className="progress-xs mb-0"
                    max="100"
                    value="72"
                    color="danger"
                  />
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/react.jpg")}
                    />
                  </a>
                </Col>
                <div className="col">
                  <h5>React Material Dashboard</h5>
                  <Progress
                    className="progress-xs mb-0"
                    max="100"
                    value="90"
                    color="info"
                  />
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img alt="..." src={require("../../../../assets/img/theme/vue.jpg")} />
                  </a>
                </Col>
                <div className="col">
                  <h5>Vue Paper UI Kit PRO</h5>
                  <Progress
                    className="progress-xs mb-0"
                    max="100"
                    value="100"
                    color="success"
                  />
                </div>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeProgress}
        </SyntaxHighlighter>
        <h3 id="messages">Messages</h3>
        <div className="ct-example bg-white">
          <ListGroup flush>
            <ListGroupItem
              className="list-group-item-action flex-column align-items-start py-4 px-4"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              <div className="d-flex w-100 justify-content-between">
                <div>
                  <div className="d-flex w-100 align-items-center">
                    <img
                      alt="..."
                      className="avatar avatar-xs mr-2"
                      src={require("../../../../assets/img/theme/team-1.jpg")}
                    />
                    <h5 className="mb-1">Tim</h5>
                  </div>
                </div>
                <small>2 hrs ago</small>
              </div>
              <h4 className="mt-3 mb-1">New order for Argon Dashboard</h4>
              <p className="text-sm mb-0">
                Doasdnec id elit non mi porta gravida at eget metus. Maecenas
                sed diam eget risus varius blandit.
              </p>
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action flex-column align-items-start py-4 px-4"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              <div className="d-flex w-100 justify-content-between">
                <div>
                  <div className="d-flex w-100 align-items-center">
                    <img
                      alt="..."
                      className="avatar avatar-xs mr-2"
                      src={require("../../../../assets/img/theme/team-2.jpg")}
                    />
                    <h5 className="mb-1">Mike</h5>
                  </div>
                </div>
                <small>1 day ago</small>
              </div>
              <h4 className="mt-3 mb-1">
                <span className="text-info">●</span>
                Your theme has been updated
              </h4>
              <p className="text-sm mb-0">
                Doasdnec id elit non mi porta gravida at eget metus. Maecenas
                sed diam eget risus varius blandit.
              </p>
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action flex-column align-items-start py-4 px-4"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              <div className="d-flex w-100 justify-content-between">
                <div>
                  <div className="d-flex w-100 align-items-center">
                    <img
                      alt="..."
                      className="avatar avatar-xs mr-2"
                      src={require("../../../../assets/img/theme/team-3.jpg")}
                    />
                    <h5 className="mb-1">Tim</h5>
                  </div>
                </div>
                <small>2 hrs ago</small>
              </div>
              <h4 className="mt-3 mb-1">New order for Argon Dashboard</h4>
              <p className="text-sm mb-0">
                Doasdnec id elit non mi porta gravida at eget metus. Maecenas
                sed diam eget risus varius blandit.
              </p>
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMessages}
        </SyntaxHighlighter>
        <h3 id="notifications">Notifications</h3>
        <div className="ct-example bg-white">
          <ListGroup flush>
            <ListGroupItem
              className="list-group-item-action"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              <Row className="align-items-center">
                <Col className="col-auto">
                  <img
                    alt="..."
                    className="avatar rounded-circle"
                    src={require("../../../../assets/img/theme/team-1.jpg")}
                  />
                </Col>
                <div className="col ml--2">
                  <div className="d-flex justify-content-between align-items-center">
                    <div>
                      <h4 className="mb-0 text-sm">John Snow</h4>
                    </div>
                    <div className="text-right text-muted">
                      <small>2 hrs ago</small>
                    </div>
                  </div>
                  <p className="text-sm mb-0">
                    Let's meet at Starbucks at 11:30. Wdyt?
                  </p>
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              <Row className="align-items-center">
                <Col className="col-auto">
                  <img
                    alt="..."
                    className="avatar rounded-circle"
                    src={require("../../../../assets/img/theme/team-2.jpg")}
                  />
                </Col>
                <div className="col ml--2">
                  <div className="d-flex justify-content-between align-items-center">
                    <div>
                      <h4 className="mb-0 text-sm">John Snow</h4>
                    </div>
                    <div className="text-right text-muted">
                      <small>3 hrs ago</small>
                    </div>
                  </div>
                  <p className="text-sm mb-0">
                    A new issue has been reported for Argon.
                  </p>
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem
              className="list-group-item-action"
              href="#pablo"
              onClick={e => e.preventDefault()}
              tag="a"
            >
              <Row className="align-items-center">
                <Col className="col-auto">
                  <img
                    alt="..."
                    className="avatar rounded-circle"
                    src={require("../../../../assets/img/theme/team-3.jpg")}
                  />
                </Col>
                <div className="col ml--2">
                  <div className="d-flex justify-content-between align-items-center">
                    <div>
                      <h4 className="mb-0 text-sm">John Snow</h4>
                    </div>
                    <div className="text-right text-muted">
                      <small>5 hrs ago</small>
                    </div>
                  </div>
                  <p className="text-sm mb-0">
                    Your posts have been liked a lot.
                  </p>
                </div>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeNotifications}
        </SyntaxHighlighter>
        <h3 id="countries">Countries</h3>
        <div className="ct-example bg-white">
          <ListGroup className="list my--3" flush>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <img
                    alt="..."
                    src={require("../../../../assets/img/icons/flags/US.png")}
                  />
                </Col>
                <div className="col">
                  <small>Country:</small>
                  <h5 className="mb-0">United States</h5>
                </div>
                <div className="col">
                  <small>Visits:</small>
                  <h5 className="mb-0">2500</h5>
                </div>
                <div className="col">
                  <small>Bounce:</small>
                  <h5 className="mb-0">30%</h5>
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <img
                    alt="..."
                    src={require("../../../../assets/img/icons/flags/DE.png")}
                  />
                </Col>
                <div className="col">
                  <small>Country:</small>
                  <h5 className="mb-0">Germany</h5>
                </div>
                <div className="col">
                  <small>Visits:</small>
                  <h5 className="mb-0">2500</h5>
                </div>
                <div className="col">
                  <small>Bounce:</small>
                  <h5 className="mb-0">30%</h5>
                </div>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="px-0">
              <Row className="align-items-center">
                <Col className="col-auto">
                  <img
                    alt="..."
                    src={require("../../../../assets/img/icons/flags/GB.png")}
                  />
                </Col>
                <div className="col">
                  <small>Country:</small>
                  <h5 className="mb-0">Great Britain</h5>
                </div>
                <div className="col">
                  <small>Visits:</small>
                  <h5 className="mb-0">2500</h5>
                </div>
                <div className="col">
                  <small>Bounce:</small>
                  <h5 className="mb-0">30%</h5>
                </div>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCountries}
        </SyntaxHighlighter>
        <h3>Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://reactstrap.github.io/components/listgroup/?ref=creativetim"
            target="_blank"
          >
            reactstrap list group documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default ListGroups;
