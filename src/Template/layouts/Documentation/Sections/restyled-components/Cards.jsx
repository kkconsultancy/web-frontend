/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

const codeExamples = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card className="card-frame">
          <CardBody>This is some text within a card body.</CardBody>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codeCardGroups = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <div className="card-group">
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This is a wider card with supporting text below as a natural
                lead-in to additional content. This content is a little bit
                longer.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This card has supporting text below as a natural lead-in to
                additional content.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This is a wider card with supporting text below as a natural
                lead-in to additional content. This card has even longer
                content than the first to show that equal height action.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
        </div>
      </>
    );
  }
}

export default Cards;
`;
const codeCardDecks = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <div className="card-deck">
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This is a wider card with supporting text below as a natural
                lead-in to additional content. This content is a little bit
                longer.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This card has supporting text below as a natural lead-in to
                additional content.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This is a wider card with supporting text below as a natural
                lead-in to additional content. This card has even longer
                content than the first to show that equal height action.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
        </div>
      </>
    );
  }
}

export default Cards;
`;
const codeCardColumns = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <div className="card-columns">
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title that wraps to a new line</CardTitle>
              <CardText>
                This is a longer card with supporting text below as a natural
                lead-in to additional content. This content is a little bit
                longer.
              </CardText>
            </CardBody>
          </Card>
          <Card className="p-3">
            <CardBody className="blockquote mb-0">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Integer posuere erat a ante.
              </p>
              <footer className="blockquote-footer">
                <small className="text-muted">
                  Someone famous in{" "}
                  <cite title="Source Title">Source Title</cite>
                </small>
              </footer>
            </CardBody>
          </Card>
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This card has supporting text below as a natural lead-in to
                additional content.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
          <Card className="bg-primary text-white text-center p-3">
            <blockquote className="blockquote mb-0">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Integer posuere erat.
              </p>
              <footer className="blockquote-footer">
                <small>
                  Someone famous in{" "}
                  <cite title="Source Title">Source Title</cite>
                </small>
              </footer>
            </blockquote>
          </Card>
          <Card className="text-center">
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This card has a regular title and short paragraphy of text
                below it.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
            />
          </Card>
          <Card className="p-3 text-right">
            <blockquote className="blockquote mb-0">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Integer posuere erat a ante.
              </p>
              <footer className="blockquote-footer">
                <small className="text-muted">
                  Someone famous in{" "}
                  <cite title="Source Title">Source Title</cite>
                </small>
              </footer>
            </blockquote>
          </Card>
          <Card>
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardText>
                This is another card with title and supporting text below.
                This card has some additional content to make it slightly
                taller overall.
              </CardText>
              <CardText>
                <small className="text-muted">Last updated 3 mins ago</small>
              </CardText>
            </CardBody>
          </Card>
        </div>
      </>
    );
  }
}

export default Cards;
`;
const codeListGroup = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card>

          <CardImg
            alt="..."
            src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
            top
          />

          <ListGroup flush>
            <ListGroupItem>Cras justo odio</ListGroupItem>
            <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
            <ListGroupItem>Vestibulum at eros</ListGroupItem>
          </ListGroup>

          <CardBody>
            <CardTitle className="mb-3" tag="h3">
              Card title
            </CardTitle>
            <CardText className="mb-4">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Facilis non dolore est fuga nobis ipsum illum eligendi nemo iure
              repellat, soluta, optio minus ut reiciendis voluptates enim
              impedit veritatis officiis.
            </CardText>
            <Button
              color="primary"
              href="#pablo"
              onClick={e => e.preventDefault()}
            >
              Go somewhere
            </Button>
          </CardBody>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codeProfile = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card className="card-profile">
          <CardImg
            alt="..."
            src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
            top
          />
          <Row className="justify-content-center">
            <Col className="order-lg-2" lg="3">
              <div className="card-profile-image">
                <a href="#pablo" onClick={e => e.preventDefault()}>
                  <img
                    alt="..."
                    className="rounded-circle"
                    src={require("../../../../assets/img/theme/team-4.jpg")}
                  />
                </a>
              </div>
            </Col>
          </Row>
          <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
            <div className="d-flex justify-content-between">
              <Button
                className="mr-4"
                color="info"
                href="#pablo"
                onClick={e => e.preventDefault()}
                size="sm"
              >
                Connect
              </Button>
              <Button
                className="float-right"
                color="default"
                href="#pablo"
                onClick={e => e.preventDefault()}
                size="sm"
              >
                Message
              </Button>
            </div>
          </CardHeader>
          <CardBody className="pt-0">
            <Row>
              <div className="col">
                <div className="card-profile-stats d-flex justify-content-center">
                  <div>
                    <span className="heading">22</span>
                    <span className="description">Friends</span>
                  </div>
                  <div>
                    <span className="heading">10</span>
                    <span className="description">Photos</span>
                  </div>
                  <div>
                    <span className="heading">89</span>
                    <span className="description">Comments</span>
                  </div>
                </div>
              </div>
            </Row>
            <div className="text-center">
              <h5 className="h3">
                Jessica Jones
                <span className="font-weight-light">, 27</span>
              </h5>
              <div className="h5 font-weight-300">
                <i className="ni location_pin mr-2" />
                Bucharest, Romania
              </div>
            </div>
          </CardBody>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codeContacts = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card>

          <CardBody>
            <Row className="align-items-center">
              <Col className="col-auto">

                <a
                  className="avatar avatar-xl rounded-circle"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <img
                    alt="..."
                    src={require("../../../../assets/img/theme/team-2.jpg")}
                  />
                </a>
              </Col>
              <div className="col ml--2">
                <h4 className="mb-0">
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    John Snow
                  </a>
                </h4>
                <p className="text-sm text-muted mb-0">Working remoteley</p>
                <span className="text-success">●</span>
                <small>Active</small>
              </div>
              <Col className="col-auto">
                <Button color="primary" size="sm" type="button">
                  Add
                </Button>
              </Col>
            </Row>
          </CardBody>
      </>
    );
  }
}

export default Cards;
`;
const codeTeamMember = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card>

          <CardBody>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              <img
                alt="..."
                className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                src={require("../../../../assets/img/theme/team-1.jpg")}
                style={{ width: "140px" }}
              />
            </a>
            <div className="pt-4 text-center">
              <h5 className="h3 title">
                <span className="d-block mb-1">Ryan Tompson</span>
                <small className="h4 font-weight-light text-muted">
                  Web Developer
                </small>
              </h5>
              <div className="mt-3">
                <Button
                  className="btn-icon-only rounded-circle"
                  color="twitter"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <i className="fab fa-twitter" />
                </Button>
                <Button
                  className="btn-icon-only rounded-circle"
                  color="facebook"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <i className="fab fa-facebook" />
                </Button>
                <Button
                  className="btn-icon-only rounded-circle"
                  color="dribbble"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <i className="fab fa-dribbble" />
                </Button>
              </div>
            </div>
          </CardBody>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codeImage = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card>

          <CardImg
            alt="..."
            src={require("../../../../assets/img/theme/img-1-1000x900.jpg")}
            top
          />

          <CardBody>
            <CardTitle className="h2 mb-0">Get started with Argon</CardTitle>
            <small className="text-muted">
              by John Snow on Oct 29th at 10:23 AM
            </small>
            <CardText className="mt-4">
              Argon is a great free UI package based on Bootstrap 4 that
              includes the most important components and features.
            </CardText>
            <Button
              className="px-0"
              color="link"
              href="#pablo"
              onClick={e => e.preventDefault()}
            >
              View article
            </Button>
          </CardBody>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codeBlockquote = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card className="bg-gradient-default">
          <CardBody>
            <CardTitle className="text-white" tag="h3">
              Testimonial
            </CardTitle>
            <blockquote className="blockquote text-white mb-0">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Integer posuere erat a ante.
              </p>
              <footer className="blockquote-footer text-danger">
                Someone famous in{" "}
                <cite title="Source Title">Source Title</cite>
              </footer>
            </blockquote>
          </CardBody>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codePricing = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card className="card-pricing bg-gradient-success border-0 text-center mb-4">
          <CardHeader className="bg-transparent">
            <h4 className="text-uppercase ls-1 text-white py-3 mb-0">
              Bravo pack
            </h4>
          </CardHeader>
          <CardBody className="px-lg-7">
            <div className="display-2 text-white">$49</div>
            <span className="text-white">per application</span>
            <ul className="list-unstyled my-4">
              <li>
                <div className="d-flex align-items-center">
                  <div>
                    <div className="icon icon-xs icon-shape bg-white shadow rounded-circle">
                      <i className="fas fa-terminal" />
                    </div>
                  </div>
                  <div>
                    <span className="pl-2 text-sm text-white">
                      Complete documentation
                    </span>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex align-items-center">
                  <div>
                    <div className="icon icon-xs icon-shape bg-white shadow rounded-circle">
                      <i className="fas fa-pen-fancy" />
                    </div>
                  </div>
                  <div>
                    <span className="pl-2 text-sm text-white">
                      Working materials in Sketch
                    </span>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex align-items-center">
                  <div>
                    <div className="icon icon-xs icon-shape bg-white shadow rounded-circle">
                      <i className="fas fa-hdd" />
                    </div>
                  </div>
                  <div>
                    <span className="pl-2 text-sm text-white">
                      2GB cloud storage
                    </span>
                  </div>
                </div>
              </li>
            </ul>
            <Button className="mb-3" color="primary" type="button">
              Start free trial
            </Button>
          </CardBody>
          <CardFooter className="bg-transparent">
            <a
              className="text-white"
              href="#pablo"
              onClick={e => e.preventDefault()}
            >
              Request a demo
            </a>
          </CardFooter>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codeOverlay = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Card className="bg-dark text-white border-0">
          <CardImg
            alt="..."
            src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
          />
          <CardImgOverlay className="d-flex align-items-center">
            <div>
              <CardTitle className="h2 text-white mb-2">Card title</CardTitle>
              <CardText>
                This is a wider card with supporting text below as a natural
                lead-in to additional content. This content is a little bit
                longer.
              </CardText>
              <CardText className="text-sm font-weight-bold">
                Last updated 3 mins ago
              </CardText>
            </div>
          </CardImgOverlay>
        </Card>
      </>
    );
  }
}

export default Cards;
`;
const codeStats1 = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Row className="mb-4">
          <Col lg="6">
            <Card className="card-stats">

              <CardBody>
                <Row>
                  <div className="col">
                    <CardTitle className="text-uppercase text-muted mb-0">
                      Total traffic
                    </CardTitle>
                    <span className="h2 font-weight-bold mb-0">350,897</span>
                  </div>
                  <Col className="col-auto">
                    <div className="icon icon-shape bg-red text-white rounded-circle shadow">
                      <i className="ni ni-active-40" />
                    </div>
                  </Col>
                </Row>
                <p className="mt-3 mb-0 text-sm">
                  <span className="text-success mr-2">
                    <i className="fa fa-arrow-up" />
                    3.48%
                  </span>
                  <span className="text-nowrap">Since last month</span>
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg="6">
            <Card className="card-stats">

              <CardBody>
                <Row>
                  <div className="col">
                    <CardTitle className="text-uppercase text-muted mb-0">
                      New users
                    </CardTitle>
                    <span className="h2 font-weight-bold mb-0">2,356</span>
                  </div>
                  <Col className="col-auto">
                    <div className="icon icon-shape bg-orange text-white rounded-circle shadow">
                      <i className="ni ni-chart-pie-35" />
                    </div>
                  </Col>
                </Row>
                <p className="mt-3 mb-0 text-sm">
                  <span className="text-success mr-2">
                    <i className="fa fa-arrow-up" />
                    3.48%
                  </span>
                  <span className="text-nowrap">Since last month</span>
                </p>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}

export default Cards;
`;
const codeStats2 = `import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
  Row,
  Col
} from "reactstrap";

class Cards extends React.Component {
  render() {
    return (
      <>
        <Row className="mb-4">
          <Col lg="6">
            <Card className="bg-gradient-default">

              <CardBody>
                <Row>
                  <div className="col">
                    <CardTitle className="text-uppercase text-muted mb-0 text-white">
                      Total traffic
                    </CardTitle>
                    <span className="h2 font-weight-bold mb-0 text-white">
                      350,897
                    </span>
                  </div>
                  <Col className="col-auto">
                    <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                      <i className="ni ni-active-40" />
                    </div>
                  </Col>
                </Row>
                <p className="mt-3 mb-0 text-sm">
                  <span className="text-white mr-2">
                    <i className="fa fa-arrow-up" />
                    3.48%
                  </span>
                  <span className="text-nowrap text-light">
                    Since last month
                  </span>
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg="6">
            <Card className="bg-gradient-primary">

              <CardBody>
                <Row>
                  <div className="col">
                    <CardTitle className="text-uppercase text-muted mb-0 text-white">
                      New users
                    </CardTitle>
                    <span className="h2 font-weight-bold mb-0 text-white">
                      2,356
                    </span>
                  </div>
                  <Col className="col-auto">
                    <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                      <i className="ni ni-atom" />
                    </div>
                  </Col>
                </Row>
                <p className="mt-3 mb-0 text-sm">
                  <span className="text-white mr-2">
                    <i className="fa fa-arrow-up" />
                    3.48%
                  </span>
                  <span className="text-nowrap text-light">
                    Since last month
                  </span>
                </p>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}

export default Cards;
`;

class Cards extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Card
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Cards provide a flexible and extensible content container with
          multiple variants and options.
        </p>
        <hr />
        <h2 id="examples">Examples</h2>
        <p>
          Cards support a wide variety of content, including images, text, list
          groups, links, and more. Below are examples of what’s supported.
        </p>
        <div className="ct-example bg-white">
          <Card className="card-frame">
            <CardBody>This is some text within a card body.</CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExamples}
        </SyntaxHighlighter>
        <h2 id="layouts">Layouts</h2>
        <p>
          In addition to styling the content within cards, Bootstrap includes a
          few options for laying out series of cards. For the time being, these
          layout options are not yet responsive.
        </p>
        <h3 id="card-groups">Card groups</h3>
        <p>
          Need a set of equal width and height cards that aren’t attached to one
          another? Use card decks.
        </p>
        <div className="ct-example bg-white">
          <div className="card-group">
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This content is a little bit
                  longer.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This card has supporting text below as a natural lead-in to
                  additional content.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This card has even longer
                  content than the first to show that equal height action.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCardGroups}
        </SyntaxHighlighter>
        <h3 id="card-decks">Card decks</h3>
        <div className="ct-example bg-white">
          <div className="card-deck">
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This content is a little bit
                  longer.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This card has supporting text below as a natural lead-in to
                  additional content.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This card has even longer
                  content than the first to show that equal height action.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCardDecks}
        </SyntaxHighlighter>
        <h3 id="card-columns">Card columns</h3>
        <p>
          Cards can be organized into Masonry-like columns with just CSS by
          wrapping them in .card-columns. Cards are built with CSS column
          properties instead of flexbox for easier alignment. Cards are ordered
          from top to bottom and left to right.
        </p>
        <div className="ct-example bg-white">
          <div className="card-columns">
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title that wraps to a new line</CardTitle>
                <CardText>
                  This is a longer card with supporting text below as a natural
                  lead-in to additional content. This content is a little bit
                  longer.
                </CardText>
              </CardBody>
            </Card>
            <Card className="p-3">
              <CardBody className="blockquote mb-0">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Integer posuere erat a ante.
                </p>
                <footer className="blockquote-footer">
                  <small className="text-muted">
                    Someone famous in{" "}
                    <cite title="Source Title">Source Title</cite>
                  </small>
                </footer>
              </CardBody>
            </Card>
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This card has supporting text below as a natural lead-in to
                  additional content.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
            <Card className="bg-primary text-white text-center p-3">
              <blockquote className="blockquote mb-0">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Integer posuere erat.
                </p>
                <footer className="blockquote-footer">
                  <small>
                    Someone famous in{" "}
                    <cite title="Source Title">Source Title</cite>
                  </small>
                </footer>
              </blockquote>
            </Card>
            <Card className="text-center">
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This card has a regular title and short paragraphy of text
                  below it.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
            <Card>
              <CardImg
                alt="..."
                src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              />
            </Card>
            <Card className="p-3 text-right">
              <blockquote className="blockquote mb-0">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Integer posuere erat a ante.
                </p>
                <footer className="blockquote-footer">
                  <small className="text-muted">
                    Someone famous in{" "}
                    <cite title="Source Title">Source Title</cite>
                  </small>
                </footer>
              </blockquote>
            </Card>
            <Card>
              <CardBody>
                <CardTitle>Card title</CardTitle>
                <CardText>
                  This is another card with title and supporting text below.
                  This card has some additional content to make it slightly
                  taller overall.
                </CardText>
                <CardText>
                  <small className="text-muted">Last updated 3 mins ago</small>
                </CardText>
              </CardBody>
            </Card>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCardColumns}
        </SyntaxHighlighter>
        <h2 id="advanced-examples">Advanced examples</h2>
        <h3 id="list-group">List group</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />

            <ListGroup flush>
              <ListGroupItem>Cras justo odio</ListGroupItem>
              <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
              <ListGroupItem>Vestibulum at eros</ListGroupItem>
            </ListGroup>

            <CardBody>
              <CardTitle className="mb-3" tag="h3">
                Card title
              </CardTitle>
              <CardText className="mb-4">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Facilis non dolore est fuga nobis ipsum illum eligendi nemo iure
                repellat, soluta, optio minus ut reiciendis voluptates enim
                impedit veritatis officiis.
              </CardText>
              <Button
                color="primary"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                Go somewhere
              </Button>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeListGroup}
        </SyntaxHighlighter>
        <h3 id="profile">Profile</h3>
        <div className="ct-example bg-white">
          <Card className="card-profile">
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
              top
            />
            <Row className="justify-content-center">
              <Col className="order-lg-2" lg="3">
                <div className="card-profile-image">
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    <img
                      alt="..."
                      className="rounded-circle"
                      src={require("../../../../assets/img/theme/team-4.jpg")}
                    />
                  </a>
                </div>
              </Col>
            </Row>
            <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div className="d-flex justify-content-between">
                <Button
                  className="mr-4"
                  color="info"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                  size="sm"
                >
                  Connect
                </Button>
                <Button
                  className="float-right"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                  size="sm"
                >
                  Message
                </Button>
              </div>
            </CardHeader>
            <CardBody className="pt-0">
              <Row>
                <div className="col">
                  <div className="card-profile-stats d-flex justify-content-center">
                    <div>
                      <span className="heading">22</span>
                      <span className="description">Friends</span>
                    </div>
                    <div>
                      <span className="heading">10</span>
                      <span className="description">Photos</span>
                    </div>
                    <div>
                      <span className="heading">89</span>
                      <span className="description">Comments</span>
                    </div>
                  </div>
                </div>
              </Row>
              <div className="text-center">
                <h5 className="h3">
                  Jessica Jones
                  <span className="font-weight-light">, 27</span>
                </h5>
                <div className="h5 font-weight-300">
                  <i className="ni location_pin mr-2" />
                  Bucharest, Romania
                </div>
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeProfile}
        </SyntaxHighlighter>
        <h3 id="contact">Contact</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardBody>
              <Row className="align-items-center">
                <Col className="col-auto">
                  <a
                    className="avatar avatar-xl rounded-circle"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <img
                      alt="..."
                      src={require("../../../../assets/img/theme/team-2.jpg")}
                    />
                  </a>
                </Col>
                <div className="col ml--2">
                  <h4 className="mb-0">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      John Snow
                    </a>
                  </h4>
                  <p className="text-sm text-muted mb-0">Working remoteley</p>
                  <span className="text-success">●</span>
                  <small>Active</small>
                </div>
                <Col className="col-auto">
                  <Button color="primary" size="sm" type="button">
                    Add
                  </Button>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeContacts}
        </SyntaxHighlighter>
        <h3 id="team-member">Team member</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardBody>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                <img
                  alt="..."
                  className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                  src={require("../../../../assets/img/theme/team-1.jpg")}
                  style={{ width: "140px" }}
                />
              </a>
              <div className="pt-4 text-center">
                <h5 className="h3 title">
                  <span className="d-block mb-1">Ryan Tompson</span>
                  <small className="h4 font-weight-light text-muted">
                    Web Developer
                  </small>
                </h5>
                <div className="mt-3">
                  <Button
                    className="btn-icon-only rounded-circle"
                    color="twitter"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <i className="fab fa-twitter" />
                  </Button>
                  <Button
                    className="btn-icon-only rounded-circle"
                    color="facebook"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <i className="fab fa-facebook" />
                  </Button>
                  <Button
                    className="btn-icon-only rounded-circle"
                    color="dribbble"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    <i className="fab fa-dribbble" />
                  </Button>
                </div>
              </div>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeTeamMember}
        </SyntaxHighlighter>
        <h3 id="image">Image</h3>
        <div className="ct-example bg-white">
          <Card>
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x900.jpg")}
              top
            />

            <CardBody>
              <CardTitle className="h2 mb-0">Get started with Argon</CardTitle>
              <small className="text-muted">
                by John Snow on Oct 29th at 10:23 AM
              </small>
              <CardText className="mt-4">
                Argon is a great free UI package based on Bootstrap 4 that
                includes the most important components and features.
              </CardText>
              <Button
                className="px-0"
                color="link"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                View article
              </Button>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeImage}
        </SyntaxHighlighter>
        <h3 id="blockquote">Blockquote</h3>
        <div className="ct-example bg-white">
          <Card className="bg-gradient-default">
            <CardBody>
              <CardTitle className="text-white" tag="h3">
                Testimonial
              </CardTitle>
              <blockquote className="blockquote text-white mb-0">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Integer posuere erat a ante.
                </p>
                <footer className="blockquote-footer text-danger">
                  Someone famous in{" "}
                  <cite title="Source Title">Source Title</cite>
                </footer>
              </blockquote>
            </CardBody>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeBlockquote}
        </SyntaxHighlighter>
        <h3 id="pricing">Pricing</h3>
        <div className="ct-example bg-white">
          <Card className="card-pricing bg-gradient-success border-0 text-center mb-4">
            <CardHeader className="bg-transparent">
              <h4 className="text-uppercase ls-1 text-white py-3 mb-0">
                Bravo pack
              </h4>
            </CardHeader>
            <CardBody className="px-lg-7">
              <div className="display-2 text-white">$49</div>
              <span className="text-white">per application</span>
              <ul className="list-unstyled my-4">
                <li>
                  <div className="d-flex align-items-center">
                    <div>
                      <div className="icon icon-xs icon-shape bg-white shadow rounded-circle">
                        <i className="fas fa-terminal" />
                      </div>
                    </div>
                    <div>
                      <span className="pl-2 text-sm text-white">
                        Complete documentation
                      </span>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="d-flex align-items-center">
                    <div>
                      <div className="icon icon-xs icon-shape bg-white shadow rounded-circle">
                        <i className="fas fa-pen-fancy" />
                      </div>
                    </div>
                    <div>
                      <span className="pl-2 text-sm text-white">
                        Working materials in Sketch
                      </span>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="d-flex align-items-center">
                    <div>
                      <div className="icon icon-xs icon-shape bg-white shadow rounded-circle">
                        <i className="fas fa-hdd" />
                      </div>
                    </div>
                    <div>
                      <span className="pl-2 text-sm text-white">
                        2GB cloud storage
                      </span>
                    </div>
                  </div>
                </li>
              </ul>
              <Button className="mb-3" color="primary" type="button">
                Start free trial
              </Button>
            </CardBody>
            <CardFooter className="bg-transparent">
              <a
                className="text-white"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                Request a demo
              </a>
            </CardFooter>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codePricing}
        </SyntaxHighlighter>
        <h3 id="overlay">Overlay</h3>
        <div className="ct-example bg-white">
          <Card className="bg-dark text-white border-0">
            <CardImg
              alt="..."
              src={require("../../../../assets/img/theme/img-1-1000x600.jpg")}
            />
            <CardImgOverlay className="d-flex align-items-center">
              <div>
                <CardTitle className="h2 text-white mb-2">Card title</CardTitle>
                <CardText>
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This content is a little bit
                  longer.
                </CardText>
                <CardText className="text-sm font-weight-bold">
                  Last updated 3 mins ago
                </CardText>
              </div>
            </CardImgOverlay>
          </Card>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeOverlay}
        </SyntaxHighlighter>
        <h3 id="stats">Stats</h3>
        <div className="ct-example bg-white">
          <Row className="mb-4">
            <Col lg="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <div className="col">
                      <CardTitle className="text-uppercase text-muted mb-0">
                        Total traffic
                      </CardTitle>
                      <span className="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <Col className="col-auto">
                      <div className="icon icon-shape bg-red text-white rounded-circle shadow">
                        <i className="ni ni-active-40" />
                      </div>
                    </Col>
                  </Row>
                  <p className="mt-3 mb-0 text-sm">
                    <span className="text-success mr-2">
                      <i className="fa fa-arrow-up" />
                      3.48%
                    </span>
                    <span className="text-nowrap">Since last month</span>
                  </p>
                </CardBody>
              </Card>
            </Col>
            <Col lg="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <div className="col">
                      <CardTitle className="text-uppercase text-muted mb-0">
                        New users
                      </CardTitle>
                      <span className="h2 font-weight-bold mb-0">2,356</span>
                    </div>
                    <Col className="col-auto">
                      <div className="icon icon-shape bg-orange text-white rounded-circle shadow">
                        <i className="ni ni-chart-pie-35" />
                      </div>
                    </Col>
                  </Row>
                  <p className="mt-3 mb-0 text-sm">
                    <span className="text-success mr-2">
                      <i className="fa fa-arrow-up" />
                      3.48%
                    </span>
                    <span className="text-nowrap">Since last month</span>
                  </p>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeStats1}
        </SyntaxHighlighter>
        <div className="ct-example bg-white">
          <Row className="mb-4">
            <Col lg="6">
              <Card className="bg-gradient-default">
                <CardBody>
                  <Row>
                    <div className="col">
                      <CardTitle className="text-uppercase text-muted mb-0 text-white">
                        Total traffic
                      </CardTitle>
                      <span className="h2 font-weight-bold mb-0 text-white">
                        350,897
                      </span>
                    </div>
                    <Col className="col-auto">
                      <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                        <i className="ni ni-active-40" />
                      </div>
                    </Col>
                  </Row>
                  <p className="mt-3 mb-0 text-sm">
                    <span className="text-white mr-2">
                      <i className="fa fa-arrow-up" />
                      3.48%
                    </span>
                    <span className="text-nowrap text-light">
                      Since last month
                    </span>
                  </p>
                </CardBody>
              </Card>
            </Col>
            <Col lg="6">
              <Card className="bg-gradient-primary">
                <CardBody>
                  <Row>
                    <div className="col">
                      <CardTitle className="text-uppercase text-muted mb-0 text-white">
                        New users
                      </CardTitle>
                      <span className="h2 font-weight-bold mb-0 text-white">
                        2,356
                      </span>
                    </div>
                    <Col className="col-auto">
                      <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                        <i className="ni ni-atom" />
                      </div>
                    </Col>
                  </Row>
                  <p className="mt-3 mb-0 text-sm">
                    <span className="text-white mr-2">
                      <i className="fa fa-arrow-up" />
                      3.48%
                    </span>
                    <span className="text-nowrap text-light">
                      Since last month
                    </span>
                  </p>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeStats2}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://reactstrap.github.io/components/card/?ref=creativetim"
            target="_blank"
          >
            reactstrap cards documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Cards;
