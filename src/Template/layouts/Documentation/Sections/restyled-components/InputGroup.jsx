/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
import classnames from "classnames";
// reactstrap components
import {
  Button,
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup
} from "reactstrap";

const codeDefault = `import React from "react";
import classnames from "classnames";
 // reactstrap components
 import {
   Button,
   FormGroup,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup
 } from "reactstrap";

 class InputGroups extends React.Component {
   state = {};
   render() {
     return (
       <>
         <FormGroup>
           <InputGroup
             className={classnames({
               focused: this.state.default && this.state.default.username
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>@</InputGroupText>
             </InputGroupAddon>
             <Input
               placeholder="Username"
               type="text"
               onFocus={e =>
                 this.setState({
                   default: { ...this.state.default, username: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   default: { ...this.state.default, username: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames({
               focused: this.state.default && this.state.default.recipient
             })}
           >
             <Input
               placeholder="Recipient's username"
               type="text"
               onFocus={e =>
                 this.setState({
                   default: { ...this.state.default, recipient: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   default: { ...this.state.default, recipient: false }
                 })
               }
             />
             <InputGroupAddon addonType="append">
               <InputGroupText>@example.com</InputGroupText>
             </InputGroupAddon>
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <label className="form-control-label" htmlFor="basic-url">
             Your vanity URL
           </label>
           <InputGroup
             className={classnames({
               focused: this.state.default && this.state.default.basicURL
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>https://example.com/users/</InputGroupText>
             </InputGroupAddon>
             <Input
               onFocus={e =>
                 this.setState({
                   default: { ...this.state.default, basicURL: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   default: { ...this.state.default, basicURL: false }
                 })
               }
               type="text"
             />
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames({
               focused: this.state.default && this.state.default.dollar
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>$</InputGroupText>
             </InputGroupAddon>
             <Input
               onFocus={e =>
                 this.setState({
                   default: { ...this.state.default, dollar: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   default: { ...this.state.default, dollar: false }
                 })
               }
             />
             <InputGroupAddon addonType="append">
               <InputGroupText>.00</InputGroupText>
             </InputGroupAddon>
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames({
               focused: this.state.default && this.state.default.textarea
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>With textarea</InputGroupText>
             </InputGroupAddon>
             <Input
               type="textarea"
               onFocus={e =>
                 this.setState({
                   default: { ...this.state.default, textarea: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   default: { ...this.state.default, textarea: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
       </>
     );
   }
 }

 export default InputGroups;
`;
const codeMerged = `import React from "react";
import classnames from "classnames";
 // reactstrap components
 import {
   Button,
   FormGroup,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup
 } from "reactstrap";

 class InputGroups extends React.Component {
   state = {};
   render() {
     return (
       <>
         <FormGroup>
           <InputGroup
             className={classnames("input-group-merge", {
               focused: this.state.merged && this.state.merged.username
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>@</InputGroupText>
             </InputGroupAddon>
             <Input
               placeholder="Username"
               type="text"
               onFocus={e =>
                 this.setState({
                   merged: { ...this.state.merged, username: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   merged: { ...this.state.merged, username: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames("input-group-merge", {
               focused: this.state.merged && this.state.merged.recipient
             })}
           >
             <Input
               placeholder="Recipient's username"
               type="text"
               onFocus={e =>
                 this.setState({
                   merged: { ...this.state.merged, recipient: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   merged: { ...this.state.merged, recipient: false }
                 })
               }
             />
             <InputGroupAddon addonType="append">
               <InputGroupText>@example.com</InputGroupText>
             </InputGroupAddon>
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <label className="form-control-label" htmlFor="basic-url">
             Your vanity URL
           </label>
           <InputGroup
             className={classnames("input-group-merge", {
               focused: this.state.merged && this.state.merged.basicURL
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>https://example.com/users/</InputGroupText>
             </InputGroupAddon>
             <Input
               type="text"
               onFocus={e =>
                 this.setState({
                   merged: { ...this.state.merged, basicURL: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   merged: { ...this.state.merged, basicURL: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames("input-group-merge", {
               focused: this.state.merged && this.state.merged.dollar
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>$</InputGroupText>
             </InputGroupAddon>
             <Input
               onFocus={e =>
                 this.setState({
                   merged: { ...this.state.merged, dollar: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   merged: { ...this.state.merged, dollar: false }
                 })
               }
             />
             <InputGroupAddon addonType="append">
               <InputGroupText>.00</InputGroupText>
             </InputGroupAddon>
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames("input-group-merge", {
               focused: this.state.merged && this.state.merged.textarea
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>With textarea</InputGroupText>
             </InputGroupAddon>
             <Input
               type="textarea"
               onFocus={e =>
                 this.setState({
                   merged: { ...this.state.merged, textarea: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   merged: { ...this.state.merged, textarea: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
       </>
     );
   }
 }

 export default InputGroups;
`;
const codeSizing = `import React from "react";
import classnames from "classnames";
 // reactstrap components
 import {
   Button,
   FormGroup,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup
 } from "reactstrap";

 class InputGroups extends React.Component {
   state = {};
   render() {
     return (
       <>
         <FormGroup>
           <InputGroup
             className={classnames("input-group-sm", {
               focused: this.state.sizing && this.state.sizing.small
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>Small</InputGroupText>
             </InputGroupAddon>
             <Input
               type="text"
               onFocus={e =>
                 this.setState({
                   sizing: { ...this.state.sizing, small: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   sizing: { ...this.state.sizing, small: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames({
               focused: this.state.sizing && this.state.sizing.default
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>Default</InputGroupText>
             </InputGroupAddon>
             <Input
               type="text"
               onFocus={e =>
                 this.setState({
                   sizing: { ...this.state.sizing, default: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   sizing: { ...this.state.sizing, default: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames("input-group-lg", {
               focused: this.state.sizing && this.state.sizing.large
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>Large</InputGroupText>
             </InputGroupAddon>
             <Input
               type="text"
               onFocus={e =>
                 this.setState({
                   sizing: { ...this.state.sizing, large: true }
                 })
               }
               onBlur={e =>
                 this.setState({
                   sizing: { ...this.state.sizing, large: false }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
       </>
     );
   }
 }

 export default InputGroups;
`;
const codeCheckboxesAndRadios = `import React from "react";
import classnames from "classnames";
 // reactstrap components
 import {
   Button,
   FormGroup,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup
 } from "reactstrap";

 class InputGroups extends React.Component {
   state = {};
   render() {
     return (
       <>
         <FormGroup>
           <InputGroup
             className={classnames({
               focused:
                 this.state.checkboxesAndRadios &&
                 this.state.checkboxesAndRadios.check
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>
                 <input type="checkbox" />
               </InputGroupText>
             </InputGroupAddon>
             <Input
               onFocus={e =>
                 this.setState({
                   checkboxesAndRadios: {
                     ...this.state.checkboxesAndRadios,
                     check: true
                   }
                 })
               }
               onBlur={e =>
                 this.setState({
                   checkboxesAndRadios: {
                     ...this.state.checkboxesAndRadios,
                     check: false
                   }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
         <FormGroup>
           <InputGroup
             className={classnames({
               focused:
                 this.state.checkboxesAndRadios &&
                 this.state.checkboxesAndRadios.radio
             })}
           >
             <InputGroupAddon addonType="prepend">
               <InputGroupText>
                 <input type="radio" />
               </InputGroupText>
             </InputGroupAddon>
             <Input
               onFocus={e =>
                 this.setState({
                   checkboxesAndRadios: {
                     ...this.state.checkboxesAndRadios,
                     radio: true
                   }
                 })
               }
               onBlur={e =>
                 this.setState({
                   checkboxesAndRadios: {
                     ...this.state.checkboxesAndRadios,
                     radio: false
                   }
                 })
               }
             />
           </InputGroup>
         </FormGroup>
       </>
     );
   }
 }

 export default InputGroups;
`;
const codeMultipleInputs = `import React from "react";
 // reactstrap components
 import {
   Button,
   FormGroup,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup
 } from "reactstrap";

 class InputGroups extends React.Component {
   state = {};
   render() {
     return (
       <>
         <InputGroup>
           <InputGroupAddon addonType="prepend">
             <InputGroupText>First and last name</InputGroupText>
           </InputGroupAddon>
           <Input />
           <Input />
         </InputGroup>
       </>
     );
   }
 }

 export default InputGroups;
`;
const codeMultipleAddons = `import React from "react";
 // reactstrap components
 import {
   Button,
   FormGroup,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup
 } from "reactstrap";

 class InputGroups extends React.Component {
   state = {};
   render() {
     return (
       <>
         <InputGroup className="mb-3">
           <InputGroupAddon addonType="prepend">
             <InputGroupText>$</InputGroupText>
             <InputGroupText>0.00</InputGroupText>
           </InputGroupAddon>
           <Input type="text" />
         </InputGroup>
         <InputGroup>
           <Input type="text" />
           <InputGroupAddon addonType="append">
             <InputGroupText>$</InputGroupText>
             <InputGroupText>0.00</InputGroupText>
           </InputGroupAddon>
         </InputGroup>
       </>
     );
   }
 }

 export default InputGroups;
`;
const codeButtonAddons = `import React from "react";
 // reactstrap components
 import {
   Button,
   FormGroup,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup
 } from "reactstrap";

 class InputGroups extends React.Component {
   state = {};
   render() {
     return (
       <>
         <InputGroup className="mb-3">
           <InputGroupAddon addonType="prepend">
             <Button color="primary" outline>Button</Button>
           </InputGroupAddon>
           <Input placeholder="" type="text" />
         </InputGroup>
         <InputGroup className="mb-3">
           <Input placeholder="Recipient's username" type="text" />
           <InputGroupAddon addonType="append">
             <Button color="primary" outline>Button</Button>
           </InputGroupAddon>
         </InputGroup>
         <InputGroup className="mb-3">
           <InputGroupAddon addonType="prepend">
             <Button color="primary" outline type="button">
               Button
             </Button>
             <Button color="primary" outline type="button">
               Button
             </Button>
           </InputGroupAddon>
           <Input placeholder="" type="text" />
         </InputGroup>
         <InputGroup>
           <Input placeholder="Recipient's username" type="text" />
           <InputGroupAddon addonType="append">
             <Button color="primary" outline type="button">
               Button
             </Button>
             <Button color="primary" outline type="button">
               Button
             </Button>
           </InputGroupAddon>
         </InputGroup>
       </>
     );
   }
 }

 export default InputGroups;
`;

class InputGroups extends React.Component {
  state = {};
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title">Input group</h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Easily extend form controls by adding text, buttons, or button groups
          on either side of textual inputs, custom selects, and custom file
          inputs.
        </p>
        <hr />
        <h2>Example</h2>
        <h3>Default</h3>
        <div className="ct-example bg-white">
          <FormGroup>
            <InputGroup
              className={classnames({
                focused: this.state.default && this.state.default.username
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>@</InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder="Username"
                type="text"
                onFocus={e =>
                  this.setState({
                    default: { ...this.state.default, username: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    default: { ...this.state.default, username: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames({
                focused: this.state.default && this.state.default.recipient
              })}
            >
              <Input
                placeholder="Recipient's username"
                type="text"
                onFocus={e =>
                  this.setState({
                    default: { ...this.state.default, recipient: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    default: { ...this.state.default, recipient: false }
                  })
                }
              />
              <InputGroupAddon addonType="append">
                <InputGroupText>@example.com</InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <label className="form-control-label" htmlFor="basic-url">
              Your vanity URL
            </label>
            <InputGroup
              className={classnames({
                focused: this.state.default && this.state.default.basicURL
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>https://example.com/users/</InputGroupText>
              </InputGroupAddon>
              <Input
                onFocus={e =>
                  this.setState({
                    default: { ...this.state.default, basicURL: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    default: { ...this.state.default, basicURL: false }
                  })
                }
                type="text"
              />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames({
                focused: this.state.default && this.state.default.dollar
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>$</InputGroupText>
              </InputGroupAddon>
              <Input
                onFocus={e =>
                  this.setState({
                    default: { ...this.state.default, dollar: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    default: { ...this.state.default, dollar: false }
                  })
                }
              />
              <InputGroupAddon addonType="append">
                <InputGroupText>.00</InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames({
                focused: this.state.default && this.state.default.textarea
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>With textarea</InputGroupText>
              </InputGroupAddon>
              <Input
                type="textarea"
                onFocus={e =>
                  this.setState({
                    default: { ...this.state.default, textarea: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    default: { ...this.state.default, textarea: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeDefault}
        </SyntaxHighlighter>
        <h3>Merged</h3>
        <p>
          You can choose to merge input group elements by removing the border
          between them using the{" "}
          <code className="highlighter-rouge">.input-group-merge</code>
          modifier class.
        </p>
        <div className="ct-example bg-white">
          <FormGroup>
            <InputGroup
              className={classnames("input-group-merge", {
                focused: this.state.merged && this.state.merged.username
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>@</InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder="Username"
                type="text"
                onFocus={e =>
                  this.setState({
                    merged: { ...this.state.merged, username: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    merged: { ...this.state.merged, username: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames("input-group-merge", {
                focused: this.state.merged && this.state.merged.recipient
              })}
            >
              <Input
                placeholder="Recipient's username"
                type="text"
                onFocus={e =>
                  this.setState({
                    merged: { ...this.state.merged, recipient: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    merged: { ...this.state.merged, recipient: false }
                  })
                }
              />
              <InputGroupAddon addonType="append">
                <InputGroupText>@example.com</InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <label className="form-control-label" htmlFor="basic-url">
              Your vanity URL
            </label>
            <InputGroup
              className={classnames("input-group-merge", {
                focused: this.state.merged && this.state.merged.basicURL
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>https://example.com/users/</InputGroupText>
              </InputGroupAddon>
              <Input
                type="text"
                onFocus={e =>
                  this.setState({
                    merged: { ...this.state.merged, basicURL: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    merged: { ...this.state.merged, basicURL: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames("input-group-merge", {
                focused: this.state.merged && this.state.merged.dollar
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>$</InputGroupText>
              </InputGroupAddon>
              <Input
                onFocus={e =>
                  this.setState({
                    merged: { ...this.state.merged, dollar: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    merged: { ...this.state.merged, dollar: false }
                  })
                }
              />
              <InputGroupAddon addonType="append">
                <InputGroupText>.00</InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames("input-group-merge", {
                focused: this.state.merged && this.state.merged.textarea
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>With textarea</InputGroupText>
              </InputGroupAddon>
              <Input
                type="textarea"
                onFocus={e =>
                  this.setState({
                    merged: { ...this.state.merged, textarea: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    merged: { ...this.state.merged, textarea: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMerged}
        </SyntaxHighlighter>
        <h2>Sizing</h2>
        <div className="ct-example bg-white">
          <FormGroup>
            <InputGroup
              className={classnames("input-group-sm", {
                focused: this.state.sizing && this.state.sizing.small
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Small</InputGroupText>
              </InputGroupAddon>
              <Input
                type="text"
                onFocus={e =>
                  this.setState({
                    sizing: { ...this.state.sizing, small: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    sizing: { ...this.state.sizing, small: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames({
                focused: this.state.sizing && this.state.sizing.default
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Default</InputGroupText>
              </InputGroupAddon>
              <Input
                type="text"
                onFocus={e =>
                  this.setState({
                    sizing: { ...this.state.sizing, default: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    sizing: { ...this.state.sizing, default: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames("input-group-lg", {
                focused: this.state.sizing && this.state.sizing.large
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Large</InputGroupText>
              </InputGroupAddon>
              <Input
                type="text"
                onFocus={e =>
                  this.setState({
                    sizing: { ...this.state.sizing, large: true }
                  })
                }
                onBlur={e =>
                  this.setState({
                    sizing: { ...this.state.sizing, large: false }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeSizing}
        </SyntaxHighlighter>
        <h2>Checkboxes and radios</h2>
        <div className="ct-example bg-white">
          <FormGroup>
            <InputGroup
              className={classnames({
                focused:
                  this.state.checkboxesAndRadios &&
                  this.state.checkboxesAndRadios.check
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <input type="checkbox" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                onFocus={e =>
                  this.setState({
                    checkboxesAndRadios: {
                      ...this.state.checkboxesAndRadios,
                      check: true
                    }
                  })
                }
                onBlur={e =>
                  this.setState({
                    checkboxesAndRadios: {
                      ...this.state.checkboxesAndRadios,
                      check: false
                    }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <InputGroup
              className={classnames({
                focused:
                  this.state.checkboxesAndRadios &&
                  this.state.checkboxesAndRadios.radio
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <input type="radio" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                onFocus={e =>
                  this.setState({
                    checkboxesAndRadios: {
                      ...this.state.checkboxesAndRadios,
                      radio: true
                    }
                  })
                }
                onBlur={e =>
                  this.setState({
                    checkboxesAndRadios: {
                      ...this.state.checkboxesAndRadios,
                      radio: false
                    }
                  })
                }
              />
            </InputGroup>
          </FormGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCheckboxesAndRadios}
        </SyntaxHighlighter>
        <h2>Multiple inputs</h2>
        <div className="ct-example bg-white">
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>First and last name</InputGroupText>
            </InputGroupAddon>
            <Input />
            <Input />
          </InputGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMultipleInputs}
        </SyntaxHighlighter>
        <h2>Multiple addons</h2>
        <div className="ct-example bg-white">
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>$</InputGroupText>
              <InputGroupText>0.00</InputGroupText>
            </InputGroupAddon>
            <Input type="text" />
          </InputGroup>
          <InputGroup>
            <Input type="text" />
            <InputGroupAddon addonType="append">
              <InputGroupText>$</InputGroupText>
              <InputGroupText>0.00</InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMultipleAddons}
        </SyntaxHighlighter>
        <h2>Button addons</h2>
        <div className="ct-example bg-white">
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <Button color="primary" outline>
                Button
              </Button>
            </InputGroupAddon>
            <Input placeholder="" type="text" />
          </InputGroup>
          <InputGroup className="mb-3">
            <Input placeholder="Recipient's username" type="text" />
            <InputGroupAddon addonType="append">
              <Button color="primary" outline>
                Button
              </Button>
            </InputGroupAddon>
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <Button color="primary" outline type="button">
                Button
              </Button>
              <Button color="primary" outline type="button">
                Button
              </Button>
            </InputGroupAddon>
            <Input placeholder="" type="text" />
          </InputGroup>
          <InputGroup>
            <Input placeholder="Recipient's username" type="text" />
            <InputGroupAddon addonType="append">
              <Button color="primary" outline type="button">
                Button
              </Button>
              <Button color="primary" outline type="button">
                Button
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeButtonAddons}
        </SyntaxHighlighter>
        <h3>Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://reactstrap.github.io/components/input-group/?ref=creativetim"
            target="_blank"
          >
            reactstrap input group documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default InputGroups;
