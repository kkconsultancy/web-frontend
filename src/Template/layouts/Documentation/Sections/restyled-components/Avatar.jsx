/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";

const codeExamples = `import React from "react";

class Avatar extends React.Component {
  render() {
    return (
      <>
        <a
          className="avatar"
          href="#pablo"
          onClick={e => e.preventDefault()}
        >
          <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
        </a>
        <a
          className="avatar rounded-circle"
          href="#pablo"
          onClick={e => e.preventDefault()}
        >
          <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
        </a>
      </>
    );
  }
}

export default Avatar;
`;
const codeSizing = `import React from "react";

class Avatar extends React.Component {
  render() {
    return (
      <>
        <a
          className="avatar avatar-xs rounded-circle"
          href="#pablo"
          onClick={e => e.preventDefault()}
        >
          <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
        </a>
        <a
          className="avatar avatar-sm rounded-circle"
          href="#pablo"
          onClick={e => e.preventDefault()}
        >
          <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
        </a>
        <a
          className="avatar rounded-circle"
          href="#pablo"
          onClick={e => e.preventDefault()}
        >
          <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
        </a>
        <a
          className="avatar avatar-lg rounded-circle"
          href="#pablo"
          onClick={e => e.preventDefault()}
        >
          <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
        </a>
        <a
          className="avatar avatar-xl rounded-circle"
          href="#pablo"
          onClick={e => e.preventDefault()}
        >
          <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
        </a>
      </>
    );
  }
}

export default Avatar`;
const codeGroup = `import React from "react";

class Avatar extends React.Component {
  render() {
    return (
      <>
        <div className="avatar-group">
          <a
            className="avatar rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-1.jpg")} />
          </a>
          <a
            className="avatar rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-2.jpg")} />
          </a>
          <a
            className="avatar rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-3.jpg")} />
          </a>
          <a
            className="avatar rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
        </div>
      </>
    );
  }
}

export default Avatar`;

class Avatar extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Avatar
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Cool avatars with different shapes, sizes and with the possibility to
          group them.
        </p>
        <hr />
        <h2 id="examples">Examples</h2>
        <p>
          Use the <code className="highlighter-rouge">.rounded-circle</code>
          modifier class to create a circle avatar.
        </p>
        <div className="ct-example bg-white">
          <a className="avatar" href="#pablo" onClick={e => e.preventDefault()}>
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
          <a
            className="avatar rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExamples}
        </SyntaxHighlighter>
        <h2 id="sizing">Sizing</h2>
        <p>
          The most basic list group is an unordered list with list items and the
          proper classes. Build upon it with the options that follow, or with
          your own CSS as needed.
        </p>
        <div className="ct-example bg-white">
          <a
            className="avatar avatar-xs rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
          <a
            className="avatar avatar-sm rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
          <a
            className="avatar rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
          <a
            className="avatar avatar-lg rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
          <a
            className="avatar avatar-xl rounded-circle"
            href="#pablo"
            onClick={e => e.preventDefault()}
          >
            <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
          </a>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeSizing}
        </SyntaxHighlighter>
        <h2 id="group">Group</h2>
        <p>
          Include multiple avatar items inside an{" "}
          <code className="highlighter-rouge">.avatar-group</code>
          container.
        </p>
        <div className="ct-example bg-white">
          <div className="avatar-group">
            <a
              className="avatar rounded-circle"
              href="#pablo"
              onClick={e => e.preventDefault()}
            >
              <img alt="..." src={require("../../../../assets/img/theme/team-1.jpg")} />
            </a>
            <a
              className="avatar rounded-circle"
              href="#pablo"
              onClick={e => e.preventDefault()}
            >
              <img alt="..." src={require("../../../../assets/img/theme/team-2.jpg")} />
            </a>
            <a
              className="avatar rounded-circle"
              href="#pablo"
              onClick={e => e.preventDefault()}
            >
              <img alt="..." src={require("../../../../assets/img/theme/team-3.jpg")} />
            </a>
            <a
              className="avatar rounded-circle"
              href="#pablo"
              onClick={e => e.preventDefault()}
            >
              <img alt="..." src={require("../../../../assets/img/theme/team-4.jpg")} />
            </a>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeGroup}
        </SyntaxHighlighter>
      </>
    );
  }
}

export default Avatar;
