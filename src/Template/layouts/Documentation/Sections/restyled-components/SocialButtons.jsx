/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// reactstrap components
import { Button } from "reactstrap";

const codeExamples = `import React from "react";
 // reactstrap components
 import { Button } from "reactstrap";

 class SocialButtons extends React.Component {
   render() {
     return (
       <>
         <Button className="btn-icon" color="facebook" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-facebook" />
           </span>
           <span className="btn-inner--text">Facebook</span>
         </Button>
         <Button className="btn-icon" color="twitter" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-twitter" />
           </span>
           <span className="btn-inner--text">Twitter</span>
         </Button>
         <Button
           className="btn-google-plus btn-icon"
           color="google"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-google-plus-g" />
           </span>
           <span className="btn-inner--text">Google +</span>
         </Button>
         <Button
           className="btn-instagram btn-icon"
           color="default"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-instagram" />
           </span>
           <span className="btn-inner--text">Instagram</span>
         </Button>
         <Button className="btn-icon" color="pinterest" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-pinterest" />
           </span>
           <span className="btn-inner--text">Pinterest</span>
         </Button>
         <Button className="btn-icon" color="youtube" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-youtube" />
           </span>
           <span className="btn-inner--text">Youtube</span>
         </Button>
         <Button className="btn-vimeo btn-icon" color="default" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-vimeo-v" />
           </span>
           <span className="btn-inner--text">Vimeo</span>
         </Button>
         <Button className="btn-slack btn-icon" color="default" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-slack" />
           </span>
           <span className="btn-inner--text">Slack</span>
         </Button>
         <Button className="btn-icon" color="dribbble" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-dribbble" />
           </span>
           <span className="btn-inner--text">Dribbble</span>
         </Button>
       </>
     );
   }
 }

 export default SocialButtons;
`;
const codeIconOnly = `import React from "react";
 // reactstrap components
 import { Button } from "reactstrap";

 class SocialButtons extends React.Component {
   render() {
     return (
       <>
         <Button className="btn-icon-only" color="facebook" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-facebook" />
           </span>
         </Button>
         <Button className="btn-icon-only" color="twitter" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-twitter" />
           </span>
         </Button>
         <Button
           className="btn-google-plus btn-icon-only"
           color="google"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-google-plus-g" />
           </span>
         </Button>
         <Button
           className="btn-instagram btn-icon-only"
           color="default"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-instagram" />
           </span>
         </Button>
         <Button className="btn-icon-only" color="pinterest" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-pinterest" />
           </span>
         </Button>
         <Button className="btn-icon-only" color="youtube" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-youtube" />
           </span>
         </Button>
         <Button
           className="btn-vimeo btn-icon-only"
           color="default"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-vimeo-v" />
           </span>
         </Button>
         <Button
           className="btn-slack btn-icon-only"
           color="default"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-slack" />
           </span>
         </Button>
         <Button className="btn-icon-only" color="dribbble" type="button">
           <span className="btn-inner--icon">
             <i className="fab fa-dribbble" />
           </span>
         </Button>
       </>
     );
   }
 }

 export default SocialButtons;
`;
const codeCircle = `import React from "react";
 // reactstrap components
 import { Button } from "reactstrap";

 class SocialButtons extends React.Component {
   render() {
     return (
       <>
         <Button
           className="btn-icon-only rounded-circle"
           color="facebook"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-facebook" />
           </span>
         </Button>
         <Button
           className="btn-icon-only rounded-circle"
           color="twitter"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-twitter" />
           </span>
         </Button>
         <Button
           className="btn-google-plus btn-icon-only rounded-circle"
           color="google"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-google-plus-g" />
           </span>
         </Button>
         <Button
           className="btn-instagram btn-icon-only rounded-circle"
           color="default"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-instagram" />
           </span>
         </Button>
         <Button
           className="btn-icon-only rounded-circle"
           color="pinterest"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-pinterest" />
           </span>
         </Button>
         <Button
           className="btn-icon-only rounded-circle"
           color="youtube"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-youtube" />
           </span>
         </Button>
         <Button
           className="btn-vimeo btn-icon-only rounded-circle"
           color="default"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-vimeo-v" />
           </span>
         </Button>
         <Button
           className="btn-slack btn-icon-only rounded-circle"
           color="default"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-slack" />
           </span>
         </Button>
         <Button
           className="btn-icon-only rounded-circle"
           color="dribbble"
           type="button"
         >
           <span className="btn-inner--icon">
             <i className="fab fa-dribbble" />
           </span>
         </Button>
       </>
     );
   }
 }

 export default SocialButtons;
`;

class SocialButtons extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Social buttons
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Pure CSS social buttons with plenty of examples. Also, easy to extend
          or add new brands.
        </p>
        <hr />
        <h2 id="examples">Examples</h2>
        <div className="ct-example bg-white">
          <Button className="btn-icon" color="facebook" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-facebook" />
            </span>
            <span className="btn-inner--text">Facebook</span>
          </Button>
          <Button className="btn-icon" color="twitter" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-twitter" />
            </span>
            <span className="btn-inner--text">Twitter</span>
          </Button>
          <Button
            className="btn-google-plus btn-icon"
            color="google"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-google-plus-g" />
            </span>
            <span className="btn-inner--text">Google +</span>
          </Button>
          <Button
            className="btn-instagram btn-icon"
            color="default"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-instagram" />
            </span>
            <span className="btn-inner--text">Instagram</span>
          </Button>
          <Button className="btn-icon" color="pinterest" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-pinterest" />
            </span>
            <span className="btn-inner--text">Pinterest</span>
          </Button>
          <Button className="btn-icon" color="youtube" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-youtube" />
            </span>
            <span className="btn-inner--text">Youtube</span>
          </Button>
          <Button className="btn-vimeo btn-icon" color="default" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-vimeo-v" />
            </span>
            <span className="btn-inner--text">Vimeo</span>
          </Button>
          <Button className="btn-slack btn-icon" color="default" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-slack" />
            </span>
            <span className="btn-inner--text">Slack</span>
          </Button>
          <Button className="btn-icon" color="dribbble" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-dribbble" />
            </span>
            <span className="btn-inner--text">Dribbble</span>
          </Button>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExamples}
        </SyntaxHighlighter>
        <h2 id="icon-only">Icon only</h2>
        <div className="ct-example bg-white">
          <Button className="btn-icon-only" color="facebook" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-facebook" />
            </span>
          </Button>
          <Button className="btn-icon-only" color="twitter" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-twitter" />
            </span>
          </Button>
          <Button
            className="btn-google-plus btn-icon-only"
            color="google"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-google-plus-g" />
            </span>
          </Button>
          <Button
            className="btn-instagram btn-icon-only"
            color="default"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-instagram" />
            </span>
          </Button>
          <Button className="btn-icon-only" color="pinterest" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-pinterest" />
            </span>
          </Button>
          <Button className="btn-icon-only" color="youtube" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-youtube" />
            </span>
          </Button>
          <Button
            className="btn-vimeo btn-icon-only"
            color="default"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-vimeo-v" />
            </span>
          </Button>
          <Button
            className="btn-slack btn-icon-only"
            color="default"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-slack" />
            </span>
          </Button>
          <Button className="btn-icon-only" color="dribbble" type="button">
            <span className="btn-inner--icon">
              <i className="fab fa-dribbble" />
            </span>
          </Button>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeIconOnly}
        </SyntaxHighlighter>
        <h2 id="circle">Circle</h2>
        <p>
          Add the <code className="highlighter-rouge">.rounded-circle</code>
          modifier class to create a fully rounded button.
        </p>
        <div className="ct-example bg-white">
          <Button
            className="btn-icon-only rounded-circle"
            color="facebook"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-facebook" />
            </span>
          </Button>
          <Button
            className="btn-icon-only rounded-circle"
            color="twitter"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-twitter" />
            </span>
          </Button>
          <Button
            className="btn-google-plus btn-icon-only rounded-circle"
            color="google"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-google-plus-g" />
            </span>
          </Button>
          <Button
            className="btn-instagram btn-icon-only rounded-circle"
            color="default"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-instagram" />
            </span>
          </Button>
          <Button
            className="btn-icon-only rounded-circle"
            color="pinterest"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-pinterest" />
            </span>
          </Button>
          <Button
            className="btn-icon-only rounded-circle"
            color="youtube"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-youtube" />
            </span>
          </Button>
          <Button
            className="btn-vimeo btn-icon-only rounded-circle"
            color="default"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-vimeo-v" />
            </span>
          </Button>
          <Button
            className="btn-slack btn-icon-only rounded-circle"
            color="default"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-slack" />
            </span>
          </Button>
          <Button
            className="btn-icon-only rounded-circle"
            color="dribbble"
            type="button"
          >
            <span className="btn-inner--icon">
              <i className="fab fa-dribbble" />
            </span>
          </Button>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCircle}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://reactstrap.github.io/components/buttons/?ref=creativetim"
            target="_blank"
          >
            reactstrap buttons documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default SocialButtons;
