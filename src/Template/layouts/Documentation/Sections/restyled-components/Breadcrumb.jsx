/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// reactstrap components
import { Breadcrumb, BreadcrumbItem } from "reactstrap";

const codeExample = `import React from "react";
// reactstrap components
import {
  Breadcrumb,
  BreadcrumbItem,
} from "reactstrap";

class Breadcrumbs extends React.Component {
  render() {
    return (
      <>
        <Breadcrumb>
          <BreadcrumbItem aria-current="page" className="active">
            Home
          </BreadcrumbItem>
        </Breadcrumb>
        <Breadcrumb>
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Home
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem aria-current="page" className="active">
            Library
          </BreadcrumbItem>
        </Breadcrumb>
        <Breadcrumb>
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Home
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Library
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem aria-current="page" className="active">
            Data
          </BreadcrumbItem>
        </Breadcrumb>
      </>
    );
  }
}

export default Breadcrumbs;
`;
const codeDark = `import React from "react";
// reactstrap components
import {
  Breadcrumb,
  BreadcrumbItem,
} from "reactstrap";

class Breadcrumbs extends React.Component {
  render() {
    return (
      <>
        <Breadcrumb listClassName="breadcrumb-dark bg-dark">
          <BreadcrumbItem aria-current="page" className="active">
            Home
          </BreadcrumbItem>
        </Breadcrumb>
        <Breadcrumb listClassName="breadcrumb-dark bg-primary">
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Home
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem aria-current="page" className="active">
            Library
          </BreadcrumbItem>
        </Breadcrumb>
        <Breadcrumb listClassName="breadcrumb-dark bg-danger">
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Home
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Library
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem aria-current="page" className="active">
            Data
          </BreadcrumbItem>
        </Breadcrumb>
      </>
    );
  }
}

export default Breadcrumbs;
`;
const codeLinks = `import React from "react";
// reactstrap components
import {
  Breadcrumb,
  BreadcrumbItem,
} from "reactstrap";

class Breadcrumbs extends React.Component {
  render() {
    return (
      <>
        <Breadcrumb listClassName="breadcrumb-links">
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Home
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <a href="#pablo" onClick={e => e.preventDefault()}>
              Library
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem aria-current="page" className="active">
            Data
          </BreadcrumbItem>
        </Breadcrumb>
      </>
    );
  }
}

export default Breadcrumbs;
`;

class Breadcrumbs extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Breadcrumb
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Indicate the current page’s location within a navigational hierarchy
          that automatically adds separators via CSS.
        </p>
        <hr />
        <h2 id="example">Example</h2>
        <div className="ct-example bg-white">
          <Breadcrumb>
            <BreadcrumbItem aria-current="page" className="active">
              Home
            </BreadcrumbItem>
          </Breadcrumb>
          <Breadcrumb>
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Home
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem aria-current="page" className="active">
              Library
            </BreadcrumbItem>
          </Breadcrumb>
          <Breadcrumb>
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Home
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Library
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem aria-current="page" className="active">
              Data
            </BreadcrumbItem>
          </Breadcrumb>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeExample}
        </SyntaxHighlighter>
        <h2 id="dark">Dark</h2>
        <div className="ct-example bg-white">
          <Breadcrumb listClassName="breadcrumb-dark bg-dark">
            <BreadcrumbItem aria-current="page" className="active">
              Home
            </BreadcrumbItem>
          </Breadcrumb>
          <Breadcrumb listClassName="breadcrumb-dark bg-primary">
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Home
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem aria-current="page" className="active">
              Library
            </BreadcrumbItem>
          </Breadcrumb>
          <Breadcrumb listClassName="breadcrumb-dark bg-danger">
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Home
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Library
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem aria-current="page" className="active">
              Data
            </BreadcrumbItem>
          </Breadcrumb>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeDark}
        </SyntaxHighlighter>
        <h2 id="links">Links</h2>
        <div className="ct-example bg-white">
          <Breadcrumb listClassName="breadcrumb-links">
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Home
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                Library
              </a>
            </BreadcrumbItem>
            <BreadcrumbItem aria-current="page" className="active">
              Data
            </BreadcrumbItem>
          </Breadcrumb>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeLinks}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://reactstrap.github.io/components/breadcrumbs/?ref=creativetim"
            target="_blank"
          >
            reactstrap Breadcrumbs documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Breadcrumbs;
