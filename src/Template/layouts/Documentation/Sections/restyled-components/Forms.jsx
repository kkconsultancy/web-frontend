/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";
// plugin that creates slider
import Slider from "nouislider";

const codeDefault = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Input placeholder="Default input" type="text" />
      </>
    );
  }
}

export default Forms;
`;
const codeAlternative = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <div className="p-4 bg-secondary">
          <Input
            className="form-control-alternative"
            placeholder="Alternative input"
            type="text"
          />
        </div>
      </>
    );
  }
}

export default Forms;
`;
const codeFlush = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Input
          className="form-control-flush"
          placeholder="Fulshed input"
          type="text"
        />
      </>
    );
  }
}

export default Forms;
`;
const codeMuted = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Input
          className="form-control-muted"
          placeholder="Muted input"
          type="text"
        />
      </>
    );
  }
}

export default Forms;
`;
const codeFormControls = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Form>
          <FormGroup>
            <label htmlFor="exampleFormControlInput1">Email address</label>
            <Input
              id="exampleFormControlInput1"
              placeholder="name@example.com"
              type="email"
            />
          </FormGroup>
          <FormGroup>
            <label htmlFor="exampleFormControlSelect1">Example select</label>
            <Input id="exampleFormControlSelect1" type="select">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </Input>
          </FormGroup>
          <FormGroup>
            <label htmlFor="exampleFormControlSelect2">
              Example multiple select
            </label>
            <Input id="exampleFormControlSelect2" multiple="" type="select">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </Input>
          </FormGroup>
          <FormGroup>
            <label htmlFor="exampleFormControlTextarea1">
              Example textarea
            </label>
            <Input id="exampleFormControlTextarea1" rows="3" />
          </FormGroup>
        </Form>
      </>
    );
  }
}

export default Forms;
`;
const codeFileBrowser = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Form>
          <div className="custom-file">
            <input
              className="custom-file-input"
              id="customFileLang"
              lang="en"
              type="file"
            />
            <label className="custom-file-label" htmlFor="customFileLang">
              Select file
            </label>
          </div>
        </Form>
      </>
    );
  }
}

export default Forms;
`;
const codeHTML5Inputs = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Form>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-text-input"
            >
              Text
            </label>
            <Input
              defaultValue="John Snow"
              id="example-text-input"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-search-input"
            >
              Search
            </label>
            <Input
              defaultValue="Tell me your secret ..."
              id="example-search-input"
              type="search"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-email-input"
            >
              Email
            </label>
            <Input
              defaultValue="argon@example.com"
              id="example-email-input"
              type="email"
            />
          </FormGroup>
          <FormGroup>
            <label className="form-control-label" htmlFor="example-url-input">
              URL
            </label>
            <Input
              defaultValue="https://www.creative-tim.com"
              id="example-url-input"
              type="url"
            />
          </FormGroup>
          <FormGroup>
            <label className="form-control-label" htmlFor="example-tel-input">
              Phone
            </label>
            <Input
              defaultValue="40-(770)-888-444"
              id="example-tel-input"
              type="tel"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-password-input"
            >
              Password
            </label>
            <Input
              defaultValue="password"
              id="example-password-input"
              type="password"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-number-input"
            >
              Number
            </label>
            <Input
              defaultValue="23"
              id="example-number-input"
              type="number"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-datetime-local-input"
            >
              Datetime
            </label>
            <Input
              defaultValue="2018-11-23T10:30:00"
              id="example-datetime-local-input"
              type="datetime-local"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-date-input"
            >
              Date
            </label>
            <Input
              defaultValue="2018-11-23"
              id="example-date-input"
              type="date"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-month-input"
            >
              Month
            </label>
            <Input
              defaultValue="2018-11"
              id="example-month-input"
              type="month"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-week-input"
            >
              Week
            </label>
            <Input
              defaultValue="2018-W23"
              id="example-week-input"
              type="week"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-time-input"
            >
              Time
            </label>
            <Input
              defaultValue="10:30:00"
              id="example-time-input"
              type="time"
            />
          </FormGroup>
          <FormGroup>
            <label
              className="form-control-label"
              htmlFor="example-color-input"
            >
              Color
            </label>
            <Input
              defaultValue="#5e72e4"
              id="example-color-input"
              type="color"
            />
          </FormGroup>
        </Form>
      </>
    );
  }
}

export default Forms;
`;
const codeSizing1 = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Input
          className="form-control-lg"
          placeholder=".form-control-lg"
          type="text"
        />
        <Input placeholder="Default input" type="text" />
        <Input
          className="form-control-sm"
          placeholder=".form-control-sm"
          type="text"
        />
      </>
    );
  }
}

export default Forms;
`;
const codeSizing2 = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <Input className="form-control-lg" type="select">
          <option>Large select</option>
        </Input>
        <Input type="select">
          <option>Default select</option>
        </Input>
        <Input className="form-control-sm" type="select">
          <option>Small select</option>
        </Input>
      </>
    );
  }
}

export default Forms;
`;
const codeCheckboxes = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <div className="custom-control custom-checkbox">
          <input
            className="custom-control-input"
            id="customCheck1"
            type="checkbox"
          />
          <label className="custom-control-label" htmlFor="customCheck1">
            Check this custom checkbox
          </label>
        </div>
      </>
    );
  }
}

export default Forms;
`;
const codeRadios = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <div className="custom-control custom-radio mb-3">
          <input
            className="custom-control-input"
            id="customRadio1"
            name="customRadio"
            type="radio"
          />
          <label className="custom-control-label" htmlFor="customRadio1">
            Toggle this custom radio
          </label>
        </div>
        <div className="custom-control custom-radio">
          <input
            className="custom-control-input"
            id="customRadio2"
            name="customRadio"
            type="radio"
          />
          <label className="custom-control-label" htmlFor="customRadio2">
            Or toggle this other custom radio
          </label>
        </div>
      </>
    );
  }
}

export default Forms;
`;
const codeInline = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <div className="custom-control custom-radio custom-control-inline">
          <input
            className="custom-control-input"
            id="customRadioInline1"
            name="customRadioInline1"
            type="radio"
          />
          <label
            className="custom-control-label"
            htmlFor="customRadioInline1"
          >
            Toggle this custom radio
          </label>
        </div>
        <div className="custom-control custom-radio custom-control-inline">
          <input
            className="custom-control-input"
            id="customRadioInline2"
            name="customRadioInline1"
            type="radio"
          />
          <label
            className="custom-control-label"
            htmlFor="customRadioInline2"
          >
            Or toggle this other custom radio
          </label>
        </div>
      </>
    );
  }
}

export default Forms;
`;
const codeDisabled = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <div className="custom-control custom-checkbox">
          <input
            className="custom-control-input"
            disabled
            id="customCheckDisabled"
            type="checkbox"
          />
          <label
            className="custom-control-label"
            htmlFor="customCheckDisabled"
          >
            Check this custom checkbox
          </label>
        </div>
        <div className="custom-control custom-radio">
          <input
            className="custom-control-input"
            disabled
            id="customRadioDisabled"
            name="radioDisabled"
            type="radio"
          />
          <label
            className="custom-control-label"
            htmlFor="customRadioDisabled"
          >
            Toggle this custom radio
          </label>
        </div>
      </>
    );
  }
}

export default Forms;
`;
const codeToggles = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <label className="custom-toggle">
          <input defaultChecked type="checkbox" />
          <span className="custom-toggle-slider rounded-circle" />
        </label>
      </>
    );
  }
}

export default Forms;
`;
const codeLabeledToggles = `import React from "react";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  render() {
    return (
      <>
        <label className="custom-toggle">
          <input defaultChecked type="checkbox" />
          <span
            className="custom-toggle-slider rounded-circle"
            data-label-off="No"
            data-label-on="Yes"
          />
        </label>
      </>
    );
  }
}

export default Forms;
`;
const codeSliders = `import React from "react";
// plugin that creates slider
import Slider from "nouislider";
// reactstrap components
import { FormGroup, Form, Input, Row, Col } from "reactstrap";

class Forms extends React.Component {
  state = {
    slider1Value: "100.00",
    slider2Values: ["200.00", "400.00"]
  };
  componentDidMount() {
    var slider1 = this.refs.slider1;
    var slider2 = this.refs.slider2;
    Slider.create(slider1, {
      start: [100],
      connect: [true, false],
      step: 0.01,
      range: { min: 100.0, max: 500.0 }
    }).on(
      "update",
      function(values, handle) {
        this.setState({
          slider1Value: values[0]
        });
      }.bind(this)
    );
    Slider.create(slider2, {
      start: [200.0, 400.0],
      connect: [false, true, false],
      step: 0.01,
      range: { min: 100.0, max: 500.0 }
    }).on(
      "update",
      function(values, handle) {
        this.setState({
          slider2Values: [values[0], values[1]]
        });
      }.bind(this)
    );
  }
  render() {
    return (
      <>
        <Form>
          <div className="input-slider-container">
            <div className="input-slider" ref="slider1" />
            <Row className="mt-3">
              <Col xs="6">
                <span className="range-slider-value">
                  {this.state.slider1Value}
                </span>
              </Col>
            </Row>
          </div>
          <div className="mt-5">
            <div ref="slider2" />
            <Row>
              <Col xs="6">
                <span className="range-slider-value value-low">
                  {this.state.slider2Values[0]}
                </span>
              </Col>
              <Col className="text-right" xs="6">
                <span className="range-slider-value value-high">
                  {this.state.slider2Values[1]}
                </span>
              </Col>
            </Row>
          </div>
        </Form>
      </>
    );
  }
}

export default Forms;
`;

class Forms extends React.Component {
  state = {
    slider1Value: "100.00",
    slider2Values: ["200.00", "400.00"]
  };
  componentDidMount() {
    var slider1 = this.refs.slider1;
    var slider2 = this.refs.slider2;
    Slider.create(slider1, {
      start: [100],
      connect: [true, false],
      step: 0.01,
      range: { min: 100.0, max: 500.0 }
    }).on(
      "update",
      function(values, handle) {
        this.setState({
          slider1Value: values[0]
        });
      }.bind(this)
    );
    Slider.create(slider2, {
      start: [200.0, 400.0],
      connect: [false, true, false],
      step: 0.01,
      range: { min: 100.0, max: 500.0 }
    }).on(
      "update",
      function(values, handle) {
        this.setState({
          slider2Values: [values[0], values[1]]
        });
      }.bind(this)
    );
  }
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Forms
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Use Bootstrap’s custom button styles for actions in forms, dialogs,
          and more with support for multiple sizes, states, and more.
        </p>
        <hr />
        <h2 id="inputs">Inputs</h2>
        <h3 id="default">Default</h3>
        <div className="ct-example bg-white">
          <Input placeholder="Default input" type="text" />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeDefault}
        </SyntaxHighlighter>
        <h3 id="alternative">Alternative</h3>
        <p>
          If you want to use forms on grayish background colors, you can use
          this beautiful alternative style which removes the borders and it is
          emphasized only by its shadow.
        </p>
        <div className="ct-example bg-white">
          <div className="p-4 bg-secondary">
            <Input
              className="form-control-alternative"
              placeholder="Alternative input"
              type="text"
            />
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeAlternative}
        </SyntaxHighlighter>
        <h3 id="flush">Flush</h3>
        <p>
          Remove all borders and shadows so you can use it inside other
          elements:
        </p>
        <div className="ct-example bg-white">
          <Input
            className="form-control-flush"
            placeholder="Fulshed input"
            type="text"
          />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeFlush}
        </SyntaxHighlighter>
        <h3 id="muted">Muted</h3>
        <p>
          Remove all borders and shadows so you can use it inside other
          elements:
        </p>
        <div className="ct-example bg-white">
          <Input
            className="form-control-muted"
            placeholder="Muted input"
            type="text"
          />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeMuted}
        </SyntaxHighlighter>
        <h2 id="form-controls">Form controls</h2>
        <p>
          Textual form controls—like{" "}
          <code className="highlighter-rouge">{`<input>`}</code>
          s, <code className="highlighter-rouge">{`<select>`}</code>
          s, and <code className="highlighter-rouge">{`<textarea>`}</code>
          s—are styled with the{" "}
          <code className="highlighter-rouge">.form-control</code>
          class. Included are styles for general appearance, focus state,
          sizing, and more.
        </p>
        <p>
          For all form control you can apply the additional modifier classes
          explained in the Inputs section:{" "}
          <code className="highlighter-rouge">.form-control-alternative</code>,{" "}
          <code className="highlighter-rouge">.form-control-flush</code>
          and <code className="highlighter-rouge">.form-control-muted</code>.
        </p>
        <div className="ct-example bg-white">
          <Form>
            <FormGroup>
              <label htmlFor="exampleFormControlInput1">Email address</label>
              <Input
                id="exampleFormControlInput1"
                placeholder="name@example.com"
                type="email"
              />
            </FormGroup>
            <FormGroup>
              <label htmlFor="exampleFormControlSelect1">Example select</label>
              <Input id="exampleFormControlSelect1" type="select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </Input>
            </FormGroup>
            <FormGroup>
              <label htmlFor="exampleFormControlSelect2">
                Example multiple select
              </label>
              <Input id="exampleFormControlSelect2" multiple="" type="select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </Input>
            </FormGroup>
            <FormGroup>
              <label htmlFor="exampleFormControlTextarea1">
                Example textarea
              </label>
              <Input id="exampleFormControlTextarea1" rows="3" />
            </FormGroup>
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeFormControls}
        </SyntaxHighlighter>
        <h3 id="file-browser">File browser</h3>
        <div className="ct-example bg-white">
          <Form>
            <div className="custom-file">
              <input
                className="custom-file-input"
                id="customFileLang"
                lang="en"
                type="file"
              />
              <label className="custom-file-label" htmlFor="customFileLang">
                Select file
              </label>
            </div>
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeFileBrowser}
        </SyntaxHighlighter>
        <h3 id="html5-inputs">HTML5 inputs</h3>
        <div className="ct-example bg-white">
          <Form>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-text-input"
              >
                Text
              </label>
              <Input
                defaultValue="John Snow"
                id="example-text-input"
                type="text"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-search-input"
              >
                Search
              </label>
              <Input
                defaultValue="Tell me your secret ..."
                id="example-search-input"
                type="search"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-email-input"
              >
                Email
              </label>
              <Input
                defaultValue="argon@example.com"
                id="example-email-input"
                type="email"
              />
            </FormGroup>
            <FormGroup>
              <label className="form-control-label" htmlFor="example-url-input">
                URL
              </label>
              <Input
                defaultValue="https://www.creative-tim.com"
                id="example-url-input"
                type="url"
              />
            </FormGroup>
            <FormGroup>
              <label className="form-control-label" htmlFor="example-tel-input">
                Phone
              </label>
              <Input
                defaultValue="40-(770)-888-444"
                id="example-tel-input"
                type="tel"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-password-input"
              >
                Password
              </label>
              <Input
                defaultValue="password"
                id="example-password-input"
                type="password"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-number-input"
              >
                Number
              </label>
              <Input
                defaultValue="23"
                id="example-number-input"
                type="number"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-datetime-local-input"
              >
                Datetime
              </label>
              <Input
                defaultValue="2018-11-23T10:30:00"
                id="example-datetime-local-input"
                type="datetime-local"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-date-input"
              >
                Date
              </label>
              <Input
                defaultValue="2018-11-23"
                id="example-date-input"
                type="date"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-month-input"
              >
                Month
              </label>
              <Input
                defaultValue="2018-11"
                id="example-month-input"
                type="month"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-week-input"
              >
                Week
              </label>
              <Input
                defaultValue="2018-W23"
                id="example-week-input"
                type="week"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-time-input"
              >
                Time
              </label>
              <Input
                defaultValue="10:30:00"
                id="example-time-input"
                type="time"
              />
            </FormGroup>
            <FormGroup>
              <label
                className="form-control-label"
                htmlFor="example-color-input"
              >
                Color
              </label>
              <Input
                defaultValue="#5e72e4"
                id="example-color-input"
                type="color"
              />
            </FormGroup>
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeHTML5Inputs}
        </SyntaxHighlighter>
        <h3 id="sizing">Sizing</h3>
        <div className="ct-example bg-white">
          <Input
            className="form-control-lg"
            placeholder=".form-control-lg"
            type="text"
          />
          <Input placeholder="Default input" type="text" />
          <Input
            className="form-control-sm"
            placeholder=".form-control-sm"
            type="text"
          />
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeSizing1}
        </SyntaxHighlighter>
        <div className="ct-example bg-white">
          <Input className="form-control-lg" type="select">
            <option>Large select</option>
          </Input>
          <Input type="select">
            <option>Default select</option>
          </Input>
          <Input className="form-control-sm" type="select">
            <option>Small select</option>
          </Input>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeSizing2}
        </SyntaxHighlighter>
        <h2 id="custom-forms">Custom forms</h2>
        <p>
          For even more customization and cross browser consistency, use our
          completely custom form elements to replace the browser defaults.
          They’re built on top of semantic and accessible markup, so they’re
          solid replacements for any default form control.
        </p>
        <h3 id="checkboxes">Checkboxes</h3>
        <div className="ct-example bg-white">
          <div className="custom-control custom-checkbox">
            <input
              className="custom-control-input"
              id="customCheck1"
              type="checkbox"
            />
            <label className="custom-control-label" htmlFor="customCheck1">
              Check this custom checkbox
            </label>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeCheckboxes}
        </SyntaxHighlighter>
        <h3 id="radios">Radios</h3>
        <div className="ct-example bg-white">
          <div className="custom-control custom-radio mb-3">
            <input
              className="custom-control-input"
              id="customRadio1"
              name="customRadio"
              type="radio"
            />
            <label className="custom-control-label" htmlFor="customRadio1">
              Toggle this custom radio
            </label>
          </div>
          <div className="custom-control custom-radio">
            <input
              className="custom-control-input"
              id="customRadio2"
              name="customRadio"
              type="radio"
            />
            <label className="custom-control-label" htmlFor="customRadio2">
              Or toggle this other custom radio
            </label>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeRadios}
        </SyntaxHighlighter>
        <h3 id="inline">Inline</h3>
        <div className="ct-example bg-white">
          <div className="custom-control custom-radio custom-control-inline">
            <input
              className="custom-control-input"
              id="customRadioInline1"
              name="customRadioInline1"
              type="radio"
            />
            <label
              className="custom-control-label"
              htmlFor="customRadioInline1"
            >
              Toggle this custom radio
            </label>
          </div>
          <div className="custom-control custom-radio custom-control-inline">
            <input
              className="custom-control-input"
              id="customRadioInline2"
              name="customRadioInline1"
              type="radio"
            />
            <label
              className="custom-control-label"
              htmlFor="customRadioInline2"
            >
              Or toggle this other custom radio
            </label>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeInline}
        </SyntaxHighlighter>
        <h3 id="disabled">Disabled</h3>
        <div className="ct-example bg-white">
          <div className="custom-control custom-checkbox">
            <input
              className="custom-control-input"
              disabled
              id="customCheckDisabled"
              type="checkbox"
            />
            <label
              className="custom-control-label"
              htmlFor="customCheckDisabled"
            >
              Check this custom checkbox
            </label>
          </div>
          <div className="custom-control custom-radio">
            <input
              className="custom-control-input"
              disabled
              id="customRadioDisabled"
              name="radioDisabled"
              type="radio"
            />
            <label
              className="custom-control-label"
              htmlFor="customRadioDisabled"
            >
              Toggle this custom radio
            </label>
          </div>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeDisabled}
        </SyntaxHighlighter>
        <h3 id="toggles">Toggles</h3>
        <div className="ct-example bg-white">
          <label className="custom-toggle">
            <input defaultChecked type="checkbox" />
            <span className="custom-toggle-slider rounded-circle" />
          </label>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeToggles}
        </SyntaxHighlighter>
        <h3 id="labeled-toggles">Labeled toggles</h3>
        <div className="ct-example bg-white">
          <label className="custom-toggle">
            <input defaultChecked type="checkbox" />
            <span
              className="custom-toggle-slider rounded-circle"
              data-label-off="No"
              data-label-on="Yes"
            />
          </label>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeLabeledToggles}
        </SyntaxHighlighter>
        <h3 id="sliders">Sliders</h3>
        <div className="ct-example bg-white">
          <Form>
            <div className="input-slider-container">
              <div className="input-slider" ref="slider1" />
              <Row className="mt-3">
                <Col xs="6">
                  <span className="range-slider-value">
                    {this.state.slider1Value}
                  </span>
                </Col>
              </Row>
            </div>
            <div className="mt-5">
              <div ref="slider2" />
              <Row>
                <Col xs="6">
                  <span className="range-slider-value value-low">
                    {this.state.slider2Values[0]}
                  </span>
                </Col>
                <Col className="text-right" xs="6">
                  <span className="range-slider-value value-high">
                    {this.state.slider2Values[1]}
                  </span>
                </Col>
              </Row>
            </div>
          </Form>
        </div>
        <SyntaxHighlighter language="jsx" style={prism}>
          {codeSliders}
        </SyntaxHighlighter>
        <h3 id="examples">Props</h3>
        <p>
          Please refer to{" "}
          <a
            href="https://reactstrap.github.io/components/form/?ref=creativetim"
            target="_blank"
          >
            reactstrap form documentation
          </a>{" "}
          and{" "}
          <a
            href="https://refreshless.com/nouislider/?ref=creativetim"
            target="_blank"
          >
            nouislider documentation
          </a>
          .
        </p>
      </>
    );
  }
}

export default Forms;
