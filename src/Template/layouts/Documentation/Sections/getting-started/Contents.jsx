/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";
// reactstrap components
import { Card, Row, Col } from "reactstrap";

const fileStructure = `Argon Dashboard PRO React
.
├── Documentation
│   └── documentation.html
├── CHANGELOG.md
├── ISSUE_TEMPLATE.md
├── README.md
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
└── src
    ├── index.js
    ├── routes.js
    ├── ../../../../assets
    │   ├── css
    │   │   ├── argon-dashboard-pro-react.css
    │   │   ├── argon-dashboard-pro-react.css.map
    │   │   └── argon-dashboard-pro-react.min.css
    │   ├── fonts
    │   │   └── nucleo
    │   ├── img
    │   │   ├── brand
    │   │   ├── icons
    │   │   │   ├── cards
    │   │   │   ├── common
    │   │   │   └── flags
    │   │   └── theme
    │   ├── scss
    │   │   ├── argon-dashboard-pro-react.scss
    │   │   ├── bootstrap
    │   │   ├── core
    │   │   ├── custom
    │   │   └── react
    │   └── vendor
    │       ├── @fortawesome
    │       │   └── fontawesome-free
    │       ├── animate.css
    │       ├── fullcalendar
    │       │   └── dist
    │       ├── nucleo
    │       ├── quill
    │       │   └── dist
    │       ├── select2
    │       │   └── dist
    │       └── sweetalert2
    │           └── dist
    ├── variables
    │   ├── charts.jsx
    │   └── general.jsx
    ├── layouts
    │   ├── Admin.jsx
    │   └── Auth.jsx
    ├── components
    │   ├── Footers
    │   │   ├── AdminFooter.jsx
    │   │   └── AuthFooter.jsx
    │   ├── Headers
    │   │   ├── AlternativeHeader.jsx
    │   │   ├── AuthHeader.jsx
    │   │   ├── CardsHeader.jsx
    │   │   ├── IndexHeader.jsx
    │   │   ├── ProfileHeader.jsx
    │   │   └── SimpleHeader.jsx
    │   ├── Navbars
    │   │   ├── AdminNavbar.jsx
    │   │   ├── AuthNavbar.jsx
    │   │   └── IndexNavbar.jsx
    │   └── Sidebar
    │       └── Sidebar.jsx
    └── views
        ├── Index.jsx
        └── pages
            ├── Calendar.jsx
            ├── Charts.jsx
            ├── Widgets.jsx
            ├── components
            │   ├── Buttons.jsx
            │   ├── Cards.jsx
            │   ├── Grid.jsx
            │   ├── Icons.jsx
            │   ├── Notifications.jsx
            │   └── Typography.jsx
            ├── dashboards
            │   ├── Alternative.jsx
            │   └── Dashboard.jsx
            ├── examples
            │   ├── Lock.jsx
            │   ├── Login.jsx
            │   ├── Pricing.jsx
            │   ├── Profile.jsx
            │   ├── Register.jsx
            │   └── Timeline.jsx
            ├── forms
            │   ├── Components.jsx
            │   ├── Elements.jsx
            │   └── Validation.jsx
            ├── maps
            │   ├── Google.jsx
            │   └── Vector.jsx
            └── tables
                ├── ReactBSTables.jsx
                ├── Sortable.jsx
                └── Tables.jsx`;

class Contents extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Contents
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Discover what's included in Argon Dashboard PRO React
        </p>
        <hr />
        <h2 id="argon-structure">Argon Dashboard PRO React structure</h2>
        <p>
          Once downloaded, unzip the compressed folder and you’ll see something
          like this:
        </p>
        <SyntaxHighlighter language="html" style={prism}>
          {fileStructure}
        </SyntaxHighlighter>
        <h2 id="bootstrap-components">Bootstrap / Reactstrap components</h2>
        <p>
          Here is the list of Bootstrap 4 / Reactstrap components that were
          restyled in Argon Dashboard PRO React:
        </p>
        <Row className="row-grid mt-5">
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Alerts</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Badge</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Buttons</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Carousel</h6>
              </div>
            </Card>
          </Col>
        </Row>
        <Row className="row-grid">
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Dropdowns</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Forms</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Modal</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Navs</h6>
              </div>
            </Card>
          </Col>
        </Row>
        <Row className="row-grid">
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Navbar</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Pagination</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Popover &amp; Tooltip</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Progress</h6>
              </div>
            </Card>
          </Col>
        </Row>
        <h2 id="argon-components">Argon Dashboard components</h2>
        <p>
          Besides giving the existing Bootstrap elements a new look, we added
          new ones, so that the interface and consistent and homogenous. Going
          through them, we added:
        </p>
        <Row className="row-grid mt-5">
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Datepicker</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Sliders</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Checkboxes</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Radio buttons</h6>
              </div>
            </Card>
          </Col>
        </Row>
        <Row className="row-grid">
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Toggle buttons</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Font Awesome</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Nucleo icons</h6>
              </div>
            </Card>
          </Col>
          <Col md="3">
            <Card className="shadow-sm">
              <div className="p-4 text-center">
                <h6 className="mb-0">Modals</h6>
              </div>
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}

export default Contents;
