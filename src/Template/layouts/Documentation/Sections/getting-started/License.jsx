/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";

class License extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            License
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          Learn more about the licenses Creative Tim offers and purchase the one
          that covers the needs of your project.
        </p>
        <hr />
        <p>
          Currently, on{" "}
          <a
            href="https://www.creative-tim.com?ref=adpr-docs-license"
            target="_blank"
          >
            Creative Tim
          </a>{" "}
          you can get the products with two types of licenses: Personal or
          Developer. If you are making a paid purchase, be sure to go through
          the table with the rights and the guidelines, so you can know what is
          the best fit for you. View the rights table and the description for
          each license on our by clicking the button below.
        </p>
        <p>
          <a
            href="https://www.creative-tim.com/license?ref=adpr-docs-license"
            target="_blank"
          >
            See licenses
          </a>
        </p>
        <p>
          Creating your web design from scratch with dedicated designers can be
          very expensive. Using our solutions you don’t have to worry about
          design. Save time and money by focusing on the business model. Are you
          ready to create something amazin?
        </p>
        <p>
          <a
            href="https://www.creative-tim.com/product/argon-dashboard-pro-react?ref=adpr-docs-license"
            target="_blank"
          >
            Puchase now
          </a>
        </p>
      </>
    );
  }
}

export default License;
