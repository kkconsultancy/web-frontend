/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";

const adminProps = `AdminNavbar.defaultProps = {
  toggleSidenav: () => {},
  sidenavOpen: false,
  theme: "dark"
};
AdminNavbar.propTypes = {
  // this is a function that toggles between opened and closed
  // left menu (Sidebar component) on mobile devices
  toggleSidenav: PropTypes.func,
  // this is a prop which tells this component if the
  // left menu (Sidebar component) is opened on mobile devices
  sidenavOpen: PropTypes.bool,
  // this prop will change the colors of the navbar
  theme: PropTypes.oneOf(["dark", "light"])
};`;

class Navbar extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Demo navbars
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          In our template product, we have three demo navbars that can be found
          inside{" "}
          <code className="highlighter-rouge">
            src/components/Navbars/AdminNavbar.jsx
          </code>
          ,{" "}
          <code className="highlighter-rouge">
            src/components/Navbars/AuthNavbar.jsx
          </code>{" "}
          and{" "}
          <code className="highlighter-rouge">
            src/components/Navbars/IndexNavbar.jsx
          </code>
          .
        </p>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Navbars/AdminNavbar.jsx
          </code>
        </h2>
        <p>
          It is used to create the navbar / header that appears on the{" "}
          <code className="highlighter-rouge">src/layouts/Admin.jsx</code>.
        </p>
        <p>
          It is diveded in two parts, the left one, that is used for the search
          component, and the right part with some links and a user.
        </p>
        <p>It has the following props:</p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {adminProps}
        </SyntaxHighlighter>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Navbars/AuthNavbar.jsx
          </code>
        </h2>
        <p>
          It is used to create the navbar / header that appears on the{" "}
          <code className="highlighter-rouge">src/layouts/Auth.jsx</code>.
        </p>
        <p>
          It is diveded in two parts, the left one, that is used for the product
          logo and links inside the app, and the right part with some links to
          navigate to some social networks.
        </p>
        <p>Everything in this component is static.</p>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Navbars/IndexNavbar.jsx
          </code>
        </h2>
        <p>
          It is used to create the navbar / header that appears on the{" "}
          <code className="highlighter-rouge">src/views/Index.jsx</code>.
        </p>
        <p>
          It is diveded in two parts, the left one, that is used for the product
          logo and links inside the app, and the right part with some links to
          navigate to some social networks.
        </p>
      </>
    );
  }
}

export default Navbar;
