/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { prism } from "react-syntax-highlighter/dist/cjs/styles/prism";

const authProps = `AuthHeader.propTypes = {
  title: PropTypes.string,
  lead: PropTypes.string
};`;

const cardsProps = `CardsHeader.propTypes = {
  name: PropTypes.string,
  parentName: PropTypes.string
};`;

const simpleProps = `SimpleHeader.propTypes = {
  name: PropTypes.string,
  parentName: PropTypes.string
};`;

class PageHeader extends React.Component {
  render() {
    return (
      <>
        <div className="ct-page-title">
          <h1 className="ct-title" id="content">
            Demo Headers
          </h1>
          <div className="avatar-group mt-3" />
        </div>
        <p className="ct-lead">
          In our template product, we have the following demo headers inside{" "}
          <code className="highlighter-rouge">
            src/components/Headers/AlternativeHeader.jsx
          </code>
          ,{" "}
          <code className="highlighter-rouge">
            src/components/Headers/AuthHeader.jsx
          </code>
          ,{" "}
          <code className="highlighter-rouge">
            src/components/Headers/CardsHeader.jsx
          </code>
          ,{" "}
          <code className="highlighter-rouge">
            src/components/Headers/IndexHeader.jsx
          </code>
          ,{" "}
          <code className="highlighter-rouge">
            src/components/Headers/ProfileHeader.jsx
          </code>{" "}
          and{" "}
          <code className="highlighter-rouge">
            src/components/Headers/SimpleHeader.jsx
          </code>
          .
        </p>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Headers/AlternativeHeader.jsx
          </code>
        </h2>
        <p>This is used inside:</p>
        <ul>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/dashboards/Alternative.jsx
            </code>
          </li>
        </ul>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Headers/AuthHeader.jsx
          </code>
        </h2>
        <p>This is used inside:</p>
        <ul>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/examples/Lock.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/examples/Login.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/examples/Pricing.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/examples/Register.jsx
            </code>
          </li>
        </ul>
        <p>And has the following props:</p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {authProps}
        </SyntaxHighlighter>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Headers/CardsHeader.jsx
          </code>
        </h2>
        <p>This is used inside:</p>
        <ul>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/Charts.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/components/Cards.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/dashboards/Dashboard.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/Widgets.jsx
            </code>
          </li>
        </ul>
        <p>And has the following props:</p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {cardsProps}
        </SyntaxHighlighter>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Headers/IndexHeader.jsx
          </code>
        </h2>
        <p>This is used inside:</p>
        <ul>
          <li>
            <code className="highlighter-rouge">src/views/Index.jsx</code>
          </li>
        </ul>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Headers/ProfileHeader.jsx
          </code>
        </h2>
        <p>This is used inside:</p>
        <ul>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/examples/Profile.jsx
            </code>
          </li>
        </ul>
        <h2 id="content">
          <code className="highlighter-rouge">
            src/components/Headers/SimpleHeader.jsx
          </code>
        </h2>
        <p>This is used inside:</p>
        <ul>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/components/Buttons.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/components/Grid.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/components/Icons.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/components/Notifications.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/components/Typography.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/examples/Timeline.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/forms/Components.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/forms/Elements.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/components/Validation.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/maps/Google.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/maps/Vector.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/tables/ReactBSTables.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/tables/Sortable.jsx
            </code>
          </li>
          <li>
            <code className="highlighter-rouge">
              src/views/pages/tables/Tables.jsx
            </code>
          </li>
        </ul>
        <p>And has the following props:</p>
        <SyntaxHighlighter language="jsx" style={prism}>
          {simpleProps}
        </SyntaxHighlighter>
      </>
    );
  }
}

export default PageHeader;
