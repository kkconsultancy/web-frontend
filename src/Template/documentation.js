/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// Getting started
import Overview from "./layouts/Documentation/Sections/getting-started/Overview.jsx";
import QuickStart from "./layouts/Documentation/Sections/getting-started/QuickStart.jsx";
import License from "./layouts/Documentation/Sections/getting-started/License.jsx";
import Contents from "./layouts/Documentation/Sections/getting-started/Contents.jsx";
import BuildTools from "./layouts/Documentation/Sections/getting-started/BuildTools.jsx";
import Variables from "./layouts/Documentation/Sections/getting-started/Variables.jsx";
import RoutingSystem from "./layouts/Documentation/Sections/getting-started/RoutingSystem.jsx";
// Foundation
import Colors from "./layouts/Documentation/Sections/foundation/Colors.jsx";
import Grid from "./layouts/Documentation/Sections/foundation/Grid.jsx";
import Typography from "./layouts/Documentation/Sections/foundation/Typography.jsx";
import Icons from "./layouts/Documentation/Sections/foundation/Icons.jsx";
// Core components
import Footer from "./layouts/Documentation/Sections/core-components/Footer.jsx";
import Navbars from "./layouts/Documentation/Sections/core-components/Navbars.jsx";
import Headers from "./layouts/Documentation/Sections/core-components/Headers.jsx";
import Sidebar from "./layouts/Documentation/Sections/core-components/Sidebar.jsx";
// Reasctrap components
import Alerts from "./layouts/Documentation/Sections/restyled-components/Alerts.jsx";
import Avatar from "./layouts/Documentation/Sections/restyled-components/Avatar.jsx";
import Badge from "./layouts/Documentation/Sections/restyled-components/Badge.jsx";
import Breadcrumb from "./layouts/Documentation/Sections/restyled-components/Breadcrumb.jsx";
import Buttons from "./layouts/Documentation/Sections/restyled-components/Buttons.jsx";
import Cards from "./layouts/Documentation/Sections/restyled-components/Cards.jsx";
import Carousel from "./layouts/Documentation/Sections/restyled-components/Carousel.jsx";
import Collapse from "./layouts/Documentation/Sections/restyled-components/Collapse.jsx";
import Dropdowns from "./layouts/Documentation/Sections/restyled-components/Dropdowns.jsx";
import Forms from "./layouts/Documentation/Sections/restyled-components/Forms.jsx";
import InputGroup from "./layouts/Documentation/Sections/restyled-components/InputGroup.jsx";
import ListGroup from "./layouts/Documentation/Sections/restyled-components/ListGroup.jsx";
import Modals from "./layouts/Documentation/Sections/restyled-components/Modals.jsx";
import Navs from "./layouts/Documentation/Sections/restyled-components/Navs.jsx";
import Navbar from "./layouts/Documentation/Sections/restyled-components/Navbar.jsx";
import Pagination from "./layouts/Documentation/Sections/restyled-components/Pagination.jsx";
import Popovers from "./layouts/Documentation/Sections/restyled-components/Popovers.jsx";
import Progress from "./layouts/Documentation/Sections/restyled-components/Progress.jsx";
import SocialButtons from "./layouts/Documentation/Sections/restyled-components/SocialButtons.jsx";
import Tables from "./layouts/Documentation/Sections/restyled-components/Tables.jsx";
import Tooltips from "./layouts/Documentation/Sections/restyled-components/Tooltips.jsx";
// Plugins
import Charts from "./layouts/Documentation/Sections/plugins/Charts.jsx";
import CopyToClipboard from "./layouts/Documentation/Sections/plugins/CopyToClipboard.jsx";
import Dropzone from "./layouts/Documentation/Sections/plugins/Dropzone.jsx";
import Fullcalendar from "./layouts/Documentation/Sections/plugins/Fullcalendar.jsx";
import ListJs from "./layouts/Documentation/Sections/plugins/ListJs.jsx";
import Maps from "./layouts/Documentation/Sections/plugins/Maps.jsx";
import ReactBSTable from "./layouts/Documentation/Sections/plugins/ReactBSTable.jsx";
import ReactDatetime from "./layouts/Documentation/Sections/plugins/ReactDatetime.jsx";
import ReactJvectormap from "./layouts/Documentation/Sections/plugins/ReactJvectormap.jsx";
import ReactNotify from "./layouts/Documentation/Sections/plugins/ReactNotify.jsx";
import ReactPerfectScrollbar from "./layouts/Documentation/Sections/plugins/ReactPerfectScrollbar.jsx";
import ReactQuill from "./layouts/Documentation/Sections/plugins/ReactQuill.jsx";
import ReactSelect from "./layouts/Documentation/Sections/plugins/ReactSelect.jsx";
import ReactSweetAlert from "./layouts/Documentation/Sections/plugins/ReactSweetAlerts.jsx";
import ReactTags from "./layouts/Documentation/Sections/plugins/ReactTags.jsx";
import ReactToPrint from "./layouts/Documentation/Sections/plugins/ReactToPrint.jsx";
import Sliders from "./layouts/Documentation/Sections/plugins/Sliders.jsx";

var docsRoutes = [
  {
    name: "Getting started",
    path: "/documentation/overview",
    routes: [
      {
        path: "/documentation/overview",
        component: Overview,
        name: "Overview"
      },
      {
        path: "/documentation/quick-start",
        component: QuickStart,
        name: "Quick Start"
      },
      {
        path: "/documentation/license",
        component: License,
        name: "License"
      },
      {
        path: "/documentation/contents",
        component: Contents,
        name: "Contents"
      },
      {
        path: "/documentation/build-tools",
        component: BuildTools,
        name: "Build Tools"
      },
      {
        path: "/documentation/variables",
        component: Variables,
        name: "Variables"
      },
      {
        path: "/documentation/routing-system",
        component: RoutingSystem,
        name: "Routing System"
      }
    ]
  },
  {
    name: "Foundation",
    path: "/documentation/colors",
    routes: [
      {
        path: "/documentation/colors",
        component: Colors,
        name: "Colors"
      },
      {
        path: "/documentation/grid",
        component: Grid,
        name: "Grid"
      },
      {
        path: "/documentation/typography",
        component: Typography,
        name: "Typography"
      },
      {
        path: "/documentation/icons",
        component: Icons,
        name: "Icons"
      }
    ]
  },
  {
    name: "Core Components",
    path: "/documentation/footer",
    routes: [
      {
        path: "/documentation/footer",
        component: Footer,
        name: "Footer"
      },
      {
        path: "/documentation/app-navigation",
        component: Navbars,
        name: "App Navbars"
      },
      {
        path: "/documentation/page-header",
        component: Headers,
        name: "Headers"
      },
      {
        path: "/documentation/sidebar",
        component: Sidebar,
        name: "Sidebar"
      }
    ]
  },
  {
    name: "Restyled Components",
    path: "/documentation/alert",
    routes: [
      {
        path: "/documentation/alert",
        component: Alerts,
        name: "Alerts"
      },
      {
        path: "/documentation/avatar",
        component: Avatar,
        name: "Avatar"
      },
      {
        path: "/documentation/badge",
        component: Badge,
        name: "Badge"
      },
      {
        path: "/documentation/breadcrumb",
        component: Breadcrumb,
        name: "Breadcrumb"
      },
      {
        path: "/documentation/buttons",
        component: Buttons,
        name: "Buttons"
      },
      {
        path: "/documentation/cards",
        component: Cards,
        name: "Cards"
      },
      {
        path: "/documentation/carousel",
        component: Carousel,
        name: "Carousel"
      },
      {
        path: "/documentation/collapse",
        component: Collapse,
        name: "Collapse"
      },
      {
        path: "/documentation/dropdowns",
        component: Dropdowns,
        name: "Dropdowns"
      },
      {
        path: "/documentation/forms",
        component: Forms,
        name: "Forms"
      },
      {
        path: "/documentation/input-group",
        component: InputGroup,
        name: "InputGroup"
      },
      {
        path: "/documentation/list-group",
        component: ListGroup,
        name: "ListGroup"
      },
      {
        path: "/documentation/modal",
        component: Modals,
        name: "Modals"
      },
      {
        path: "/documentation/navbar",
        component: Navbar,
        name: "Navbars"
      },
      {
        path: "/documentation/navs",
        component: Navs,
        name: "Navs"
      },
      {
        path: "/documentation/pagination",
        component: Pagination,
        name: "Pagination"
      },
      {
        path: "/documentation/popovers",
        component: Popovers,
        name: "Popovers"
      },
      {
        path: "/documentation/progress",
        component: Progress,
        name: "Progress"
      },
      {
        path: "/documentation/social-buttons",
        component: SocialButtons,
        name: "Social Buttons"
      },
      {
        path: "/documentation/tables",
        component: Tables,
        name: "Tables"
      },
      {
        path: "/documentation/tooltips",
        component: Tooltips,
        name: "Tooltips"
      }
    ]
  },
  {
    name: "Plugins",
    path: "/documentation/charts",
    routes: [
      { path: "/documentation/charts", component: Charts, name: "Charts" },
      {
        path: "/documentation/copy-to-clipboard",
        component: CopyToClipboard,
        name: "CopyToClipboard"
      },
      {
        path: "/documentation/dropzone",
        component: Dropzone,
        name: "Dropzone"
      },
      {
        path: "/documentation/full-calendar",
        component: Fullcalendar,
        name: "Fullcalendar"
      },
      { path: "/documentation/list-js", component: ListJs, name: "List.Js" },
      { path: "/documentation/maps", component: Maps, name: "Maps" },
      {
        path: "/documentation/react-bs-table",
        component: ReactBSTable,
        name: "React BS Table"
      },
      {
        path: "/documentation/react-datetime",
        component: ReactDatetime,
        name: "React Datetime"
      },
      {
        path: "/documentation/react-jvectormap",
        component: ReactJvectormap,
        name: "React Jvectormap"
      },
      {
        path: "/documentation/react-notification-alert",
        component: ReactNotify,
        name: "React Notification"
      },
      {
        path: "/documentation/react-perfect-scrollbar",
        component: ReactPerfectScrollbar,
        name: "React Perfect Scrollbar"
      },
      {
        path: "/documentation/react-quill",
        component: ReactQuill,
        name: "React Quill"
      },
      {
        path: "/documentation/react-select",
        component: ReactSelect,
        name: "React Select"
      },
      {
        path: "/documentation/react-sweet-alert",
        component: ReactSweetAlert,
        name: "React Sweet Alert"
      },
      {
        path: "/documentation/react-tags",
        component: ReactTags,
        name: "React Tags"
      },
      {
        path: "/documentation/react-to-print",
        component: ReactToPrint,
        name: "React To Print"
      },
      { path: "/documentation/sliders", component: Sliders, name: "Sliders" }
    ]
  },
  { redirect: true, path: "/documentation", pathTo: "/documentation/overview" }
];

export default docsRoutes;
