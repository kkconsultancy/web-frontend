@echo off

if [%1]==[] GOTO Continue
if [%2]==[] GOTO Continue
if [%3]==[] GOTO Continue

git pull
git add .
git commit -m %1
git push origin master
cd functions
git pull
git add .
git commit -m %2
git push origin master
cd ..
cd src/Middleware
git pull
git add .
git commit -m %3
git push origin master
cd ../..

:Continue